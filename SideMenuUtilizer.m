//
//  SideMenuUtilizer.m
//  RadyUs
//
//  Created by Frane Poljak on 19/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "SideMenuUtilizer.h"
#import "ExploreMap.h"
#import "OneOnOneChat.h"
#import "CloudView.h"
#import "Profile.h"

@interface SideMenuUtilizer ()

@property (nonatomic) BOOL firstAppearance;

@end

@implementation SideMenuUtilizer
@synthesize sideMenu;
@synthesize originY;

CGPoint panStartLocation;
BOOL wasHidingBackButton;
UIBarButtonItem *leftBarButtonItem;

const char notificationKey;


- (void)viewDidLoad {
	
    [super viewDidLoad];

    _firstAppearance = YES;
	
	if (self.navigationController.viewControllers.count > 1) {
		self.navigationController.navigationBar.topItem.title = @" ";
	}
	
	_previousTitle = self.title ?: @" ";
	
	if ([_previousTitle isEqualToString:@""]) {
		_previousTitle = @" ";
	}
	
	self.title = _previousTitle;
	
    _slideMenuCreated = NO;
    _slideMenuShown = NO;
    _slideMenuSliding = NO;
	
	if (self.isBeingPresented) {
		return;
	}
	
    _containerView = [[UIView alloc] initWithFrame:self.view.bounds];
    [_containerView setAutoresizesSubviews:self.view.autoresizesSubviews];
    [_containerView setAutoresizingMask:self.view.autoresizingMask];
    [_containerView setBackgroundColor:self.view.backgroundColor];
	[self.view setBackgroundColor:[UIColor darkTextColor]];
    
    for (UIView *subview in self.view.subviews) {
        [subview removeFromSuperview];
        [_containerView addSubview:subview];
    }
    
    [self.view addSubview:_containerView];

	//// extend edges under top bars mora bit checked, inace ne nece valjat frame origin y kad je otvoren sidemenu!
	originY = 64;//[NSStringFromClass(self.class) isEqualToString:@"Profile"] ? 0 : 64;//self.tabBarController != nil ? 64 : 0; //zato ovo ne valja
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }

    _menuSlideGesture = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(slideMenuPan:)];
    [_menuSlideGesture setEdges:UIRectEdgeLeft];

	if ([self conformsToProtocol:@protocol(UIGestureRecognizerDelegate)]) {
		_menuSlideGesture.delegate = (id<UIGestureRecognizerDelegate>)self;
	}

	_menuSlideGesture.cancelsTouchesInView = YES;
	
    [self.view addGestureRecognizer:_menuSlideGesture];
    
    _viewForHidingMenu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, _containerView.frame.size.height)];
    UIPanGestureRecognizer *panBack = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(slideMenuPan:)];
    [_viewForHidingMenu addGestureRecognizer:panBack];
}

-(void)receivedNotification:(NSDictionary *)aNotification {

    NSDictionary *userInfo = [aNotification valueForKey:@"userInfo"];

    int type = (int)[([userInfo valueForKey:@"type"] ?: @0) integerValue];

    switch (type) {

        case 1: {

            if ([self isKindOfClass:[OneOnOneChat class]]) {

                OneOnOneChat *vc = (OneOnOneChat *)self;
                NSString *friendId = vc.friend ? [vc.friend valueForKey:@"id"] : vc.friendId;

                if (![friendId isEqualToString:[userInfo valueForKey:@"user_id"]]) {

                    [self displayNotification:userInfo];
                }

            } else {

                [self displayNotification:userInfo];
            }

            break;
        }

        case 2: {

            if ([self isKindOfClass:[CloudView class]]) {

                CloudView *vc = (CloudView *)self;
                NSString *cloudId = vc.cloud ? [((NSDictionary *)vc.cloud) valueForKey:@"id"] : vc.cloudId;

                if (![cloudId isEqualToString:[userInfo valueForKey:@"cloud_id"]]) {

                    [self displayNotification:userInfo];
                }

            } else {
                
                [self displayNotification:userInfo];
            }

            break;
        }

        case 3: {

            if ([self isKindOfClass:[Profile class]]) {

                Profile *vc = (Profile *)self;
                NSString *userID = vc.userInfo ? [((NSDictionary *)vc.userInfo) valueForKey:@"id"] : vc.userID;

                if (![userID isEqualToString:[userInfo valueForKey:@"user_id"]]) {

                    [self displayNotification:userInfo];
                }

            } else {
                
                [self displayNotification:userInfo];
            }

            break;
        }

        case 4: {

            break;
        }

        default: { // unknown

            break;
        }

    }
}

-(void)displayNotification:(NSDictionary *)userInfo {

    if (_notificationTimer != nil) {
        if (_notificationTimer.valid) {
            [_notificationTimer invalidate];
        }
        _notificationTimer = nil;
    }
    if (_notificationView != nil) {
        [_notificationView removeFromSuperview];
        _notificationView = nil;
    }

    _notificationView = [[UIView alloc] initWithFrame:CGRectMake(0, self.isBeingPresented ? 20 : 64, self.view.frame.size.width, 0)];

    objc_setAssociatedObject(_notificationView, &notificationKey, userInfo, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

    [_notificationView.layer setZPosition:10001];
    _notificationView.alpha = 0.88;
    _notificationView.backgroundColor = [UIColor darkTextColor];
    [_notificationView setClipsToBounds:YES];
    _notificationView.autoresizesSubviews = YES;

    UIButton *closeButton = [[UIButton alloc] initWithFrame: CGRectMake(_notificationView.frame.size.width - 30, -30, 10, 20)];
    //[closeButton setTitle:@"X" forState:UIControlStateNormal]; // test, TODO: background image instead
    [closeButton setBackgroundImage:[UIImage imageNamed:@"dialog_cancel"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissNotificationView) forControlEvents:UIControlEventTouchUpInside];

    [_notificationView addSubview:closeButton];

    UILabel *alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, -40, _notificationView.frame.size.width - 50, 30)];
    [alertLabel setTextColor:[UIColor whiteColor]];
    [alertLabel setText:[[userInfo  valueForKey:@"aps"] valueForKey:@"alert"]];

    [_notificationView addSubview:alertLabel];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(notificationTapped:)];
    [_notificationView addGestureRecognizer:tap];

    [self.view addSubview:_notificationView];

    for (UIView *subview in _notificationView.subviews) {
        [subview setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    }

    [UIView animateWithDuration:0.25 animations:^{
        _notificationView.frame = CGRectMake(0, self.isBeingPresented ? 0 : 64, self.view.frame.size.width, 50);
    }];

    _notificationTimer = [NSTimer timerWithTimeInterval:5.0 target:self selector:@selector(dismissNotificationView) userInfo:nil repeats:NO];

}

-(void)dismissNotificationView {

    if (_notificationTimer != nil) {
        if (_notificationTimer.valid) {
            [_notificationTimer invalidate];
        }
        _notificationTimer = nil;
    }

    if (_notificationView != nil) {
        [UIView animateWithDuration:0.25 animations:^{
            _notificationView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [_notificationView removeFromSuperview];
            _notificationView = nil;
        }];
    }
}

-(void)notificationTapped:(id)sender {

    NSDictionary *userInfo = objc_getAssociatedObject([(UIGestureRecognizer *)sender view], &notificationKey);

    int type = (int)[([userInfo valueForKey:@"type"] == nil ? @0 : [userInfo valueForKey:@"type"]) integerValue];

    UINavigationController *navCtrl = (UINavigationController *)self.navigationController;
    UIViewController *vc;

    switch (type) {

        case 1: {

            NSString *userId = [userInfo valueForKey:@"user_id"];

            if (userId != nil) {
                vc = (OneOnOneChat *)[self.storyboard instantiateViewControllerWithIdentifier:@"OneOnOneChat"];
                [(OneOnOneChat *)vc setFriendId:userId];
            }

            break;
        }

        case 2: {

            NSString *cloudId = [userInfo valueForKey:@"cloud_id"];

            if (cloudId != nil) {
                vc = (CloudView *)[self.storyboard instantiateViewControllerWithIdentifier:@"CloudView"];
                [(CloudView *)vc setCloudId:cloudId];
            }

            break;
        }

        case 3: {


            break;
        }

        case 4: {

            break;
        }
            
        default: { // unknown

            //vc = (Profile *)[self.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
            break;
        }
    }

    if (vc != nil) {
        if (!self.isBeingPresented && navCtrl != nil) { // check if we can dismiss?
            [navCtrl pushViewController:vc animated:YES];
        }
    }

    [self dismissNotificationView];
}

-(void)viewWillAppear:(BOOL)animated {

    if (!self.isBeingPresented) {
        _menuSlideGestureForNavigationBar = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(slideMenuPan:)];
        [_menuSlideGestureForNavigationBar setEdges:UIRectEdgeLeft];
        [self.navigationController.navigationBar addGestureRecognizer:_menuSlideGestureForNavigationBar];
    }
}

-(void)setBadgeNumber:(NSInteger)badgeNumber forRow:(NSInteger)row {
    if (self.isBeingPresented) {
        return;
    }
	[(SideMenu *)sideMenu setBadgeNumber:badgeNumber forRow:row];
}

-(void)incrementBadgeNumberForRow:(NSInteger)row {
    [(SideMenu *)sideMenu incrementBadgeNumberForRow:row];
}

-(void)decrementBadgeNumberForRow:(NSInteger)row {
    [(SideMenu *)sideMenu decrementBadgeNumberForRow:row];
}

-(void)viewDidAppear:(BOOL)animated {

	NSLog(@"viewDidAppear: %@", NSStringFromClass(self.class));

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareForBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnFromBackground) name:UIApplicationWillEnterForegroundNotification object:nil];
	
	self.title = (_previousTitle ?: self.title) ?: @" ";
	
	if ([self.navigationController viewControllers].count > 1) {
		self.navigationController.navigationBar.topItem.title = self.title;
	}
	
	if (![self.navigationItem hidesBackButton]) {
		[self.navigationItem.backBarButtonItem setTintColor:[UIColor whiteColor]];
	}

    if (!animated && !_firstAppearance) {
        return;
    }

    if (_firstAppearance) {
        _firstAppearance = NO;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_whisper" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_message" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_friend_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_mention" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_unknown" object:nil];
}

-(void)prepareForBackground {

	[self hideSideMenu:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_whisper" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_message" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_friend_request" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_mention" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_unknown" object:nil];

    if (_notificationTimer != nil) {
        if (_notificationTimer.valid) {
            [_notificationTimer invalidate];
        }
        _notificationTimer = nil;
    }
    if (_notificationView != nil) {
        [_notificationView removeFromSuperview];
        _notificationView = nil;
    }
}

-(void)returnFromBackground {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_whisper" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_message" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_friend_request" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_mention" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"RadyusReceivedRemoteNotification_unknown" object:nil];
}

-(void)slideMenuPan:(UIScreenEdgePanGestureRecognizer *)sender {
	
    CGPoint stopLocation = [sender locationInView:self.view.window];
    CGPoint velocity = [sender velocityInView:sender.view];
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        if (!_slideMenuCreated) {
            [self createSideMenu];
        } else {
            [sideMenu.view setAlpha:1.0f];
            [_containerView.layer setCornerRadius:4.0f];
        }
        panStartLocation = [sender locationInView:self.view.window];
        _slideMenuSliding = YES;
        
    }
    else if (sender.state == UIGestureRecognizerStateEnded) {
        
        CGFloat dx = stopLocation.x - panStartLocation.x;
        
        //_slideMenuSliding = NO;
		
        if (dx > self.view.window.frame.size.width / 2 - 60 || velocity.x > 1200 || (panStartLocation.x > 100 && [sender velocityInView:sideMenu.view].x > 0)) { //manji velocity?
            [self showSideMenu:sender];
        } else {
            [self hideSideMenu:sender];
        }
        
    } else if (sender.state == UIGestureRecognizerStateChanged) { //swipe in progress
        
        if ([sender locationInView:self.view.window].x > self.view.frame.size.width - 60 && velocity.x > 0) {
            return;
        }
        //[sender translationInView:self.view.window];
        
        CGFloat resizeFactor = [sender locationInView:self.view.window].x / (self.view.frame.size.width - 60);

        CGFloat scale = 1 - (80 / self.view.window.bounds.size.height * resizeFactor);
        
        CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, scale);
        _containerView.transform = transform;
		self.navigationController.navigationBar.transform = transform;
        
        _containerView.frame = CGRectMake([sender locationInView:self.view.window].x,
                                     40 * resizeFactor * scale,//self.navigationController.navigationBar.frame.size.height + 20 + 40 * resizeFactor, //samo 40 *...
                                     self.view.window.frame.size.width * scale,
                                     self.view.window.frame.size.height - 20 - self.navigationController.navigationBar.frame.size.height + originY - 80 * resizeFactor);
        
        self.navigationController.navigationBar.frame = CGRectMake([sender locationInView:self.view.window].x,
                                                                   (20 + 40 * resizeFactor) * scale,
                                                                   self.navigationController.navigationBar.frame.size.width,
                                                                   self.navigationController.navigationBar.frame.size.height);
        
        sideMenu.view.frame = CGRectMake( - self.view.frame.size.width + 60 + [sender locationInView:self.view.window].x, originY - self.navigationController.navigationBar.frame.size.height,
                                         self.view.frame.size.width - 60, sideMenu.view.frame.size.height);
    }
}

-(void)createSideMenu {
	
	if (self.isBeingPresented) {
		return;
	}

    if (_slideMenuCreated) {
        return;
    }

    NSLog(@"%@ creating side menu...", NSStringFromClass(self.class));

    sideMenu = (SideMenu *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];

    _slideMenuCreated = YES;

    [self addChildViewController:sideMenu];
    sideMenu.view.frame = CGRectMake(- self.view.frame.size.width + 60, originY - self.navigationController.navigationBar.frame.size.height,
                                     self.view.frame.size.width - 60, self.view.window.frame.size.height - 20);
    
    [self.view addSubview:sideMenu.view];
    [sideMenu didMoveToParentViewController:self];
    [sideMenu.view setAutoresizingMask:UIViewAutoresizingNone];
    [sideMenu.view setUserInteractionEnabled:YES];
    [self.view bringSubviewToFront:sideMenu.view];
}

-(void)showSideMenu:(id)sender {
    
    if (_slideMenuShown && !_slideMenuSliding) {
        return;
    }
		
	[self sidemenuWillShow];
    
    [self.view endEditing:YES];

    [sideMenu.view setAlpha:1.0f];
	
	if (!_slideMenuShown && [[self.navigationController viewControllers] count] > 1) {
		
		UINavigationItem *navItem = (self.tabBarController == nil) ? self.navigationItem : self.tabBarController.navigationItem;
		
		wasHidingBackButton = navItem.hidesBackButton;
		leftBarButtonItem = navItem.leftBarButtonItem;
	
		[navItem setHidesBackButton:YES animated:YES];
		navItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bars.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleSlideMenu:)];//hideSideMenu?
		[navItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
	
	}
	
    CGFloat duration = 0.6f;
    
    if ([sender isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
		//NSLog(@"Location: %f", [sender locationInView:self.view.window].x);
        CGFloat loc = [(UIScreenEdgePanGestureRecognizer *)sender locationInView:self.view].x;
        if (loc <= 0) {
            loc = [(UIScreenEdgePanGestureRecognizer *)sender locationInView:sideMenu.view].x;
        }
        duration *= (self.view.frame.size.width - 60 - loc) / (self.view.frame.size.width - 60);
    }
    
    if (!_slideMenuCreated) {
        [self createSideMenu];
    }
    
    _slideMenuSliding = YES;
    
    [_containerView.layer setCornerRadius:4.0f];
    
    [UIView animateWithDuration:duration animations:^{
        //sideMenu.view.frame = CGRectMake(0, -64, self.view.frame.size.width - 60, self.view.frame.size.height + 64);
        
        CGFloat scale = 1 - (80 / self.view.window.bounds.size.height);
        
        CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, scale);
        _containerView.transform = transform;
		self.navigationController.navigationBar.transform = transform;
		
        _containerView.frame = CGRectMake(self.view.window.frame.size.width - 60,
                                     40 * scale, //self.navigationController.navigationBar.frame.size.height + 60, //40
                                     self.view.window.frame.size.width * scale,
                                     self.view.window.frame.size.height - self.navigationController.navigationBar.frame.size.height + originY - 100);
        
        self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width - 60,
                                                                   60 * scale,
                                                                   self.navigationController.navigationBar.frame.size.width,
                                                                   self.navigationController.navigationBar.frame.size.height);
        
        sideMenu.view.frame = CGRectMake(0, originY - self.navigationController.navigationBar.frame.size.height, sideMenu.view.frame.size.width, sideMenu.view.frame.size.height);
    } completion:^(BOOL finished){
        
        _slideMenuSliding = NO;
        _slideMenuShown = YES;
        //[_containerView setUserInteractionEnabled:NO];
        [_containerView addSubview:_viewForHidingMenu];
        [sideMenu.view setUserInteractionEnabled:YES];//
        //[sideMenu.view becomeFirstResponder];
		
		[self sidemenuDidShow];
		
    }];
}

-(void)hideSideMenu:(id)sender {

    if (!_slideMenuShown && !_slideMenuSliding) {
        return;
    }
	
	[self sidemenuWillHide];
	
    CGFloat duration = 0.6f;
    
    if (sender == nil) {
        duration = 0.3f;
		self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, 20, /////????
                                                                   self.navigationController.navigationBar.frame.size.width,
                                                                   self.navigationController.navigationBar.frame.size.height);
    } else if ([sender isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        //CGFloat loc = MAX([(UIScreenEdgePanGestureRecognizer *)sender locationInView:sideMenu.view].x, [(UIScreenEdgePanGestureRecognizer *)sender locationInView:self.view].x);
        CGFloat loc = [(UIScreenEdgePanGestureRecognizer *)sender locationInView:sideMenu.view].x;
        if (loc <= 0) {
            loc = [(UIScreenEdgePanGestureRecognizer *)sender locationInView:self.view].x;
        }
        duration *= loc / (self.view.frame.size.width - 60);
    }

    _slideMenuSliding = YES;
    
    [UIView animateWithDuration:duration animations:^{
        
        CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        _containerView.transform = transform;
		self.navigationController.navigationBar.transform = transform;
		
        _containerView.frame = CGRectMake(0, 0,//self.navigationController.navigationBar.frame.size.height + 20,
                                     self.view.window.frame.size.width,
                                          self.view.frame.size.height);//self.view.window.bounds.size.height - self.navigationController.navigationBar.frame.size.height - 20);
        
        self.navigationController.navigationBar.frame = CGRectMake(0, 20,
                                                                   self.navigationController.navigationBar.frame.size.width,
                                                                   self.navigationController.navigationBar.frame.size.height);
        
        sideMenu.view.frame = CGRectMake(- self.view.frame.size.width + 60, originY - self.navigationController.navigationBar.frame.size.height,
                                         self.view.frame.size.width - 60, sideMenu.view.frame.size.height); //self.view.window.frame.size.height - 20);
        
    } completion:^(BOOL finished){
        _slideMenuSliding = NO;
        _slideMenuShown = NO;
        [sideMenu.view setAlpha:0.0f];
        [self.view becomeFirstResponder];
        [_containerView.layer setCornerRadius:0.0f];
        //[_containerView setUserInteractionEnabled:YES];
        [_viewForHidingMenu removeFromSuperview];
		
		if ([[self.navigationController viewControllers] count] > 1) {
			
			UINavigationItem *navItem = (self.tabBarController == nil) ? self.navigationItem : self.tabBarController.navigationItem;
			
			navItem.leftBarButtonItem = leftBarButtonItem;
			[navItem setHidesBackButton:wasHidingBackButton animated:YES];
		}

		[self sidemenuDidHide];
		
    }];
    
}

-(void)toggleSlideMenu:(id)sender {

    if (_slideMenuSliding) {
        return;
    }
    
    if (_slideMenuShown) {
        [self hideSideMenu:sender];
    } else {
        [self showSideMenu:sender];
    }
	
}

-(void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {

    if (self.isBeingPresented && [viewControllerToPresent isKindOfClass:[SideMenuUtilizer class]]) {
        [self viewWillDisappear:flag];
    }

    [super presentViewController:viewControllerToPresent animated:flag completion:completion];
}

-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {

    NSLog(@"dismissViewControllerAnimated, presentingViewController: %@", NSStringFromClass(self.presentingViewController.class));

    if ([self.presentingViewController isKindOfClass:[SideMenuUtilizer class]]) {
        [self.presentingViewController viewWillAppear:flag];
    }

    __strong UIViewController *presentingViewController = self.presentingViewController;

    [super dismissViewControllerAnimated:flag completion: ^(void){
        if ([presentingViewController isKindOfClass:[SideMenuUtilizer class]]) {
            [presentingViewController viewDidAppear:flag];
        }
        completion();
    }];
}

-(void)viewWillDisappear:(BOOL)animated {

    NSLog(@"viewWillDisappear: %@", NSStringFromClass(self.class));

    if (!self.isBeingPresented) {

        [self.navigationController.navigationBar removeGestureRecognizer:_menuSlideGestureForNavigationBar];
        [self hideSideMenu:nil];
    }
	
	_previousTitle = self.title;
	
	self.title = @" ";
	//self.navigationController.navigationBar.topItem.title = self.title;

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_whisper" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_message" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_friend_request" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_mention" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_unknown" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	//NSLog(@"SideMenuUtilizer prepareforsegue");
	//[self hideSideMenu:sender];
}

-(void)handleStatusBarFrameChange:(NSNotification *)aNotification {
	
	CGFloat scale = 1 - (80 / self.view.window.bounds.size.height);
	
	[self.navigationController.navigationBar.layer removeAllAnimations];
	self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width - 60,
															   60 * scale,
															   self.navigationController.navigationBar.frame.size.width,
															   self.navigationController.navigationBar.frame.size.height);
	[self.navigationController.navigationBar.layer removeAllAnimations];
	
}

-(void)sidemenuWillShow {
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleStatusBarFrameChange:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleStatusBarFrameChange:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
}

-(void)sidemenuDidShow {
	//NSLog(@"sidemenuDidShow, y: %f", _containerView.frame.origin.y);
}

-(void)sidemenuWillHide {}

-(void)sidemenuDidHide {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)dealloc {

    if (_notificationTimer != nil) {
        if (_notificationTimer.valid) {
            [_notificationTimer invalidate];
        }
        _notificationTimer = nil;
    }
    if (_notificationView != nil) {
        [_notificationView.layer removeAllAnimations];
        [_notificationView removeFromSuperview];
        _notificationView = nil;
    }

    [self.view.layer removeAllAnimations];

	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
}

@end
