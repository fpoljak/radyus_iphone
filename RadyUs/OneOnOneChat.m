//
//  OneOnOneChat.m
//  Radyus
//
//  Created by Locastic MacbookPro on 24/09/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "OneOnOneChat.h"
#import "UIImage+OrientationUpImage.h"
#import "UIImage+Trim.h"

@interface OneOnOneChat ()

@end

@implementation OneOnOneChat

@synthesize checkedForNewMessages;

BOOL hasOlderMessages;

- (void)viewDidLoad {
	
	[super viewDidLoad];

	_dbManager = [[DBManager alloc] initWithDatabaseFilename:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"] stringByAppendingString:@".sqlt"]];
	
	_cloudViewState = CSChatInFullScreen;
	
	hasOlderMessages = YES;
	checkedForNewMessages = false;

	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;

	if (_friend != nil) {
		
		[self displayTitleView];
		[self loadMessages];
		
	} else {
		
		if (_friendId == nil) {
			//pop
		} else {
			
			_friend = [_dbManager loadUserWithId:_friendId];
			
			if (_friend == nil) {
				[_apiService getUsersByIdList:[NSArray arrayWithObject:_friendId]];
			} else {
			
				[self displayTitleView];
				[self loadMessages];
				
			}
			
		}
		
	}
	
	[_messageInputView setBackgroundColor:[UIColor clearColor]];
	[_messageInputPlaceholderView setFrame:_messageInputView.frame];
	[_messageInputPlaceholderView setAutoresizingMask:_messageInputView.autoresizingMask];
	[_messageInputPlaceholderView setHidden:NO];
	[_messageInputPlaceholderView setAlpha:1.0f];
	
	[_messageInputView.layer setCornerRadius:5.0f];
	[_messageInputPlaceholderView.layer setCornerRadius:5.0f];
	
	UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chatViewDoubleTapped:)];
	[doubleTap setNumberOfTapsRequired:2];
	[_chatView addGestureRecognizer:doubleTap];
	
	[_chatView.panGestureRecognizer requireGestureRecognizerToFail:self.menuSlideGesture];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMessage:) name:@"RadyusReceivedRemoteNotification_whisper" object:nil];
	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMessage:) name:@"RadyusReceivedRemoteNotification" object:nil]; //test
	
}

-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {
	
	_friend = [(NSArray *)usersList firstObject];
	
	[self displayTitleView];
	
	[self loadMessages];
	
}

-(void)receivedMessage:(NSDictionary *)aNotification {

    NSDictionary *userInfo = [aNotification valueForKey:@"userInfo"];

	if ([userInfo valueForKey:@"user_id"] == nil || [[_friend valueForKey:@"id"] isEqualToString:[userInfo valueForKey:@"user_id"]]) {
	
		[self checkForNewMessages];
	}
}

-(void)prepareForBackground {

    [super prepareForBackground];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_whisper" object:nil];
}

-(void)returnFromBackground {

    NSLog(@"OneOnOneChat returnFromBackground");
    [super returnFromBackground];
    [self checkForNewMessages];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMessage:) name:@"RadyusReceivedRemoteNotification_whisper" object:nil];
}


-(void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
	
	///// UIKeyboardWillHide, dismiss interactively?
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[_messageInputView becomeFirstResponder];///

    if (checkedForNewMessages) {
        [self checkForNewMessages];
    }
	
}

-(void)viewWillDisappear:(BOOL)animated {
	
	[super viewWillDisappear:animated];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldDisableUserInteraction {
	return NO;
}

-(void)displayTitleView {
	
	NSString *title = [_friend valueForKey:@"nameFirst"];
	
	if (![[_friend valueForKey:@"namelast"] isEqualToString:@""]) {
		title = [NSString stringWithFormat:@"%@ %@", title, [_friend valueForKey:@"nameLast"]];
	}
	
	self.title = title;
	
	UIButton *titleButton = [[UIButton alloc] init];
	
	[titleButton setTitle:[self.title stringByAppendingString:@" >"] forState:UIControlStateNormal]; ////ili view profile pa desno?
	
	[titleButton addTarget:self action:@selector(goToFriendProfile) forControlEvents:UIControlEventTouchUpInside];
	
	[self.navigationItem setTitleView:titleButton];

}

-(void)goToFriendProfile {
	
	Profile *profileView = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
	[profileView setUserID:[_friend valueForKey:@"id"]];
	[self.navigationController pushViewController:profileView animated:YES];
	
}

-(void)loadMessages {

    if (_friend == nil) {
        return;
    }

	NSArray *messages = [_dbManager loadMessages:[_friend valueForKey:@"id"]];
	
	if (messages.count == 0) {
		[self loadMore];
		return;
	}
	
	[self loadMessagesSuccess:(NSJSONSerialization *)messages];
	
	if (messages.count > 30) {
		[_dbManager clearOldMessagesForCloud:[_friend valueForKey:@"id"]];
	}
	
	//if less look for older?
	
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
	
	if ([scrollView isEqual:_chatView]) {
		
		if (targetContentOffset->y > scrollView.contentSize.height - scrollView.frame.size.height - 20 && _chatView.messageButton != nil) {
			[_chatView.messageButton removeFromSuperview];
			_chatView.messageButton = nil;
        } else if (targetContentOffset->y <= 0) {
			if (![_chatView isInLoadPreviousMode] && hasOlderMessages) {
				[self loadMore];
			}
		}
		
	}
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	return YES;
}

-(void)resizeInputView:(UITextView *)textView {
	
	CGFloat maxHeight = _chatView.frame.size.height / 2;
	CGRect currentFrame = textView.frame;
	CGSize newSize = CGSizeMake(currentFrame.size.width, MAX(_chatView.imageToDisplay == nil ? 38 : 52, MIN([textView.text isEqualToString:@""] ? 0 : textView.contentSize.height, maxHeight)));
	CGFloat heightDiff = newSize.height - currentFrame.size.height;
	
	[_chatControlsContainer setFrame:CGRectMake(_chatControlsContainer.frame.origin.x, _chatControlsContainer.frame.origin.y - heightDiff, _chatControlsContainer.frame.size.width,
												_chatControlsContainer.frame.size.height + heightDiff)];
	[_chatView setFrame:CGRectMake(_chatView.frame.origin.x, _chatView.frame.origin.y, _chatView.frame.size.width, _chatView.frame.size.height - heightDiff)];
	[textView setFrame:CGRectOffset(CGRectInset(currentFrame, 0, - heightDiff / 2), 0, heightDiff / 2)];
	[_messageInputPlaceholderView setFrame:textView.frame];
	[_messageInputPlaceholderView setTextContainerInset:textView.textContainerInset];
	[_addPhotoControlsContainer setFrame:_chatControlsContainer.bounds];
	
}

-(void)chatViewToFullScreen {
	
	[self.view endEditing:YES];
	
	[UIView animateWithDuration:0.4f animations:^{
		[_chatView setFrame:CGRectMake(0, _chatView.frame.origin.y, _chatView.frame.size.width, self.view.frame.size.height - _chatView.frame.origin.y - _chatControlsContainer.frame.size.height)];
		[_chatControlsContainer setFrame:CGRectMake(0, self.view.frame.size.height - _chatControlsContainer.frame.size.height, _chatControlsContainer.frame.size.width, _chatControlsContainer.frame.size.height)];
	} completion:^(BOOL finished) {
		_cloudViewState = CSChatInFullScreen;
		if (_chatView.messageButton != nil) {
			[_chatView.messageButton removeFromSuperview];
			_chatView.messageButton = nil;
			[_chatView showMessageButton];
		}
	}];
	
}

-(void)expandChatView:(id)sender {
	
	CGFloat duration = _kbAnimationDuration > 0 ? _kbAnimationDuration : 0.3;
	
	BOOL messageButttonShown = NO;
	
	if (_chatView.messageButton != nil) {
		messageButttonShown = YES;
		[_chatView.messageButton removeFromSuperview];
		_chatView.messageButton = nil;
	}

	CGRect chatControlsFrame = CGRectMake(0, self.view.frame.size.height - _kbSize.height - _chatControlsContainer.frame.size.height, _chatView.frame.size.width,
										  _chatControlsContainer.frame.size.height);
	
	BOOL shouldAdjustContentOffset = _chatView.contentOffset.y > _chatView.contentSize.height - _chatView.frame.size.height - 80; // or something
	
	[UIView animateWithDuration:duration delay:0.0 options:_kbAnimationEffect animations:^{
		
		[_chatView.counterLabel setFrame:CGRectMake(0, 64, self.view.frame.size.width, _chatView.counterLabel.frame.size.height)];
		
		[_chatView setFrame:CGRectMake(0, 64 + _chatView.counterLabel.frame.size.height, _chatView.frame.size.width,
									   self.view.frame.size.height - chatControlsFrame.size.height - _chatView.counterLabel.frame.size.height - 64 - _kbSize.height)];
		[_chatControlsContainer setFrame:chatControlsFrame];
		
		if (shouldAdjustContentOffset) {
			[_chatView setContentOffset:CGPointMake(0, MIN(MAX(0, _chatView.contentOffset.y + _kbSize.height), _chatView.contentSize.height - _chatView.frame.size.height))];
		}
		
	} completion:^(BOOL finished) {
		_cloudViewState = CSChatExpanded;
		if (messageButttonShown) {
			[_chatView showMessageButton];
		}
	}];
	
}

-(void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {

	[super presentViewController:viewControllerToPresent animated:flag completion:completion];
	
	if ([_messageInputView isFirstResponder]) {
		[self chatViewToFullScreen];
	}
	
}

-(void)chatViewDoubleTapped:(UITapGestureRecognizer *)sender {
	
	if (_cloudViewState == CSChatExpanded) {
		[self chatViewToFullScreen];
	} else if (_cloudViewState == CSChatInFullScreen) {
		if (![_messageInputView isFirstResponder]) {
			[_messageInputView becomeFirstResponder];
		} else {
			[self expandChatView:sender];
		}
	}
	
}


-(void)displayAppMessage:(NSString *)message {
	
	NSDictionary *msg = [[NSDictionary alloc] initWithObjects: [NSArray arrayWithObject:message] forKeys: [NSArray arrayWithObject:@"text"]];
	
	if (_chatView.isInLoadPreviousMode) {
		[_chatView.messageBuffer addObject:msg];
	} else {
		[_chatView displayAppMessage:msg];
	}
	
}

-(void)loadMore {
	_loadingMessageBuffer = [[NSMutableArray alloc] init]; ////?????
	NSLog(@"loading older messages, first message time: %@", [_chatView.firstMessage.nextMessage.message valueForKey:@"time"]);
	[_apiService load1on1MessagesWithFriend:[_friend valueForKey:@"id"] since:nil until:[_chatView.firstMessage.nextMessage.message valueForKey:@"time"] limit:20];
}

-(void)sortMessages:(NSMutableArray *)messages {

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
	
	[messages sortUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
		
		NSDate *d1 = [dateFormatter dateFromString:obj1[@"time"]];
		NSDate *d2 = [dateFormatter dateFromString:obj2[@"time"]];
		
		return [d1 compare:d2];
		
	}];

}

-(void)loadMessagesSuccess:(NSJSONSerialization *)response {
	
	//Operation queue!
	
	NSMutableArray *messages = [NSMutableArray arrayWithArray:(NSArray *)response];
	
	[self sortMessages:messages];

	BOOL previousMode =
	(([_chatView.lastMessageDate isEqualToString:@""] && [_chatView.tempLastMessageDate isEqualToString:@""]) ||
		(messages.count > 0 && [([_chatView.lastMessageDate isEqualToString:@""] ? _chatView.tempLastMessageDate : _chatView.lastMessageDate) compare:
								[[messages firstObject] valueForKey:@"time"] options:NSNumericSearch] == NSOrderedDescending));
	
	NSString *userId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"];
	
	if (previousMode) {
		hasOlderMessages = messages.count > 1;
	}
	
	if (previousMode && hasOlderMessages && !_chatView.isInLoadPreviousMode) {
		[_chatView enterLoadPreviousMode];
	}
	
	if (hasOlderMessages || !previousMode) {
		
		NSString *firstMesssageTime = [_chatView.firstMessage.nextMessage.message valueForKey:@"time"];
		
		for (NSJSONSerialization *message in messages) {
			
			if (previousMode && [message isEqual:[messages lastObject]] && ([[message valueForKey:@"id"] isEqualToString:[_chatView.lastMessage.message valueForKey:@"id"]]
				|| [[message valueForKey:@"time"] isEqualToString:firstMesssageTime])) { //temp, should be id, id must always be present!
				continue;
			} else if (!previousMode && [message isEqual:[messages firstObject]] &&
					   ([[message valueForKey:@"id"] isEqualToString:[_chatView.lastMessage.message valueForKey:@"id"]]
						|| [[message valueForKey:@"time"] isEqualToString:[_chatView.lastMessage.message valueForKey:@"time"]])) { //temp
				continue;
			}
			
			[_dbManager saveMessage:(NSJSONSerialization *)message];
			
			if (_messageLoading) { //maknit!!!
				
				if (![_loadingMessageBuffer containsObject:message]) {
					[_loadingMessageBuffer addObject:message];
				}
				
			} else {
				
				[_loadingMessageBuffer removeObject:message]; //maknit!!!
				
				NSDictionary *fullMessage = [self createMessage:message fromUser:_friend];
				
				if ([[message valueForKey:@"user"] isEqualToString:userId]) {
					[_chatView displayUserMessage:fullMessage];
				} else {
					[_chatView displayMessage:fullMessage];
				}
				
				[_dbManager saveMessage:(NSJSONSerialization *)fullMessage];
				
			}
			
		}

	}
	
	if (previousMode) {
		[_chatView leaveLoadPreviousMode];
	} else {

        BOOL shouldScrollToBottom = (_chatView.contentSize.height - _chatView.contentOffset.y < _chatView.frame.size.height * 1.6);
        // Maknit?
        if (shouldScrollToBottom) {
            [_chatView setContentOffset:CGPointMake(0, MAX(0, _chatView.contentSize.height - _chatView.frame.size.height)) animated:YES];
        }
	}

    if (!checkedForNewMessages) {

        checkedForNewMessages = true;
        [self checkForNewMessages];

    } else {
        if ([(NSArray*)response count] > 1) {
            // TODO: nove poruke! (zasad nece bit nikad izvrseno, kad budu socketi odlučit je li potrebno (ovisno o tome hoće li u inbox.m slušati, vjerojatno bolje ne)
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RadyusNew1on1Message" object:self/*nil?*/ userInfo:(NSDictionary *)[messages lastObject]];
        }
    }
	
	// TODO: napravit da radi isto za cloud i 1on1, prebacit hendlanje u chatView, ili cak novu klasu napravit, nekakav ChatViewHelper
	
}

-(void)checkForNewMessages {

    if (_friend == nil) {
        return;
    }

    NSLog(@"checkForNewMessages");
	
	if (_chatView.lastMessageDate == nil || [_chatView.lastMessageDate isEqualToString:@""]) {
		return;
	}
	
	[_apiService load1on1MessagesWithFriend:[_friend valueForKey:@"id"] since:_chatView.lastMessageDate until:nil limit:0];
	
}

-(NSDictionary *)createMessage:(NSJSONSerialization *)message fromUser:(NSDictionary *)user {
	
	NSArray *_keys = @[@"user",
					   @"nameFirst",
					   @"nameLast",
					   @"userImage",
					   @"time",
					   @"text"];
	
	NSMutableArray *keys = [NSMutableArray arrayWithArray:_keys];
	
	NSArray *objects = [NSArray arrayWithObjects: [message valueForKey:@"user"],
						[user valueForKey:@"nameFirst"] ?: @"",
						[user valueForKey:@"nameLast"] ?: @"",
						[[user valueForKey:@"pictures"] firstObject] ?: @"",
						[message valueForKey:@"time"],
						[message valueForKey:@"text"],
						[message valueForKey:@"picture"],
						nil];
	
	if ([message valueForKey:@"picture"] != nil) {
		[keys addObject:@"image_id"];
	}
	
	return [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
	
}


-(void)postMessageSuccess:(NSJSONSerialization *)response {
	
	NSLog(@"successfully posted message: %@", (NSDictionary *)response);
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"RadyusNew1on1Message" object:self/*nil?*/ userInfo:(NSDictionary *)response];
	
	[_chatView postMessage:_messageInputView withTime:[response valueForKey:@"time"]];
	[self resizeInputView:_messageInputView];
	[_messageInputView setBackgroundColor:[UIColor clearColor]];
	[_dbManager saveMessage:response];
	[_sendButton setUserInteractionEnabled:YES];
	// TODO: ne disejblat send botun, nego prikazat poruku i ako fejla resend botun!
	
}

-(void)postMessageFailed {
	
	[_sendButton setUserInteractionEnabled:YES];
	
	NSString *message = @"Message not sent! Check your connection and try again.";
	
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
	
	[alert addAction:close];
	
	[self presentViewController:alert animated:YES completion:nil];
	
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	UIImage *_selectedImage = [info valueForKey:UIImagePickerControllerEditedImage] ?: [info valueForKey:UIImagePickerControllerOriginalImage];
	
	UIImage *selectedImage = [[[_selectedImage orientationUpImage] imageByTrimmingTransparentPixels] imageByTrimmingBlackPixels];
	
	_chatView.imageToDisplay = selectedImage;
	[picker dismissViewControllerAnimated:YES completion:nil];
	[self cancelAddPhotoClicked:nil];
	[_chatView addThumbnail:_messageInputView];
	[self resizeInputView:_messageInputView];

}

-(IBAction)addPhotoClicked:(id)sender {
	
	[self chatViewToFullScreen];
	[_addPhotoControlsContainer setHidden:NO];
	[_chatControlsContainer bringSubviewToFront:_addPhotoControlsContainer];
	
}

-(IBAction)cancelAddPhotoClicked:(id)sender {
	
	[_addPhotoControlsContainer setHidden:YES];
	
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {

	[_sendButton setUserInteractionEnabled:YES];
	
}

-(IBAction)postMessage:(id)sender {
	
	if (_messageInputView.text.length == 0 && _chatView.imageToDisplay == nil) {
		return;
	}
	
	[_sendButton setUserInteractionEnabled:NO];
	
	if (_chatView.imageToDisplay != nil) {
		// TODO: new instance of service for upload images?
		_apiService.trackProgress = YES;
		[_apiService postNewImage:_chatView.imageToDisplay];
		return;
	}
	
	[_apiService post1on1Message:_messageInputView.text toFriend:[_friend valueForKey:@"id"] withImage:nil];
	
}

-(void)apiService:(ApiService *)service uploadProgress:(CGFloat)progress {
	[_chatView updateUploadProgress:progress];
}

// TODO: new instance of apiService for receiving new messages (temp, until sockets)
-(void)newPictureSuccess:(NSString *)pictureId {
	_apiService.trackProgress = NO;
	[_apiService post1on1Message:_messageInputView.text toFriend:[_friend valueForKey:@"id"] withImage:pictureId];
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
	return YES;
}

-(void)textViewDidChange:(UITextView *)textView {
	
	if (textView.text.length == 0) {
		[_messageInputView setBackgroundColor:[UIColor clearColor]];
	} else {
		[_messageInputView setBackgroundColor:[UIColor whiteColor]];
	}
	
	[self resizeInputView:textView];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
	
	if (action == @selector(paste:)) {
		return [UIPasteboard generalPasteboard].string != nil || [UIPasteboard generalPasteboard].image != nil;
	}
	
	return [super canPerformAction:action withSender:sender];
	
}

-(void)paste:(id)sender {
	[self overridePaste:sender];
}

-(void)overridePaste:(id)sender {
	
	//NSLog(@"OneOnOneChat paste detected, sender: %@", sender);
	
	if ([_messageInputView isFirstResponder]) {
		
		UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
		
		/*
		for (NSString *pasteboardType in pasteboard.pasteboardTypes) {
			NSLog(@"pasteboardType: %@", pasteboardType);
		} //debug
		*/
		
		if (pasteboard.image != nil) {
			
			[_chatView removeThumbnail:_messageInputView];
			_chatView.imageToDisplay = pasteboard.image;
			[_chatView addThumbnail:_messageInputView];
			[self resizeInputView:_messageInputView];

		}
		
		if (pasteboard.string != nil) {
			
			NSRange range = _messageInputView.selectedRange;
			UITextPosition *beginning = _messageInputView.beginningOfDocument;
			UITextPosition *start = [_messageInputView positionFromPosition:beginning offset:range.location];
			UITextPosition *end = [_messageInputView positionFromPosition:start offset:range.length];
			UITextRange *textRange = [_messageInputView textRangeFromPosition:start toPosition:end];
			[_messageInputView replaceRange:textRange withText:pasteboard.string];
			
		}
		
		//other types?
		
	}
	
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
	
	NSDictionary* info = [aNotification userInfo];
	_kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	_kbAnimationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	_kbAnimationEffect = curve << 16;
	NSLog(@"Keyboard will show, size: %@", NSStringFromCGSize(_kbSize));
	[self expandChatView:_chatView];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
	
}

-(void)keyboardWillChangeFrame:(NSNotification *)aNotification {
	
	NSDictionary* info = [aNotification userInfo];
	_kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	_kbAnimationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	//curve??
	NSLog(@"Keyboard will change frame, size: %@", NSStringFromCGSize(_kbSize));
	[self expandChatView:nil];
	
}

-(void)sidemenuWillShow {
	[super sidemenuWillShow];
	[self chatViewToFullScreen];
}

-(void)sidemenuDidShow {
	[super sidemenuDidShow];
	[self chatViewToFullScreen];
}

-(void)sidemenuDidHide {
    [super sidemenuDidHide]; //
	[self chatViewToFullScreen];
}

-(void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
