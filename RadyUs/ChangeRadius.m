//
//  ChangeRadius.m
//  RadyUs
//
//  Created by Frane Poljak on 18/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ChangeRadius.h"

@interface ChangeRadius ()

@end

@implementation ChangeRadius

BOOL firstRun = YES;

- (void)viewDidLoad {
    [super viewDidLoad];
	
    firstRun = YES;
    // Do any additional setup after loading the view.
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
	
    [super viewDidLayoutSubviews];
    [self sliderValueChanged:_radiusSlider];
	
	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];
	
	if (isFeet) {
		[_minDistanceLabel setText:@"66 ft"];
		[_medDistanceLabel setText:@"2460 ft"];
		[_maxDistanceLabel setText:@"4921 ft"];
	}
	
    firstRun = NO;
}

-(IBAction)sliderValueChanged:(UISlider *)sender {
	
	AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];
	
	CGFloat newRadius = roundf(sender.value / 10) * 10;
    if (newRadius == parent.radius && !firstRun) {
        return;
    }
	
	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];

	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		
		[parent setRadius:newRadius];
		[parent.mapView removeOverlay:parent.radiusCircle];
		parent.radiusCircle = [MKCircle circleWithCenterCoordinate:parent.cloudLocation radius:parent.radius];
		[parent.mapView addOverlay:parent.radiusCircle];
		[parent setMapBoundsToRadiusWithCenterIn:parent.cloudLocation];
		[parent.radiusButton setTitle:[NSString stringWithFormat:@" %.0f %@", parent.radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateNormal];
		[parent.radiusButton setTitle:[NSString stringWithFormat:@" %.0f %@", parent.radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateHighlighted];
		[parent.radiusButton setTitle:[NSString stringWithFormat:@" %.0f %@", parent.radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateSelected];
		//[parent.mapView setRegion:parent.mapView.region animated:TRUE];
		[parent validateForm];
		
	}];
	
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
