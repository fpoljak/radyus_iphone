//
//  AsyncImageLoadManager.h
//  Radyus
//
//  Created by Frane Poljak on 05/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DBManager.h"

@protocol AsyncImageLoadDelegate <NSObject>

@optional

-(void) loadSuccess:(UIImageView *)imageView;
-(void) errorLoadingImage:(UIImageView *)imageView;

@end


@interface AsyncImageLoadManager : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, assign) id<AsyncImageLoadDelegate> delegate;

@property (nonatomic) UIBackgroundTaskIdentifier downloadTaskID;

@property (nonatomic, weak) UIImageView *imageView;
@property (nonatomic) BOOL useActivityIndicator;
@property (nonatomic, retain) UIActivityIndicatorView *actInd;
@property (nonatomic, retain) NSString *imageId;
@property (nonatomic, retain) NSString *imageUrl;
@property (nonatomic, retain) NSMutableData *imageData;
@property (nonatomic, retain) NSURLConnection *imageConnection;

@property (nonatomic, retain) DBManager *dbManager;

@property (nonatomic) NSInteger delegateStrongReferenceCounter;

-(instancetype)initWithImageId:(NSString *)imageId imageView:(UIImageView *)imageView useActivityIndicator:(BOOL)useActivityIndicator;
-(instancetype)initWithImageUrl:(NSString *)url imageView:(UIImageView *)imageView useActivityIndicator:(BOOL)useActivityIndicator;
-(void)loadImage;

@end
