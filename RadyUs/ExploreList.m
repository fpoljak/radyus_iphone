//
//  ExploreList.m
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ExploreList.h"
#import "UIImageView+NoAvatar.h"
#import "CloudView.h"
#import "ViewFriends.h"

@interface ExploreList ()

@end

@implementation ExploreList
@synthesize radius;
@synthesize access;

static char cloudUserKey;
static char cloudUserListKey;
static char cloudKey;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	NSLog(@"a viewDidLoad");
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
	_indexOfCloudBeingLoaded = 0;
	_arrayOfCloudUsersArrays = [[NSMutableArray alloc] init];
	
	[_tableView setDelaysContentTouches:YES];
	
    radius = [[NSUserDefaults standardUserDefaults] objectForKey:@"RadiusForReadingClouds"] == nil ? 1000.0f : [[NSUserDefaults standardUserDefaults] floatForKey:@"RadiusForReadingClouds"];
    access = [[NSUserDefaults standardUserDefaults] objectForKey:@"AccesForReadingClouds"] == nil ? @"All" : [[NSUserDefaults standardUserDefaults] stringForKey:@"AccesForReadingClouds"];
    
	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];

	[_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateNormal];
	[_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateHighlighted];
	
	[_optionsButton addTarget:self action:@selector(highlightButton:) forControlEvents:UIControlEventTouchDown];
	[_optionsButton addTarget:self action:@selector(unhighlightButton:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
	
	UIImageView *ddarr = [[UIImageView alloc] initWithFrame:CGRectMake(_optionsButton.frame.size.width - 40, _optionsButton.frame.size.height / 2 - 10, 24, 21)];
	[ddarr setContentMode:UIViewContentModeCenter];
	[ddarr setImage:[UIImage imageNamed:@"dropdownarrow.png"]];
	[_optionsButton addSubview:ddarr];
	
	if (_cloudList != nil) {
		[self testCustomTable];
	}

}

-(void) highlightButton:(UIButton *)sender {
	[sender setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
}

-(void) unhighlightButton:(UIButton *)sender {
		[sender setBackgroundColor:UIColorFromRGB(0xE7E7E7)];
}

-(IBAction)tabChanged:(UIButton *)sender {
    [self.tabBarController setSelectedIndex: 0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 0;//[[_cloudList valueForKey:@"data"] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"ProfileCloudsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
	
	//[cell setNeedsUpdateConstraints];
	//[cell updateConstraintsIfNeeded];
	
    //NSLog(@"Cell width: %f", cell.contentView.frame.size.width);
	
	NSJSONSerialization *cloud = [_cloudList objectAtIndex:indexPath.row];
	
	UILabel *descriptionLabel = (UILabel *)[cell viewWithTag:4];
	//[descriptionLabel setPreferredMaxLayoutWidth:cell.frame.size.width - 24];
	[descriptionLabel setText:[cloud valueForKey:@"name"]];
	
	
	UIButton *viewAllUsers = (UIButton *)[cell viewWithTag:20];
	
	if (indexPath.row >= _indexOfCloudBeingLoaded || _arrayOfCloudUsersArrays.count == 0) {
		[viewAllUsers setHidden:YES];
	} else {
		//display users
		//[self displayCloudUsers:[_arrayOfCloudUsersArrays objectAtIndex:indexPath.row] inCell:cell indexPath:indexPath];
		
		NSLog(@"Displaying cloud users, indexPath.row = %ld...", (unsigned long)indexPath.row);
		
		NSArray *cloudUsersList = [_arrayOfCloudUsersArrays objectAtIndex:indexPath.row];
		
		for (int i = 0; i < 4 && i < cloudUsersList.count; i++) {
			
			NSJSONSerialization *user = [cloudUsersList objectAtIndex:i];
			
			UIImageView *avatar = (UIImageView *)[cell viewWithTag:i + 11];
			objc_setAssociatedObject(avatar, &cloudUserKey, user, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
			
			// asyncimageloader... avatar
			[avatar setImage:[UIImage imageNamed:@"avatar.png"]];//
			
			UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewCloudUserProfile:)];
			
			[avatar setUserInteractionEnabled:YES];
			[avatar addGestureRecognizer:avatarTap];
			
		}
		
		if (cloudUsersList.count > 4) {
			UIButton *more = (UIButton *)[cell viewWithTag:20];
			[more setHidden:NO];
			[more.titleLabel setText:[NSString stringWithFormat:@"+%ld", (long)cloudUsersList.count - 4]];
			//[more addTarget:self action:@selector(viewCloudUsers:) forControlEvents:UIControlEventTouchUpInside];
		}
		
		[cell setNeedsDisplay];
		
	}
	
	
	//[_apiService getUsersByIdList:(NSArray*)[cloud valueForKey:@"users"]];
	//[self displayCloudUsers:cloud inCell:cell indexPath:indexPath];
	
    return  cell;
}

-(void)viewCloudUserProfile:(UITapGestureRecognizer *)sender {
	UIImageView *avatar = (UIImageView *)sender.view;
	[self highlightItem:avatar];
	NSJSONSerialization *user = objc_getAssociatedObject(avatar, &cloudUserKey);
	[self performSegueWithIdentifier:self.tabBarController != nil ? @"ViewCloudUserProfileList" : @"ViewCloudUserClusterList" sender:user];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	_selectedCloud = [_cloudList objectAtIndex:indexPath.row];
	[(ExploreTabBarController *)self.tabBarController setSelectedCloud:_selectedCloud];
	if (![[_selectedCloud valueForKey:@"inRange"] boolValue]) {
		[self.tabBarController performSegueWithIdentifier:@"ViewCloudUnavailable" sender:tableView];
	} else {
		[self.tabBarController performSegueWithIdentifier:@"ViewRadyus" sender:tableView];
	}

}

-(BOOL)shouldDisableUserInteraction {
	return NO;
}

-(void)commitSettings:(NSDictionary *)settings {
    //access
    radius = [(NSNumber *)[settings objectForKey:@"radius"] floatValue];
    access = (NSString *)[settings objectForKey:@"access"];
    
	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];
	
	[_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateNormal];
	[_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateHighlighted];

}

-(void)reloadTable {
	
	_arrayOfCloudUsersArrays = [[NSMutableArray alloc] init];
	_indexOfCloudBeingLoaded = 0;
	
	//[_tableView reloadData];
	
	_cloudsCount = _cloudList.count;
	
	/*
	if (_cloudsCount == 0) {
		return;
	}
	*/
	
	[self testCustomTable];
	//[_apiService getUsersByIdList:(NSArray *)[[(NSArray *)[_cloudList valueForKey:@"data"] objectAtIndex:0] valueForKey:@"users"]];
	
}

-(void)apiService:(ApiService *)service receivedUserIdList:(NSArray *)userIdList {
	
	if (userIdList.count == 0) {
		[self apiService:service receivedUsersList:(NSJSONSerialization *)[NSArray new]];
	} else {
		[service getUsersByIdList:userIdList];
	}
	
}

-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {
	
	NSArray *cloudUsers = (NSArray *)usersList;
	
	[_arrayOfCloudUsersArrays addObject:cloudUsers];
	
	//NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_indexOfCloudBeingLoaded inSection:0];
	
	//if (_tableView != nil) {
		//[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
		//} else {
	if ([_scrollView viewWithTag:1000 + _indexOfCloudBeingLoaded] != nil) {
		[self displayCloudUsers:cloudUsers inView:[_scrollView viewWithTag:1000 + _indexOfCloudBeingLoaded]];
	}
	//}
	
	if (++_indexOfCloudBeingLoaded < _cloudsCount) {
		//[_apiService getUsersByIdList:(NSArray *)[[(NSArray *)[_cloudList valueForKey:@"data"] objectAtIndex:_indexOfCloudBeingLoaded] valueForKey:@"users"]];
		[service getCloudUsers:(NSString *)[[_cloudList objectAtIndex:_indexOfCloudBeingLoaded] valueForKey:@"id"]];
	}

}

-(void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
	
	[self setCloudCountLabel];
	
	for (UIView *subview in _scrollView.subviews) {
		[subview setNeedsDisplay]; ////////?
		//[subview setNeedsLayout];
		//[subview layoutIfNeeded];
	}
	//[self testCustomTable];
	
}

-(void)setCloudCountLabel {

	int count = (int)_cloudList.count;
	[_cloudCountLabel setText:[NSString stringWithFormat:@"There %@ %@ cloud%@ in %@", count == 1 ? @"is" : @"are",
							   count == 0 ? @"no" : [NSString stringWithFormat:@"%d", count],
							   count == 1 ? @"" : @"s", self.tabBarController == nil ? @"cluster" : @"range"]];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ChangeExploreOptionsList"]) {
        ExploreOptionsDialog *dvc = [segue destinationViewController];
		ExploreTabBarController *parent = (ExploreTabBarController *)self.parentViewController; //tabbarcontroller
		//access
        [dvc setDelegate:parent];
        [dvc setRadius:radius];
	} else if ([segue.identifier isEqualToString:@"ViewCloudUserProfileList"] || [segue.identifier isEqualToString:@"ViewCloudUserClusterList"]) {
		Profile *dvc = [segue destinationViewController];
		[dvc setUserInfo:sender];
	} else if ([segue.identifier isEqualToString:@"ViewCloudClusterList"] || [segue.identifier isEqualToString:@"ViewRadyus"]) {
		CloudView *dvc = [segue destinationViewController];
		[dvc setCloud:_selectedCloud];
	} else if ([segue.identifier hasPrefix:@"ViewCloudUnavailabl"]) {
		UnavailableCloudView *dvc = [segue destinationViewController];
		[dvc setCloud:_selectedCloud];
		[dvc setUserLocation:_userLocation];
		[dvc setParentVC:self];
	}

}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {

	if (self.tabBarController == nil) {
		return;
	}
	
	if (targetContentOffset->y == 0 && scrollView.contentOffset.y < - 36) { ////////odredit točno koliko
		//NSLog(@"Should reload");
		*targetContentOffset = CGPointMake(0, -36);
		[scrollView setContentOffset:CGPointMake(0, -36) animated:YES];
		ExploreMap *mapController = (ExploreMap *)[self.tabBarController.viewControllers objectAtIndex:0];
		[mapController reloadClouds];
		if (_reloadIndicator == nil) {
			_reloadIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
			[_reloadIndicator setFrame:CGRectMake(scrollView.frame.size.width / 2 - _reloadIndicator.frame.size.width / 2, - 18 - _reloadIndicator.frame.size.height / 2, _reloadIndicator.frame.size.width, _reloadIndicator.frame.size.height)];
		}
		
		[_scrollView addSubview:_reloadIndicator];
		[_reloadIndicator startAnimating];
		
	}

}

-(void)testCustomTable {
	
	/*static */CGFloat startingY/* = NAN*/;
	
	//if (isnan(startingY)) {
	startingY = self.tabBarController == nil ? 64 : 128;//_tableView.frame.origin.y;
	//}
	
	NSLog(@"testCustomTable, _tableView == nil: %d, startingY = %f", _tableView == nil, startingY);

	if (_tableView != nil || self.tabBarController == nil) {
		[_cloudCountLabel removeFromSuperview];
		[_tableView removeFromSuperview];
		_tableView.delegate = nil;
		_tableView = nil;
		
		[_cloudCountLabel setFrame:CGRectMake(0, startingY, self.view.frame.size.width, 30)];
		[_cloudCountLabel.layer setCornerRadius:15.0f];
		[self.containerView addSubview:_cloudCountLabel];

	}
	
	if (_scrollView == nil || self.tabBarController == nil) {
		
		_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, startingY + 30, self.view.frame.size.width, self.view.frame.size.height - startingY - 30)];
		//other properties
		_scrollView.delegate = self;
		_scrollView.bounces = YES;
		_scrollView.alwaysBounceVertical = YES;
		[_scrollView setBackgroundColor:UIColorFromRGB(0xF9F9F9)];
		[self.containerView addSubview:_scrollView];

	} else {
		[self clearCloudList];
	}
	
	_cloudsCount = _cloudList.count;
	
	[self drawCloudList:_cloudList];
	
}

-(void)clearCloudList {
	
	if (_scrollView != nil) {
		
		for (__strong UIView *subview in _scrollView.subviews) {
			[subview removeFromSuperview];
			subview = nil;
		}
		
		[_scrollView setContentOffset:CGPointZero];
		[_scrollView setContentSize:_scrollView.frame.size];
		
	}
}

-(void)drawCloudList:(NSArray *)cloudList {

	CGFloat currentY = 0;
	
	//header
	for (NSJSONSerialization *cloud in cloudList) {
		currentY += [self drawCloudListItem:cloud startingY:currentY];
	}
	
	[_scrollView setContentSize:CGSizeMake(self.view.frame.size.width, MAX(currentY, self.view.frame.size.height - _scrollView.frame.origin.y))];
	
	//[_apiService getUsersByIdList:(NSArray *)[[(NSArray *)[_cloudList valueForKey:@"data"] objectAtIndex:0] valueForKey:@"users"]];
	
	if (_cloudList.count > 0) {
		[_apiService getCloudUsers:(NSString *)[[_cloudList objectAtIndex:0] valueForKey:@"id"]];
	}
	
}

-(CGFloat)drawCloudListItem:(NSJSONSerialization *)cloud startingY:(CGFloat)startingY{

	static NSInteger index = 1000;
	if (startingY == 0) {
		index = 1000;
	}
	
	UIButton *listItem = [[UIButton alloc] init];
	[listItem setTag:index++];
	[listItem setAutoresizesSubviews:NO];
	[listItem setBackgroundColor:_scrollView.backgroundColor];
	
	UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 12, self.view.frame.size.width - 16, 1)];
	
	//background color, text color, font, font size, line break, max num of lines,...
	[descLabel setNumberOfLines:5];
	[descLabel setLineBreakMode:NSLineBreakByWordWrapping];
	[descLabel setPreferredMaxLayoutWidth:self.view.frame.size.width - 16];
	[descLabel setText:[cloud valueForKey:@"name"]];
	[descLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:21]];
	
	CGSize maxSize = CGSizeMake(self.view.frame.size.width - 16, CGFLOAT_MAX);
	CGSize requiredSize = [descLabel sizeThatFits:maxSize];
	[descLabel setFrame:CGRectMake(8, 12, requiredSize.width, requiredSize.height)];
	
	[listItem setFrame:CGRectMake(0, startingY, self.view.frame.size.width, descLabel.frame.size.height + 76)];
	
	[listItem addSubview:descLabel];
	
	int max_images = MIN(((int)self.view.frame.size.width - 58) % 50, 9);
	
	for (int i = 0; i < max_images; i++) { //calculate max
		UIImageView *avatar = [[UIImageView alloc] initWithFrame:CGRectMake(8 + 42 * i, descLabel.frame.size.height + 28, 34, 34)];
		[avatar setTag:i + 11];
		[avatar setBackgroundColor:[UIColor clearColor]];
		[avatar.layer setCornerRadius:17.0f];
		[avatar setClipsToBounds:YES];
		[listItem addSubview:avatar];
	}
	
	UIButton *viewAll = [[UIButton alloc] initWithFrame:CGRectMake(8 + 42 * (max_images - 1), descLabel.frame.size.height + 28, 34, 34)];
	[viewAll setBackgroundColor:[UIColor lightGrayColor]];
	[viewAll setTag:20];
	[viewAll.layer setCornerRadius:17.0f];
	[viewAll setHidden: YES];
	
	[listItem addSubview:viewAll];
	
	objc_setAssociatedObject(listItem, &cloudKey, cloud, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	
	[listItem addTarget:self action:@selector(cloudListItemSelected:) forControlEvents:UIControlEventTouchUpInside];
	[listItem addTarget:self action:@selector(highlightItem:) forControlEvents:UIControlEventTouchUpOutside|UIControlEventTouchDown|UIControlEventTouchCancel]; ////////prominit
	
	[_scrollView addSubview: listItem];
	
	UIView *listDivider = [[UIView alloc] initWithFrame:CGRectMake(8, listItem.frame.size.height - 0.5f, listItem.frame.size.width - 8, 0.5f)];
	[listDivider setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
	[listItem addSubview:listDivider];
	
	return listItem.frame.size.height;

}

-(void)cloudListItemSelected:(UIButton *)sender {
	
	NSJSONSerialization *cloud = objc_getAssociatedObject(sender, &cloudKey);
	_selectedCloud = cloud;
	[(ExploreTabBarController *)self.tabBarController setSelectedCloud:_selectedCloud];
	ExploreMap * mapController = (ExploreMap *)[self.tabBarController.viewControllers objectAtIndex:0];
	[(ExploreTabBarController *)self.tabBarController setUserLocation:mapController.userLocation];
	  
	if (![[_selectedCloud valueForKey:@"inRange"] boolValue]) {
		[(self.tabBarController ?: self) performSegueWithIdentifier:(self.tabBarController != nil ? @"ViewCloudUnavailable" : @"ViewCloudUnavailableClusterList") sender:sender];
	} else {
		[(self.tabBarController ?: self) performSegueWithIdentifier:(self.tabBarController != nil ? @"ViewRadyus" : @"ViewCloudClusterList") sender:sender];
	}

}

-(void)highlightItem:(UIView *)sender {
	[sender setAlpha:0.75f];
	[self performSelector:@selector(deselectCloudListItem:) withObject:sender afterDelay:0.5f];
}


-(void)deselectCloudListItem:(UIView *)sender {
	[sender setAlpha:1.0f];
}

-(void)displayCloudUsers:(NSArray *)cloudUsersList inView:(UIView *) listItem {
	
	if (_scrollView == nil) {//privremeno
		return;
	}
	
	int max_images = MIN(((int)self.view.frame.size.width - 58) % 50, 9);
	
	for (int i = 0; i < max_images && i < cloudUsersList.count; i++) { //max_images - 1 ako je count > max
		
		NSJSONSerialization *user = [cloudUsersList objectAtIndex:i];
		
		UIImageView *avatar = (UIImageView *)[listItem viewWithTag:i + 11];
		
		if (avatar != nil) { //privremeno
			objc_setAssociatedObject(avatar, &cloudUserKey, user, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
			
			NSArray *pictures = (NSArray *)[user valueForKey:@"pictures"];
			
			if ([pictures count] == 0) {
				[avatar setDefaultImageWithFirstName:[user valueForKey:@"nameFirst"] lastName:[user valueForKey:@"nameLast"]];
			} else {
				[avatar setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
				AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:[pictures firstObject] imageView:avatar useActivityIndicator:NO];
				loader.delegate = self;
				[loader loadImage];
			}
			
			UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewCloudUserProfile:)];
			
			[avatar setUserInteractionEnabled:YES];
			[avatar addGestureRecognizer:avatarTap];

		} else {
			return; //privremeno
		}
		
	}
	
	if (cloudUsersList.count > max_images) {
		UIButton *more = (UIButton *)[listItem viewWithTag:20];
		[more setHidden:NO];
		[more setTitle:[NSString stringWithFormat:@"+%ld", (long)cloudUsersList.count - max_images] forState:UIControlStateNormal];
		objc_setAssociatedObject(more, &cloudUserListKey, cloudUsersList, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		[more addTarget:self action:@selector(viewCloudUsers:) forControlEvents:UIControlEventTouchUpInside];
	}
	
	[listItem setNeedsDisplay];
	
}

-(void) viewCloudUsers:(id)sender {
	
	NSArray *cloudUsers = objc_getAssociatedObject(sender, &cloudUserListKey);
	
	ViewFriends *viewUsers = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewFriends"];
	[viewUsers setFriendsList:[NSMutableArray arrayWithArray: cloudUsers]];
	[viewUsers setTitle:@"People in cloud"];
	
	[self.navigationController pushViewController:viewUsers animated:YES];
	
}

-(void)viewWillDisappear:(BOOL)animated {
	if (self.tabBarController == nil && ![self.navigationController.viewControllers containsObject:self]) {
		[_apiService cancelRequests];
	}
}

@end
