//
//  LinkPreview.m
//  Radyus
//
//  Created by Frane Poljak on 04/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "LinkPreview.h"

@implementation LinkPreview

#define MAX_TITLE_LENGTH 100
#define MAX_DESCRIPTION_LENGTH 250
#define MAX_LOAD_ATTEMPTS 5

extern const char linkPreviewDelegateStrongReferenceKey;

static NSOperationQueue *_loadFaviconQueue;

-(NSOperationQueue *)loadFaviconQueue {
	return _loadFaviconQueue;
}

-(void)_init {
	
	static dispatch_once_t oncePredicate;
	
	dispatch_once(&oncePredicate, ^{
		_loadFaviconQueue = [[NSOperationQueue alloc] init];
		[_loadFaviconQueue setQualityOfService:NSQualityOfServiceUserInitiated];
		[_loadFaviconQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
	});

	_referenceCounter = 0;
	_loadAttemptCount = 0;
	
	_hasImage = NO;
	_hasMedia = NO;
	
	_shouldShowLoadingIndicator = YES;
	
	[self setMediaPlaybackAllowsAirPlay:YES];
	[self setMediaPlaybackRequiresUserAction:YES];
	//[self setAllowsLinkPreview:YES]; //if ios 9.x+
	[self setAllowsInlineMediaPlayback:YES];
	[self setSuppressesIncrementalRendering:YES];
	//[self setScalesPageToFit:YES];
	//[self setPaginationMode:UIWebPaginationModeTopToBottom];
	//[self setPaginationBreakingMode:];
	//[self setPageLength:self.frame.size.height];
	[self setMultipleTouchEnabled:YES];
	[self setLayoutMargins:UIEdgeInsetsZero];
	[self setKeyboardDisplayRequiresUserAction:YES];
	//[self setGapBetweenPages:0.0f];
	[self setExclusiveTouch:YES];
	[self setDataDetectorTypes:UIDataDetectorTypeAll];
	[self setClipsToBounds:YES];
	[self setAlpha:0.95f];
	
	[self setBackgroundColor:[UIColor whiteColor]];
	[self.layer setZPosition:9.0f];
	
	[self.scrollView setShowsVerticalScrollIndicator:NO];
	[self.scrollView setShowsHorizontalScrollIndicator:NO];
	
	_loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	
	self.layer.shouldRasterize = YES;
	self.layer.masksToBounds = YES;

}

-(instancetype)init {

	if (self = [super init]) {
		[self _init];
	}
	
	return self;
	
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
	
	if (self = [super initWithCoder:aDecoder]) {
		[self _init];
	}
	
	return self;
	
}

-(instancetype)initWithFrame:(CGRect)frame {
	
	if (self = [super initWithFrame:frame]) {
		[self _init];
	}
	
	return self;
	
}

-(void)loadPreview {
	
	_responseData = [[NSMutableData alloc] init];

	//check _maxWidth
	NSString *urlFormat = @"https://api.embed.ly/1/extract?url=%@&maxwidth=%ld&key=97292af232de470ba6a01b29da4886b9&videosrc=true"; //&luxe=1
	NSString *urlString = [NSString stringWithFormat:urlFormat, _previewUrl, (long)_maxWidth];
	NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	//[request setValue:@"max-age=3600" forHTTPHeaderField:@"Cache-Control"];
	//[request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
	NSLog(@"urlString: %@", urlString);
	
	_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	if (self.delegate != nil) { ////////////
		
		if (_shouldShowLoadingIndicator) {
			_loadingIndicator.center = CGPointMake(_maxWidth / 2, _maxWidth / 4);
			[self addSubview:_loadingIndicator];
			
			[_loadingIndicator startAnimating];
		}
		
		[_connection start];
		
	}
	
}

-(void)displayPreview {
	
	if (_siteData == nil) {
		//display error message and reload button
		//or try few times and gracefully fail if unable to load
		return;
	}
	
	/* invalid url:
	 error: 404: - hide (set height to 0, resize message, remove from superview)
	 */
	
	
	/*
	 -app_links
	 -url
	 -title
	 -description
	 -safe
	 -embeds
	 -favicon_url
	 -images[]:
		-thumbnail_url
		-width
		-height
		-(entropy, colors[])
	 -media[]:
		-type
		-url
		-html (check first, if empty than url
		-width //watch out for max_width and max_height(?)
		-height
		----------------
	-language
	-published
	-offset
	-authors
	-lead
	-cache_age
	-content
	-entities
	-keywords
	-related
	-provider_url
	-provider_name
	-provider_display
	 
	*/
	
	
	// app links: http://stackoverflow.com/a/28638647/1630623 (can't use canOpenUrl!)
	
	//add meta tags
	NSString *style = @"body {margin: 0px !important; padding: 0px;} h2 {font-size:17px; padding: 5px 32px 10px 5px; margin: 0px;} p {font-size: 13px; padding: 5px; margin: 0px;}";
	
	NSString *body;
	
	//check 'media' first!
	 if ([[[_siteData valueForKey:@"media"] valueForKey:@"html"] isKindOfClass:[NSString class]]) {
		
		body = [[[_siteData valueForKey:@"media"] valueForKey:@"html"] stringByReplacingOccurrencesOfString:@"src=\"//" withString:@"src=\"https://"]; //check type
		
		 //images[0].url ako je image/gif media
		 
	 } else if ([[[_siteData valueForKey:@"media"] valueForKey:@"type"] isKindOfClass:[NSString class]]) {
		
		NSString *mediaUrl = [[_siteData valueForKey:@"media"] valueForKey:@"url"];
		
		if ([[[_siteData valueForKey:@"media"] valueForKey:@"type"] isEqualToString:@"photo"]) { //image, video, ...
	 
			NSString *imageFormat = @"<img id=\"preview-img\" src=\"%@\"/ style=\"max-width:%ldpx;\" onclick=\"togglePlay()\"/>";
			body = [NSString stringWithFormat:imageFormat, mediaUrl, (long)_maxWidth];
			
			NSURL *_url = [NSURL URLWithString:mediaUrl];
			NSString *imgExtension = [[[[NSURL alloc] initWithScheme:[_url scheme] host:[_url host] path:[_url path]] pathExtension] lowercaseString];
			// maybe better: http://stackoverflow.com/a/28125134/1630623
			
			NSLog(@"imgExtension: %@", imgExtension);
			if ([imgExtension isEqualToString:@"jpg"] || [imgExtension isEqualToString:@"jpeg"] || [imgExtension isEqualToString:@"png"] || [imgExtension isEqualToString:@"svg"] || [imgExtension isEqualToString:@"bmp"] || [imgExtension isEqualToString:@"apng"] || [imgExtension isEqualToString:@"ico"]) {
				//ico?
                //https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#Supported_image_formats
				//http://www.htmlgoodies.com/tutorials/web_graphics/article.php/3479931/Image-Formats.htm
				//https://en.wikipedia.org/wiki/Comparison_of_web_browsers#Image_format_support
				
				body = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", mediaUrl, [body stringByReplacingOccurrencesOfString:@" onclick=\"togglePlay()\"" withString:@""]];
				_hasImage = YES;
				
				// TODO: no link, on click open in gallery? (maybe too much? :) )

			} else {
				
				NSString *scriptFormat = @"<script>function togglePlay(){var e=document.getElementById(\"preview-img\");e&&playing&&freezeGif(e)}function createElement(e,t){var n=document.createElement(e);return t(n),n}function unFreezeGif(e){var t=document.getElementById(\"clone\");t&&t.parentNode.removeChild(t),playing=!0}function freezeGif(e){var t,n=e.width,i=e.height,a=createElement(\"canvas\",function(e){e.width=n,e.height=i}),r=0,o=function(){for(a.getContext(\"2d\").drawImage(e,0,0,n,i),r=0;r<e.attributes.length;r++)t=e.attributes[r],'\"'!==t.name&&(\"id\"==t.name?a.setAttribute(\"id\",\"clone\"):\"onclick\"!=t.name?a.setAttribute(t.name,t.value):a.setAttribute(\"onclick\",\"unFreezeGif()\"));a.style.position=\"absolute\",e.parentNode.insertBefore(a,e),playing=!1};e.complete?o():e.addEventListener(\"load\",o,!0)}var playing=!0;</script>";
				//gif toggle play
				// (check if is gif? http://stackoverflow.com/a/11250140/1630623 ,  https://gist.github.com/lakenen/3012623 probably more expensive then togglePlay script itself!)
				// maybe check if .jpg .jpeg .png etc and not load script if one of those? in that case enable full screen preview!
				// http://stackoverflow.com/a/5607694/1630623
				
				body = [body stringByAppendingString:scriptFormat];
				////[mediaUrl stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"]];
				
			}
			
		}
		
	}
	
	
	if (body == nil) { //no media
		
		body = @"";
		
		NSString *link = [_siteData valueForKey:@"url"];
		
		if ([_siteData valueForKey:@"title"] != nil && [[_siteData valueForKey:@"title"] isKindOfClass:[NSString class]]) {
			
			if ([_siteData valueForKey:@"favicon_url"] != nil && [[_siteData valueForKey:@"favicon_url"] isKindOfClass:[NSString class]]) {
				
				// watch for http://stackoverflow.com/questions/3855913/loading-an-ico-image-switches-red-channel-with-the-blue-one
				
				__block NSString *favicon_url = [_siteData valueForKey:@"favicon_url"];
				
				if (![favicon_url isEqualToString:@""]) {
					
					__weak typeof (self) this = self;
					
					[[self loadFaviconQueue] addOperationWithBlock:^{
						
						//NSLog(@"start loading favicon: %@", favicon_url);
						UIImage *favicon = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[favicon_url stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"]]]];
						
						if (favicon != nil) {
						
							//NSLog(@"loaded favicon: %@", favicon_url);
							UIImageView *faviconIV = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 24, 8, 16, 16)];
							[faviconIV setBackgroundColor:[UIColor clearColor]];
							[faviconIV setContentMode:UIViewContentModeScaleToFill];
							[faviconIV setImage:favicon];
							[faviconIV.layer setZPosition:100.0f];
							
							if (this != nil) {
								[[NSOperationQueue mainQueue] addOperationWithBlock:^{
									//NSLog(@"displaying favicon: %@", favicon_url);
									if (this != nil) {
										[this addSubview:faviconIV]; //doesn't show immedeately!
										[faviconIV setNeedsDisplay];
										[faviconIV setNeedsLayout];
										[this setNeedsLayout];
										[this layoutIfNeeded];
									}
								}];
							}
							
						}
						
					}];
					
				}
			}
			
			NSString *titleFormat = @"<h2>%@</h2>"; //class, link?
			NSString *title = [_siteData valueForKey:@"title"];
			if (title.length > MAX_TITLE_LENGTH) {
				title = [[title substringToIndex:MAX_TITLE_LENGTH - 3] stringByAppendingString:@"..."];
			}
			body = [body stringByAppendingString:[NSString stringWithFormat:titleFormat, title]];
			
		}
		
		if ([_siteData valueForKey:@"images"] != nil && ![[_siteData valueForKey:@"images"] isKindOfClass:[NSNull class]]) { //favicon_url (parse colors, dark background if needed)
			
			_hasImage = YES;
			
			style = [style stringByAppendingString:@" #preview-img {display:block; margin: 0px auto 0px auto; padding: 0px;}"];
			
			if ([[_siteData valueForKey:@"images"] isKindOfClass:[NSArray class]]) {
				
				if ([(NSArray *)[_siteData valueForKey:@"images"] count] > 0) {
					body = [body stringByAppendingString:[NSString stringWithFormat:@"<a href=\"%@\"><img id=\"preview-img\" src=\"%@\"/ style=\"max-width:%ldpx;\"/></a>",
														  link, [[[_siteData valueForKey:@"images"] firstObject] valueForKey:@"url"], (long)_maxWidth]];
				}
				
			} else if ([[_siteData valueForKey:@"images"] isKindOfClass:[NSDictionary class]]) { // never?
				
				if ([[_siteData valueForKey:@"images"] valueForKey:@"url"] != nil) {
					body = [body stringByAppendingString:[NSString stringWithFormat:@"<a href=\"%@\"><img id=\"preview-img\" src=\"%@\"/ style=\"max-width:%ldpx;\"/></a>",
														  link, [[_siteData valueForKey:@"images"] valueForKey:@"url"], (long)_maxWidth]];
				}
			
			}
		}
		
		if ([_siteData valueForKey:@"description"] != nil && [[_siteData valueForKey:@"description"] isKindOfClass:[NSString class]]) {
			NSString *descFormat = @"<p>%@</p>"; //class, link?
			NSString *description = [_siteData valueForKey:@"description"];
			if (description.length > MAX_DESCRIPTION_LENGTH) { //(check 'chars' param in request
				description = [[description substringToIndex:MAX_DESCRIPTION_LENGTH - 3] stringByAppendingString:@"..."];
			}
			body = [body stringByAppendingString:[NSString stringWithFormat:descFormat, description]];
		}
		
		/*
		NSString *_link = [link copy];
		
		if (_link.length > MAX_TITLE_LENGTH) {
			_link = [[_link substringToIndex:MAX_TITLE_LENGTH - 3] stringByAppendingString:@"..."];
		}
		*/
		
		if (![body isEqualToString:@""]) {
			body = [body stringByAppendingString:[NSString stringWithFormat:
					    @"<p style=\"max-width:100%%;overflow:hidden;display:block;white-space:nowrap;text-overflow: ellipsis;\">See more at <a href=\"%@\">%@</a></p>", link, link]];
		}
		
	} else {
		
		_hasMedia = YES;
		self.suppressesIncrementalRendering = NO;
		
		/* //temp
		body = [body stringByReplacingOccurrencesOfString:@"width=\"2\"" withString:@"width=\"200\""];
		body = [body stringByReplacingOccurrencesOfString:@"height=\"1\"" withString:@"height=\"100\""];
		 */
		
		/*
			Some domain specific work (youtube etc...)
		 */
		
		/* //not working, and some of it even causes: "This application is modifying the autolayout engine from a background thread, which can lead to engine corruption and weird crashes.
				//This will cause an exception in a future release."! (playsinline?, removal of allowfullscreen?)
		 
		NSString *hostname = [[[NSURL URLWithString:[_siteData valueForKey:@"url"]].host stringByDeletingPathExtension] pathExtension];
		NSLog(@"hostname: %@", hostname);
		if ([hostname isEqualToString:@"youtube"]) { //or youtu.be i to
			body = [body stringByReplacingOccurrencesOfString:@"\" width=\"" withString:@"&playsinline=1\" width=\""]; //temp, should be done with regex
			body = [body stringByReplacingOccurrencesOfString:@"&image=http" withString:@"&playsinline=1&image=http"]; //temp, should be done with regex
			//body = [body stringByReplacingOccurrencesOfString:@"<iframe " withString:@"<iframe webkit-playsinline "]; //temp, should be done with regex
			body = [body stringByReplacingOccurrencesOfString:@"></iframe>" withString:@" playsinline webkit-playsinline></iframe>"]; //temp, should be done with regex
            //body = [body stringByReplacingOccurrencesOfString:@" allowfullscreen" withString:@""];
			//TODO: do with regex-es (add only if not already present)
			//TODO: watch for the keyboard appear event!
			//TODO: allow login
		}
		*/
		
		if ([[[_siteData valueForKey:@"media"] valueForKey:@"type"] isEqualToString:@"video"]) {
			body = [body stringByReplacingOccurrencesOfString:@"></iframe>" withString:@" playsinline webkit-playsinline></iframe>"];
		}
		
	}
	
	if ((_noContent = ([body isEqualToString:@""] || body == nil))) { //shouldn't be nil, but...
		
		//check if image? 
			//try to load
			//if fails
		//or else
		
		//NSLog(@"No content!");
		if ([self.delegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
			
			if (_referenceCounter <= 0) {
				objc_setAssociatedObject(self, &linkPreviewDelegateStrongReferenceKey, self.delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
				_referenceCounter = 0;
			}
			
			_referenceCounter++;
			
			[self.delegate webViewDidFinishLoad:self];
			
		}
		
		return;
		
	} else {
		body = [body stringByReplacingOccurrencesOfString:@"src=\"http://" withString:@"src=\"https://"];
	}
	
	if (_customCSS == nil) {
		_customCSS = @"";
	}
	
	style = [style stringByAppendingFormat:@" %@", _customCSS];
	
	NSString *css = _hasMedia ? @"" : @"<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css\"/>"; //test
	NSString *content = [NSString stringWithFormat:@"<!DOCTYPE html><html><head>%@<style>%@</style></head><body>%@</body></html>", css, style, body];
	
	if ([[_siteData valueForKey:@"app_links"] isKindOfClass:[NSArray class]]) {
		
		for (NSDictionary *link in [_siteData valueForKey:@"app_links"]) {
			if ([[link valueForKey:@"type"] isEqualToString:@"ios"]) {
				_appLink = link;
			}
		}
		
	}
	
	NSLog(@"content: %@", content);
	[self loadHTMLString:content baseURL:nil];
	
}

-(void)reloadPreview {
	
	[_connection cancel];
	_loadAttemptCount = 0;
	[self loadPreview];
	
}

-(void)clearPreview {
	[self loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
}

-(void)displayUnsafeURLWarning {
	
	NSString *style = @"body {margin: 0px !important; padding: 8px; text-align: center;} h2 {font-size: 15px; padding: 0px; margin: 0px; text-align: center;}\
p {font-size: 13px; padding: 0px; margin: 0px; text-align: center;}";
	
	NSString *bodyFormat = @"<img src=\"%@\"/><h2>Warning!</h2><p>This link is potentially harmful</p><a href=\"reloadPreview\"><p style=\"margin-top:5px;\">Load anyway</p></a>";
	NSString *imgPath = [[NSBundle mainBundle] URLForResource:@"Warning_Triangle" withExtension:@".png"].absoluteString;
	// TODO: find good icon, retina, svg?
	
	/* //alternative:
	 NSURL *url=[[NSBundle mainBundle] bundleURL];
	 [webView loadHTMLString:string baseURL:url];
	 */
	
	NSLog(@"imgPath: %@", imgPath);
	NSString *body = [NSString stringWithFormat:bodyFormat, imgPath];
	
	NSString *content = [NSString stringWithFormat:@"<!DOCTYPE html><html><head>%@<style>%@</style></head><body>%@</body></html>", _customCSS ?: @"", style, body];
	
	[self loadHTMLString:content baseURL:nil];
	
}

-(void)adjustSize {
	
	CGFloat minHeight = _maxWidth / 2;
	
	/* //invalid url response:
	 {
	 "error_code" = 404;
	 "error_message" = "HTTP 503: Service Unavailable";
	 type = error;
	 }
	 */
	
	BOOL urlIsInvalid = ([[_siteData valueForKey:@"error_code"] isKindOfClass:[NSString class]] && [[_siteData valueForKey:@"error_code"] isEqualToString:@"404"]);
	CGFloat newHeight;
	
	if (_noContent || urlIsInvalid) {
		
		newHeight = 0;
		
	} else {
		
		NSString *result = [self stringByEvaluatingJavaScriptFromString:
						@"(function(){var c=document.body.childNodes;var h=0;var i;for(i=0;i<c.length;i++){h+=c[i].offsetHeight||c[i].clientHeight||c[i].innerHeight||0;}return h;})();"];
		//img inside "a" not calculated, add, put onload da je tek vidljiva (ili on fail da je nevidljiva)!
	
		NSInteger height = [result integerValue];
	
		/* //ne triba više zbog padding-a
		 if (!_hasMedia) {
			height += 25;
		 }
		 */
	
		if (_hasImage) {
			
			NSInteger imgHeight = [[self stringByEvaluatingJavaScriptFromString:@"(function(){var i=document.getElementById(\"preview-img\");if(!i){return 0;};var h=i.offsetHeight||i.clientHeight||i.innerHeight||0;return h;})();"] integerValue];
			
			NSLog(@"imgHeight: %ld", (long)imgHeight);
			if (_hasMedia) {
				height = imgHeight;
			} else {
				height += imgHeight;
			}
			
		}
	
		if (height <= self.frame.size.height) {
			
			if (height == 0) { //no content
				newHeight = 0;
			} else {
				newHeight = MAX(height, minHeight);
			}
			
		} else {
		
			self.scrollView.scrollEnabled = (height > _maxHeight);
			newHeight = MIN(height, _maxHeight);
			
		}
	
	}
	
	CGRect newFrame = self.frame;
	newFrame.size.height = newHeight;
	self.frame = newFrame;
	
	[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, MAX(self.frame.size.height, self.scrollView.contentSize.height))]; //no horizontal scrolloing!
	
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[_responseData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	//check if ok
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	if (self.delegate == nil) {
		return;
	}
	
	NSError *error;
	NSDictionary *response = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:_responseData options:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
	
	_siteData = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary *)response];
	
	NSLog(@"site data: %@", _siteData);
	if (_siteData == nil || [_siteData isEqualToDictionary:@{}] || [[_siteData valueForKey:@"type"] isEqualToString:@"error"] || [[_siteData valueForKey:@"safe"] boolValue]) {
		[self displayPreview];
	} else {
		//unsafe URL
		//not detecting as well as SafeBrowsingService, use that? (and this too?, alreadyConfirmed if both show unsafe, so user doesn't confirm twice (if he wants to 'see anyway'))
		//test malicious site: http://www.ianfette.org
		[self displayUnsafeURLWarning];
	}
	
}

-(NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
	_loadAttemptCount++;
	return request;
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	if (self.delegate != nil) {
		if (_loadAttemptCount < MAX_LOAD_ATTEMPTS) {
			[self performSelector:@selector(loadPreview) withObject:nil afterDelay:_loadAttemptCount * _loadAttemptCount / 2];
		}
	}
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	return cachedResponse;
}

-(void)dealloc {
	
	[[self loadFaviconQueue] cancelAllOperations];
	[_connection cancel];
	_connection = nil;
	[self.layer removeAllAnimations];
	[UIView cancelPreviousPerformRequestsWithTarget:self];
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	
}

@end
