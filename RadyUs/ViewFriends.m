//
//  ViewFriends.m
//  RadyUs
//
//  Created by Frane Poljak on 05/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ViewFriends.h"
#import "Profile.h"
#import "TransparentDialog.h"
#import "UIImageView+NoAvatar.h"

@interface ViewFriends () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, ApiServiceDelegate, AsyncImageLoadDelegate, TransparentDialogDelegate>

@end

@implementation ViewFriends
@synthesize delegate;

static char layerKey;
static char mutualFriendsKey;
static char serviceFriendKey;
static char userImageKey;
static char addRemoveFriendKey;

CALayer *selectedLayer;
BOOL searchViewRepositioning;
int lineFeedCount;

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	//title (friends, people in cloud,...?
	
	/*if (_friendIdList == nil) {
		_friendsList = [[NSMutableArray alloc] init];
		
		for (int i = 0; i < 60; i++) {////test
			[_friendsList addObject:@{@"id": [NSString stringWithFormat:@"ABCD11%d", i], @"nameFirst": [NSString stringWithFormat:@"Ime%d", i], @"nameLast": [NSString stringWithFormat:@"Prezime%d", i]}];
			//@"ime": [NSString stringWithFormat:@"Ime%d Prezime%d", i, i]
			//nameFirest, nameLast, username....
			_filteredFriendList = [NSMutableArray arrayWithArray:_friendsList];
			_chosenFriends = [[NSMutableArray alloc] init];
		}	
	} else {*/
		//get friends
	
	
	if (self.navigationController != nil) {
		self.navigationController.navigationBar.topItem.title = @" ";
	}

	_currentFragment = @"";
	_userImageHashMap = [[NSMutableDictionary alloc] init];
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
	if (_friendsList != nil) {
		[self apiService:_apiService receivedUsersList:(NSJSONSerialization *)_friendsList];
	} else if (_friendIdList != nil) {
		[_apiService getUsersByIdList:_friendIdList];
	} else {
		[_apiService getFriends:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	}
	
	if (![_selectionType isEqualToString:@"single"]) { //!=nil?
		_getPendingService = [[ApiService alloc] init]; 
		_getPendingService.delegate = self;
		[_getPendingService getPendingRequests];
		
		_getRequestedService = [[ApiService alloc] init];
		_getRequestedService.delegate = self;
		[_getRequestedService getRequestedFriends];

	}
    
    UITapGestureRecognizer *textTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
	textTap.delegate = self;
    [_searchView addGestureRecognizer:textTap];
    
    searchViewRepositioning = NO;
    lineFeedCount = 0;
    
    [_searchView addObserver:self forKeyPath:@"text" options:0 context:nil];
   
}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	return YES;
}

-(BOOL)shouldDisableUserInteraction {
	return _filteredFriendList == nil;
}

-(void)apiService:(ApiService *)service receivedUserIdList:(NSArray *)userIdList {
	
	if ([service isEqual:_apiService]) {

		_friendIdList = [[NSMutableArray alloc] initWithArray:userIdList];

	}
	
	if (userIdList.count == 0) {
		[self apiService:service receivedUsersList:(NSJSONSerialization *)[NSArray array]];
	} else {
		[service getUsersByIdList:userIdList];
	}
	
}

-(void)getPendingSuccess:(NSArray *)pending {
	_pending = [NSMutableArray arrayWithArray:pending ?: [NSArray array]];
	if (_requested != nil) {
		[_tableView reloadData];
	}
}

-(void)getRequestedSuccess:(NSArray *)requested {
	_requested = [NSMutableArray arrayWithArray:requested ?: [NSArray array]];
	if (_pending != nil) {
		[_tableView reloadData];
	}
}

-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {
	
	if ([service isEqual:_apiService]) {
		
		_friendsList = [NSMutableArray arrayWithArray:(NSArray *)usersList];
		
		for (NSDictionary *friend in _friendsList) {
			if (![[friend valueForKey:@"id"] isEqualToString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]]) {
				[self findFriendsOfAFriend:friend];
			}
		}
		
		_filteredFriendList = [NSMutableArray arrayWithArray:_friendsList];
		_chosenFriends = [[NSMutableArray alloc] init];
		
		[_tableView reloadData];
		
		[self loadUserImages];
		
	} else {
		
		NSDictionary *friend = objc_getAssociatedObject(service, &serviceFriendKey);
		objc_setAssociatedObject(friend, &mutualFriendsKey, [NSArray arrayWithArray:[self mutualFriends:usersList]], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		
		if ([_filteredFriendList containsObject:friend]) {
			[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[_filteredFriendList indexOfObject:friend] + (int)!![_selectionType isEqualToString:@"multiple"] inSection:0]] withRowAnimation:UITableViewRowAnimationNone];

		}
		
	}
	
}

-(void)findFriendsOfAFriend:(NSDictionary *)friend {
	
	ApiService *service = [[ApiService alloc] init];
	service.delegate = self;
	
	objc_setAssociatedObject(service, &serviceFriendKey, friend, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	
	[service getFriends:[friend valueForKey:@"id"]];
	
}

-(void)loadUserImages {
	
	for (NSDictionary *user in _friendsList) {
		
		NSString *imageId = [(NSArray *)[user valueForKey:@"pictures"] firstObject];
		UIImageView *tempIW = [[UIImageView alloc] initWithFrame:CGRectMake(-54, -54, 54, 54)];

		tempIW.alpha = 0;
		[self.view addSubview:tempIW];
		
		if (imageId != nil) {
		
			objc_setAssociatedObject(tempIW, &userImageKey, user, OBJC_ASSOCIATION_ASSIGN);
			
			AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:imageId imageView:tempIW useActivityIndicator:NO];
			loader.delegate = self;
			[loader loadImage];
			
		} else {
			
			[tempIW setDefaultImageWithFirstName:[user valueForKey:@"nameFirst"] lastName:[user valueForKey:@"nameLast"]];
			[_userImageHashMap setValue:tempIW.image forKey:[user valueForKey:@"id"]];
			
			[tempIW removeFromSuperview];
			
			NSUInteger row = [_filteredFriendList indexOfObject:user];
			
			if (row == NSNotFound) {
				return;
			}
			
			row +=  ([_selectionType isEqualToString:@"multiple"] ? 1 : 0);
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
			[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
			
		}
		
	}

}

-(void)loadSuccess:(UIImageView *)imageView {
	
	NSDictionary *friend = objc_getAssociatedObject(imageView, &userImageKey);
	
	NSUInteger row = [_friendsList indexOfObject:friend];
	
	if (row == NSNotFound) {
		return;
	}
	
	[_userImageHashMap setValue:imageView.image forKey:[[_friendsList objectAtIndex:row] valueForKey:@"id"]];
	
	row = [_filteredFriendList indexOfObject:friend];
	
	if (row == NSNotFound) {
		return;
	}
	
	row +=  ([_selectionType isEqualToString:@"multiple"] ? 1 : 0);

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];

	[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
	
	[imageView removeFromSuperview];
	
}

-(NSArray *)mutualFriends:(NSJSONSerialization *)personList {
	
	NSMutableArray *mutFriends = [[NSMutableArray alloc] init];
	NSArray *myFriends = (NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"userFriends"] ?: [NSArray new];
	
	for (NSString *friend in [(NSArray *)personList valueForKey:@"id"]) {
		if ([myFriends containsObject:friend]) {
			[mutFriends addObject:friend];
		}
	}
	
	return mutFriends;
	
}

-(void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
	
    if (_searchView != nil) {
		
        //[_searchView setContentSize:CGSizeMake(_searchView.frame.size.width - _searchView.contentInset.left - _searchView.contentInset.right - _searchView.textContainerInset.left - _searchView.textContainerInset.right, _searchView.contentSize.height)];
        [_searchView setContentSize:CGSizeMake(1.0f, _searchView.contentSize.height)];
        [_searchView setContentOffset:CGPointMake(0, 0)];
        _searchView.showsHorizontalScrollIndicator = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
        [_searchView becomeFirstResponder];
		
    }
	
}

-(void)viewWillDisappear:(BOOL)animated {
	
	[super viewWillDisappear:animated];

    [_searchView removeObserver:self forKeyPath:@"text"];
	[_apiService cancelRequests];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    BOOL selectAll = _searchView != nil && [_selectionType isEqualToString:@"multiple"] && indexPath.row == 0;
    
    NSString *identifier = selectAll ? @"AddFriendsSelectAll" : @"FriendsListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
	
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if (selectAll) {
        return cell;
    }
	
	NSJSONSerialization *friend = [_filteredFriendList objectAtIndex:indexPath.row - ((_searchView == nil || [_selectionType isEqualToString:@"single"]) ? 0 : 1)];
	NSString *fullName = [NSString stringWithFormat:@"%@ %@", (NSString *)[friend valueForKey:@"nameFirst"], (NSString *)[friend valueForKey:@"nameLast"]];
	
	UIImageView *avatar = (UIImageView *)[cell viewWithTag:3];
	[avatar.layer setCornerRadius:27.0f];
	[avatar setClipsToBounds:YES];
	UIImage *image = (UIImage *)[_userImageHashMap valueForKey:[friend valueForKey:@"id"]];
	[avatar setImage:image];
	
    UILabel *name = (UILabel *)[cell viewWithTag:1];
    [name setText:fullName];
	
	UILabel *mutualFriendsLabel = (UILabel *)[cell viewWithTag:2];
	
	NSArray *mf;
	NSString *mfText;
	
	UIButton *addRemoveBtn = (UIButton *)[cell viewWithTag:4];
	
	if ([[friend valueForKey:@"id"] isEqualToString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]]) { //own
		
		mfText = @"";
		[addRemoveBtn setHidden:YES];
		[addRemoveBtn setUserInteractionEnabled:NO];
		
	} else {
		
		if (objc_getAssociatedObject(friend, &mutualFriendsKey) == nil) {
			mfText = @"";
		} else {
			mf = objc_getAssociatedObject(friend, &mutualFriendsKey);
			mfText = [NSString stringWithFormat:@"%@ mutual friend%@", mf.count > 0 ? [NSString stringWithFormat:@"%ld", (long)mf.count] : @"No", mf.count != 1 ? @"s" : @""];
			NSLog(@"mfText: %@", mfText);
		}
		
		BOOL isAFriend = [(NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"userFriends"] containsObject:[friend valueForKey:@"id"]];
		
		if ([_selectionType isEqualToString:@"single"]) {
			[addRemoveBtn setHidden:YES];
		} else if (isAFriend) {
			
			[addRemoveBtn setHidden:NO];
			[addRemoveBtn setUserInteractionEnabled:YES];
			[addRemoveBtn setTitle:@"Remove" forState:UIControlStateNormal];
			[addRemoveBtn setTitle:@"Remove" forState:UIControlStateHighlighted];
			
		} else { //check if pending or requested!!
			
			if (_pending != nil && _requested != nil) {
				
				if ([_pending containsObject:[friend valueForKey:@"id"]]) {
					[addRemoveBtn setTitle:@"Accept" forState:UIControlStateNormal];
					[addRemoveBtn setTitle:@"Accept" forState:UIControlStateHighlighted];
				} else if ([_requested containsObject:[friend valueForKey:@"id"]]) {
					[addRemoveBtn setTitle:@"Cancel" forState:UIControlStateNormal]; //decline?
					[addRemoveBtn setTitle:@"Cancel" forState:UIControlStateHighlighted]; //decline? (user profile too)
				} else {
					[addRemoveBtn setTitle:@"Add" forState:UIControlStateNormal];
					[addRemoveBtn setTitle:@"Add" forState:UIControlStateHighlighted];
				}
			
				//make all initially hidden
				[addRemoveBtn setHidden:NO];
				[addRemoveBtn setUserInteractionEnabled:YES];
				
			}
		
			objc_setAssociatedObject(addRemoveBtn, &addRemoveFriendKey, friend, OBJC_ASSOCIATION_ASSIGN); //
			[addRemoveBtn addTarget:self action:@selector(addRemoveFriend:) forControlEvents:UIControlEventTouchUpInside];
			
		}
		
	}
	
	[mutualFriendsLabel setText:mfText];
	
    return  cell;

}

-(void)addRemoveFriend:(id)sender {
	
	_friendToAddOrRemove = (NSDictionary *)objc_getAssociatedObject(sender, &addRemoveFriendKey);
	
	if (_friendToAddOrRemove == nil) { //shouldn't happen
		return;
	}
	
	//local nnotifications for friend request events, change list accordingly
	BOOL isAFriend = [(NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"userFriends"] containsObject:[_friendToAddOrRemove valueForKey:@"id"]];
	BOOL isPending = [_pending containsObject:[_friendToAddOrRemove valueForKey:@"id"]];
	BOOL isRequestd = [_requested containsObject:[_friendToAddOrRemove valueForKey:@"id"]];
	
	//define action
	
	TransparentDialog *dialog = [self.storyboard instantiateViewControllerWithIdentifier:@"TransparentDialogFriends"];
	dialog.delegate = self;
	
	NSString *cancelTitle;
	NSString *confirmTitle;
	NSString *questionText;
	
	if (isPending) { //action
		cancelTitle = @"Reject";
		confirmTitle = @"Accept";
		questionText = @"Accept friend request?";
		_addRemoveAction = @"add";
	} else if (isRequestd) {
		cancelTitle = @"Abort, I changed my mind!";
		confirmTitle = @"Cancel";
		questionText = @"Cancel friend request";
		_addRemoveAction = @"remove";
	} else if (isAFriend) {
		cancelTitle = @"Abort, I changed my mind!";
		confirmTitle = @"Unfriend";
		questionText = @"Remove from friends?";
		_addRemoveAction = @"remove";
	} else {
		cancelTitle = @"Nope, I give up";
		confirmTitle = @"Send";
		questionText = @"Send friend request?";
		_addRemoveAction = @"add";
	}
	
	[dialog setCancelTitle:cancelTitle];
	[dialog setConfirmTitle:confirmTitle];
	[dialog setQuestionText:questionText];
	
	[self presentViewController:dialog animated:YES completion:nil];
	
}

-(void)confirmSelected {

	if (_friendToAddOrRemove == nil || [_friendToAddOrRemove valueForKey:@"id"] == nil) {
		return;
	}
	
	if ([_addRemoveAction isEqualToString:@"add"]) {
		[_apiService addFriend:[_friendToAddOrRemove valueForKey:@"id"]];
	} else if ([_addRemoveAction isEqualToString:@"remove"]) {
		[_apiService unfriendUser:[_friendToAddOrRemove valueForKey:@"id"]];
	} else {
		_addRemoveAction = nil;
		_friendToAddOrRemove = nil;
	}

}

-(void)cancelSelected {
	
	NSLog(@"_friendToAddOrRemove: %@", _friendToAddOrRemove);
	
	_addRemoveAction = nil;
	_friendToAddOrRemove = nil;
	
}

-(void)addFriendSuccess:(NSJSONSerialization *)response {

	NSString *userId = [_friendToAddOrRemove valueForKey:@"id"]; //[response valueForKey:@"id"] ?: @""; //trenutno ne vraca ID!
	
	BOOL isPending = [_pending containsObject:[_friendToAddOrRemove valueForKey:@"id"]];
	
	if (isPending) {
		[_pending removeObject:userId];
	}
	
	NSInteger row = [_filteredFriendList indexOfObject:userId];
	
	if (row != NSNotFound) {
		[_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
	}
	
	_addRemoveAction = nil;
	_friendToAddOrRemove = nil;
	
}

-(void)unfriendSuccess:(NSJSONSerialization *)response {

	NSString *userId = [_friendToAddOrRemove valueForKey:@"id"]; //[response valueForKey:@"id"] ?: @""; //trenutno ne vraca ID!
	
	BOOL isPending = [_pending containsObject:[_friendToAddOrRemove valueForKey:@"id"]];
	BOOL isRequestd = [_requested containsObject:[_friendToAddOrRemove valueForKey:@"id"]];
	
	if (isPending) {
		[_pending removeObject:userId];
	} else if (isRequestd) {
		[_requested removeObject:userId];
	}
	//user defaults now automatically adjusted
	//if in own friend list remove from arrays
	
	NSInteger row = [_filteredFriendList indexOfObject:userId];
	
	if (row != NSNotFound) { //if in own friend list reload whole table
		[_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
	}

	_addRemoveAction = nil;
	_friendToAddOrRemove = nil;
	
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_filteredFriendList count] + ((_searchView == nil || [_selectionType isEqualToString:@"single"]) ? 0 : 1);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (_searchView != nil && [_selectionType isEqualToString:@"multiple"] && indexPath.row == 0) { //style
        return 40;
    }
	
    return 70;
	
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_searchView == nil) {
		
        [self performSegueWithIdentifier:@"ViewProfileFromList" sender:indexPath];
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
		
        return;
		
    }
    
    if ([_selectionType isEqualToString:@"single"]) {
		
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate didChooseSingleFriend:[_filteredFriendList objectAtIndex:indexPath.row]];
        }];
		
        return;
		
    }
    
    [_searchView resignFirstResponder];
    
    if (indexPath.row == 0) {
		
        if (_filteredFriendList.count == 0) {//deselect
			
			[_chosenFriends removeAllObjects];
            _filteredFriendList = [NSMutableArray arrayWithArray:_friendsList];
            //kvačica
            [_tableView reloadData];
            [_createButton setAlpha:0.0f];
			
        } else {
			
            searchViewRepositioning = YES;
            _chosenFriends = [NSMutableArray arrayWithArray:_friendsList];
            [_filteredFriendList removeAllObjects];
            [self deleteAllLayers];
            [_searchView setText:@""];
            _currentFragment = @"";
            lineFeedCount = 0;
            //kvačica
            [self adjustSearchTextViewSize];
            searchViewRepositioning = NO;
            [_tableView reloadData];
            [_createButton setAlpha:1.0f];
			
        }
		
        return;
		
    }
    
    NSDictionary *friend = [_filteredFriendList objectAtIndex:indexPath.row - 1];
    [_chosenFriends addObject:friend];
	
	NSString *fullName = [NSString stringWithFormat:@"%@ %@", (NSString *)[friend valueForKey:@"nameFirst"], (NSString *)[friend valueForKey:@"nameLast"]];
	
    [_searchView setText:[[_searchView.text substringToIndex:_searchView.text.length - _currentFragment.length] stringByAppendingString:fullName]];
    
    _currentFragment = @"";

    _filteredFriendList = [NSMutableArray arrayWithArray:_friendsList];
    [_filteredFriendList removeObjectsInArray:_chosenFriends]; //samo selected

    [_tableView reloadData];
    
    [_createButton setAlpha:1.0f];

}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if (range.length == 1) {
        
        if (_currentFragment.length == 0) {
			
            if (selectedLayer != nil) { //pozicioniranje kursora
                [self deleteLayer:selectedLayer];
            } else if ([_chosenFriends count] > 0) {
				[self selectLayer:[self getSublayerForFriend:[_chosenFriends lastObject]]]; /////????? pozicija kursora??
            }
            
            return NO;
			
        }
        
        _currentFragment = [_currentFragment substringToIndex:[_currentFragment length] - 1];
        [self filterFriendList:_currentFragment];
		
		return  YES;
		
    }
    
    if (selectedLayer != nil) {
        [self deselectLayer:selectedLayer];
    }
	
	_currentFragment = [_currentFragment stringByAppendingString:text];
	[self filterFriendList:_currentFragment];
        
	return YES;
	
}

-(CALayer *)getSublayerForFriend:(NSDictionary *)friend {

    for (NSInteger i = _searchView.layer.sublayers.count - 1; i > -1; i--) {
		
        CALayer *sublayer = [_searchView.layer.sublayers objectAtIndex:i];
		
		if ([objc_getAssociatedObject(sublayer, &layerKey) isEqualToDictionary:friend]) {
            return sublayer;
        }
		
    }
    
    return nil;
	
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	
    if([keyPath isEqualToString:@"text"] && object == _searchView) {
        
        if ([_chosenFriends count] == 0) {
            return;
        }
        
        if (!searchViewRepositioning) {

            searchViewRepositioning = YES;
			
			NSString *fullName = [NSString stringWithFormat:@"%@ %@", (NSString *)[[_chosenFriends lastObject] valueForKey:@"nameFirst"], (NSString *)[[_chosenFriends lastObject] valueForKey:@"nameLast"]];
			
            NSString *name = fullName;
			NSRange range = [[_searchView text] rangeOfString:name options:NSBackwardsSearch]; //searchRange?
            BOOL newLine = [self didGoInNewLine:range];
            
            CGRect rect = [self frameOfTextRange:range inTextView:_searchView];
            
            if (rect.origin.x + rect.size.width > _searchView.frame.size.width - _searchView.contentInset.left - _searchView.contentInset.right - _searchView.textContainerInset.left - _searchView.textContainerInset.right || newLine) {
                _searchView.text = [_searchView.text substringToIndex:_searchView.text.length - name.length];
                _searchView.text = [[_searchView.text stringByAppendingString:@"\n \n"] stringByAppendingString:name];
                if (!newLine) {
                    lineFeedCount += 2;
                }
            }
            
            _searchView.text = [_searchView.text stringByAppendingString:@"      "];
            
            [self highlightName:[_chosenFriends lastObject]];
            
            [self performSelectorOnMainThread:@selector(adjustSearchTextViewSize) withObject:nil waitUntilDone:NO];
            
            searchViewRepositioning = NO;
        }
		
	}
	
}

-(BOOL)didGoInNewLine:(NSRange)range{
	
    if (range.location == 0) {
        return NO;
    }
	
    NSRange oldRange = NSMakeRange(range.location - 1, 1);
    CGRect oldFrame = [self frameOfTextRange:oldRange inTextView:_searchView];
    //CGRect newFrame = [self frameOfTextRange:range inTextView:_searchView];
    UITextPosition *pos = _searchView.endOfDocument;
    CGRect newFrame = [_searchView caretRectForPosition:pos];
    //CGRect newFrame = [self frameOfTextRange:range inTextView:_searchView];
    
    //NSLog(@"old cursor position: %f, %f; new cursor position %f, %f", oldFrame.origin.x, oldFrame.origin.y, newFrame.origin.x, newFrame.origin.y);
    
    if (newFrame.origin.y > oldFrame.origin.y || newFrame.origin.x < oldFrame.origin.x) {
        lineFeedCount += 2;
    }
    
    return newFrame.origin.y > oldFrame.origin.y; //|| newFrame.origin.x < oldFrame.origin.x;// || oldFrame.size.height > newFrame.size.height; //samo origin
	
}

- (CGRect)frameOfTextRange:(NSRange)range inTextView:(UITextView *)textView {
	
    UITextPosition *beginning = textView.beginningOfDocument;
    UITextPosition *start = [textView positionFromPosition:beginning offset:range.location];
    UITextPosition *end = [textView positionFromPosition:start offset:range.length];
    UITextRange *textRange = [textView textRangeFromPosition:start toPosition:end];
    CGRect rect = [textView firstRectForRange:textRange];
    
    //textView.selectedRange = range;
    //UITextRange *textRange = [textView selectedTextRange];
    //CGRect rect = [textView firstRectForRange:textRange];//caretrect
    
    return [textView convertRect:rect fromView:textView.textInputView];
	
}

- (void)textViewDidChange:(UITextView *)textView
{
    //[self adjustSearchTextViewSize];
}

-(void)filterFriendList:(NSString *) filter {
	
    NSPredicate *predicate;
    
    if ([filter isEqualToString:@""]) {
		
		predicate = [NSPredicate predicateWithValue:YES];
	
	} else if ([filter containsString:@" "]) {
		
		NSMutableArray *arr = [NSMutableArray arrayWithArray: [filter componentsSeparatedByString:@" "]];
		NSString *last = [arr lastObject];
		[arr removeLastObject];
		NSString *first = [arr componentsJoinedByString:@" "];
		
		if ([last isEqualToString:@""]) {
			predicate = [NSPredicate predicateWithFormat:@"nameFirst ENDSWITH[cd] %@", first];
		} else {
			predicate = [NSPredicate predicateWithFormat:@"(nameFirst ENDSWITH[cd] %@ && nameLast BEGINSWITH[cd] %@) || nameFirst CONTAINS[cd] %@", first, last, filter];
		}
		
	} else {
		
		predicate = [NSPredicate predicateWithFormat:@"nameFirst CONTAINS[cd] %@ || nameLast CONTAINS[cd] %@", filter, filter];
		
	}
	
	////http://stackoverflow.com/questions/13984846/how-to-join-two-strings-for-nspredicate-ie-firstname-and-lastname
	////http://stackoverflow.com/questions/13479542/nspredicate-combine-contains-with-in
	
    _filteredFriendList = [NSMutableArray arrayWithArray:[_friendsList filteredArrayUsingPredicate:predicate]];
    [_filteredFriendList removeObjectsInArray:_chosenFriends];
    [_tableView reloadData];
	
}

-(void)highlightName:(NSDictionary *)friend {
    
	NSString *name = [NSString stringWithFormat:@"%@ %@", (NSString *)[friend valueForKey:@"nameFirst"], (NSString *)[friend valueForKey:@"nameLast"]];

    NSRange match = [[_searchView text]rangeOfString:name options:NSBackwardsSearch];
         
    CGRect textRect = [self frameOfTextRange:match inTextView:_searchView];
    textRect.origin.y = (_searchView.font.lineHeight + 2.75) * lineFeedCount + _searchView.textContainerInset.top - 2;
    textRect = CGRectInset(textRect, -10, -5);

    CALayer* roundRect = [CALayer layer];
    [roundRect setFrame:textRect];
    [roundRect setBounds:textRect];
    [roundRect setCornerRadius:textRect.size.height / 2];
    [roundRect setBackgroundColor:UIColorFromRGB(0xE7E7E7).CGColor];
    [roundRect setZPosition:-1.0f];
    
    objc_setAssociatedObject(roundRect, &layerKey, friend, OBJC_ASSOCIATION_RETAIN);
    
    [[_searchView layer] addSublayer:roundRect];
    
}

-(void)handleTap:(UITapGestureRecognizer *)sender {

	NSLog(@"handleTap:");
    [_searchView becomeFirstResponder];
    
    CGPoint tapLocation = [sender locationInView:_searchView];
    
    for (CALayer *layer in [[_searchView layer] sublayers]) {
        if (CGRectContainsPoint([layer frame], tapLocation)) {
            NSDictionary *friend = objc_getAssociatedObject(layer, &layerKey);
            
            if (friend != nil) {
                [self selectLayer:layer];
                return;
            }
        }
    }
    
    if (selectedLayer != nil) {
        [self deselectLayer:selectedLayer];
    }
    
}

-(void)selectLayer:(CALayer *)layer {
    
    if ([layer isEqual:selectedLayer]) {
        [self deselectLayer:layer];
    }
    else {
        if (selectedLayer != nil) {
            [self deselectLayer:selectedLayer];
        }
        [layer setBackgroundColor:UIColorFromRGB(0xFFD203).CGColor];
        selectedLayer = layer;
    }
    
}

-(void)deselectLayer:(CALayer *)layer {
    
    [layer setBackgroundColor:UIColorFromRGB(0xE7E7E7).CGColor];
    selectedLayer = nil;

}

-(void)deleteAllLayers {
    
    for (int i = 0; i < _searchView.layer.sublayers.count; i++) {
		
        CALayer *fl = [_searchView.layer.sublayers objectAtIndex:i];
		
        if (objc_getAssociatedObject(fl, &layerKey) != nil) {
			
            objc_setAssociatedObject(fl, &layerKey, nil, OBJC_ASSOCIATION_RETAIN);
            [fl removeFromSuperlayer];
            i--;
			
        }
		
    }
	
}

-(void)deleteLayer:(CALayer *)layer {

    NSDictionary *friend = objc_getAssociatedObject(layer, &layerKey);
    
    [_chosenFriends removeObject:friend];

	NSString *fullName = [NSString stringWithFormat:@"%@ %@", (NSString *)[friend valueForKey:@"nameFirst"], (NSString *)[friend valueForKey:@"nameLast"]];
	
    NSLog(@"Deleting: %@, remaining: %lu", fullName, (unsigned long)_chosenFriends.count);
    [self deleteAllLayers];
    
    searchViewRepositioning = YES;
    _searchView.text = @"";
    
    lineFeedCount = 0;
    
    for (NSDictionary *friend in _chosenFriends) {
		
        NSString *name = [NSString stringWithFormat:@"%@ %@", (NSString *)[friend valueForKey:@"nameFirst"], (NSString *)[friend valueForKey:@"nameLast"]];
        
        _searchView.text = [_searchView.text stringByAppendingString:name];
		
		NSRange searchRange = NSMakeRange(0, MIN(_searchView.selectedRange.location + _searchView.selectedRange.length + name.length, _searchView.text.length));
        NSRange range = [[_searchView text] rangeOfString:name options:NSBackwardsSearch range:searchRange];
		
        CGRect rect = [self frameOfTextRange:range inTextView:_searchView];
        
        BOOL newLine = [self didGoInNewLine:range];
        
        if (rect.origin.x + rect.size.width > _searchView.frame.size.width - _searchView.contentInset.left - _searchView.contentInset.right - _searchView.textContainerInset.left - _searchView.textContainerInset.right || newLine) {
			
            _searchView.text = [_searchView.text substringToIndex:_searchView.text.length - name.length];
            _searchView.text = [[_searchView.text stringByAppendingString:@"\n \n"] stringByAppendingString:name]; // \r\n?
			
			if (!newLine) {
                lineFeedCount += 2;
            }
			
        }
        
        _searchView.text = [_searchView.text stringByAppendingString:@"      "];
        
        [self highlightName:friend];

    }
    
    //_searchView.text = [_searchView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    searchViewRepositioning = NO;
    
    selectedLayer = nil;
    
    [self filterFriendList:_currentFragment];
    
    [self adjustSearchTextViewSize];
    
    if (_chosenFriends.count == 0) {
        [_createButton setAlpha:0.0f];
    }
    
}

-(void)adjustSearchTextViewSize {
    
    CGFloat fixedWidth = _searchView.frame.size.width;
    CGSize newSize = [_searchView sizeThatFits:CGSizeMake(fixedWidth, CGFLOAT_MAX)];
    CGRect newFrame = _searchView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), MAX(MIN(newSize.height, 136), 64));
    _searchView.frame = newFrame;
    _tableView.frame = CGRectMake(0, newFrame.origin.y + newFrame.size.height, _tableView.frame.size.width, self.view.frame.size.height - newFrame.origin.y - newFrame.size.height);
    
    //[_searchView setContentSize:CGSizeMake(_searchView.frame.size.width - _searchView.contentInset.left - _searchView.contentInset.right - _searchView.textContainerInset.left - _searchView.textContainerInset.right, _searchView.contentSize.height)];
    
    [_searchView setContentSize:CGSizeMake(1.0f, _searchView.contentSize.height)];
    [_searchView setContentOffset:CGPointMake(0, _searchView.contentSize.height - _searchView.frame.size.height)];
	
}

-(IBAction)dismiss:(id)sender {
	
    [self dismissViewControllerAnimated:YES completion:^{
    
        id friendsObject = [sender tag] == 1001 ? (/*_chosenFriends.count == _friendsList.count ? @"All" : */_chosenFriends) : ([_selectionType isEqualToString:@"single"] ? nil : @[]);
		
		if ([_selectionType isEqualToString:@"single"]) {
			[self.delegate didChooseSingleFriend:friendsObject];
		} else {
			[self.delegate didChooseFriends:friendsObject];
		}
		
    }];
	
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([segue.identifier isEqualToString:@"ViewProfileFromList"]) {
		Profile *dvc = [segue destinationViewController];
		[dvc setUserInfo:[_friendsList objectAtIndex:[(NSIndexPath *)sender row]]];
	}
	
}

@end
