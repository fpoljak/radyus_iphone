//
//  EditProfile.m
//  Radyus
//
//  Created by Locastic MacbookPro on 11/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "EditProfile.h"
#import "UIImage+OrientationUpImage.h"
#import "UIImage+Trim.h"

@interface EditProfile () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, ApiServiceDelegate, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, AsyncImageLoadDelegate, TransparentDialogDelegate>

@end

@implementation EditProfile

- (void)viewDidLoad {
	
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;

	
	//load images
	//_images = [[NSMutableArray alloc] initWithCapacity:6];
	
	[_firstLastName setDelegate:self];
	[_userName setDelegate:self];
	[_birthday setDelegate:self];
	[_descriptionTV setDelegate:self];
	
	[_scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 876)];
	
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
	[_scrollView addGestureRecognizer:tap];
	
	_datePicker = [[UIDatePicker alloc] init];
	[_datePicker setDatePickerMode:UIDatePickerModeDate];
	[_datePicker addTarget:self action:@selector(dateValueChanged:) forControlEvents:UIControlEventValueChanged];
	
	_genderPicker = [[UIPickerView alloc] init];
	_genderPicker.delegate = self;
	
	[_gender setInputView:_genderPicker];
	
	NSJSONSerialization *userInfo = [[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"];
	_userInfo = [(NSMutableDictionary *)userInfo mutableCopy];
	
	NSString *first = [_userInfo valueForKey:@"nameFirst"];
	NSString *last = [_userInfo valueForKey:@"nameLast"];
	
	[_firstLastName setText:[NSString stringWithFormat:@"%@ %@", first, last]];
	[_userName setText:[_userInfo valueForKey:@"username"]];
	
	NSString *fullDate = [_userInfo valueForKey:@"birthday"];
	NSString *shortDate = [fullDate substringToIndex:MIN(10, fullDate.length)];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	
	NSDate *date;
	NSString *formattedDate = @"";
	
	if (![shortDate isEqualToString:@""]) {
		[dateFormatter setDateFormat:@"YYYY-MM-dd"];
		date = [dateFormatter dateFromString:shortDate];
		
		@try {
			[_datePicker setDate:date];
		}
		@catch (NSException *exception) {
			//
		}
		
		[dateFormatter setDateFormat:@"MM/dd/YYYY"];
		formattedDate = [dateFormatter stringFromDate:date];

	}
	
	[_birthday setText:formattedDate];
	[_birthday setInputView:_datePicker];
	
	[_descriptionTV setText:[_userInfo valueForKey:@"description"]];
	[_counterLabel setText:[NSString stringWithFormat:@"%lu/200", (unsigned long)_descriptionTV.text.length]];
	[_email setText:[_userInfo valueForKey:@"email"]];
	[_gender setText:[NSString stringWithFormat:@"%@", [[_userInfo valueForKey:@"gender"] intValue] == 2 ? @"Male" : @"Female"]];
	
	if ([[_userInfo valueForKey:@"gender"] intValue] == 2) {
		[_genderPicker selectRow:1 inComponent:0 animated:NO];
	}
	
	_endEditingButton = [[UIButton alloc] init];
	[_endEditingButton setBackgroundColor:[UIColor greenColor]];
	[_endEditingButton setTitle:@"Done" forState:UIControlStateNormal];
	[_endEditingButton addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
	
	_imagesArray = [NSArray arrayWithObjects:_profileImage, _galleryImage1, _galleryImage2, _galleryImage3, _galleryImage4, _galleryImage5, nil];
	
	int tag = 0;
	for (UIImageView *image in _imagesArray) {
		[image setClipsToBounds:YES];
		[image setTag:++tag];
		UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap:)];
		[image addGestureRecognizer:imageTap];
		UIPanGestureRecognizer *imagePan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(imagePan:)];
		[image addGestureRecognizer:imagePan];
		[image setUserInteractionEnabled:NO];
	}
	
	NSArray *userImageIds = (NSArray *)[_userInfo valueForKey:@"pictures"];

	for (int i = 0; i < userImageIds.count; i++) {
		
		UIImageView *imageView = [_imagesArray objectAtIndex:i];
		
		AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:[userImageIds objectAtIndex:i] imageView:imageView useActivityIndicator:YES];
		loader.delegate = self;
		//[imageView setUserInteractionEnabled:NO];
		[imageView setImage:nil];
		[loader loadImage];
		
	}
	
	[_editGalleryButton addTarget:self action:@selector(enterEditGalleryMode:) forControlEvents:UIControlEventTouchUpInside];
	
}

/* sprijeciti da gesture rcognizer prima vise od jednog toucha
 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
 // Allow gesture to handle touch only if not currently active
 if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
 return NO;
 }
 return YES;
 }
*/

-(void)enterEditGalleryMode:(id)sender {
	
	[_editGalleryButton removeTarget:self action:@selector(enterEditGalleryMode:) forControlEvents:UIControlEventTouchUpInside];
	[_editGalleryButton addTarget:self action:@selector(leaveEditGalleryMode:) forControlEvents:UIControlEventTouchUpInside];
	
	[_editGalleryButton setTitle:@"Done" forState:UIControlStateNormal];
	[_editGalleryButton setTitle:@"Done" forState:UIControlStateHighlighted];

	for (UIImageView *iw in _imagesArray) {
		[iw setUserInteractionEnabled:YES];
		if (iw.tag < 10) {
			[iw setImage:[UIImage imageNamed:@"profileAddImage.png"]];
		}
	}
	
}

-(void)leaveEditGalleryMode:(id)sender {
	
	[_editGalleryButton removeTarget:self action:@selector(leaveEditGalleryMode:) forControlEvents:UIControlEventTouchUpInside];
	[_editGalleryButton addTarget:self action:@selector(enterEditGalleryMode:) forControlEvents:UIControlEventTouchUpInside];
	
	[_editGalleryButton setTitle:@"Edit" forState:UIControlStateNormal];
	[_editGalleryButton setTitle:@"Edit" forState:UIControlStateHighlighted];
	
	for (UIImageView *iw in _imagesArray) {
		[iw setUserInteractionEnabled:NO];
		if (iw.tag < 10) {
			[iw setImage:nil];
		}
	}
	
}

-(void)errorLoadingImage:(UIImageView *)imageView { //content mode center
	[imageView setImage:[UIImage imageNamed:@"profileAddImage.png"]]; ////
	//[imageView setUserInteractionEnabled:YES];
	//NSLog(@"Error loading image");
}

-(void)loadSuccess:(UIImageView *)imageView {
	[imageView setContentMode: UIViewContentModeScaleAspectFill];
	imageView.tag += 10;
	//[imageView setUserInteractionEnabled:YES];
}

-(void)deleteImage:(UIImageView *)imageView {
	
	if (imageView.tag < 10) {
		return;
	}
	
	[imageView setImage:nil];
	[imageView setContentMode:UIViewContentModeCenter];
	imageView.tag -= imageView.tag > 100 ? 100 : 10;
	[imageView setImage:[UIImage imageNamed:@"profileAddImage.png"]];
	
}

-(void)viewDidAppear:(BOOL)animated {

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];

}

- (void)keyboardWillShow:(NSNotification *)aNotification {
	
	if ([_editingTextInput isEqual:_birthday] || [_editingTextInput isEqual:_gender]) {
		NSDictionary* info = [aNotification userInfo];
		CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
		if (kbSize.height == 0) {
			return;
		}
		
		double kbAnimationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
		UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
		NSInteger kbAnimationEffect = curve << 16;
		NSLog(@"Keyboard will show, size: %@, duration: %lf, animationEffect: %ld", NSStringFromCGSize(kbSize), kbAnimationDuration, (long)kbAnimationEffect);
		
		[_endEditingButton setFrame:CGRectMake(0, self.view.frame.size.height - 45, self.view.frame.size.width, 45)];
		[self.view addSubview:_endEditingButton];
		[self.view bringSubviewToFront:_endEditingButton];
		[UIView animateWithDuration:kbAnimationDuration + 0.025 delay:0.025f options:kbAnimationEffect animations:^{
			[_endEditingButton setFrame:CGRectMake(0, self.view.frame.size.height - kbSize.height - 45, self.view.frame.size.width, 45)];
		} completion:^(BOOL finished) {
			//
		}];
	}
	
}

-(void)doneEditing:(id)sender {
	
	if (_editingTextInput == _gender) {
		[_genderPicker removeFromSuperview];
		[_scrollView setContentOffset:CGPointMake(0, _scrollView.contentSize.height - _scrollView.frame.size.height) animated:YES];
	}
	
	[self hideKeyboard:sender];
	[_endEditingButton removeFromSuperview];

}


-(void)switchImagesFromImageview:(UIImageView *) imageView1 toImageView:(UIImageView *) imageView2 {
	enum UIViewContentMode temp = imageView1.contentMode;
	UIImage *image = imageView1.image;
	NSInteger tag = imageView1.tag;
	
	[imageView1 setContentMode:imageView2.contentMode];
	[imageView1 setImage:imageView2.image];
	[imageView1 setTag:imageView2.tag];
	
	[imageView2 setContentMode:temp];
	[imageView2 setImage:image];
	[imageView2 setTag:tag];
	
}

-(void)highlightImage:(UIImageView *)imageView {
	[imageView setAlpha:0.75f];
	_highlightedImageView = imageView;
}

-(void)unHighlightImage:(UIImageView *)imageVew {
	[imageVew setAlpha:1.0f];
	_highlightedImageView = nil;
}

-(void)imagePan:(UIPanGestureRecognizer *)sender {
	
	static UIImageView *pannedImage;
	static CGPoint startingLocation;
	CGPoint gestureLocation = [sender locationInView:_scrollView];
	
	if (sender.state == UIGestureRecognizerStateBegan) {
		
		startingLocation = gestureLocation;
		_currentImageView = (UIImageView *)sender.view;
		pannedImage = [[UIImageView alloc] initWithFrame:_currentImageView.frame];
		[pannedImage setClipsToBounds:YES];
		[pannedImage setContentMode:_currentImageView.contentMode];
		[pannedImage setImage:_currentImageView.image];
		[pannedImage setTag:_currentImageView.tag];
		
		if ([sender.view isEqual:_profileImage]) {
			pannedImage.frame = CGRectMake(gestureLocation.x - 27, gestureLocation.y - 27, 54, 54);
		}
		
		[_scrollView addSubview:pannedImage];
		
	} else if (sender.state == UIGestureRecognizerStateChanged) {
		
		[pannedImage setFrame:CGRectMake(gestureLocation.x - 27, gestureLocation.y - 27, 54, 54)];
		
		BOOL intersects = NO;
		for (UIImageView *iw in _imagesArray) {
			
			if (!iw.userInteractionEnabled) {
				continue;
			}
			
			if (CGRectIntersectsRect(iw.frame, pannedImage.frame)) {

				if (![iw isEqual:_currentImageView]) {
					intersects = YES;
					if (![iw isEqual:_highlightedImageView]) { //
						[self highlightImage:iw];
					}
					break;
				}

			}
			
		}
		
		if (!intersects && _highlightedImageView != nil) {
			[self unHighlightImage:_highlightedImageView];
		}
		
	} else if (sender.state == UIGestureRecognizerStateEnded || sender.state == UIGestureRecognizerStateFailed || sender.state == UIGestureRecognizerStateCancelled) {
		if (_highlightedImageView != nil) {
			if (sender.state == UIGestureRecognizerStateEnded) {
				[self switchImagesFromImageview:_currentImageView toImageView:_highlightedImageView];
			}
		}
		
		if (_highlightedImageView == nil || sender.state == UIGestureRecognizerStateFailed || sender.state == UIGestureRecognizerStateCancelled) {
			CGFloat xDist = (startingLocation.x - gestureLocation.x);
			CGFloat yDist = (startingLocation.y - gestureLocation.y);
			CGFloat distance = sqrt((xDist * xDist) + (yDist * yDist));
			
			[UIView animateWithDuration:0.0025 * distance animations:^{
				pannedImage.frame = _currentImageView.frame;
				pannedImage.contentMode = _currentImageView.contentMode;
			} completion:^(BOOL finished) {
				[pannedImage removeFromSuperview];
				pannedImage = nil;
			}];
		} else {
			[pannedImage removeFromSuperview];
			pannedImage = nil;

		}
		
		if (_highlightedImageView != nil) {
			[self unHighlightImage:_highlightedImageView];
		}
		
	}
	
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return row == 0 ? @"Female" : @"Male";
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return 2;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	[_gender setText:row == 1 ? @"Male" : @"Female" ];
}

- (void)dateValueChanged:(UIDatePicker *)sender{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MM/dd/yyyy"];
	_birthday.text = [dateFormatter stringFromDate:[sender date]];
}

-(void)hideKeyboard:(id)sender {
	[self.view endEditing:YES];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	if (textField.frame.origin.y > 240) {
		[_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y - 120) animated:YES];
	}
	
	_editingTextInput = textField;
	
	return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	if (textView.frame.origin.y > 240) {
		[_scrollView setContentOffset:CGPointMake(0, textView.frame.origin.y - 120) animated:YES];
	}
	
	_editingTextInput = textView;
	
	return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	
	NSCharacterSet *cs = [NSCharacterSet newlineCharacterSet];
						 
	if (text.length == 1 && [text stringByTrimmingCharactersInSet:cs].length == 0) {
		//if (![textView isFirstResponder] && ![textView canResignFirstResponder]) {
		//	[textView becomeFirstResponder];
		//}
		[textView resignFirstResponder];
		return NO;
	}
	
	if (range.length == 0 && (textView.text.length > 199)) {
		return NO;
	}
	
	return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView {
	//[_endEditingButton removeFromSuperview];
	[_gender becomeFirstResponder];
	return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
	NSLog(@"textFieldShouldEndEditing");
	[_endEditingButton removeFromSuperview];
	if ([textField isEqual:_birthday]) {
		[_descriptionTV becomeFirstResponder];
	}
	
	return YES;
}

/*
- (BOOL)disablesAutomaticKeyboardDismissal
{
	return NO;
}
*/

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
	
	if(textField.returnKeyType==UIReturnKeyNext) {
		UIView *next = [_scrollView viewWithTag:textField.tag+1];
		[next becomeFirstResponder];
	} else if (textField.returnKeyType==UIReturnKeyDone || textField.returnKeyType==UIReturnKeyDefault) {
		[textField resignFirstResponder];
	}
	return YES;

}

-(void)textViewDidChange:(UITextView *)textView {
	[_counterLabel setText:[NSString stringWithFormat:@"%lu/200", (unsigned long)textView.text.length]];
}

-(IBAction)dismiss:(id)sender {
	
	//if sender != nil alert discard changes?, if no -> return
	
	if (_editingTextInput == _gender) {
		[_genderPicker removeFromSuperview];
	}
	
	[self.view endEditing:YES];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	
	[_genderPicker removeFromSuperview];
	_gender.inputView = nil;
	_genderPicker = nil;
	
	[_datePicker removeFromSuperview];
	_birthday.inputView = nil;
	_birthday = nil;
	
	[self dismissViewControllerAnimated:YES completion:^{
		if (sender == nil && _profileViewController != nil) { //reload user
			[(Profile *)_profileViewController setUserInfo:[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"]];
			[(Profile *)_profileViewController displayUser];
		} else if (sender != nil) {
			[_apiService cancelRequests]; ////?
		}
	}];
	
}

-(IBAction)submitChanges:(id)sender {

	NSMutableArray *arr = [NSMutableArray arrayWithArray: [_firstLastName.text componentsSeparatedByString:@" "]];
	NSString *last = [arr lastObject];
	[arr removeLastObject];
	NSString *first = [arr componentsJoinedByString:@" "];
	
	[_userInfo setValue:first forKey:@"nameFirst"];
	[_userInfo setValue:last forKey:@"nameLast"];
	[_userInfo setValue:_birthday.text forKey:@"birthday"];
	[_userInfo setValue:_userName.text forKey:@"username"];
	[_userInfo setValue:_descriptionTV.text forKey:@"description"];
	[_userInfo setValue:[NSNumber numberWithInt:([_gender.text isEqualToString:@"Male"] ? 2 : 1)] forKey:@"gender"];
	
	NSArray *userImageIds = (NSArray *)[_userInfo valueForKey:@"pictures"];
	_editedImageIdArray = [[NSMutableArray alloc] init]; //property
	_imagesToUpload = [[NSMutableArray alloc] init]; //property
	_indexOfImageBeingUploaded = 0;
	
	for (UIImageView *iw in _imagesArray) {
		NSLog(@"imageView tag: %ld", (long)iw.tag);
		if (iw.tag > 100) {
			[_editedImageIdArray addObject:@""];
			[_imagesToUpload addObject:iw.image];
		} else if (iw.tag > 10) {
			[_editedImageIdArray addObject:[userImageIds objectAtIndex:iw.tag - 11]];
		}
		//else ignore
	}
	
	if (_imagesToUpload.count > 0) {
		[_apiService postNewImage:[_imagesToUpload firstObject]];
	} else {
		[_userInfo setValue:_editedImageIdArray forKey:@"pictures"];
		[_apiService postUserInfo:_userInfo];
	}

}

-(void)newPictureSuccess:(NSString *)pictureId {
	static int i = -1;
	while (![[_editedImageIdArray objectAtIndex:++i] isEqualToString:@""] && i < 6) {}
	[_editedImageIdArray replaceObjectAtIndex:i withObject:pictureId];
	
	if (++_indexOfImageBeingUploaded < _imagesToUpload.count) {
		[_apiService postNewImage:[_imagesToUpload objectAtIndex:_indexOfImageBeingUploaded]];
	} else {
		i = -1;
		[_userInfo setValue:_editedImageIdArray forKey:@"pictures"];
		NSLog(@"Final Pictures array: %@", [_editedImageIdArray componentsJoinedByString:@", "]);
		[_apiService postUserInfo:_userInfo];
	}
	
}

-(void)userInfoPostSuccess:(NSJSONSerialization *)response {
	
	[self dismiss:nil];
}

//update images

-(void)imageTap:(UITapGestureRecognizer *)sender {
	_currentImageView = (UIImageView *)sender.view;
	[self performSegueWithIdentifier:@"EditProfileAddPhoto" sender:sender];
}

-(BOOL)shouldDisableUserInteraction {
	return YES; ////?
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	UIImage *_selectedImage = [info valueForKey:UIImagePickerControllerEditedImage] ?: [info valueForKey:UIImagePickerControllerOriginalImage];
	
	/*
	CGFloat w = _selectedImage.size.width;
	CGFloat h = _selectedImage.size.height;
	CGFloat max_width = 828;
	CGFloat max_height = 1432;
	CGFloat totalAspect = 1;
	
	NSLog(@"width: %f, height: %f, max_width: %f, max_height: %f", w, h, max_width, max_height);
	
	if (w > max_width) {
		CGFloat aspect = w / max_width;
		w = max_width;
		h /= aspect;
		totalAspect *= aspect;
	}
	
	if (h > max_height) {
		CGFloat aspect = h / max_height;
		h = max_height;
		w /= aspect;
		totalAspect *= aspect;
	}
	
	CGFloat scale = (totalAspect == 0 || totalAspect < 1 ? 1 : totalAspect);
	NSLog(@"scale: %f", scale);
	*/
	
	UIImage *selectedImage = [[[_selectedImage orientationUpImage] imageByTrimmingTransparentPixels] imageByTrimmingBlackPixels];
	
	//NSLog(@"New dimensions: %fx%f", selectedImage.size.width, selectedImage.size.height);
	
	[picker dismissViewControllerAnimated:YES completion:^{
		[_currentImageView setContentMode: UIViewContentModeScaleAspectFill];
		if (_currentImageView.tag < 17) {
			_currentImageView.tag += _currentImageView.tag > 6 ? 90 : 100;
		}
		[_currentImageView setImage: selectedImage];

	}];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:@"EditProfileAddPhoto"]) {
		TransparentDialog *dvc = [segue destinationViewController];
		[dvc setParentVC:self];
		[dvc setDelegate:self];
		[dvc setHideConfirmButton:NO];
		[dvc setConfirmTitle:@"Delete photo"];
	}
	
}

-(void)confirmSelected {
	[self deleteImage:_currentImageView];
}

-(void)customDialogAction:(NSDictionary *)info {
	
	UIImage *selectedImage = [info valueForKey:@"image"];
	
	[_currentImageView setContentMode: UIViewContentModeScaleAspectFill];
	if (_currentImageView.tag < 17) {
		_currentImageView.tag += _currentImageView.tag > 6 ? 90 : 100;
	}
	
	[_currentImageView setImage: selectedImage];
	
}

@end
