//
//  CloudView.m
//  RadyUs
//
//  Created by Frane Poljak on 11/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "CloudView.h"
#import "UIImage+OrientationUpImage.h"
#import "UIImage+Trim.h"
#import "UIImageView+NoAvatar.h"
#import "UIAlertController+Window.h"
#import "ViewFriends.h"

@interface CloudView ()

@end

@implementation CloudView

@synthesize boundingMapRect;
@synthesize coordinate;
@synthesize loaded;
@synthesize radius;
@synthesize access;
@synthesize checkedForNewMessages;

static char cloudUserKey;
static char cloudCreatorKey;
static char hashtagKey;


BOOL shouldCenterMap;
BOOL hasOlderMessages;

NSInteger _currentZoom = -1;

CGFloat startingVelocity;

////////////
//For tutorial: https://github.com/JoeFryer/JDFTooltips
////////////

- (void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	NSString *dbName = [NSString stringWithFormat:@"%@.sqlt", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	_dbManager = [[DBManager alloc] initWithDatabaseFilename: dbName];
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
	_joinButton.enabled = NO;

	_messageUserLoading = NO;
	
	_kbAnimationDuration = 0.3f; //default
	
	loaded = NO;
	_firstLocationUpdate = YES;
	shouldCenterMap = YES;
	
	radius = [[[NSUserDefaults standardUserDefaults] objectForKey:@"RadiusForReadingClouds"] doubleValue] ?: 1000.0f;
	access = [[NSUserDefaults standardUserDefaults] objectForKey:@"AccessForReadingClouds"] ?: @"All";
	
	//_mapView.delegate = self;
	_mapView.showsUserLocation = YES; ////both

    if (_cloud) {
        [self initializeCloud];
    } else {
        // check cloudId!! // get in db!!??
        [_apiService getCloudsByIdList:[NSArray arrayWithObject:_cloudId]];
    }

	[_messageInputView.layer setCornerRadius:5.0f];
	[_messageInputPlaceholderView.layer setCornerRadius:5.0f];
		
	[_chatView.panGestureRecognizer requireGestureRecognizerToFail:self.menuSlideGesture];
		
	UISwipeGestureRecognizer *chatControlsSwipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(chatCtrlSwipeDown:)];
	[chatControlsSwipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
		
	[_chatControlsContainer addGestureRecognizer:chatControlsSwipeDown]; ////////izbacit?
	
	for (UIView *subview in _mapView.subviews)
		for (UIGestureRecognizer *gr in subview.gestureRecognizers)
			[gr requireGestureRecognizerToFail:self.menuSlideGesture];
	
}

-(void)apiService:(ApiService *)service receivedCloudList:(NSJSONSerialization *)cloudList {

    _cloud = [(NSArray *)cloudList firstObject];

    [self initializeCloud];
    [self viewDidAppear:NO];
    [self loadMessages];
}

-(void)initializeCloud {

    [self distanceUnitChanged];

    ////check if active....
    if (![[_cloud valueForKey:@"active"] boolValue]) {
        [self insertReviveCloudView];
    }

    [_joinView setAlpha:0.0f];

    if ([_cloud valueForKey:@"creator"] != nil) {
        _cloudCreatorService = [[ApiService alloc] init];
        _cloudCreatorService.delegate = self;
        [_cloudCreatorService getUsersByIdList:[NSArray arrayWithObject:[_cloud valueForKey:@"creator"]]];
    }

    //disable chat?

    _duration = 3600; // temp, will read from cloud info

    [self displayDuration];

    [self startTimer];

    hasOlderMessages = YES;
    checkedForNewMessages = NO;

    [(UIButton *)[_cloudOverviewContainer viewWithTag:20] setHidden:YES];

    BOOL anonymus = [[_cloud valueForKey:@"creatorAnon"] boolValue];

    if (anonymus) {

        [_cloudCreatorButton setHidden:YES];
        [_cloudCreatorButton setUserInteractionEnabled:NO];
        [self.navigationItem setTitleView:nil];
        self.title = @"Anonymus";
        self.previousTitle = self.title;
        self.navigationItem.title = self.title;
        
    }

    NSString *title = [_cloud valueForKey:@"name"];

    CGFloat fontSize = (title.length > 125) ? 15 : (title.length > 100 ? 16 : (title.length > 75 ? 17 : (title.length > 50 ? 18 : (title.length > 25 ? 19 : 20))));
    [_descriptionLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:fontSize]];

    [_descriptionLabel setText:title];
    [self highlightHashtags:_descriptionLabel]; //prominit u UITextView
    UITapGestureRecognizer *hashtagTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(titleTapped:)];
    [_descriptionLabel addGestureRecognizer:hashtagTap];

    //UIPanGestureRecognizer *pgr = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    //_chatView.counterLabel.userInteractionEnabled = YES;
    //[_chatView.counterLabel addGestureRecognizer:pgr]; //izbacit

    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chatViewDoubleTapped:)];
    [doubleTap setNumberOfTapsRequired:2];
    [_chatView addGestureRecognizer:doubleTap];

    UITapGestureRecognizer *mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMapTap:)];
    [mapTap setNumberOfTapsRequired:2];
    [mapTap setCancelsTouchesInView:YES]; //ne radi, use shouldrecognizesym....
    [mapTap setDelegate:self];
    [_mapView addGestureRecognizer:mapTap];
}

-(void)startTimer {

    _durationTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(decrementDuration)
                                                    userInfo:nil
                                                     repeats:YES];
}

-(void)stopTimer {

    if (!_durationTimer) {
        return;
    }

    [_durationTimer invalidate];
    _durationTimer = nil;
}

-(void)resetTimer {
    [self stopTimer];
    _duration = 3600; // temp, use _chatView.lastMessage time!
    [self displayDuration];
    [self startTimer];
}

-(void)displayDuration {

    int seconds = _duration % 60;
    int minutes = (int)((_duration - seconds) / 60);
    int hours = 0;

    if (minutes == 60) {
        minutes = 0;
        hours = 1;
    }


    NSString *durationString = [NSString stringWithFormat:@"%d:%02d:%02d", hours, minutes, seconds];

    [_durationLabel setText:durationString];
}

-(void)receivedMessage:(NSDictionary *)aNotification {

    NSDictionary *userInfo = [aNotification valueForKey:@"userInfo"];

    if ([userInfo valueForKey:@"cloud_id"] == nil || [(_cloud == nil ? _cloudId : ((NSString *)[_cloud valueForKey:@"id"])) isEqualToString:[userInfo valueForKey:@"cloud_id"]]) {

        [self checkForNewMessages];
    }
}


-(void)decrementDuration {

    _duration = MAX(0, _duration - 1);

    // if 0 ....

    [self displayDuration];
}

-(void)createReviveCloudView {
	
	_reviveCloudView = [[UIView alloc] initWithFrame:CGRectMake(0, _mapView.frame.origin.y, _mapView.frame.size.width, 64)];
	[_reviveCloudView setBackgroundColor:UIColorFromRGB(0x4A4A4A)];
	
	UILabel *bigLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 12, 225, 24)];
	[bigLabel setBackgroundColor:[UIColor clearColor]];
	[bigLabel setFont:[UIFont fontWithName:@"GothamRounded-Bold" size:21.0f]];
	[bigLabel setTextColor:[UIColor whiteColor]];
	[bigLabel setText:@"Revive this cloud!"];
	
	UILabel *smallLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 40, 225, 12)];
	[smallLabel setBackgroundColor:[UIColor clearColor]];
	[smallLabel setFont:[UIFont fontWithName:@"GothamRounded-Bold" size:10.0f]];
	[smallLabel setTextColor:UIColorFromRGB(0xA8A8A8)];
	[smallLabel setText:@"Click OK if you want to reactivate this cloud"];
	
	UIView *line = [[UIView alloc] initWithFrame:CGRectMake(_reviveCloudView.frame.size.width - 64, 14, 2, 36)];
	[line setBackgroundColor:UIColorFromRGB(0x656565)];
	
	UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(_reviveCloudView.frame.size.width - 60, 14, 52, 36)];
	[button setBackgroundColor:[UIColor clearColor]];
	[button setTitleColor:UIColorFromRGB(0xFFD203) forState:UIControlStateNormal];
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	[button.titleLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:16.0f]];
	[button setTitle:@"OK" forState:UIControlStateNormal];
	[button addTarget:self action:@selector(reviveCloud) forControlEvents:UIControlEventTouchUpInside];
	
	[_reviveCloudView addSubview:bigLabel];
	[_reviveCloudView addSubview:smallLabel];
	[_reviveCloudView addSubview:line];
	[_reviveCloudView addSubview:button];
}

-(void) insertReviveCloudView {
	
	return; //privremeno
	
	[_chatControlsContainer setUserInteractionEnabled:NO];
	[_chatView setUserInteractionEnabled:NO];
	
	[self expandMap:nil];
	
	if (_reviveCloudView == nil) {
		[self createReviveCloudView];
	}
	
	[_cloudOverviewContainer addSubview:_reviveCloudView];
	[_mapView setFrame:CGRectMake(0, _reviveCloudView.frame.origin.y + _reviveCloudView.frame.size.height, _mapView.frame.size.width, _mapView.frame.size.height - _reviveCloudView.frame.size.height)];
	
	_cloudViewState = CSCloudInactive; //onemogućit prikaz chatview-a
	
}

-(void) deactivateCloud {
	
	//apiservice deactivate
	[_dbManager setCloud:[_cloud valueForKey:@"id"] active:NO];
	//move to apiservice //u listi također!
	[self insertReviveCloudView];
	
}

-(void) reviveCloud {
	
	//revive in api
	[_dbManager setCloud:[_cloud valueForKey:@"id"] active:YES];
	//move to apiservice //u listi također!
	[self removeReviveCloudView];
	
}

-(void) removeReviveCloudView {
	
	[_reviveCloudView removeFromSuperview];
	[_mapView setFrame:CGRectMake(0, _mapView.frame.origin.y - _reviveCloudView.frame.size.height, _mapView.frame.size.width, _mapView.frame.size.height + _reviveCloudView.frame.size.height)];
	[self resetViewsToOriginalPosition:nil];
	[_chatView setUserInteractionEnabled:YES];
	[_chatControlsContainer setUserInteractionEnabled:YES];
	
}

/*
-(void) highlightButton:(UIButton *)sender {
	[sender setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
}

-(void) unhighlightButton:(UIButton *)sender {
	[sender setBackgroundColor:UIColorFromRGB(0xE7E7E7)];
}
*/

/*
-(void)sidemenuWillShow {
	[super sidemenuWillShow];
	//[self chatViewToFullScreen]; //try UIResponder+FirstResponder.h hack in the sidemenu
}
*/

-(void)sidemenuDidShow {
	[super sidemenuDidShow];
	[self chatViewToFullScreen];
}

-(void)sidemenuDidHide {
	
	[super sidemenuDidHide];
	[self chatViewToFullScreen];
	
}

-(void)displayAppMessage:(NSString *)message {
	
	NSDictionary *msg = [[NSDictionary alloc] initWithObjects: [NSArray arrayWithObject:message] forKeys: [NSArray arrayWithObject:@"text"]];
	
	if (_chatView.isInLoadPreviousMode) {
		[_chatView.messageBuffer addObject:msg];
	} else {
		[_chatView displayAppMessage:msg];
	}
	
}

-(void)chatCtrlSwipeDown:(id)sender { //cloud ////////izbacit
	if (_cloudViewState != CSStateOriginal) {
		[self chatViewToFullScreen];
	}
}

/*
 -(BOOL)userInCloud {
	NSLog(@"User id: %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]);
	return [(NSArray *)[_cloud valueForKey:@"users"] containsObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
 }
 */

-(IBAction)joinCloud:(id)sender {
	[_apiService joinCloud:[_cloud valueForKey:@"id"]];
}

-(void) joinCloudSuccess:(NSJSONSerialization *)response {
	
	[self removeJoinCloudView];
	[_menuButton setEnabled:YES];
	
	[_cloudUsersList addObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"]];
	
	[_chatControlsContainer setUserInteractionEnabled:YES];
	
	[self loadMore]; ////do we want to show previous?
	
}

-(void)leaveCloud{
	[_apiService leaveCloud:[_cloud valueForKey:@"id"]];
}

-(void)didLeaveCloud:(NSJSONSerialization *)response {
	
	//delete messages from db?
	NSLog(@"Left cloud, going to Explore screen...");
	UIViewController *explore = [self.storyboard instantiateViewControllerWithIdentifier:@"Explore"];
	
	for (UIViewController *vc in self.navigationController.viewControllers) { //remove all observers?
		
		if ([vc isKindOfClass:[UITabBarController class]]) {
			
			for (UIViewController *_vc in vc.childViewControllers) {
				
				[[NSNotificationCenter defaultCenter] removeObserver:_vc];
			
				if ([_vc isKindOfClass:[ExploreMap class]]) {
					[CLLocationManager cancelPreviousPerformRequestsWithTarget:_vc]; //nepotrebno?
				}
				
				[NSObject cancelPreviousPerformRequestsWithTarget:_vc];
				//[_vc viewWillDisappear:NO];
				
			}
			
		}
		
		[[NSNotificationCenter defaultCenter] removeObserver:vc];
		[NSObject cancelPreviousPerformRequestsWithTarget:vc];
		[vc viewWillDisappear:NO];
		
	}
	
	//[self.navigationController setViewControllers:[NSArray arrayWithObject:explore] animated:YES];
	[self.navigationController performSelector:@selector(setViewControllers:) withObject:[NSArray arrayWithObject:explore] afterDelay:0.3];
	
}

-(void)removeJoinCloudView {
	
	[UIView animateWithDuration:0.4f delay:0.1f options:UIViewAnimationOptionCurveEaseOut animations:^{
		_joinView.alpha = 0.0f;
	} completion:^(BOOL finished) {
		[_joinView removeFromSuperview];
		_joinView = nil;
	}];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMessage:) name:@"RadyusReceivedRemoteNotification_message" object:nil];
}

-(IBAction)showActionSheet:(id)sender{
	
	UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Cloud actions" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
	
	/* //test
	UIAlertAction *showAntorher = [UIAlertAction actionWithTitle:@"Show another" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[self showActionSheet:showAntorher];
	}];
	*/
	
	UIAlertAction *shareAction = [UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[self facebookShare];
	}];
	
	UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		//
	}];
	
	UIAlertAction *leaveAction = [UIAlertAction actionWithTitle:@"Leave cloud" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
		[self leaveCloud];
	}];
	
	UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
	
	//[actionSheet addAction:showAntorher];
	[actionSheet addAction:shareAction];
	[actionSheet addAction:reportAction];
	[actionSheet addAction:leaveAction];
	[actionSheet addAction:cancelAction];
	
	//[self presentViewController:actionSheet animated:YES completion:nil];
	[actionSheet show];
	
}

-(void)facebookShare {
	
	// TODO: check documentation for changes, create content (requires web page to be done), test
	FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
	content.contentURL = [NSURL URLWithString:@"rady.us"];
	//content.description = ....
	//content.imageURL = ....
	[FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
	
}

-(void)loadMessages {

    if (_cloud == nil) {
        return;
    }

	NSArray *messages = [_dbManager loadMessages:[_cloud valueForKey:@"id"]];
	
	NSLog(@"messages count: %ld", (long)messages.count);
	if (messages.count == 0) {
		[self loadMore];
		return;
	}
	
	[self loadMessagesSuccess:(NSJSONSerialization *)messages];
	
	if (messages.count > 30) { ////remove
		[_dbManager clearOldMessagesForCloud:[_cloud valueForKey:@"id"]];
		//if less look for older?
	}
	
}

-(void)loadMore {
	[_apiService loadMessagesInCloud:[_cloud valueForKey:@"id"] fromTime:@"" toTime:[_chatView.firstMessage.nextMessage.message valueForKey:@"time"] limit:20];
}

-(void)viewDidAppear:(BOOL)animated {
	
	NSLog(@"b viewDidAppear");
	
	[self distanceUnitChanged];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(distanceUnitChanged) name:@"RadyusDistanceUnitChangedNotification" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUserLocationChange:) name:@"UserLocationChanged" object:nil];
	
	[super viewDidAppear:animated];
	
	if (loaded) {
        if (checkedForNewMessages) {
            [self checkForNewMessages]; //// ????
        }
		return;
	}

	if (!_firstLocationUpdate) {
		//_firstLocationUpdate = YES;
		[_locationManager stopUpdatingLocation];
	}

    if (!_cloud) {
        return;
    }
	
	_pinHashMap = [[NSMutableDictionary alloc] init];
		
	NSArray *center = (NSArray *)[_cloud valueForKey:@"center"];
	if (center == nil || center.count < 2)
		center = [NSArray arrayWithObjects:[_cloud valueForKey:@"longitude"], [_cloud valueForKey:@"latitude"], nil];
	
	double lat = [[center objectAtIndex:1] doubleValue];
	double lng = [[center objectAtIndex:0] doubleValue];
		
	[self initializeMap:CLLocationCoordinate2DMake(lat, lng)];
		
	//[_apiService getUsersByIdList:(NSArray*)[_cloud valueForKey:@"users"]];
    [_apiService getCloudUsers:_cloud == nil ? _cloudId : ((NSString *)[_cloud valueForKey:@"id"])];
	
	loaded = YES; ////both
	
	//[self cancelAddPhotoClicked:nil];
		
	_chatView.delegate = self;
	CGSize size = _chatView.contentSize;
	size.width = _chatView.frame.size.width;
	[_chatView setContentSize:size];
	[_chatView setDelaysContentTouches:YES];
	[_chatView setCanCancelContentTouches:NO];
	_cloudViewState = CSStateOriginal;
	[_chatControlsContainer sendSubviewToBack:_messageInputPlaceholderView];
	[_messageInputView setBackgroundColor:[UIColor clearColor]];
	[_messageInputView setContentSize:CGSizeMake(1.0f, _messageInputView.contentSize.height)];
}

- (CGRect)frameOfTextRange:(NSRange)range inTextView:(UITextView *)textView {
	
	UITextPosition *beginning = textView.beginningOfDocument;
	UITextPosition *start = [textView positionFromPosition:beginning offset:range.location];
	UITextPosition *end = [textView positionFromPosition:start offset:range.length];
	UITextRange *textRange = [textView textRangeFromPosition:start toPosition:end];
	CGRect rect = [textView firstRectForRange:textRange];
	
	return [textView convertRect:rect fromView:textView.textInputView];
	
}

-(void) highlightHashtags:(UITextView *)textView {
	
	NSArray *hashtagRanges = [self rangesOfHashtagsInString:textView.text];
	
	for (NSTextCheckingResult *result in hashtagRanges) {
		
		//NSLog(@"hashtag range: %@", NSStringFromRange(result.range));
		CGRect frame = [self frameOfTextRange:result.range inTextView:textView];
		CALayer *layer = [[CALayer alloc] init];
		[layer setFrame:CGRectInset(CGRectOffset(frame, 0, -1), -0.5, 1)];
		[layer setZPosition:1.0f]; ////?
		[layer setBackgroundColor:UIColorFromRGB(0xCC9933).CGColor];
		[layer setOpacity:0.3f];
		[layer setCornerRadius:4.0f];
		
		NSString *hashtag = [textView.text substringWithRange:result.range];
		
		objc_setAssociatedObject(layer, &hashtagKey, hashtag, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		
		[textView.layer addSublayer:layer];
		
	}
	
}

-(NSArray *)rangesOfHashtagsInString:(NSString *)string {
	
	NSRegularExpression *hashtagDetector = [[NSRegularExpression alloc] initWithPattern: @"#\\w\\w*" options:NSRegularExpressionCaseInsensitive error:nil];
	
	NSArray *hashtagRanges = [hashtagDetector matchesInString:string options:NSMatchingWithoutAnchoringBounds range:NSMakeRange(0, string.length)];
	
	return hashtagRanges;
	
}

-(void)hashtagTapped:(NSString *)hashtag {
	NSLog(@"hashtag tapped: %@", hashtag);
	//TODO: hashtag search
}

-(void)titleTapped:(UITapGestureRecognizer *)sender {
	
	CGPoint tapLocation = [sender locationInView:sender.view];
	
	for (CALayer *layer in [[sender.view layer] sublayers]) {
		
		if (CGRectContainsPoint([layer frame], tapLocation)) {
			
			NSString *hashtag = objc_getAssociatedObject(layer, &hashtagKey);
			
			if (hashtag != nil) {
				[self hashtagTapped:hashtag];
				return;
			}
			
		}
		
	}
	
}

-(void)distanceUnitChanged {

    if (!_cloud) {
        return;
    }

	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];
	[_radiusLabel setText:[NSString stringWithFormat:@"%.0f %@", (float)((long)[[_cloud valueForKey:@"radius"] integerValue] * (isFeet ? 3.28084 : 1)), isFeet ? @"ft": @"m"]];
	
}

-(void)prepareForBackground {
	
	[super prepareForBackground];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusReceivedRemoteNotification_message" object:nil];
}

-(void)returnFromBackground {

    [super returnFromBackground];
    [self checkForNewMessages];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMessage:) name:@"RadyusReceivedRemoteNotification_message" object:nil];
}

// TODO: use common location mamager!

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
	
	if (status == kCLAuthorizationStatusDenied) {
		//handle denied
	}
	
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	
	if (error.code == kCLErrorDenied) {
		//handle denied
	}
	
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	
	static int tryCount = 0;
	
	CLLocation *location = [locations lastObject]; //parse all, look for best accuracy?
	
	if (_firstLocationUpdate) {
		
		if (location.horizontalAccuracy > 60 && ++tryCount < 3) {
			NSLog(@"Location accuracy: %lf, try count: %d", location.horizontalAccuracy, tryCount);
			return;
		}
		
		tryCount = 0;
		
		[manager stopUpdatingLocation];
		
		[_apiService hideLoadingView];
		
	} else {
		
		if (location.horizontalAccuracy > 100) { //////// find optimal value
			return;
		}
		
		if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive && location.speed > 1.0) { //1.0?
			
			NSLog(@"Speed: %f", location.speed);
			[_locationManager setDistanceFilter:10 + location.speed * 10];
			[_locationManager stopUpdatingLocation];
			[_locationManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay: 25 + location.speed / 2];
			////? odredit neki maksimum (i minimum)?
			//desired accuracy?
			
		}
		
	}
	
	[_mapView setCenterCoordinate:location.coordinate animated:YES];
	
	_userLocation = location.coordinate;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"UserLocationChanged" object:self userInfo:[NSDictionary dictionaryWithObject:location forKey:@"newLocation"]];
	
	if (_firstLocationUpdate) {
		[self initializeMap:location.coordinate];
		_firstLocationUpdate = NO;
	}
	
}

-(void)initializeMap:(CLLocationCoordinate2D)location {
	
	[_mapView setCenterCoordinate:location animated:YES];
	
	MKCircle *radiusCircle = [MKCircle circleWithCenterCoordinate:location radius:[[_cloud valueForKey:@"radius"] floatValue]];
	[_mapView addOverlay:radiusCircle];
	
	NSArray *center = (NSArray *)[_cloud valueForKey:@"center"];
	
	//when loaded from db coordinates are not an array, change?
	if (center == nil || center.count < 2)
		center = [NSArray arrayWithObjects:[_cloud valueForKey:@"longitude"], [_cloud valueForKey:@"latitude"], nil];
		
	double lat = [[center objectAtIndex:1] doubleValue];
	double lng = [[center objectAtIndex:0] doubleValue];
		
	MapPin *cloud = [[MapPin alloc] initWithCoordinates:CLLocationCoordinate2DMake(lat, lng)
													 id:_cloud == nil ? _cloudId : ((NSString *)[_cloud valueForKey:@"id"])
											  placeName:[_cloud valueForKey:@"name"]
											description:[_cloud valueForKey:@"name"]
											  isInRange:YES];
	
	[_mapView addAnnotation:cloud];
	
	[self setMapBoundsToRadiusWithCenterIn:location];
	
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	
	NSLog(@"Task failed, code: %d", errorCode);
	
	//if invalid refresh token go to login page
	//display message to user (your session has exipred....)
	//pribacit u apiservice
	if (errorCode == 401 /*???*/ /*|| errorCode == 500*/) {
		
		if ([FBSDKAccessToken currentAccessToken] != nil) {
			FBSDKLoginManager *fbLoginManager = [[FBSDKLoginManager alloc] init];
			[fbLoginManager logOut];
		}
		
		[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"accessToken"];
		
		UIViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstPage"];
		
		for (UIViewController *vc in self.navigationController.viewControllers) { //remove all observers?
			if ([vc isKindOfClass:[UITabBarController class]]) {
				for (UIViewController *_vc in vc.childViewControllers) {
					[[NSNotificationCenter defaultCenter] removeObserver:_vc];
					if ([_vc isKindOfClass:[ExploreMap class]]) {
						[CLLocationManager cancelPreviousPerformRequestsWithTarget:_vc]; //nepotrebno?
					}
					[NSObject cancelPreviousPerformRequestsWithTarget:_vc];
					//[_vc viewWillDisappear:NO];
				}
			}
			[[NSNotificationCenter defaultCenter] removeObserver:vc];
			[NSObject cancelPreviousPerformRequestsWithTarget:vc];
			[vc viewWillDisappear:NO];
		}
		
		[self.navigationController performSelector:@selector(setViewControllers:) withObject:[NSArray arrayWithObject: loginView] afterDelay:0.31];
		
	} else {
		
		if (_userInCloud) {
			[_chatControlsContainer setUserInteractionEnabled:YES]; ///////???
		}
		
	}
	
}

-(BOOL)shouldDisableUserInteraction {
	return NO;
}

-(void)sortMessages:(NSMutableArray *)messages {
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
	
	[messages sortUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
		
		NSDate *d1 = [dateFormatter dateFromString:obj1[@"time"]];
		NSDate *d2 = [dateFormatter dateFromString:obj2[@"time"]];
		
		return [d1 compare:d2];
		
	}];

}

-(void)loadMessagesSuccess:(NSJSONSerialization *)response {
	
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		
		NSMutableArray *messages = [NSMutableArray arrayWithArray:(NSArray *)response];
		
		[self sortMessages:messages];
		
		BOOL shouldEnterPreviousMode =
		(([_chatView.lastMessageDate isEqualToString:@""] && [_chatView.tempLastMessageDate isEqualToString:@"" ]) ||
		 (messages.count > 0 && [([_chatView.lastMessageDate isEqualToString:@""] ? _chatView.tempLastMessageDate : _chatView.lastMessageDate) compare:
								 [[messages firstObject] valueForKey:@"time"] options:NSNumericSearch] == NSOrderedDescending)); ////////provjerit!
		
		BOOL shouldLeaveLoadPreviousMode = YES;
		
		if (shouldEnterPreviousMode) {
			
			if (!_chatView.isInLoadPreviousMode) {
				NSLog(@"entering load previous mode...");
				[_chatView enterLoadPreviousMode];
			}
			
		} else {
			
			if (_chatView.isInLoadPreviousMode) {
				[_chatView.messageBuffer addObjectsFromArray:messages];
				return;
			}
			
		}
		
		NSString *userId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"];
		
		if (_chatView.isInLoadPreviousMode) {
			hasOlderMessages = [messages count] > 1;
		}
		
		BOOL messageSkipped = NO; ///////?????
		
		if (hasOlderMessages || !shouldEnterPreviousMode) { ////////check!
			
			for (NSJSONSerialization *message in messages) {
				
				NSString *firstMesssageTime = [_chatView.firstMessage.nextMessage.message valueForKey:@"time"];
				
				if (shouldEnterPreviousMode && [message isEqual:[messages lastObject]]
					&& ([[message valueForKey:@"id"] isEqualToString:[_chatView.lastMessage.message valueForKey:@"id"]]
					|| [[message valueForKey:@"time"] isEqualToString:firstMesssageTime])) { //temp, should be id, id must always be present!
					
					NSLog(@"Skipping last old message...");
					continue;
					
				} else if (!shouldEnterPreviousMode && [message isEqual:[messages firstObject]]
						   && ([[message valueForKey:@"id"] isEqualToString:[_chatView.lastMessage.message valueForKey:@"id"]]
						   || [[message valueForKey:@"time"] isEqualToString:[_chatView.lastMessage.message valueForKey:@"time"]])) { //temp
							   
					NSLog(@"Skipping first new message...");
					continue;
							   
				}

				/*consider best(fastest) option*/
				////vidit šta je brže http://stackoverflow.com/questions/21157109/which-is-has-faster-performance-indexesofobjectspassingtest-or-filteredarrayusin
				
				// probably slowest
				//NSPredicate *userFilter = [NSPredicate predicateWithFormat:@"id = %@", [message valueForKey:@"user"]];
				
				// faster
				//NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
				//	return [evaluatedObject[@"id"] isEqualToString:[message valueForKey:@"user"]];
				//}]; //upotrijebit i u ViewFriends kod filtriranja usera!
				
				// probably fastest
				NSInteger index = [_cloudUsersList indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
					return [obj[@"id"] isEqualToString:[message valueForKey:@"user"]] && (*stop = YES); //is stop necessary?
				}];
				
				// consider fast enumeration! (some tests show it's fastest)
				
				//NSJSONSerialization *user = [[_cloudUsersList filteredArrayUsingPredicate:userFilter] firstObject]; //filter
				NSJSONSerialization *user = index == NSNotFound ? nil : [_cloudUsersList objectAtIndex:index];
				
				//NSLog(@"message user: %@", user);
				if (user == nil) {
					
					user = (NSJSONSerialization *)[_dbManager loadUserWithId:[message valueForKey:@"user"]];
					
					if (user != nil) {
						[_cloudUsersList addObject:user];
					}
					
					/*
					NSArray *allUsers = [_dbManager loadUsers]; ////loadat samo jedan put, kad se loada novi dodat, NSMutableArray
					NSUInteger userIndex = [[allUsers valueForKey:@"id"] indexOfObjectIdenticalTo:[message valueForKey:@"user"]];
					
					if (userIndex != NSNotFound) {
						user = (NSJSONSerialization *)[allUsers objectAtIndex:userIndex];
						[_cloudUsersList addObject:user];
					}
					*/
					
				}
				
				if (user != nil) {
					// TODO: if loaded from database don't save!
					[_dbManager saveMessage:message];
				}
				
				NSDictionary *fullMessage = [self createMessage:message fromUser:user];
				
				//BOOL firstMessage = _chatView.lastMessage == nil;
				
				if ([[message valueForKey:@"user"] isEqualToString:userId]) {
					[_chatView displayUserMessage:fullMessage];
				} else {
					[_chatView displayMessage:fullMessage];
				}

                [self resetTimer]; ///???
			}
			
		}
		
		if (_chatView.isInLoadPreviousMode) {
			
			if (shouldLeaveLoadPreviousMode) {
				
				NSLog(@"Leaving load previous mode...");
				[_chatView leaveLoadPreviousMode];
				
				if (!checkedForNewMessages) {
					//only when done with first load!!!
					checkedForNewMessages = YES;
					[[NSOperationQueue mainQueue] addOperationWithBlock:^{
						[self checkForNewMessages];
					}];
				}
				
			}
			
		} else {
			
			if (messages.count > (messageSkipped ? 1 : 0)) {

                BOOL shouldScrollToBottom = (_chatView.contentSize.height - _chatView.contentOffset.y < _chatView.frame.size.height * 1.6);
                // Maknit?
                if (shouldScrollToBottom) {
                    [_chatView setContentOffset:CGPointMake(0, MAX(0, _chatView.contentSize.height - _chatView.frame.size.height)) animated:YES];
                }
			}
			
		}
		
	}];
	
}

-(void)checkForNewMessages {

    if (_cloud == nil) {
        return;
    }
	
	if (_chatView.lastMessageDate == nil || [_chatView.lastMessageDate isEqualToString:@""]) {
		//no need for checking if no messages were in local db, it will load anyway!
		return;
	}
	
	NSLog(@"Checking for new messages, lastMessageDate = %@", _chatView.lastMessageDate); //////?????????
	
	// TODO: should always work with [_chatView.lastMessage.message valueForKey:@"time"]
	[_apiService loadMessagesInCloud:[_cloud valueForKey:@"id"] fromTime:[_chatView lastMessageDate] toTime:nil limit:0];
	
}

-(void)postMessageSuccess:(NSJSONSerialization *)response {
	
	NSLog(@"successfully posted message: %@", (NSDictionary *)response);
	
	[_chatView postMessage:_messageInputView withTime:[response valueForKey:@"time"]]; ////response
	[self resizeInputView:_messageInputView];
	[_messageInputView setBackgroundColor:[UIColor clearColor]];
	[_dbManager saveMessage:response];
	[_sendButton setUserInteractionEnabled:YES];

    [self resetTimer];
}

-(void)postMessageFailed {
	
	[_sendButton setUserInteractionEnabled:YES];
	
	NSString *message = @"Message not sent! Check your connection and try again.";
	
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
	
	[alert addAction:close];
	
	[self presentViewController:alert animated:YES completion:nil];
	
}

-(void)displaySingleCloud:(NSDictionary *)cloudObj {
	
	double lat = [[(NSArray *)[cloudObj valueForKey:@"center"] objectAtIndex:1] doubleValue];
	double lng = [[(NSArray *)[cloudObj valueForKey:@"center"] objectAtIndex:0] doubleValue];
	MapPin *cloud = [[MapPin alloc] initWithCoordinates:CLLocationCoordinate2DMake(lat, lng) id:[cloudObj valueForKey:@"id"] placeName:[cloudObj valueForKey:@"name"] description:[cloudObj valueForKey:@"name"] isInRange:[[cloudObj valueForKey:@"inRange"] boolValue]];
	[_pinHashMap setValue:cloudObj forKey:[cloudObj valueForKey:@"id"]];
	[_mapView addAnnotation:cloud];
	
}

-(void)displayCloudList:(NSJSONSerialization *)cloudList { //in cloudList
	
	for (NSDictionary *cloudObj in [cloudList valueForKey:@"data"]) {
		[self displaySingleCloud:cloudObj];
	}
	
	self.clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:[_mapView annotations]]; //clusteringManager
	
}

-(void)apiService:(ApiService *)service receivedUserIdList:(NSArray *)userIdList {
	
	if ([service isEqual:_apiService]) {
		
		NSString *myId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"];
		
		if ([userIdList containsObject:myId]) {
			
			_userInCloud = YES;
			[_joinView removeFromSuperview];
			[_chatControlsContainer setUserInteractionEnabled:YES];

            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMessage:) name:@"RadyusReceivedRemoteNotification_message" object:nil];
			
		} else {
			
			_userInCloud = NO;
			[_joinView setAlpha:1.0f];
			[_menuButton setEnabled:NO];
			
		}
		
	}
	
	[service getUsersByIdList:userIdList];
	
}

-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {
	
	if ([service isEqual:_apiService]) {
		
		_cloudUsersList = [NSMutableArray arrayWithArray:(NSArray *)usersList];
		
		if (_userInCloud) {
			[self loadMessages];
		} else {
			_joinButton.enabled = YES;
		}
		
		[self displayCloudUsers];
		
	} else if ([service isEqual:_cloudCreatorService]) {
		
		NSJSONSerialization *creator = [(NSArray *)usersList firstObject];
		
		if (creator == nil) {//creator deleted profile
			
			//NSLog(@"creator deleted profile!");
			[_cloudCreatorButton setHidden:YES];
			[_cloudCreatorButton setUserInteractionEnabled:NO];
			[self.navigationItem setTitleView:nil];
			self.title = @"Anonymus";
			self.previousTitle = self.title;
			self.navigationItem.title = self.title;
			return;
			
		}
		
		UIImageView *creatorAvatar = _cloudCreatorButton.imageView;
		[creatorAvatar.layer setCornerRadius:17.0f];
		[creatorAvatar setClipsToBounds:YES];
		[creatorAvatar setContentMode:UIViewContentModeCenter];
		
		AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:[(NSArray *)[creator valueForKey:@"pictures"] firstObject] imageView:creatorAvatar useActivityIndicator:NO];
		loader.delegate = self;
		
		[_cloudCreatorButton setHidden:NO];
		
		NSString *creatorName = @" ";
		creatorName = [creatorName stringByAppendingString:[creator valueForKey:@"nameFirst"]];
		
		if (![[creator valueForKey:@"nameLast"] isEqualToString:@""]) {
			creatorName = [[[creatorName stringByAppendingString:@" "] stringByAppendingString:[[creator valueForKey:@"nameLast"] substringToIndex:1]] stringByAppendingString:@"."];
		}
		
		[_cloudCreatorButton setTitle:creatorName forState:UIControlStateNormal];
		objc_setAssociatedObject(_cloudCreatorButton, &cloudCreatorKey, creator, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		
		[loader loadImage];
		
	} 
	
}

-(void)loadSuccess:(UIImageView *)imageView {
	
	if ([imageView isEqual:_cloudCreatorButton.imageView]) {
		
		UIGraphicsBeginImageContext(CGSizeMake(34, 34)); /////privremeno
		
		[_cloudCreatorButton.imageView.image drawInRect:CGRectMake(0, 0, 34, 34)];
		
		UIImage *avatar = UIGraphicsGetImageFromCurrentImageContext();
		
		UIGraphicsEndImageContext();
		
		[_cloudCreatorButton setImage:avatar forState:UIControlStateNormal];
	}
	
}

-(void)displayCloudUsers {
	
	for (int i = 0; i < 4 && i < _cloudUsersList.count; i++) {
		
		NSJSONSerialization *user = [_cloudUsersList objectAtIndex:i];
		
		UIImageView *avatar = (UIImageView *)[_cloudOverviewContainer viewWithTag:i + 11];
		[avatar.layer setCornerRadius:17.0f];
		[avatar setClipsToBounds:YES];
		objc_setAssociatedObject(avatar, &cloudUserKey, user, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		
		NSArray *pictures = (NSArray *)[user valueForKey:@"pictures"];
		
		if ([pictures count] == 0) {
			[avatar setDefaultImageWithFirstName:[user valueForKey:@"nameFirst"] lastName:[user valueForKey:@"nameLast"]];
		} else {
			[avatar setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
			AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:[pictures firstObject] imageView:avatar useActivityIndicator:NO];
			loader.delegate = self;
			[loader loadImage];
		}
		
		UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewCloudUserProfile:)];
		
		[avatar setUserInteractionEnabled:YES];
		[avatar addGestureRecognizer:avatarTap];
		
	}
	
	if (_cloudUsersList.count > 4) {
		UIButton *more = (UIButton *)[_cloudOverviewContainer viewWithTag:20];
		[more setHidden:NO];
		[more setTitle:[NSString stringWithFormat:@"+%ld", (long)_cloudUsersList.count - 4] forState:UIControlStateNormal];
		//[more addTarget:self action:@selector(viewCloudUsers:) forControlEvents:UIControlEventTouchUpInside];
	}
	
}

-(void)viewCloudUserProfile:(UITapGestureRecognizer *)sender {
	UIImageView *avatar = (UIImageView *)sender.view;
	NSJSONSerialization *user = objc_getAssociatedObject(avatar, &cloudUserKey);
	[self performSegueWithIdentifier:@"ViewCloudUserProfile" sender:user];
}

/*
 -(void)viewCloudUsers:(id)sender {
	[self performSegueWithIdentifier:@"ViewCloudUsers" sender:sender];
 }
 */

-(NSInteger) zoomScaleToZoomLevel:(MKZoomScale) scale {
	
	double totalTilesAtMaxZoom = MKMapSizeWorld.width / 256.0;
	NSInteger zoomLevelAtMaxZoom = log2(totalTilesAtMaxZoom);
	NSInteger zoomLevel = MAX(0, zoomLevelAtMaxZoom + floor(log2f(scale) + 0.5));
	
	return zoomLevel;
	
}


-(CLLocationDistance)distanceBetweenCoordinate:(CLLocationCoordinate2D)originCoordinate andCoordinate:(CLLocationCoordinate2D)destinationCoordinate {
	
	CLLocation *originLocation = [[CLLocation alloc] initWithLatitude:originCoordinate.latitude longitude:originCoordinate.longitude];
	CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:destinationCoordinate.latitude longitude:destinationCoordinate.longitude];
	CLLocationDistance distance = [originLocation distanceFromLocation:destinationLocation];
	
	return distance;
	
}


-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
	
	if ([overlay isKindOfClass:[MKCircle class]]) {
		
		MKCircleRenderer *radiusView = [[MKCircleRenderer alloc] initWithCircle:overlay];
		radiusView.fillColor = [UIColor colorWithRed:1.0f green:210.0f/255.0f blue:3.0f/255.0f alpha:75.0f/255.0f];
		
		return radiusView;
		
	} else return nil;
	
}

-(void) setMapBoundsToRadiusWithCenterIn:(CLLocationCoordinate2D)location {
	
	CGFloat factor = 2.1;
	CGFloat _radius = [[_cloud valueForKey:@"radius"] floatValue];
	CGFloat fractionLatLon = _mapView.region.span.latitudeDelta / _mapView.region.span.longitudeDelta;
	CGFloat latDistance = (fractionLatLon > 1) ? _radius * factor * fractionLatLon : _radius * factor;
	CGFloat lonDistance = (fractionLatLon > 1) ? _radius * factor : _radius * factor * fractionLatLon;
	
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, latDistance, lonDistance);
	
	[_mapView setRegion:region animated:YES];
	
}

-(UIImage *)imageFromText:(NSString *)text
{
	
	UIFont *font = [UIFont fontWithName:@"GothamRounded-Bold" size:20.0f]; //size as parameter?
	
	NSDictionary *attr = @{NSFontAttributeName:font, NSForegroundColorAttributeName: UIColorFromRGB(0x4A4A4A)};
	CGSize size  = [text sizeWithAttributes:attr];
	
	UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
	
	// optional: add a shadow, to avoid clipping the shadow you should make the context size bigger
	//
	// CGContextRef ctx = UIGraphicsGetCurrentContext();
	// CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [[UIColor grayColor] CGColor]);
	
	[text drawAtPoint:CGPointMake(0.0, 0.0) withAttributes:attr];
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
	
	if ([annotation isKindOfClass:[MKUserLocation class]]) {
		MKAnnotationView *ann = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"RadyUsUserLocationAnnotation"];
		
		if (ann) {
			return ann;
		}
		
		ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"RadyUsUserLocationAnnotation"];
		ann.enabled = YES;
		ann.draggable = NO;
		ann.canShowCallout = NO;
		
		CGSize pinSize = CGSizeMake(33, 43);
		
		UIGraphicsBeginImageContext(pinSize);
		
		UIImage *cloud = [UIImage imageNamed:@"user_location_pin.png"];
		UIImage *icon = [UIImage imageNamed:@"radyus_icon.png"];
		
		[icon drawInRect:CGRectMake(3, 8, 26, 26)];
		[cloud drawInRect:CGRectMake(0, 0, 33, 43)];
		
		UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
		
		UIGraphicsEndImageContext();
		
		[ann setImage: combined];

		ann.layer.anchorPoint = CGPointMake(0.5f, 1.0f);
		
		return ann;
	}
	
	if ([annotation isKindOfClass:[MapPin class]]) {
		
		MKAnnotationView *ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[(MapPin *)annotation unique_id]];
		ann.enabled = YES;
		ann.draggable = NO;
		ann.canShowCallout = NO;
		
		BOOL isInRange = [(MapPin *)annotation isInReach];
		
		UIImage *cloud = (isInRange) ? [UIImage imageNamed:@"map_cloud.png"] : [UIImage imageNamed:@"map_cloud_grey.png"];
		
		CGSize cloudSize = cloud.size;
		
		UIGraphicsBeginImageContext(cloudSize);
		
		//BOOL isInRange = [self distanceBetweenCoordinate:[annotation coordinate] andCoordinate:_userLocation] < radius;
		
		UIImage *icon = [UIImage imageNamed:@"icon_grill.png"];
		[cloud drawInRect:CGRectMake(0, 0, cloudSize.width, cloudSize.height)];
		[icon drawInRect:CGRectMake((cloudSize.width - icon.size.width) / 2,
									(cloudSize.height - icon.size.height) / 2,
									icon.size.width, icon.size.height)];
		
		UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
		
		UIGraphicsEndImageContext();
		
		[ann setImage: combined];
		ann.layer.anchorPoint = CGPointMake(0.5f, 0.5f);
		
		return  ann;
		
	}
	
	return nil;
}

-(void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
	
	[super presentViewController:viewControllerToPresent animated:flag completion:completion];
	
	if ([_messageInputView isFirstResponder]) [self chatViewToFullScreen];

}

/////////izbacit
-(void)testFriendMessage {  //receivedMessage:message
	
	if (self == nil) {
		return;
	}
	
	NSArray *_keys = @[@"user",    ////uskladit key-eve
					   @"username",
					   @"userImage",
					   @"distance",
					   @"time",
					   @"text"]; //date, time, save messages to local database?
	
	NSInteger randomNumber = arc4random() % 5;
	
	NSMutableArray *keys = [NSMutableArray arrayWithArray:_keys];
	if (randomNumber == 3) {
		[keys addObject:@"imageToDisplay"];
	}
	
	NSString *dateString = @"";
	
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	[df setDateFormat:@"MM/dd/YYYY h:mm:ss"];
	dateString = [df stringFromDate:[NSDate date]];
	
	NSArray *objects = [NSArray arrayWithObjects: @"ABCD1235",
						@"Ana",
						[UIImage imageNamed:@"avatar.png"],
						[NSNumber numberWithFloat:187.462f],
						dateString,
						@"Some random text blabla some veryveryveryveryveryveryveryveryveryveryvervyer long message blablablablablabalbla\nwww.index.hr",
						(randomNumber == 3) ? [UIImage imageNamed:@"test_profile_picture.png"] : nil,
						nil];
	
	NSDictionary *message = [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
	
	if (_chatView.isInLoadPreviousMode) {
		[_chatView.messageBuffer addObject:message];
	} else {
		[_chatView displayMessage:message];
	}
	
	//(if new message _dbManager savemessage...)
	
	///////
	/*
	 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	 [dateFormatter setDateStyle:NSDateFormatterShortStyle];
	 [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	 [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	 NSString *currentTime = [dateFormatter stringFromDate:[NSDate date]];
	 NSLog(@"Current time: %@", currentTime);
	 */
	//http://stackoverflow.com/questions/566265/retrieving-current-local-time-on-iphone
	//http://stackoverflow.com/questions/15615637/how-to-get-local-time-on-ios?lq=1
	///////
	
	/* //play simple sound sample code, //#import <AudioToolbox/AudioToolbox.h>
	 http://stackoverflow.com/questions/4724980/making-the-iphone-vibrate
	 
	 NSString *path  = [[NSBundle mainBundle] pathForResource:@"soundeffect" ofType:@"m4a"];
	 NSURL *pathURL = [NSURL fileURLWithPath : path];
	 
	 SystemSoundID audioEffect;
	 AudioServicesCreateSystemSoundID((CFURLRef) pathURL, &audioEffect);
	 AudioServicesPlaySystemSound(audioEffect);
	 
	 // call the following function when the sound is no longer used
	 // (must be done AFTER the sound is done playing)
	 AudioServicesDisposeSystemSoundID(audioEffect);
	 */ //http://stackoverflow.com/questions/10329291/play-a-short-sound-in-ios
	
	
	//NSString *path  = [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"] pathForResource:@"sms-received4" ofType:@"caf"];
	//NSURL *pathURL = [NSURL fileURLWithPath : path];
	
	//SystemSoundID audioEffect;
	//AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
	//AudioServicesPlaySystemSound(audioEffect);
	//static int soundID = 1020;
	//AudioServicesPlaySystemSound(soundID++);
	//AudioServicesDisposeSystemSoundID(audioEffect);
}

-(NSDictionary *)createMessage:(NSJSONSerialization *)message fromUser:(NSJSONSerialization *)user {
	
	NSArray *_keys = @[@"id",
					   @"cloud",
					   @"user",
					   @"nameFirst",
					   @"nameLast",
					   @"userImage",
					   //@"distance",
					   @"time",
					   @"text",
					   @"shouldLoadUser",
					   @"hashtags",
					   @"mentions"];
	
	NSMutableArray *keys = [NSMutableArray arrayWithArray:_keys];
	
	///NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
	///[formatter setDateFormat:@"MM/dd/yy HH:mm:ss a"];  //calculate timezone (GMT, localTimezone)
	///NSDate *date = [formatter dateFromString:[message valueForKey:@"dateTime"] ?: [NSDate date]];
	
	
	//double lat = [[[user valueForKey:@"center"] lastObject] doubleValue] ?: _userLocation.latitude;
	//double lng = [[[user valueForKey:@"center"] firstObject] doubleValue] ?: _userLocation.longitude;
	
	//double distance = [self distanceBetweenCoordinate:_userLocation andCoordinate:CLLocationCoordinate2DMake(lat, lng)];
	
	NSLog(@"user: %@", user);
	NSLog(@"message: %@", message);
	NSArray *objects = [NSArray arrayWithObjects:
						[message valueForKey:@"id"] ?: @"",
						[_cloud valueForKey:@"id"],
						[message valueForKey:@"user"],
						[user valueForKey:@"nameFirst"] ?: @"",
						[user valueForKey:@"nameLast"] ?: @"",
						[[user valueForKey:@"pictures"] firstObject] ?: @"",
						//[NSNumber numberWithDouble:distance],
						[message valueForKey:@"time"],  //format
						[message valueForKey:@"text"],
						(user == nil ? @"yes" : @"no"),
						([message valueForKey:@"hashtags"] ?: [NSArray new]),
						([message valueForKey:@"mentions"] ?: [NSArray new]),
						[message valueForKey:@"picture"],
						nil];
	
	if ([message valueForKey:@"picture"] != nil) {
		[keys addObject:@"image_id"];
	}
	
	return [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
	
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	UIImage *_selectedImage = [info valueForKey:UIImagePickerControllerEditedImage] ?: [info valueForKey:UIImagePickerControllerOriginalImage];
	
	/*
	 CGFloat w = _selectedImage.size.width;
	 CGFloat h = _selectedImage.size.height;
	 CGFloat max_width = 828;
	 CGFloat max_height = 1432;
	 CGFloat totalAspect = 1;
	 
	 NSLog(@"width: %f, height: %f, max_width: %f, max_height: %f", w, h, max_width, max_height);
	 
	 if (w > max_width) {
		CGFloat aspect = w / max_width;
		w = max_width;
		h /= aspect;
		totalAspect *= aspect;
	 }
	 
	 if (h > max_height) {
		CGFloat aspect = h / max_height;
		h = max_height;
		w /= aspect;
		totalAspect *= aspect;
	 }
	 
	 CGFloat scale = (totalAspect == 0 || totalAspect < 1 ? 1 : totalAspect);
	 NSLog(@"scale: %f", scale);
	 */
	
	UIImage *selectedImage = [[[_selectedImage orientationUpImage] imageByTrimmingTransparentPixels] imageByTrimmingBlackPixels];
	
	//NSLog(@"New dimensions: %fx%f", selectedImage.size.width, selectedImage.size.height);
	
	_chatView.imageToDisplay = selectedImage;
	[picker dismissViewControllerAnimated:YES completion:nil];
	[self cancelAddPhotoClicked:nil];
	[_chatView addThumbnail:_messageInputView];
	[self resizeInputView:_messageInputView];
	
}////chatview

-(IBAction)postMessage:(id)sender {
	
	if (_messageInputView.text.length == 0 && _chatView.imageToDisplay == nil) {
		return;
	}
	
	[_sendButton setUserInteractionEnabled:NO];
	
	if (_chatView.imageToDisplay != nil) {
		// TODO: new instance of service for upload images?
		_apiService.trackProgress = YES;
		[_apiService postNewImage:_chatView.imageToDisplay];
		return;
	}
	
	[_apiService postMessage:_messageInputView.text toCloud:[_cloud valueForKey:@"id"] withImage:nil];
	
}

-(void)apiService:(ApiService *)service uploadProgress:(CGFloat)progress {
	if ([service isEqual:_apiService]) { //upload service!
		[_chatView updateUploadProgress:progress];
	}
}

// TODO: new instance of apiService for receiving new messages (temp, until sockets)
-(void)newPictureSuccess:(NSString *)pictureId {
	_apiService.trackProgress = NO;
	[_apiService postMessage:_messageInputView.text toCloud:[_cloud valueForKey:@"id"] withImage:pictureId];
}

-(void)handleMapTap:(UITapGestureRecognizer *)sender {
	if (_cloudViewState == CSStateOriginal) {
		[self expandMap:sender];
	} else {
		[self resetViewsToOriginalPosition:sender];
	}
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	//NSLog(@"gestureRecognizer view class: %@", NSStringFromClass(gestureRecognizer.view.class));
	
	if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]] && [otherGestureRecognizer.view isKindOfClass:[MKMapView class]]) {
		return NO;
	}
	
	return YES;
}

-(void)expandChatView:(id)sender {
	
	[self.view bringSubviewToFront:_chatView];
	[self.view bringSubviewToFront:_chatControlsContainer];
	[self.view sendSubviewToBack:_cloudOverviewContainer];
	
	CGFloat duration = _kbAnimationDuration > 0 ? _kbAnimationDuration : 0.4;
	
	//_cloudViewState = CSStateResizing;
	
	if (sender == nil) {
		duration = 0.1;
	} else if ([sender isKindOfClass:[UIPanGestureRecognizer class]]) {
		duration *= fabs([sender translationInView:self.view].y) / _cloudOverviewContainer.frame.size.height;
	}
	
	BOOL messageButttonShown = NO;
	
	if (_chatView.messageButton != nil) {
		messageButttonShown = YES;
		[_chatView.messageButton removeFromSuperview];
		_chatView.messageButton = nil;
	}
	
	CGRect chatControlsFrame = CGRectMake(0, self.view.frame.size.height - _kbSize.height - _chatControlsContainer.frame.size.height, _chatView.frame.size.width,
										  _chatControlsContainer.frame.size.height);
	
	BOOL shouldAdjustContentOffset = _chatView.contentOffset.y > _chatView.contentSize.height - _chatView.frame.size.height - 80; // or something
	
	[UIView animateWithDuration:duration delay:0.0 options:_kbAnimationEffect animations:^{
		
		[_chatView.counterLabel setFrame:CGRectMake(0, _cloudOverviewContainer.frame.origin.y, self.view.frame.size.width, _chatView.counterLabel.frame.size.height)];
		
		[_chatView setFrame:CGRectMake(0, _cloudOverviewContainer.frame.origin.y + _chatView.counterLabel.frame.size.height, _chatView.frame.size.width,
									   self.view.frame.size.height - chatControlsFrame.size.height - _chatView.counterLabel.frame.size.height - _cloudOverviewContainer.frame.origin.y - _kbSize.height)];
		
		[_chatControlsContainer setFrame:chatControlsFrame];
		
		if (shouldAdjustContentOffset) {
			[_chatView setContentOffset:CGPointMake(0, MIN(MAX(0, _chatView.contentOffset.y + _kbSize.height), _chatView.contentSize.height - _chatView.frame.size.height))];
		}
		
	} completion:^(BOOL finished) {
		_cloudViewState = CSChatExpanded;
		[self replaceBackButton];
		if (messageButttonShown) {
			[_chatView showMessageButton];
		}
	}];
}

-(void)replaceBackButton {
	
	UINavigationItem *navItem = (self.tabBarController == nil) ? self.navigationItem : self.tabBarController.navigationItem;
	
	navItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowdown.png"] style:UIBarButtonItemStylePlain target:self action:@selector(resetViewsToOriginalPosition:)];
	[navItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
	[navItem setHidesBackButton:YES animated:YES];
	
}

-(void)resetBackButton {
	UINavigationItem *navItem = (self.tabBarController == nil) ? self.navigationItem : self.tabBarController.navigationItem;
	
	navItem.leftBarButtonItem = nil;
	[navItem setHidesBackButton:NO animated:YES];
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
	
	NSDictionary* info = [aNotification userInfo];
	_kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	_kbAnimationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	_kbAnimationEffect = curve << 16;
	NSLog(@"Keyboard will show, size: %@", NSStringFromCGSize(_kbSize));
	[self expandChatView:_chatView];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(void)keyboardWillChangeFrame:(NSNotification *)aNotification {
	NSDictionary* info = [aNotification userInfo];
	_kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	_kbAnimationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	NSLog(@"Keyboard will change frame, size: %@", NSStringFromCGSize(_kbSize));
	[self expandChatView:nil];
}

-(void)expandMap:(id)sender {
	
	if (sender != nil && _cloudViewState == CSCloudInactive) {
		return;
	}
	
	[_cloudOverviewContainer.layer setZPosition:101.0f];
	
	CGFloat duration = [sender isKindOfClass:[UIPanGestureRecognizer class]] ? 0.3f : 0.5f;
	
	NSLog(@"Expanding map...");
	//_cloudViewState = CSStateResizing;
	
	[self.view endEditing:YES];
	
	[UIView animateWithDuration:duration animations:^{
		[_cloudOverviewContainer setFrame:CGRectMake(0, _cloudOverviewContainer.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - _cloudOverviewContainer.frame.origin.y)];
	} completion:^(BOOL finished) {
		
		[self.view bringSubviewToFront:_cloudOverviewContainer];
		
		NSArray *center = (NSArray *)[_cloud valueForKey:@"center"];
		if (center == nil || center.count < 2) center = [NSArray arrayWithObjects:[_cloud valueForKey:@"longitude"], [_cloud valueForKey:@"latitude"], nil];
		
		double lat = [[center objectAtIndex:1] doubleValue];
		double lng = [[center objectAtIndex:0] doubleValue];
		
		[self setMapBoundsToRadiusWithCenterIn:CLLocationCoordinate2DMake(lat, lng)];
		_cloudViewState = CSMapExpanded;
	}];
}

-(void)partiallyExpandMap:(CGFloat)translationY {
	
	if (translationY < 0) {
		return;
	}
	
	[_cloudOverviewContainer setFrame:CGRectMake(0, _cloudOverviewContainer.frame.origin.y, self.view.frame.size.width,
												 self.view.frame.size.height - _cloudOverviewContainer.frame.origin.y - _chatView.frame.size.height - _chatView.counterLabel.frame.size.height - _chatControlsContainer.frame.size.height + translationY)];
	[self.view bringSubviewToFront:_cloudOverviewContainer];
}

-(void)resetViewsToOriginalPosition:(id)sender {
	
	if (sender != nil && _cloudViewState == CSCloudInactive) {
		return;
	}
	
	CGRect counterLabelOriginalFrame = CGRectMake(0, _cloudOverviewContainer.frame.origin.y + _cloudOverviewContainer.frame.size.height,
												  self.view.frame.size.width, _chatView.counterLabel.frame.size.height);
	CGRect chatViewOriginalFrame = CGRectMake(0, counterLabelOriginalFrame.origin.y + counterLabelOriginalFrame.size.height, self.view.frame.size.width,
											  self.view.frame.size.height - _chatControlsContainer.frame.size.height - _cloudOverviewContainer.frame.size.height - _cloudOverviewContainer.frame.origin.y
											  -counterLabelOriginalFrame.size.height);
	CGRect chatControlsOriginalFrame = CGRectMake(0, self.view.frame.size.height - _chatControlsContainer.frame.size.height, self.view.frame.size.width, _chatControlsContainer.frame.size.height);
	
	CGRect cloudOverviewOriginalFrame = CGRectMake(0, _cloudOverviewContainer.frame.origin.y, self.view.frame.size.width,
												   self.view.frame.size.height - _cloudOverviewContainer.frame.origin.y - _chatView.counterLabel.frame.size.height - _chatView.frame.size.height - _chatControlsContainer.frame.size.height);
	
	CGFloat duration = (_cloudViewState == CSChatExpanded) ? 0.6f : 0.4f;
	
	//_cloudViewState = CSStateResizing;
	
	/*if ([sender isKindOfClass:[UIPanGestureRecognizer class]]) {
	 duration *= (_cloudOverviewContainer.frame.size.height - fabs([sender translationInView:self.view].y)) / _cloudOverviewContainer.frame.size.height;
	 } //izbacit*/
	
	[self.view endEditing:YES];
	
	[UIView animateWithDuration:duration animations:^{
		if (_cloudViewState == CSChatExpanded || _cloudViewState == CSChatInFullScreen) {
			[_chatView.counterLabel setFrame:counterLabelOriginalFrame];
			[_chatView setFrame:chatViewOriginalFrame];
			[_chatControlsContainer setFrame:chatControlsOriginalFrame];
		} else {
			[_cloudOverviewContainer setFrame:cloudOverviewOriginalFrame];
		}
		
	} completion:^(BOOL finished) {
		
		[self resetBackButton];
		
		NSArray *center = (NSArray *)[_cloud valueForKey:@"center"];
		if (center == nil || center.count < 2) center = [NSArray arrayWithObjects:[_cloud valueForKey:@"longitude"], [_cloud valueForKey:@"latitude"], nil];
		
		double lat = [[center objectAtIndex:1] doubleValue];
		double lng = [[center objectAtIndex:0] doubleValue];
		
		[self setMapBoundsToRadiusWithCenterIn:CLLocationCoordinate2DMake(lat, lng)];
		
		if (_chatView.messageButton != nil) {
			[_chatView.messageButton removeFromSuperview];
			_chatView.messageButton = nil;
			[_chatView showMessageButton];
		}
		[_cloudOverviewContainer.layer setZPosition:0.0f];
		_cloudViewState = CSStateOriginal;
	}];
}

/*
 -(void)handlePan:(UIPanGestureRecognizer *)sender {
 
 if (sender.state == UIGestureRecognizerStateBegan) {
 
 //_cloudViewState = CSStateResizing;
 
 if (_chatView.messageButton != nil) {
 [_chatView.messageButton setAlpha:0.0f];
 }
 
 startingVelocity = [sender velocityInView:sender.view].y;
 
 if (startingVelocity < 0) {
 [self.view bringSubviewToFront:_chatView];
 [self.view bringSubviewToFront:_chatControlsContainer];
 } else {
 [self.view bringSubviewToFront:_cloudOverviewContainer];
 }
 
 } else if (sender.state == UIGestureRecognizerStateEnded) {
 
 if ([sender translationInView:self.view].y > _chatView.frame.size.height / 2 || [sender velocityInView:self.view].y > 1600) {
 if (_cloudViewState == CSStateOriginal && startingVelocity > 0) {
 [self expandMap:sender];
 } else {
 [self resetViewsToOriginalPosition:sender];
 }
 } else if ([sender translationInView:self.view].y < - _cloudOverviewContainer.frame.size.height / 2 || [sender velocityInView:self.view].y < -1600) {
 if (_cloudViewState == CSStateOriginal && startingVelocity < 0) {
 [self expandChatView:sender];
 }
 } else {
 if (_cloudViewState == CSStateOriginal) {
 [self resetViewsToOriginalPosition:sender];
 } else {
 [self expandChatView:sender];
 }
 }
 
 } else if (sender.state == UIGestureRecognizerStateChanged) {
 //NSLog(@"Translation: %f", [sender translationInView:self.view].y);
 if (startingVelocity < 0 || _cloudViewState == CSChatExpanded) {
 [self partiallyExpandChatView:[sender translationInView:self.view].y];
 } else {
 [self partiallyExpandMap:[sender translationInView:self.view].y];
 }
 
 }
 }
 */

/*
 -(void)partiallyExpandChatView:(CGFloat)translationY {
 
 if (translationY > 0 && _cloudViewState != CSChatExpanded) {
 return;
 }
 
 NSLog(@"partiallyExpandChatView: %f", translationY);
 
 CGFloat origin = (_cloudViewState == CSChatExpanded) ? 0 : _cloudOverviewContainer.frame.size.height;
 CGFloat d = (_cloudViewState != CSChatExpanded) ? 0 : _cloudOverviewContainer.frame.size.height;
 
 [_chatView.counterLabel setFrame:CGRectMake(0, _cloudOverviewContainer.frame.origin.y + origin + translationY,
 self.view.frame.size.width, _chatView.counterLabel.frame.size.height)];
 [_chatView setFrame:CGRectMake(0, _cloudOverviewContainer.frame.origin.y + _chatView.counterLabel.frame.size.height + origin + translationY, self.view.frame.size.width,
 self.view.frame.size.height - _cloudOverviewContainer.frame.size.height - translationY -
 origin - _chatControlsContainer.frame.size.height - _chatView.counterLabel.frame.size.height)];
 [_chatControlsContainer setFrame:CGRectMake(0, self.view.frame.size.height - _chatControlsContainer.frame.size.height - d + translationY,
 self.view.frame.size.width, _chatControlsContainer.frame.size.height)];
 }
 */

-(void)chatViewToFullScreen {
	
	//_cloudViewState = CSStateResizing;
	[self.view endEditing:YES];
	
	CGFloat duration = (self.slideMenuSliding || self.slideMenuShown) ? 0.0f : 0.3f;
	
	[UIView animateWithDuration:duration animations:^{
		[_chatView setFrame:CGRectMake(0, _chatView.frame.origin.y, _chatView.frame.size.width, self.view.frame.size.height - _chatView.frame.origin.y - _chatControlsContainer.frame.size.height)];
		[_chatControlsContainer setFrame:CGRectMake(0, self.view.frame.size.height - _chatControlsContainer.frame.size.height, _chatControlsContainer.frame.size.width, _chatControlsContainer.frame.size.height)];
	} completion:^(BOOL finished) {
		_cloudViewState = CSChatInFullScreen;
		if (_chatView.messageButton != nil) {
			[_chatView.messageButton removeFromSuperview];
			_chatView.messageButton = nil;
			[_chatView showMessageButton];
		}
	}];
}

-(void)chatViewDoubleTapped:(UITapGestureRecognizer *)sender {
	//[self.view endEditing:YES];
	
	if (_cloudViewState == CSChatExpanded) {
		[self chatViewToFullScreen];
	} else if (_cloudViewState == CSChatInFullScreen) {
		if (![_messageInputView isFirstResponder]) {
			[_messageInputView becomeFirstResponder];
		} else {
			[self expandChatView:sender];
		}
	}
	
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
	
	if ([scrollView isEqual:_chatView]) {
		
		if (targetContentOffset->y > scrollView.contentSize.height - scrollView.frame.size.height - 20 && _chatView.messageButton != nil) {
			[_chatView.messageButton removeFromSuperview];
			_chatView.messageButton = nil;
		} else if (targetContentOffset->y <= 0/* && scrollView.contentOffset.y < 0*/) { //check if no more messages to load?
			if (![_chatView isInLoadPreviousMode] && hasOlderMessages) {
				[self loadMore];
			}
		}
		
	}////chatview
	
}

/*
 -(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	NSLog(@"textView shouldChangeTextInRange: %@ replacementText: %@", NSStringFromRange(range), text);
	return YES;
 }
 */

-(void)textViewDidChange:(UITextView *)textView {
	
	if (textView.text.length == 0) {
		[_messageInputView setBackgroundColor:[UIColor clearColor]];
	} else {
		[_messageInputView setBackgroundColor:[UIColor whiteColor]];
	}
	
	[self resizeInputView:textView];
} 

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
	
	if (action == @selector(paste:)) {
		return [UIPasteboard generalPasteboard].string != nil || [UIPasteboard generalPasteboard].image != nil;
	}
	
	return [super canPerformAction:action withSender:sender];
	
}

-(void)paste:(id)sender {
	[self overridePaste:sender];
}

-(void)overridePaste:(id)sender {
	
	NSLog(@"CloudView paste detected, sender: %@", sender);
	if ([_messageInputView isFirstResponder]) {
		
		UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
		
		for (NSString *pasteboardType in pasteboard.pasteboardTypes) {
			NSLog(@"pasteboardType: %@", pasteboardType);
		} //debug
		
		if (pasteboard.image != nil) {
			_chatView.imageToDisplay = pasteboard.image;
			[_chatView addThumbnail:_messageInputView];
			// TODO: check if existing is removed
			[self resizeInputView:_messageInputView];
		}
		
		if (pasteboard.string != nil) {
			
			NSRange range = _messageInputView.selectedRange;
			UITextPosition *beginning = _messageInputView.beginningOfDocument;
			UITextPosition *start = [_messageInputView positionFromPosition:beginning offset:range.location];
			UITextPosition *end = [_messageInputView positionFromPosition:start offset:range.length];
			UITextRange *textRange = [_messageInputView textRangeFromPosition:start toPosition:end];
			[_messageInputView replaceRange:textRange withText:pasteboard.string];
			
		}
		
		//other types?
		
	}
	
}

-(void)resizeInputView:(UITextView *)textView {
	CGFloat maxHeight = _chatView.frame.size.height / 2;
	CGRect currentFrame = textView.frame;
	CGSize newSize = CGSizeMake(currentFrame.size.width, MAX(_chatView.imageToDisplay == nil ? 38 : 52, MIN([textView.text isEqualToString:@""] ? 0 : textView.contentSize.height, maxHeight)));
	CGFloat heightDiff = newSize.height - currentFrame.size.height;
	
	[_chatControlsContainer setFrame:CGRectMake(_chatControlsContainer.frame.origin.x, _chatControlsContainer.frame.origin.y - heightDiff, _chatControlsContainer.frame.size.width,
												_chatControlsContainer.frame.size.height + heightDiff)];
	[_chatView setFrame:CGRectMake(_chatView.frame.origin.x, _chatView.frame.origin.y, _chatView.frame.size.width, _chatView.frame.size.height - heightDiff)];
	[textView setFrame:CGRectOffset(CGRectInset(currentFrame, 0, - heightDiff / 2), 0, heightDiff / 2)];
	[_messageInputPlaceholderView setFrame:CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, textView.frame.size.height)];
	[_messageInputPlaceholderView setTextContainerInset:textView.textContainerInset];
	[_addPhotoControlsContainer setFrame:_chatControlsContainer.bounds];
} ////chatview

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	//if not chatViewStretched
	//[self expandChatView:textView];
	return YES;
	
	//http://stackoverflow.com/a/25152478/1630623 za keyboard frame
} ////chatview

-(void)textViewDidEndEditing:(UITextView *)textView {
	//return to original position?
	//[self chatViewToFullScreen];
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
	return YES;
}
////chatview
/*
 -(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
 
 }
 */

-(IBAction)addPhotoClicked:(id)sender {
	[self chatViewToFullScreen];
	[_addPhotoControlsContainer setHidden:NO];
}

-(IBAction)cancelAddPhotoClicked:(id)sender {
	[_addPhotoControlsContainer setHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
	
	if ((!animated && [self.navigationController.viewControllers indexOfObject:self.tabBarController ?: self] + 1 < self.navigationController.viewControllers.count) || [self.navigationController.viewControllers indexOfObject:self.tabBarController ?: self]==NSNotFound) {
		
		// u dealloc!
		NSLog(@"Canceling requests, deallocating map objects...");
		[_apiService cancelRequests];
		_apiService = nil;
		[_cloudCreatorService cancelRequests];
		[_mapView removeAnnotations:_mapView.annotations];
		self.clusteringManager = nil;
		//_mapView.delegate = nil;
		[_mapView removeFromSuperview]; ////nije testirano
		_mapView = nil;					////	-\\-
		
		[self.view.layer removeAllAnimations];
		[UIView cancelPreviousPerformRequestsWithTarget:self];
		[NSObject cancelPreviousPerformRequestsWithTarget:self];
		
	}
	
	[super viewWillDisappear:animated];
		
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusDistanceUnitChangedNotification" object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"UserLocationChange" object:nil];
	
}

-(void)handleUserLocationChange:(NSNotification *)aNotification {

    if (!_cloud) {
        return;
    }

	static BOOL notified = NO; ////////////ne smi bit static?
	
	CLLocation *newLocation = [[aNotification userInfo] valueForKey:@"newLocation"];
	_userLocation = newLocation.coordinate;
	
	NSArray *center = (NSArray *)[_cloud valueForKey:@"center"];
	if (center == nil || center.count < 2) center = [NSArray arrayWithObjects:[_cloud valueForKey:@"longitude"], [_cloud valueForKey:@"latitude"], nil];
	
	double lat = [[center objectAtIndex:1] doubleValue];
	double lng = [[center objectAtIndex:0] doubleValue];
	
	CLLocationCoordinate2D cloudCenter = CLLocationCoordinate2DMake(lat, lng);
	
	if ([self distanceBetweenCoordinate:_userLocation andCoordinate:cloudCenter] > [[_cloud valueForKey:@"radius"] doubleValue] && !notified) {
		
		notified = YES;
		
		NSString *message = @"You left the radius! Get back in the in range to continue chatting.";
		
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
			////////if viewcontrollers.count > 1, else viewcontrollers = ...explore ??
			[self.navigationController popViewControllerAnimated:YES];
		}];
		
		[alert addAction:close];
		
		//[self presentViewController:alert animated:YES completion:nil];
		[alert show];
		
	}
	
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender { //ExploreMap
	
	if ([[segue identifier] isEqualToString:@"ViewCloudUsers"]) {
		ViewFriends *dvc = [segue destinationViewController];
		[dvc setFriendsList:[NSMutableArray arrayWithArray:_cloudUsersList]];
		[dvc setTitle:@"People in cloud"];
	} else if ([segue.identifier isEqualToString:@"ViewCloudUserProfile"]) {
		Profile *dvc = [segue destinationViewController];
		[dvc setUserInfo:sender];
	} else if ([segue.identifier isEqualToString:@"ViewCloudCreatorProfile"]) {
		Profile *dvc = [segue destinationViewController];
		[dvc setUserInfo:objc_getAssociatedObject(sender, &cloudCreatorKey)];
	}
	
}

-(void)dealloc {
    [self stopTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // unnecessary?
}

@end
