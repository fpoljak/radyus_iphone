//
//  ApiService.h
//  RadyUs
//
//  Created by Frane Poljak on 15/04/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DBManager.h"

@protocol ApiServiceDelegate;

@interface ApiService : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, assign) id<ApiServiceDelegate> delegate;

-(void) doSignUpWithFirstName:(NSString *)first lastName:(NSString *)last userName:(NSString *)user email:(NSString *)mail password:(NSString *)password birthday:(NSString *)birthday gender:(NSInteger)gender;
-(void) getAccesTokenForUser:(NSString *)userName password:(NSString *)password;
-(void) fbAuth:(NSString *)fbAccessToken;
-(void) getUserInfo:(NSString *)accessToken;
-(void) getUsersByIdList:(NSArray *)usersIdList;
-(void)getUsersByUsernameList:(NSArray *)usernameList;
-(void) createCloudWithName:(NSString *)name description:(NSString *)description centerLat:(double)lat centerLng:(double)lng radius:(NSInteger)radius duration:(NSInteger)duration anonymus:(BOOL)anonymus public:(BOOL)public users:(NSArray *)users;
-(void) getCloudsByLatitude:(double)latitude longitude:(double)longitude inRadius:(NSInteger)maxDistance;
-(void) getCloudsByUserId:(NSString *)userId;
-(void) getCloudsByIdList:(NSArray *)idList;
-(void) getCloudUsers:(NSString *)cloudID;
-(void) refreshAccesToken;
-(void) postUserInfo:(NSDictionary *)userInfo;
-(void) postUserLatitude:(double)lat longitude:(double)lng;
-(void) joinCloud:(NSString *)cloudID;
-(void) leaveCloud:(NSString *)cloudID;
-(void) addFriend:(NSString *)userId;
-(void) unfriendUser:(NSString *)userId;
-(void) getFriends:(NSString *)userId;
-(void) getPendingRequests;
-(void) getRequestedFriends;
-(void) changePassword:(NSString *)newPassword;
-(void) postNewImage:(UIImage *)image;
-(void) postImagesIdArray:(NSArray *)imageIdList;
-(void) postMessage:(NSString *)messsage toCloud:(NSString *)cloudId withImage:(NSString *)image;
-(void) post1on1Message:(NSString *)message toFriend:(NSString *)friendId withImage:(NSString *)image;
-(void) loadMessagesInCloud:(NSString *)cloudId fromTime:(NSString *)fromTime toTime:(NSString *)toTime limit:(NSUInteger)limit;
-(void) load1on1MessagesWithFriend:(NSString *)friendId since:(NSString *)since until:(NSString *)until limit:(NSUInteger)limit;

-(void) cancelRequests;

-(void) showLoadingViewWithTitle:(NSString *)title;
-(void) hideLoadingView;

@property (nonatomic, retain) NSURLConnection *signupConn;
@property (nonatomic, retain) NSURLConnection *getTokenConn;
@property (nonatomic, retain) NSURLConnection *getProfileInfoConn;
@property (nonatomic, retain) NSURLConnection *createCloudConn;
@property (nonatomic, retain) NSURLConnection *getCloudsByLocationConn;
@property (nonatomic, retain) NSURLConnection *getCloudsByUserIdConn;
@property (nonatomic, retain) NSURLConnection *getCloudsByIdListConn;
@property (nonatomic, retain) NSURLConnection *getCloudUsersConn;
@property (nonatomic, retain) NSURLConnection *postUserInfoConnection;
@property (nonatomic, retain) NSURLConnection *postUserLocationConnection;
@property (nonatomic, retain) NSURLConnection *joinCloudConnection;
@property (nonatomic, retain) NSURLConnection *leaveCloudConnection;
@property (nonatomic, retain) NSURLConnection *fbAuthConnection;
@property (nonatomic, retain) NSURLConnection *getUsersConnection;
@property (nonatomic, retain) NSURLConnection *addFriendConnection;
@property (nonatomic, retain) NSURLConnection *unfriendConnection;
@property (nonatomic, retain) NSURLConnection *changePasswordConnection;
@property (nonatomic, retain) NSURLConnection *getFriendsConnection;
@property (nonatomic, retain) NSURLConnection *getPendingConnection;
@property (nonatomic, retain) NSURLConnection *getRequestedConnection;
@property (nonatomic, retain) NSURLConnection *anewPictureConnection;
@property (nonatomic, retain) NSURLConnection *postUserImagesConnection;
@property (nonatomic, retain) NSURLConnection *postMessageConnection;
@property (nonatomic, retain) NSURLConnection *loadMessagesConnection;
@property (nonatomic, retain) NSURLConnection *post1on1MessageConnection;
@property (nonatomic, retain) NSURLConnection *load1on1MessagesConnection;

@property (nonatomic, retain) NSMutableArray <NSURLConnection *> *runningOperations;

@property (nonatomic, retain) NSString *currentImage;
@property (nonatomic, retain) NSString *currentImageId;
@property (nonatomic) UIBackgroundTaskIdentifier uploadTaskID;

@property (nonatomic, retain) DBManager *dbManager;

@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSMutableData *messagesData;

@property (nonatomic, retain) UIView *loadingView;
@property (nonatomic, retain) UILabel *loadingMessage;
@property (nonatomic, retain) UIActivityIndicatorView *activityIndicator;

@property (nonatomic) BOOL trackProgress;

@property (nonatomic) NSInteger delegateStrongReferenceCounter;

@end



@protocol ApiServiceDelegate <NSObject>

-(BOOL) shouldDisableUserInteraction;

@optional

-(void) apiService: (ApiService *)service taskFailedWithErrorCode:(uint) errorCode;
-(void) signupSuccess:(NSJSONSerialization *)responseData;
-(void) receivedAccessToken:(NSString *) accessToken refreshToken:(NSString *)refreshToken socketToken:(NSString *)socketToken;
-(void) receivedUserInfo:(NSJSONSerialization *) userInfo;
-(void) apiService: (ApiService *)service receivedUsersList:(NSJSONSerialization *) usersList;
-(void) cloudCreated:(NSJSONSerialization *) response;
-(void) apiService:(ApiService *)service receivedCloudList:(NSJSONSerialization *) cloudList;
-(void) receivedCloudIdList:(NSArray *) cloudIdList;
-(void) apiService: (ApiService *)service receivedUserIdList:(NSArray *) userIdList;
-(void) joinCloudSuccess:(NSJSONSerialization *) response;
-(void) didLeaveCloud:(NSJSONSerialization *) response;
-(void) userLocationPostSuccess;
-(void) userInfoPostSuccess:(NSJSONSerialization *) response;
-(void) addFriendSuccess:(NSJSONSerialization *) response;
-(void) unfriendSuccess:(NSJSONSerialization *) response;
-(void) getPendingSuccess:(NSArray *) pending;
-(void) getRequestedSuccess:(NSArray *) requested;
-(void) changePasswordSuccess:(NSJSONSerialization *) response;
-(void) newPictureSuccess:(NSString *)pictureId;
-(void) postMessageSuccess:(NSJSONSerialization *)response;
-(void) postMessageFailed;////
-(void) loadMessagesSuccess:(NSJSONSerialization *)response;

-(void) apiService: (ApiService *)service downloadProgress:(CGFloat)progress;
-(void) apiService: (ApiService *)service uploadProgress:(CGFloat)progress;

@end
