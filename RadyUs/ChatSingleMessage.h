//
//  ChatSingleMessage.h
//  RadyUs
//
//  Created by Frane Poljak on 03/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
#import "AsyncImageLoadManager.h"
#import "SafeBrowsingService.h"
#import "ApiService.h"
#import "LinkPreview.h"

@interface ChatSingleMessage : UIView <UITextViewDelegate, SafeBrowsingServiceDelegate, ApiServiceDelegate, UIWebViewDelegate>

enum MessageType : NSUInteger{
    MTAppMessage = 0,
    MTUserMessage = 1,
    MTFriendMessage = 2,
};

@property (nonatomic) enum MessageType messageType;
@property (nonatomic, retain) NSMutableDictionary *message;
@property (nonatomic, retain) UILabel *dateTimeLabel;
@property (nonatomic) BOOL displayHeader;
@property (nonatomic) BOOL shouldAnimate;
@property (nonatomic) CGFloat frameX;
@property (nonatomic) CGFloat frameY;
@property (nonatomic) CGRect layerFrame;
@property (nonatomic, retain) CALayer *backgroundLayer;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UIImageView *avatar;
@property (nonatomic, retain) UILabel *nameLabel;
@property (nonatomic, retain) NSMutableArray *links;
@property (nonatomic) NSInteger linkIndex;
@property (nonatomic) NSInteger linkStartingY;
@property (nonatomic, weak) ChatSingleMessage *previousMessage;
@property (nonatomic, retain) ChatSingleMessage *nextMessage;

@property (nonatomic) BOOL deallocated;

-(id)initWithType:(enum MessageType)type message:(NSDictionary *)message displayHeader:(BOOL)displayHeader;

-(CGFloat)displayInContainer:(UIScrollView *)messageLayout startingY:(CGFloat)startingY;
-(CGFloat)displayImage:(UIImage *)image inContainer:(UIScrollView *)container startingY:(CGFloat)startingY;

-(void)adjustCorners;

@end
