//
//  EmailLogin.m
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "EmailLogin.h"

@interface EmailLogin ()

@end

@implementation EmailLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard:)];
    
    [self.view addGestureRecognizer:tap];

}

-(IBAction)doLogin:(id)sender {

	[_apiService getAccesTokenForUser:[_userTF text] password:[_passTF text]];
	
}

-(void)receivedAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken socketToken:(NSString *)socketToken {
	
	//CHECK IN LOCAL STORAGE?
	[_apiService getUserInfo:accessToken];
	
}

-(void)receivedUserInfo:(NSJSONSerialization *)userInfo {
	
	[self performSegueWithIdentifier:@"LoginSuccess" sender:userInfo];
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	//
}

-(BOOL)shouldDisableUserInteraction {
	return YES; ////NO, disejblat botun?
}

-(IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField.returnKeyType==UIReturnKeyNext) {
        // find the text field with next tag
        UIView *next = [self.view viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone || textField.returnKeyType==UIReturnKeyDefault) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([[segue identifier] isEqualToString:@"LoginSuccess"]) {
		Profile *dvc = [segue destinationViewController];
		[dvc setUserInfo:sender];
	}
}

-(void)viewWillDisappear:(BOOL)animated {
	if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
		[_apiService cancelRequests];
	}
	[super viewWillDisappear:animated];
}

@end
