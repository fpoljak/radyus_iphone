//
//  ViewFriends.h
//  RadyUs
//
//  Created by Frane Poljak on 05/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AsyncImageLoadManager.h"

@protocol ChooseFriendsDelegate <NSObject>

-(void)didChooseFriends:(id)sender;
-(void)didChooseSingleFriend:(id)sender;

@end

#import <objc/runtime.h>
#import "ApiService.h"
#import "SideMenuUtilizer.h"

@interface ViewFriends : SideMenuUtilizer <UIGestureRecognizerDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UITextView *searchView;
@property (nonatomic, retain) NSMutableArray *friendIdList;
@property (nonatomic, retain) NSMutableArray *friendsList;
@property (nonatomic, retain) NSString *currentFragment;
@property (nonatomic, retain) NSMutableArray *chosenFriends;
@property (nonatomic, retain) NSMutableArray *filteredFriendList;
@property (nonatomic, retain) IBOutlet UIButton *createButton;
@property (nonatomic, retain) NSString *selectionType;
@property (nonatomic, retain) NSMutableDictionary *userImageHashMap;
@property (nonatomic, assign) id<ChooseFriendsDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *pending;
@property (nonatomic, retain) NSMutableArray *requested;
@property (nonatomic, retain) NSString *addRemoveAction;
@property (nonatomic, retain) NSDictionary *friendToAddOrRemove;

@property (nonatomic, retain) ApiService *apiService;
@property (nonatomic, retain) ApiService *getPendingService;
@property (nonatomic, retain) ApiService *getRequestedService;

@end
