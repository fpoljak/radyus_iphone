//
//  LocationManagerHandler.m
//  Radyus
//
//  Created by Frane Poljak on 17/03/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "LocationManagerHandler.h"

@implementation LocationManagerHandler

BOOL canTrackLocationInBackground;
BOOL locationUpdatesRunning;
static CLLocationManager *defaultManager;

-(instancetype)init {
	if (self = [super init]) {
		[self initialize];
	}
	return self;
}

-(void)initialize {
	
	static dispatch_once_t dispatchOnce;
	
	dispatch_once(&dispatchOnce, ^{
		defaultManager = [[CLLocationManager alloc] init];
		defaultManager.delegate = self;
		locationUpdatesRunning = NO;
		defaultManager.distanceFilter = 10.0f;
		defaultManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
	});
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareForBackground) name:UIApplicationWillResignActiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnFromBackground) name:UIApplicationDidBecomeActiveNotification object:nil];
	
	if (!(canTrackLocationInBackground = CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedAlways)) {
		[defaultManager requestAlwaysAuthorization];
	}
	
	if ((canTrackLocationInBackground = CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedAlways)
		|| CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
		
		//not needed?
		[defaultManager startUpdatingLocation];
		locationUpdatesRunning = YES;
	} else if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusDenied || CLLocationManager.authorizationStatus == kCLAuthorizationStatusRestricted) {
		[self handleDenialOfLocationService];
	}

	//what if unknown?
	
}

+(CLLocation *)lastKnownLocation {
	return ([self locationInvalid] ? nil : defaultManager.location);
}

-(BOOL)applicationIsActive {
	return [UIApplication sharedApplication].applicationState == UIApplicationStateActive;
}

-(void)handleDenialOfLocationService {
	
	if (locationUpdatesRunning) {
	
		if ([self applicationIsActive]) {
			[defaultManager stopUpdatingLocation];
		} else {
			[defaultManager stopMonitoringSignificantLocationChanges];
		}
		
		locationUpdatesRunning = NO;
		
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:RLocationServicesDeniedNotification object:nil];
	
}

-(void)handleAlwaysAllowed {
	
	if (!locationUpdatesRunning) {
		
		if ([self applicationIsActive]) {
			[defaultManager startUpdatingLocation];
		} else {
			[defaultManager startMonitoringSignificantLocationChanges];
		}
		
		locationUpdatesRunning = YES;
		
	}
	
}

-(void)handleWhenInUseOnlyAllowed {
	
	if (!locationUpdatesRunning) {
		
		if ([self applicationIsActive]) {
			[defaultManager startUpdatingLocation];
		} else {
			[defaultManager startMonitoringSignificantLocationChanges];
		}
		
		locationUpdatesRunning = YES;
		
	}

}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
	
	switch (status) {
		
		case kCLAuthorizationStatusDenied:
		case kCLAuthorizationStatusRestricted: {
			[self handleDenialOfLocationService];
			break;
		}
		
		case kCLAuthorizationStatusAuthorizedWhenInUse: {
			[self handleWhenInUseOnlyAllowed];
			break;
		}
		
		case kCLAuthorizationStatusAuthorizedAlways: {
			[self handleAlwaysAllowed];
			break;
		}
	
		default: //unknown?
			break;
	}
	
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	[[NSNotificationCenter defaultCenter] postNotificationName:RLocationManagerErrorNotification object:nil userInfo:[NSDictionary dictionaryWithObject:error forKey:@"error"]];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
	//velocity
	[[NSNotificationCenter defaultCenter] postNotificationName:RUserLocationChangedNotification object:nil userInfo:[NSDictionary dictionaryWithObject:locations forKey:@"locations"]];
}

-(void)prepareForBackground {
	[defaultManager stopUpdatingLocation];
	if (canTrackLocationInBackground) {
		[defaultManager startMonitoringSignificantLocationChanges];
	}
}

-(void)returnFromBackground {
	[defaultManager stopMonitoringSignificantLocationChanges];
	CGFloat delay = [self.class locationInvalid] ? 0.0f : MAX(0, 60 + [defaultManager.location.timestamp timeIntervalSinceNow]); // determine best interval
	[defaultManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay:delay];
}

+(BOOL)locationInvalid {
	return defaultManager.location == nil || defaultManager.location.horizontalAccuracy < 0 || defaultManager.location.horizontalAccuracy > 50; // or less?, check timestamp?
}

-(void)dealloc {
	
	if (locationUpdatesRunning) {

		if ([self applicationIsActive]) {
			[defaultManager stopUpdatingLocation];
		} else {
			[defaultManager stopMonitoringSignificantLocationChanges];
		}

	}
	
	defaultManager.delegate = nil;
	defaultManager = nil;
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
}

@end
