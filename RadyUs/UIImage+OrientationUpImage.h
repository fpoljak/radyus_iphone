//
//  UIImage+OrientationUpImage.h
//  Radyus
//
//  Created by Frane Poljak on 28/10/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (OrientationUpImage)

-(UIImage *)orientationUpImage;

@end
