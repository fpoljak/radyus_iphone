//
//  ExploreOptionsDialog.h
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ExploreSettingsDelegate <NSObject>

-(void)commitSettings:(NSDictionary *)settings;

@end

//#import "ExploreMap.h"
#import "ExploreList.h"
#import "SideMenuUtilizer.h"

@interface ExploreOptionsDialog : SideMenuUtilizer

@property (nonatomic, assign) id<ExploreSettingsDelegate> delegate;

@property (nonatomic, retain) NSString *access;
@property (nonatomic, retain) IBOutlet UIView *allCheck;
@property (nonatomic, retain) IBOutlet UIView *publicCheck;
@property (nonatomic, retain) IBOutlet UIView *friendsCheck;
@property (nonatomic, retain) IBOutlet UIView *oneOnOneCheck;
@property (nonatomic, retain) IBOutlet UILabel *radiusLabel;
@property (nonatomic, retain) IBOutlet UISlider *radiusSlider;
@property (nonatomic) CGFloat radius;

@end
