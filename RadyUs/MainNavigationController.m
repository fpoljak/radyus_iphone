//
//  MainNavigationController.m
//  RadyUs
//
//  Created by Frane Poljak on 03/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "MainNavigationController.h"

@interface MainNavigationController ()

@end

@implementation MainNavigationController 

- (void)viewDidLoad {
	
    [super viewDidLoad];

	//self.modalPresentationCapturesStatusBarAppearance = NO;
	
	[self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamRounded-Medium" size:20.0f], NSFontAttributeName, nil]];
	self.navigationController.view.backgroundColor = [UIColor whiteColor];
	
	// TODO: subclass UINavigationBar - set it in IB!
	// example: https://gist.github.com/timothyarmes/7080170
	
	// to do it programatically: (not needed at this time!)
	// http://stackoverflow.com/questions/1869331/set-a-custom-subclass-of-uinavigationbar-in-uinavigationcontroller-programmatica
	// or this? http://stackoverflow.com/questions/16373345/how-to-subclass-uinavigationbar-correctly-also-using-appearance
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
