//
//  ChangePassword.m
//  Radyus
//
//  Created by Locastic MacbookPro on 15/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ChangePassword.h"
#import "ActionSuccessIndicator.h"

@interface ChangePassword ()

@end

@implementation ChangePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)validateForm {
	
	if (_oldPassword.text.length == 0) {
		//message?
		return NO;
	}
	
	if (_anewPassword.text.length == 0) {
		//message?
		return NO;
	}
	
	if (![_anewPassword.text isEqualToString:_confirmPassword.text]) {
		//message?
		return NO;
	}
	
	return YES;
	
}

-(IBAction)changePassword:(id)sender {
	
	if ([self validateForm]) {
		[_apiService changePassword:_anewPassword.text];
	}
}

-(void)changePasswordSuccess:(NSJSONSerialization *)response {

	ActionSuccessIndicator *indicator = [ActionSuccessIndicator new];
	[indicator showWithSuccess:YES inViewController:self duration:1.4f message:@"Password successfully changed" completion:^(BOOL finished) {
		[self dismiss:nil];
	}];
	
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	ActionSuccessIndicator *indicator = [ActionSuccessIndicator new];
	[indicator showWithSuccess:YES inViewController:self duration:1.4f message:@"Error changing password!" completion:nil];
}

-(BOOL)shouldDisableUserInteraction {
	return YES; ////??
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
	
	if(textField.returnKeyType==UIReturnKeyNext) {
		// find the text field with next tag
		UIView *next = [self.view viewWithTag:textField.tag+1];
		[next becomeFirstResponder];
	} else if (textField.returnKeyType==UIReturnKeyDone || textField.returnKeyType==UIReturnKeyDefault) {
		[textField resignFirstResponder];
		//[_containerView setContentOffset:CGPointZero animated:YES];
	}
	return YES;
}


-(IBAction)dismiss:(id)sender {

	[self dismissViewControllerAnimated:YES completion:^{
		
	}];
	
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
