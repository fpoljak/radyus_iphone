//
//  Inbox.h
//  Radyus
//
//  Created by Frane Poljak MacbookPro on 17/09/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "SideMenuUtilizer.h"
#import "ApiService.h"
#import "AsyncImageLoadManager.h"
#import "DBManager.h"
#import "ViewFriends.h"

@interface Inbox : SideMenuUtilizer <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, ApiServiceDelegate, AsyncImageLoadDelegate, ChooseFriendsDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UISearchBar *filterInput;

@property (nonatomic, retain) ApiService *apiService;
@property (nonatomic, retain) DBManager *dbManager;

@property (nonatomic, retain) NSMutableArray *chats;
@property (nonatomic, retain) NSMutableArray *filteredChats;
@property (nonatomic, retain) NSMutableDictionary *userImageHashMap;

@end
