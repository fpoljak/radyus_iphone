//
//  Inbox.m
//  Radyus
//
//  Created by Locastic MacbookPro on 17/09/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "Inbox.h"
#import "OneOnOneChat.h"
#import "UIImageView+NoAvatar.m"
#import "ViewFriends.h"

@interface Inbox ()

@property (nonatomic) NSInteger newMessagesCount;
@property (nonatomic, retain) NSMutableDictionary *hasNewMessages;

@end

@implementation Inbox

static char userImageKey;

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	_chats = [NSMutableArray new];
	
	_filteredChats = [NSMutableArray new];
	_userImageHashMap = [NSMutableDictionary new];
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;

    _newMessagesCount = 0;
    _hasNewMessages = [[NSMutableDictionary alloc] init];

	NSString *dbName = [NSString stringWithFormat:@"%@.sqlt", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	_dbManager = [[DBManager alloc] initWithDatabaseFilename: dbName];
	
	NSString *userId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"];
	NSString *syncedKey = [@"isSynchronized-" stringByAppendingString:userId];
	NSString *syncedValue = [[NSUserDefaults standardUserDefaults] valueForKey:syncedKey];
	
	if (syncedValue != nil && [syncedValue isEqualToString:@"yes"]) {
		[self initInboxView];
	} else {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initInboxView) name:@"RadyusFirstLoadSyncComplete" object:nil];
	}
	
	[[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];

    [self setNewMessagesCount:0];
}

-(void)initInboxView {
	
	NSArray *friends = [NSArray arrayWithArray:[[NSUserDefaults standardUserDefaults] valueForKey:@"userFriends"]];
	
	if (friends == nil) {
		[_apiService getFriends:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	} else {
		[_apiService getUsersByIdList:friends];
	}
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusFirstLoadSyncComplete" object:nil];
	
}

-(void)didReceiveMessage:(NSNotification *)notification {
	
	NSDictionary *message = [notification userInfo];
	NSMutableDictionary *friend = [[NSMutableDictionary alloc] initWithDictionary:message copyItems:YES];
	[friend setValue:[friend valueForKey:@"user"] ? ([[friend valueForKey:@"user"] isEqualToString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]] ? [friend valueForKey:@"to"] : [friend valueForKey:@"user"]) : [friend valueForKey:@"to"] forKey:@"id"];
	
	if ([friend valueForKey:@"nameFirst"] == nil) {
		
		NSDictionary *friendDetail = [_dbManager loadUserWithId:[friend valueForKey:@"id"]];
		
		[friendDetail enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
			
			if ([friend valueForKey:key] == nil) {
				[friend setValue:obj forKey:key];
			}
			
		}];
		
	}
	
	[self addChatWithFriend:friend message:message]; ////
	
}

-(void)apiService:(ApiService *)service receivedUserIdList:(NSArray *)userIdList {
	
	if (userIdList.count == 0) {
		[self apiService:service receivedUsersList:(NSJSONSerialization *)[NSArray new]];
	} else {
		[service getUsersByIdList:userIdList];
	}
	
}

-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {
	
	for (NSDictionary *friend in (NSArray *)usersList) {

		NSArray *lastMessage = [[_dbManager loadMessages:[friend valueForKey:@"id"]] lastObject];
	
		if (lastMessage == nil) {
			continue;
		}
		
		NSString *text = [lastMessage valueForKey:@"text"];
		NSString *user = [lastMessage valueForKey:@"user"];
		BOOL myMessage = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"] isEqualToString:user];
		
		if ([text isEqualToString:@""]) {
			text = [NSString stringWithFormat:@"%@ sent an image", myMessage ? @"You" : [friend valueForKey:@"nameFirst"]];
		} else if (myMessage) {
			text = [@"You: " stringByAppendingString:text];
		}
		
		NSString *time = [lastMessage valueForKey:@"time"];
		
		NSMutableDictionary *chat = [[NSMutableDictionary alloc] initWithDictionary:friend copyItems:YES];
		[chat setValue:text forKey:@"lastMessage"];
		[chat setValue:time forKey:@"lastMessageTime"];
		[_chats addObject:chat];

	}
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
	
	_chats = [NSMutableArray arrayWithArray:[_chats sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
		NSDate *d1 = [dateFormatter dateFromString:obj1[@"lastMessageTime"]];
		NSDate *d2 = [dateFormatter dateFromString:obj2[@"lastMessageTime"]];
		
		return [d2 compare:d1];
	}]];
	
	_filteredChats = [NSMutableArray arrayWithArray:_chats];
	
	[_tableView reloadData];
	
	[self loadUserImages];
	
	//[[UIBarButtonItem appearanceWhenContainedIn:self.class, nil] setTitlePositionAdjustment:UIOffsetMake(0.0f, 12.0f) forBarMetrics:UIBarMetricsDefault];
	UIBarButtonItem *newChat = [[UIBarButtonItem alloc] initWithTitle:@"+" style:UIBarButtonItemStylePlain target:self action:@selector(newChatClicked:)]; //ikona umisto plusa?
	[newChat setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamRounded-Light" size:30.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
	[newChat setTintColor:[UIColor whiteColor]];
	
	[self.navigationItem setRightBarButtonItem:newChat animated:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMessage:) name:@"RadyusNew1on1Message" object:nil];
	
	[self checkForNewMessages];
	
}

-(void)checkForNewMessages {
	
	for (NSDictionary *friend in _chats) { //vidit za sve prijatelje!!
		ApiService *service = [[ApiService alloc] init];
		service.delegate = self;
		NSLog(@"Loading new messages for friend: %@", [friend valueForKey:@"nameFirst"]);
		[service load1on1MessagesWithFriend:[friend valueForKey:@"id"] since:[friend valueForKey:@"lastMessageTime"] until:nil limit:0];
	}
	
}

-(void)receivedNotification:(NSDictionary *)aNotification {

    if (![[[aNotification valueForKey:@"userInfo"] valueForKey:@"type"] isEqualToNumber:@1]) {

        [super receivedNotification:aNotification];
        return;
    }

    NSString *_friendId = [[aNotification valueForKey:@"userInfo"] valueForKey:@"user_id"];

    if (_friendId == nil) {
        return;
    }

    NSDictionary *friend = [[_chats filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id = %@", _friendId]] firstObject];

    ApiService *service = [[ApiService alloc] init];
    service.delegate = self;

    if (friend != nil) {
        [service load1on1MessagesWithFriend:[friend valueForKey:@"id"] since:[friend valueForKey:@"lastMessageTime"] until:nil limit:0];
    } else {
        [service load1on1MessagesWithFriend:_friendId since:nil until:nil limit:10];
    }
}

-(void)loadMessagesSuccess:(NSJSONSerialization *)response {
	
	if ([(NSArray *)response count] > 1) {
		
		NSMutableArray *messages = [NSMutableArray arrayWithArray:(NSArray *)response];

        NSLog(@"New messages: %@", messages);

		[_dbManager saveMessages:messages];

        for (NSDictionary *message in messages) {
            if (![[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"] isEqualToString:[message valueForKey:@"user"]]) {
                if (_hasNewMessages[message[@"user"]] == nil || ![_hasNewMessages[message[@"user"]] boolValue]) {
                    _newMessagesCount++;
                    [self incrementBadgeNumberForRow:1];
                }
                _hasNewMessages[message[@"user"]] = @YES;
            }
        }
		
		//sort
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
		
		[messages sortUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
			
			NSDate *d1 = [dateFormatter dateFromString:obj1[@"time"]];
			NSDate *d2 = [dateFormatter dateFromString:obj2[@"time"]];
			
			NSLog(@"date d1: %@", [dateFormatter stringFromDate:d1]);
			
			return [d1 compare:d2]; // od starije
			
		}];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"RadyusNew1on1Message" object:self/*nil?*/ userInfo:(NSDictionary *)[messages lastObject]];
		
	}
	
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(BOOL)shouldDisableUserInteraction {
	return NO;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return _filteredChats.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InboxCell"];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InboxCell"];
	}
	
	UILabel *nameLabel = (UILabel *)[cell viewWithTag:1];
	UILabel *lastMessageLabel = (UILabel *)[cell viewWithTag:2];
	UIImageView *avatar = (UIImageView *)[cell viewWithTag:3];
	//timeLabel
	
	NSDictionary *chat = [_filteredChats objectAtIndex:indexPath.row];
	
	NSString *first = [chat valueForKey:@"nameFirst"];
	NSString *last = [chat valueForKey:@"nameLast"];
	
	[nameLabel setText:[NSString stringWithFormat:@"%@ %@", first, last]];
	[lastMessageLabel setText:[chat valueForKey:@"lastMessage"]];
	
	[avatar.layer setCornerRadius:27.0f];
	[avatar setClipsToBounds:YES];
	UIImage *image = (UIImage *)[_userImageHashMap valueForKey:[chat valueForKey:@"id"]];
	[avatar setImage:image];
	
	UILabel *messageTime = (UILabel *)[cell viewWithTag:4];
	[messageTime setFrame:CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width + 3, nameLabel.frame.origin.y, 80, 15)];

	if (![[chat valueForKey:@"lastMessageTime"] isEqualToString:@""]) {
		
		NSString *timeString = (NSString *)[chat valueForKey:@"lastMessageTime"];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
		NSDate *dateTime = [dateFormatter dateFromString:timeString];

		dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
		
		NSCalendar *calenar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
		[calenar setTimeZone:[NSTimeZone localTimeZone]];
		BOOL isToday = [calenar isDateInToday:dateTime];
		//BOOL isYesterday = [calenar isDateInYesterday:dateTime];
		
		NSString *dateFormat = isToday ? @"HH:mm" : @"MMM. dd."; //check if this year
		
		[dateFormatter setDateFormat:dateFormat];
		[messageTime setText:[dateFormatter stringFromDate:dateTime]];

	} else {
		[messageTime setText:@""];
	}

    if (_hasNewMessages[[chat valueForKey:@"id"]] != nil && [_hasNewMessages[[chat valueForKey:@"id"]] boolValue]) {
        [cell setBackgroundColor:UIColorFromRGB(0xE0E0E0)];
        [messageTime setTextColor:[UIColor darkTextColor]];
        [messageTime setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:12.0f]];
        [lastMessageLabel setTextColor:[UIColor darkTextColor]];
        [lastMessageLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:11.0f]];
    } else {
        [cell setBackgroundColor:[UIColor clearColor]];
    }
	
	return cell;
	
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	[self.view endEditing:YES];

    NSDictionary *friend = (NSDictionary *)[_filteredChats objectAtIndex:indexPath.row];

    OneOnOneChat *chatView = [self.storyboard instantiateViewControllerWithIdentifier:@"OneOnOneChat"];

    [chatView setFriend:friend];
	
	[self.navigationController pushViewController:chatView animated:YES];
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (_hasNewMessages[friend[@"id"]]) {
        _newMessagesCount = MAX(0, _newMessagesCount - 1);
        [self decrementBadgeNumberForRow:1];
        _hasNewMessages[friend[@"id"]] = @NO;
    }

    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)setNewMessagesCount:(NSInteger)newMessagesCount {
    // singleton service for app badges, refresh on viewDidAppear in SideMenuUtilizer;
    // badges for 1on1 messages, cloud messages, friend requests;
    //self.newMessagesCount = newMessagesCount;
    [self setBadgeNumber:newMessagesCount forRow:1];
}

-(void)loadUserImages {
	
	for (NSDictionary *user in _chats) {
		
		NSString *imageId = [(NSArray *)[user valueForKey:@"pictures"] firstObject];
		
		if ([_userImageHashMap valueForKey:[user valueForKey:@"id"]] == nil) {

			UIImageView *tempIW = [[UIImageView alloc] initWithFrame:CGRectMake(-54, -54, 54, 54)];
			tempIW.alpha = 0;
			
			objc_setAssociatedObject(tempIW, &userImageKey, user, OBJC_ASSOCIATION_ASSIGN);
			
			[self.view addSubview:tempIW];
			
			if (imageId != nil) {
				
				AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:imageId imageView:tempIW useActivityIndicator:NO];
				loader.delegate = self;
				[loader loadImage];
				
			} else {
				
				[tempIW setDefaultImageWithFirstName:[user valueForKey:@"nameFirst"] lastName:[user valueForKey:@"nameLast"]]; //check!
				[_userImageHashMap setValue:tempIW.image forKey:[user valueForKey:@"id"]];
				
				[tempIW removeFromSuperview];
				
				NSUInteger row = [[_filteredChats valueForKey:@"id"] indexOfObject:[user valueForKey:@"id"]];
				
				if (row == NSNotFound) {
					return;
				}
				
				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
				[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
				
			}
			
		}
		
	}
	
}

-(void)loadSuccess:(UIImageView *)imageView {
	
	NSDictionary *chat = objc_getAssociatedObject(imageView, &userImageKey);
	
	NSInteger row = [_chats indexOfObject:chat];
	
	if (row == NSNotFound) {
		return;
	}
	
	[_userImageHashMap setValue:imageView.image forKey:[[_chats objectAtIndex:row] valueForKey:@"id"]];
	
	row = [_filteredChats indexOfObject:chat];
	
	if (row == NSNotFound) {
		return;
	}

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
	
	[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
	
	[imageView removeFromSuperview];
	imageView = nil;
	
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:YES animated:YES];
	return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	[self filterFriendList:searchText];
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
	[searchBar setShowsCancelButton:NO animated:YES];
	return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:NO animated:YES];
	_filterInput.text = @"";
	[self.view endEditing:YES];
	_filteredChats = [NSMutableArray arrayWithArray:_chats];
	[_tableView reloadData];
}

-(void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:NO animated:YES];
	[self.view endEditing:YES];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:NO animated:YES];
	[self.view endEditing:YES];
}

-(void)filterFriendList:(NSString *) filter {
	
	NSPredicate *predicate;
	
	if ([filter isEqualToString:@""]) {
		
		predicate = [NSPredicate predicateWithValue:YES];
		
	} else if ([filter containsString:@" "]) {
		
		NSMutableArray *arr = [NSMutableArray arrayWithArray: [filter componentsSeparatedByString:@" "]];
		NSString *last = [arr lastObject];
		[arr removeLastObject];
		NSString *first = [arr componentsJoinedByString:@" "];
		
		if ([last isEqualToString:@""]) {
			predicate = [NSPredicate predicateWithFormat:@"nameFirst ENDSWITH[cd] %@", first];
		} else {
			predicate = [NSPredicate predicateWithFormat:@"(nameFirst ENDSWITH[cd] %@ && nameLast BEGINSWITH[cd] %@) || nameFirst CONTAINS[cd] %@", first, last, filter];
		}
		
	} else {
		
		predicate = [NSPredicate predicateWithFormat:@"nameFirst CONTAINS[cd] %@ || nameLast CONTAINS[cd] %@", filter, filter];
		
	}
	
	_filteredChats = [NSMutableArray arrayWithArray:[_chats filteredArrayUsingPredicate:predicate]];
	[_tableView reloadData];
	
}

-(void)errorLoadingImage:(UIImageView *)imageView {

}

-(IBAction)newChatClicked:(id)sender {
	
	ViewFriends *dvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PickFriends"];
	[dvc setSelectionType:@"single"];
	[dvc setTitle:@"Select friend"];
	dvc.delegate = self;
	[self presentViewController:dvc animated:YES completion:nil];
	
}

-(void)didChooseSingleFriend:(id)sender {
	
	NSDictionary *friend = (NSDictionary *)sender;
	
	if (friend == nil) {
		return;
	}
	
	[self addChatWithFriend:friend message:nil];
	
	OneOnOneChat *chatView = [self.storyboard instantiateViewControllerWithIdentifier:@"OneOnOneChat"];
	[chatView setFriend:(NSDictionary *)friend];

    if (_hasNewMessages[friend[@"id"]]) {
        _newMessagesCount = MAX(0, _newMessagesCount - 1);
        [self decrementBadgeNumberForRow:1];
        _hasNewMessages[friend[@"id"]] = @NO;
    }

    NSUInteger row = [[_chats valueForKey:@"id"] indexOfObject:[friend valueForKey:@"id"]];

    if (row != NSNotFound) {

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }

	[self.navigationController pushViewController:chatView animated:YES];

}

-(void)addChatWithFriend:(NSDictionary *)friend message:(NSDictionary *)message {
	
	NSUInteger chatIndex = [[_chats valueForKey:@"id"] indexOfObject:[friend valueForKey:@"id"]];
	
	if (chatIndex == NSNotFound) {
		
		NSMutableDictionary *chat = [[NSMutableDictionary alloc] initWithDictionary:friend copyItems:YES];
		[chat setValue:[message valueForKey:@"text"] ?: @"" forKey:@"lastMessage"];
		[chat setValue:[message valueForKey:@"time"] ?: @"" forKey:@"lastMessageTime"]; ////...
		[_chats insertObject:chat atIndex:0];
		
		[self loadUserImages]; ///pazit na indexe, searchbar mora bit prazan
		
	} else if (message != nil) {
		
		NSMutableDictionary *chat = [_chats objectAtIndex:chatIndex];
		[_chats removeObject:chat];
		
		NSString *text = [message valueForKey:@"text"];
		BOOL myMessage = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"] isEqualToString:[message valueForKey:@"user"]];

		if ([text isEqualToString:@""]) {
			text = [NSString stringWithFormat:@"%@ sent an image", myMessage ? @"You" : [friend valueForKey:@"nameFirst"]];
		} else if (myMessage) {
			text = [@"You: " stringByAppendingString:text];
		}

		[chat setValue:text forKey:@"lastMessage"];
		[chat setValue:[message valueForKey:@"time"] ?: @"" forKey:@"lastMessageTime"]; ////...time
		[_chats insertObject:chat atIndex:0];
		
	}
	
	if (message == nil) {
		[self searchBarCancelButtonClicked:_filterInput];
	} else {
		if (![_filterInput.text isEqualToString: @""]) { //check for nil?
			[self filterFriendList:_filterInput.text];
		} else {
			_filteredChats = [NSMutableArray arrayWithArray:_chats];
			[_tableView reloadData];
		}
	}
	
}

-(void)didChooseFriends:(id)sender {
	//// do nothing, should never be called
}

-(void)dealloc { // U SideMenuUtilizer!
	
	//[[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusNew1on1Message" object:nil];
	//[[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusFirstLoadSyncComplete" object:nil]; //if not synced?
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
}

@end
