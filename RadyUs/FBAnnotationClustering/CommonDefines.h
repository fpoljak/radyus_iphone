//
//  CommonDefines.h
//  Radyus
//
//  Created by Locastic MacbookPro on 17/03/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#ifndef CommonDefines_h
#define CommonDefines_h

#define TEST_DEFINE_VALUE 0

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define RUserLocationChangedNotification @"RUserLocationChangedNotification"
#define RLocationManagerErrorNotification @"RLocationManagerErrorNotification"
#define RLocationServicesDeniedNotification @"RLocationServicesDeniedNotification"

#endif /* CommonDefines_h */

