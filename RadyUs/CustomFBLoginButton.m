//
//  CustomFBLoginButton.m
//  Radyus
//
//  Created by Locastic MacbookPro on 17/09/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "CustomFBLoginButton.h"

@implementation CustomFBLoginButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)layoutSubviews {
	
	[super layoutSubviews];
	
	[self.layer setCornerRadius:self.frame.size.height / 2];
	[[self.layer.sublayers firstObject] setCornerRadius:self.frame.size.height / 2];
	
	CGFloat offsetX = (self.frame.size.width - 271) / 2;
	
	for (UIView *subview in self.subviews) {
		if ([subview isKindOfClass:[UIImageView class]]) {
			if (subview.frame.size.width < 30 && subview.frame.size.height < 30) {
				[subview setFrame:CGRectMake(subview.frame.origin.x + self.frame.size.height / 2 + subview.frame.size.height / 6 + offsetX,
											 subview.frame.origin.y + subview.frame.size.height / 6,
											 subview.frame.size.width * 2 / 3,
											 subview.frame.size.height * 2 / 3)];
				break;
			}
		}
	}
	
}

@end
