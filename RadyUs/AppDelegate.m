//
//  AppDelegate.m
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AppDelegate.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

#import "OneOnOneChat.h"
#import "CloudView.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    /*for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }*/
	
	[[NSURLCache sharedURLCache] setMemoryCapacity:1024*1024*16]; ////
	[[NSURLCache sharedURLCache] setDiskCapacity:1024*1024*32]; ////
	
	//NSLog(@"Memory cache usage: %ld", (unsigned long)[[NSURLCache sharedURLCache] currentMemoryUsage]);
	//NSLog(@"Disk cache usage: %ld", (unsigned long)[[NSURLCache sharedURLCache] currentDiskUsage]);
	
	[FBSDKLoginButton class];
	
	//fb app id 1582108762029815
    //fb app secret 213105f4f0354ea61463af4e323b25ea
	
	[[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:
																		 UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeNone
																									categories:nil]];
	
	[[UIApplication sharedApplication] registerForRemoteNotifications];
	
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
	
	[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
	[[UIBarButtonItem appearanceWhenContainedIn:[UIImagePickerController class], nil] setTintColor:[UIColor redColor]];
	
	//[Instabug startWithToken:@"a0b7752477550594c8d5e83e9580f94b" captureSource:IBGCaptureSourceUIKit invocationEvent:IBGInvocationEventShake];
	
    //[Fabric with:@[[Crashlytics class]]];

	if (launchOptions != nil) {
		
		NSDictionary *_notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]; // userInfo?

        if (_notification != nil && [_notification valueForKey:@"userInfo"] != nil) {
            _notification = [_notification valueForKey:@"userInfo"];
        }

        NSDictionary *notification = _notification;

		if (notification != nil) { // Launched from push notification

            //[self receivedNotificationTest:notification];
			//[self performSelector:@selector(receivedNotificationTest:) withObject:_notification afterDelay:3.0];

			NSNumber *type = [_notification valueForKey:@"type"];

            if (type == nil) {
                type = @0;
            }
			
			if ([type isEqualToNumber:@1]) {
				
				__strong NSString *userId = [notification valueForKey:@"user_id"];

                if (userId == nil) { //temp
                    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
                }

                double delayInSeconds = 5.0; //??
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);

                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                    OneOnOneChat *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OneOnOneChat"];
                    UINavigationController *navCtrl = (UINavigationController *)self.window.rootViewController;

                    [vc setFriendId:userId];

                    if (!([navCtrl.viewControllers.lastObject isKindOfClass:[OneOnOneChat class]] && [(OneOnOneChat *)navCtrl.viewControllers.lastObject friendId] == userId)) { //TODO: check id

                        if (navCtrl.viewControllers.lastObject.presentedViewController != nil) { // check if we should dismiss?

                            [navCtrl.viewControllers.lastObject.presentedViewController dismissViewControllerAnimated:NO completion:^{
                                [navCtrl pushViewController:vc animated:YES];
                            }];
                            
                        } else {
                            [navCtrl pushViewController:vc animated:YES];
                        }
                    }

                });
				
            } else if ([type isEqualToNumber:@2]) {

                __strong NSString *cloudId = [notification valueForKey:@"cloud_id"];

                if (cloudId == nil) { //temp
                    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
                }

                double delayInSeconds = 5.0; //??
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);

                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                    CloudView *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CloudView"];
                    UINavigationController *navCtrl = (UINavigationController *)self.window.rootViewController;

                    [vc setCloudId:cloudId];

                    if (navCtrl.viewControllers.lastObject.presentedViewController != nil) { // check if we should dismiss?

                        [navCtrl.viewControllers.lastObject.presentedViewController dismissViewControllerAnimated:NO completion:^{
                            [navCtrl pushViewController:vc animated:YES];
                        }];

                    } else {
                        [navCtrl pushViewController:vc animated:YES];
                    }

                });

                /*
                if (!([navCtrl.viewControllers.lastObject isKindOfClass:[CloudView class]])) { //TODO: check id

                    CloudView *cloudView = (CloudView *)navCtrl.viewControllers.lastObject;

                    NSString *_cloudId = [cloudView cloud] ? [[cloudView cloud] valueForKey:@"id"] : [cloudView cloudId];

                    if (_cloudId == cloudId) {
                        return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
                    }

                    if (navCtrl.viewControllers.lastObject.presentedViewController != nil) { // check if we should dismiss?

                        [navCtrl.viewControllers.lastObject.presentedViewController dismissViewControllerAnimated:NO completion:^{
                            [navCtrl pushViewController:vc animated:NO];
                        }];
                        
                    } else {
                        [navCtrl pushViewController:vc animated:NO];
                    }
                }*/
            }
			
		}
		
	}
	
	return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
	
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo { // not fired?

	//NSLog(@"received remote notofication: %@", userInfo); //{aps:{alert:{},badge:1,sound:'ping.aiff', messageFrom: 'Mate baturina', user_id: 'drwvrwvwqrv...'}}
	//[self receivedNotificationTest:userInfo];

    NSArray *notificationTypes = [NSArray arrayWithObjects:@"unknown", @"whisper", @"message", @"friend_request", @"mention", nil];

	NSInteger type = [([userInfo valueForKey:@"type"] ?: @0) integerValue];
    NSString *notificationName = [notificationTypes objectAtIndex: type];
	notificationName = [@"RadyusReceivedRemoteNotification_" stringByAppendingString:notificationName];
	[[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self userInfo:userInfo];
}


-(void)receivedNotificationTest:(NSDictionary *)userInfo {
	
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notification received" message:[NSString stringWithFormat:@"%@", userInfo] preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:close];

	UINavigationController *navCtrl = (UINavigationController *)self.window.rootViewController;
	if (navCtrl.viewControllers.lastObject.presentedViewController == nil) {
		[navCtrl.viewControllers.lastObject presentViewController:alert animated:YES completion:nil];
    } else {
        [navCtrl.viewControllers.lastObject.presentedViewController presentViewController:alert animated:YES completion:nil];
    }
	
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
	
	if (application.applicationState == UIApplicationStateActive) {
        //http://stackoverflow.com/questions/22085234/didreceiveremotenotification-fetchcompletionhandler-open-from-icon-vs-push-not
		[self application:application didReceiveRemoteNotification:userInfo];
        //[self receivedNotificationTest:_lastNotification];
        _lastNotification = nil;
    } else {
        _lastNotification = userInfo;
    }

	completionHandler(UIBackgroundFetchResultNoData); //temp
	
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	
	NSString *tokenString = [deviceToken base64EncodedStringWithOptions:0];//[[NSString alloc] initWithData:deviceToken encoding:NSUTF8StringEncoding];
	NSLog(@"Registered for remote notifications, token: %@", tokenString); ///ako je nil?? (ne bi triba nit)
	
	if ([[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"] == nil || ![tokenString isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"]]) {
		
		if ([[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"] != nil) { //device token changed
			[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"accessToken"];
		}
		
		[[NSUserDefaults standardUserDefaults] setValue:tokenString forKey:@"deviceToken"];
		[[NSUserDefaults standardUserDefaults] synchronize];
		[[NSNotificationCenter defaultCenter] postNotificationName:@"RadyusRegisteredForRemoteNotifications" object:nil];
		
	}

}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
	
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	
	NSLog(@"Failed to register for remote notificatins, reason: %@", error.localizedDescription);
#ifdef DEBUG
	NSLog(@"Debug mode, setting 'test' as deviceToken...");
	[[NSUserDefaults standardUserDefaults] setValue:@"test" forKey:@"deviceToken"];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"RadyusRegisteredForRemoteNotifications" object:nil];
#else
	[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"deviceToken"];
#endif
	[[NSUserDefaults standardUserDefaults] synchronize];
	
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
	
	BFURL *parsedUrl = [BFURL URLWithInboundURL:url sourceApplication:sourceApplication];
	if ([parsedUrl appLinkData]) {
		// this is an applink url, handle it here
		NSURL *targetUrl = [parsedUrl targetURL];
		[[[UIAlertView alloc] initWithTitle:@"Received link:"
									message:[targetUrl absoluteString]
								   delegate:nil
						  cancelButtonTitle:@"OK"
						  otherButtonTitles:nil] show];////////maknit?
	}
	
	return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
	
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	// [FBAppCall handleDidBecomeActive]; //old sdk

    if (_lastNotification != nil) {

        __strong NSDictionary *_notification = _lastNotification;
        _lastNotification = nil;

        if (_notification != nil && [_notification valueForKey:@"userInfo"] != nil) {
            _notification = [_notification valueForKey:@"userInfo"];
        }

        NSDictionary *notification = _notification;

        if (notification != nil) { // Launched from push notification

            //[self receivedNotificationTest:notification];
            //[self performSelector:@selector(receivedNotificationTest:) withObject:_notification afterDelay:3.0];

            NSNumber *type = [_notification valueForKey:@"type"];

            if (type == nil) {
                type = @0;
            }

            if ([type isEqualToNumber:@1]) {

                __strong NSString *userId = [notification valueForKey:@"user_id"];

                if (userId == nil) { //temp
                    [FBSDKAppEvents activateApp];
                    return;
                }

                double delayInSeconds = 1.0; //??
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);

                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                    OneOnOneChat *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OneOnOneChat"];
                    UINavigationController *navCtrl = (UINavigationController *)self.window.rootViewController;

                    [vc setFriendId:userId];

                    if (!([navCtrl.viewControllers.lastObject isKindOfClass:[OneOnOneChat class]] && [(OneOnOneChat *)navCtrl.viewControllers.lastObject friendId] == userId)) { //TODO: check id

                        if (navCtrl.viewControllers.lastObject.presentedViewController != nil) { // check if we should dismiss?

                            [navCtrl.viewControllers.lastObject.presentedViewController dismissViewControllerAnimated:NO completion:^{
                                [navCtrl pushViewController:vc animated:YES];
                            }];

                        } else {
                            [navCtrl pushViewController:vc animated:YES];
                        }
                    }

                });

            } else if ([type isEqualToNumber:@2]) {

                __strong NSString *cloudId = [notification valueForKey:@"cloud_id"];

                if (cloudId == nil) { //temp
                    [FBSDKAppEvents activateApp];
                    return;
                }

                double delayInSeconds = 1.0; //??
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);

                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                    CloudView *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CloudView"];
                    UINavigationController *navCtrl = (UINavigationController *)self.window.rootViewController;

                    [vc setCloudId:cloudId];

                    if (navCtrl.viewControllers.lastObject.presentedViewController != nil) { // check if we should dismiss?

                        [navCtrl.viewControllers.lastObject.presentedViewController dismissViewControllerAnimated:NO completion:^{
                            [navCtrl pushViewController:vc animated:YES];
                        }];

                    } else {
                        [navCtrl pushViewController:vc animated:YES];
                    }

                });

            }
            
        }
        
    }

	[FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
