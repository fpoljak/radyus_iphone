//
//  AddIcon.h
//  RadyUs
//
//  Created by Frane Poljak on 16/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "MapPin.h"
#import "SideMenuUtilizer.h"

@interface AddIcon : SideMenuUtilizer <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate>

@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;

@property (nonatomic, retain) UIViewController *parentVC;

@end
