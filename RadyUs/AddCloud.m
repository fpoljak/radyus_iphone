//
//  AddCloud.m
//  RadyUs
//
//  Created by Frane Poljak on 16/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AddCloud.h"
#import "ViewFriends.h"
#import "AddIcon.h"

@interface AddCloud ()
@end

@implementation AddCloud
@synthesize boundingMapRect;
@synthesize coordinate;
@synthesize radius;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loaded = NO;
    
    [_mapView setShowsUserLocation:NO];
    _mapStretched = NO;
	
	[_anonymusView setAlpha:0.0f];
    
    radius = 1000;
    _accessType = CAUndefined;
	_cloudUsers = [[NSMutableArray alloc] init];
	_radyusDefined = NO;
    _durationDefined = NO;
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
     /*
    for (UIButton *button in [_buttonContainer subviews]) {
        //NSLog(@"button");
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal]; //CFCFCF
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateSelected]; //4A4A4A
    }
    */
    
    //[[UIButton appearanceWhenContainedIn:[self class], [_buttonContainer class], nil] setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    //[[UIButton appearanceWhenContainedIn:[self class], [_buttonContainer class], nil] setTitleColor:[UIColor darkGrayColor] forState:UIControlStateSelected];
    
    _icon = [UIImage imageNamed:@"add_icon.png"]; //plus
    /*
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard:)];
    
    [tap setDelaysTouchesBegan:NO];
    
    [self.view addGestureRecognizer:tap];
    */
}

-(void)viewWillAppear:(BOOL)animated {
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
	[self setNeedsStatusBarAppearanceUpdate];
}

-(void)deselectButtonsExcept:(UIButton* )sender {
    for (UIButton *button in [_buttonContainer subviews]) {
        if (![button isEqual:sender]) {
            BOOL needsToStaySelected = NO;
            
            switch (button.tag) {
                case 1:
                {
                    needsToStaySelected = _cloudDescriptionTextView.text.length > 0;
                    break;
                }
                case 2:
                {
                    needsToStaySelected = _accessType != CAUndefined;
                    break;
                }
                case 3:
                {
                    needsToStaySelected = _radyusDefined;
                    break;
                }
                case 4:
                {
                    needsToStaySelected = _durationDefined;
                    break;
                }
    
                default:
                    break;
            }
            
            [button setSelected:needsToStaySelected];
        }
    }
}

-(IBAction)audienceButtonClick:(id)sender {
	if (_accessType == CAPublic) {
		[self showAnonymusView];
	}
    [self hideKeyboard:sender];
    UITabBarController *tb = (UITabBarController *)[[self childViewControllers] lastObject];
    [tb setSelectedIndex:0];
    [self deselectButtonsExcept:sender];
    [_audienceButton setSelected:YES];
    [_titleLabel setText:@"Add Access"];
}

-(IBAction)radiusButtonClick:(id)sender {
	[self hideAnonymusView];
    [self hideKeyboard:sender];
    UITabBarController *tb = (UITabBarController *)[[self childViewControllers] lastObject];
    [tb setSelectedIndex:1];
    [self deselectButtonsExcept:sender];
    [_radiusButton setSelected:YES];
    _radyusDefined = YES;
    [_titleLabel setText:@"Add Radyus"];
}

-(IBAction)timeButtonClick:(id)sender {
	[self hideAnonymusView];
    [self hideKeyboard:sender];
    UITabBarController *tb = (UITabBarController *)[[self childViewControllers] lastObject];
    [tb setSelectedIndex:2];
    [self deselectButtonsExcept:sender];
    [_timeButton setSelected:YES];
    _durationDefined = YES;
    [_titleLabel setText:@"Add Duration"];
}

-(IBAction)titleButtonClick:(id)sender {
	[self hideAnonymusView];
    [_cloudDescriptionTextView becomeFirstResponder];
    [self deselectButtonsExcept:sender];
    [_titleButton setSelected:YES];
    [_titleLabel setText:@"Add Text"];
}

-(IBAction)hideKeyboard:(id)sender {
    if (!_mapStretched) {
        [self stretchMap];
    }
    [self.view endEditing:YES];
}

-(void)hideAnonymusView {

	[UIView animateWithDuration:0.3f animations:^{
		[_anonymusView setAlpha:0.0f];
	}];
	
}

-(void)showAnonymusView {
	
	NSLog(@"Showing anonymusView...");
	[self.view bringSubviewToFront:_anonymusView];
	[UIView animateWithDuration:0.3f animations:^{
		[_anonymusView setAlpha:1.0f];
	}];
	
}

-(void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    if (_loaded) {
        return;
    }
    
    [_cloudDescriptionTextView becomeFirstResponder];
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;//10.0?
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;//prominit?
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    
    if (locationManager.location != nil) {
        
        [self initializeMap:locationManager.location.coordinate];
        
    } else {
        
        [locationManager startUpdatingLocation];
        
    }
    
    _loaded = YES;
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [manager stopUpdatingLocation];
    
    CLLocation *location = [locations lastObject];
    NSLog(@"found");
    [_mapView setCenterCoordinate:location.coordinate animated:YES];
    
    [self initializeMap:location.coordinate];
}

-(void)initializeMap:(CLLocationCoordinate2D)location {
    NSLog(@"initializeMap");
    
    _cloudLocation = location;
    
    [_mapView setCenterCoordinate:location animated:YES];
    
    _radiusCircle = [MKCircle circleWithCenterCoordinate:location radius:radius];
    [_mapView addOverlay:_radiusCircle];
    
    _addCloudPin = [[MapPin alloc] initWithCoordinates:location id:@"1234ABCDEF" placeName:@"Ana" description:@"Drinkopoly players wanted..." isInRange:YES];
    [_mapView addAnnotation:_addCloudPin];
    
    [self setMapBoundsToRadiusWithCenterIn:location];
    
}

-(void) setMapBoundsToRadiusWithCenterIn:(CLLocationCoordinate2D)location {
     /*
    if (location == nil) {
        location = _mapView.centerCoordinate;
    }
      */
    CGFloat fractionLatLon = _mapView.region.span.latitudeDelta / _mapView.region.span.longitudeDelta;
    CGFloat latDistance = (fractionLatLon > 1) ? radius * 2.4 * fractionLatLon : radius * 2.4;
    CGFloat lonDistance = (fractionLatLon > 1) ? radius * 2.4 : radius * 2.4 * fractionLatLon;
    
    //NSLog(@"fractionLatLon: %f, latDistance: %f, lonDistance: %f", fractionLatLon, latDistance, lonDistance);
    // small radiuses?
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, latDistance, lonDistance);
    
    [_mapView setRegion:region animated:YES];

}

-(void)stretchMap {
    [self.view sendSubviewToBack:_cloudDescriptionTextView];
    [self.view sendSubviewToBack:_cloudDescriptionPlaceholderView];
	[_counterLabel setHidden:YES];
    [UIView animateWithDuration:0.5f animations:^{
        [_mapView setFrame:CGRectMake(0, 64, self.view.frame.size.width, _containerView2.frame.origin.y - 109)];
        [_buttonContainer setFrame:CGRectMake(0, _containerView2.frame.origin.y - 45, self.view.frame.size.width, 45)];
    } completion:^(BOOL finished){
        _mapStretched = YES;
        [self setMapBoundsToRadiusWithCenterIn:_addCloudPin.coordinate];
    }];

}

-(void)returnMapToOriginalBounds{
    [UIView animateWithDuration:0.5f animations:^{
        [_mapView setFrame:CGRectMake(0, 64, self.view.frame.size.width, _cloudDescriptionTextView.frame.origin.y - 109)];
        [_buttonContainer setFrame:CGRectMake(0, _cloudDescriptionTextView.frame.origin.y - 45, self.view.frame.size.width, 45)];
    } completion:^(BOOL finished){
        _mapStretched = NO;
		[_counterLabel setHidden:NO];
        [self setMapBoundsToRadiusWithCenterIn:_addCloudPin.coordinate];
    }];
    
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleRenderer *radiusView = [[MKCircleRenderer alloc] initWithCircle:overlay];
        //radiusView.strokeColor = [UIColor colorWithRed:1.0f green:210.0f/255.0f blue:3.0f/255.0f alpha:30.0f/255.0f];
        radiusView.fillColor = [UIColor colorWithRed:1.0f green:210.0f/255.0f blue:3.0f/255.0f alpha:75.0f/255.0f];
        
        return radiusView;
        
    } else return nil;
    
}

-(void)pinTapped:(UITapGestureRecognizer *)sender {
    MKAnnotationView *pin = (MKAnnotationView *)sender.view;
    
    if ([pin.reuseIdentifier isEqualToString:_addCloudPin.unique_id]) {
        CGFloat x = [sender locationInView:sender.view].x;
        CGFloat y = [sender locationInView:sender.view].y;
        CGFloat w = sender.view.frame.size.width / 2;
        CGFloat h = sender.view.frame.size.height / 2;
        
        if (x > w - 16 && x < w + 16 && y > h - 16 && y < h + 16) {
            [self addIcon:pin];
        }
    }
   
}

-(void)refreshAddCloudPin {
    [_mapView removeAnnotation:_addCloudPin];
    [_mapView addAnnotation:_addCloudPin];
    //[_mapView viewForAnnotation:_addCloudPin];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {//nepotrebno?
        MKAnnotationView *ann = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"RadyUsUserLocationAnnotation"];
        
        if (ann) {
            return ann;
        }
        
        ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"RadyUsUserLocationAnnotation"];
        ann.enabled = YES;
        ann.draggable = NO;
        ann.canShowCallout = NO;
        
        CGSize pinSize = CGSizeMake(33, 43);
        
        UIGraphicsBeginImageContext(pinSize);
        
        UIImage *cloud = [UIImage imageNamed:@"user_location_pin.png"];
        UIImage *icon = [UIImage imageNamed:@"radyus_icon.png"];
        
        [icon drawInRect:CGRectMake(3, 8, 26, 26)];
        [cloud drawInRect:CGRectMake(0, 0, 33, 43)];
        
        UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        
        [ann setImage: combined];
        ann.layer.anchorPoint = CGPointMake(0.5f, 1.0f);
        
        return ann;
    }
    
    if ([annotation isKindOfClass:[MapPin class]]) {
         /*
        MKAnnotationView *ann = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:[(MapPin *)annotation unique_id]];
        
        UITapGestureRecognizer *pinTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pinTapped:)];
        
        if (ann) {
            [ann addGestureRecognizer:pinTap];
            return ann;
        }
          */// NSLog(@"%@", [(MapPin *)annotation unique_id]);
        MKAnnotationView *ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[(MapPin *)annotation unique_id]];
        ann.enabled = YES;
        ann.draggable = NO;
        ann.canShowCallout = NO;
        UITapGestureRecognizer *pinTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pinTapped:)];//
        [ann addGestureRecognizer:pinTap];
        
        
        UIImage *cloud = [UIImage imageNamed:@"map_cloud.png"]; //drugi cloud icon?
        
        UIGraphicsBeginImageContext(CGSizeMake(cloud.size.width, cloud.size.height));
        
        [cloud drawInRect:CGRectMake(0, 0, cloud.size.width, cloud.size.height)];
        CGFloat x = (cloud.size.width - _icon.size.width) / 2;
        CGFloat y = (cloud.size.height - _icon.size.height) / 2;
        [_icon drawInRect:CGRectMake(x, y, _icon.size.width, _icon.size.height)];//izračunat
        
        UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        
        [ann setImage: combined];
        //[ann setCenterOffset:CGPointMake(- cloud.size.width / 2, cloud.size.height / 2)];
        ann.layer.anchorPoint = CGPointMake(0.5f, 0.5f);
        //[ann setCalloutOffset:CGPointMake(-43, 30)];
        
        /*
         UIButton *iwButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
         [iwButton addTarget:self action:@selector(iwbutton:) forControlEvents:UIControlEventTouchUpInside];
         ann.rightCalloutAccessoryView = iwButton;
         */
        
        return  ann;
        
    }
    
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addIcon:(id)sender {
    [self performSegueWithIdentifier:@"CloudAddIcon" sender:sender];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"CloudAddIcon"]) {
        AddIcon *dvc = [segue destinationViewController];
        [dvc setParentVC:self];
    } else if ([[segue identifier] isEqualToString:@"CloudAddFriends"]) {
        ViewFriends *dvc = [segue destinationViewController];
        [dvc setDelegate:[[[self.childViewControllers objectAtIndex:0] childViewControllers] objectAtIndex:0]];
        NSString *selectionType = (NSString *)sender;
        [dvc setSelectionType:selectionType];
    }
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    if (_mapStretched) {
        [self returnMapToOriginalBounds];
    }
    
}

-(void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length > 0) {
        [textView setBackgroundColor:self.view.backgroundColor];
    } else {
        [textView setBackgroundColor:[UIColor clearColor]];
    }
    
    [_counterLabel setText:[NSString stringWithFormat:@"%lu", 150 - (unsigned long)textView.text.length]];
    
    [self validateForm];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //NSLog(@"range length: %ld, text length: %ld", range.length, textView.text.length);
    
    if (range.length == 0 && (textView.text.length > 149 || [text isEqualToString:@"\n"])) { //max 150 znakova?
        return NO;
    }
    
    return YES;
}

-(IBAction)createCloud:(id)sender {
	[_apiService createCloudWithName:@"some name" description:_cloudDescriptionTextView.text centerLat:_cloudLocation.latitude centerLng:_cloudLocation.longitude
							  radius:radius duration:_cloudDuration anonymus:[_anonymusSwitch isOn] && _accessType == CAPublic public:_accessType == CAPublic users:_cloudUsers]; // icon, smallicon
}

-(void)cloudCreated:(NSJSONSerialization *)response {
	//
	NSLog(@"Cloud created, name: %@", [response valueForKey:@"name"]);
	[self dismiss:response];
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	////
}

-(BOOL)shouldDisableUserInteraction {
	return YES;
}

-(IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
		//NSLog(@"Sender class %@", NSStringFromClass([sender class]));
		if (sender != nil && ![sender isKindOfClass:[UIButton class]]) { //||nsarray?, not uibutton, tag !=...
			if ([_delegate respondsToSelector:@selector(cloudCreated:)]) {
    			[_delegate cloudCreated:(NSJSONSerialization *)sender];
			}
		}
    }];
}

-(BOOL) validateForm {

    if ([_cloudDescriptionTextView.text isEqualToString:@""]) {
		[_createButton setAlpha:0.3f];
		[_createButton setEnabled:NO];
        return NO;
    }
    
    if (_accessType == CAUndefined) {
        return NO;
    }
    
    if (!_radyusDefined) {
        return NO;
    }

    if (!_durationDefined && _accessType != CA1on1) {
        return NO;
    }

	[_createButton setAlpha:1.0f];
	[_createButton setEnabled:YES];
    return YES;

}

-(void)viewWillDisappear:(BOOL)animated {
	if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
		[_apiService cancelRequests];
	}
	[super viewWillDisappear:animated];
}

@end

