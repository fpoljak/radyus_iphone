//
//  Archive.h
//  Radyus
//
//  Created by Locastic MacbookPro on 30/07/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuUtilizer.h"
#import "DBManager.h"
#import "CloudView.h"

@interface Archive : SideMenuUtilizer <UISearchBarDelegate>

@property (nonatomic, retain) DBManager *dbManager;
@property (nonatomic, retain) NSMutableArray *clouds;
@property (nonatomic, retain) NSMutableArray *filteredClouds;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) NSDictionary *selectedCloud;
@property (nonatomic) BOOL nonArchive;

@property (nonatomic, retain) UISearchBar *searchBar;

@end
