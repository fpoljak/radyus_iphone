//
//  ChatView.h
//  RadyUs
//
//  Created by Frane Poljak on 03/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatSingleMessage.h"
#import "RoundedButton.h"
#import "DBManager.h"

@interface ChatView : UIScrollView  <AsyncImageLoadDelegate>

-(void)displayMessage:(NSDictionary *)message;
-(void)displayUserMessage:(NSDictionary *)message;
-(void)displayAppMessage:(NSDictionary *)message;
-(void)clearChatView;
-(IBAction)addPhoto:(id)sender;
-(void)postMessage:(id)sender withTime:(NSString *)time;
-(void)showMessageButton;
-(void)addThumbnail:(id)sender;
-(void)removeThumbnail:(id)sender;
-(void) updateUploadProgress:(CGFloat)progress;
-(void) enterLoadPreviousMode;
-(void) leaveLoadPreviousMode;

- (UIViewController *)parentViewController;

@property (nonatomic, retain) UIImage *imageToDisplay;
@property (nonatomic) CGFloat currentY;
@property (nonatomic) NSInteger messageCounter;
@property (nonatomic, retain) IBOutlet UILabel *counterLabel;
@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *lastUserID;
@property (nonatomic, retain) NSString *tempLastUserID;
@property (nonatomic, retain) NSString *lastMessageDate;
@property (nonatomic, retain) NSString *tempLastMessageDate;
@property (nonatomic, retain) NSString *firstMessagedate;
@property (nonatomic, retain) NSString *tempFirstMessageDate;
@property (nonatomic) CGFloat tempCurrentYOffset;
@property (nonatomic, retain) UIButton *messageButton;
@property (nonatomic, retain) UITapGestureRecognizer *thumbnailTap;
@property (nonatomic) BOOL isInLoadPreviousMode;
@property (nonatomic) BOOL isBeingSwiped;
@property (nonatomic) BOOL chatviewRepositioning;
@property (nonatomic, retain) NSMutableArray *messageBuffer;
@property (nonatomic, retain) ChatSingleMessage *firstMessage;
@property (nonatomic, retain) ChatSingleMessage *lastMessage;
@property (nonatomic, weak) ChatSingleMessage *tempLastMessage;
@property (nonatomic, weak) ChatSingleMessage *tempFirstMessage;
@property (nonatomic, weak) ChatSingleMessage *tempMessage;

@property (nonatomic, retain) NSDateFormatter *dateFormatter;
@property (nonatomic, retain) NSCalendar *calendar;

@property (nonatomic, retain) UIProgressView *progressView;

@property (nonatomic) BOOL firstLoad;

//@property (nonatomic, strong) DBManager *dbManager;

@end
