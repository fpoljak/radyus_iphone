//
//  MaleFemaleSegmentedControl.m
//  RadyUs
//
//  Created by Frane Poljak on 03/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "MaleFemaleSegmentedControl.h"

@implementation MaleFemaleSegmentedControl

-(void)setupAppearance {
	
	[self setTranslatesAutoresizingMaskIntoConstraints:YES];
	[self setFrame:CGRectMake(self.frame.origin.x, 4.5f, self.frame.size.width, 41)];
	
	CGRect mframe = CGRectMake(42, 12, 15, 16);
	UIImageView *male = [[UIImageView alloc] initWithFrame:mframe];
	[male setImage:[UIImage imageNamed:@"male.png"]];
	[[[self subviews] objectAtIndex:1] addSubview:male];
	
	CGRect fframe = CGRectMake(36, 11, 11, 18);
	UIImageView *female = [[UIImageView alloc] initWithFrame:fframe];
	[female setImage:[UIImage imageNamed:@"female.png"]];
	[[[self subviews] objectAtIndex:0] addSubview:female];
	
	CGRect rect1 = CGRectMake(0, 0, 1, 41);//[[[self subviews] objectAtIndex:1] frame];
	UIGraphicsBeginImageContextWithOptions(rect1.size, NO, 1.0);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
	CGContextFillRect(context, rect1);
	UIImage *blank1 = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	CGRect rect2 = [[[self subviews] objectAtIndex:1] frame];
	UIGraphicsBeginImageContextWithOptions(rect2.size, NO, 1.0);
	context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
	CGContextFillRect(context, rect1);
	UIImage *blank2 = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	[self setDividerImage:blank1 forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
	[self setDividerImage:blank1 forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	
	UIImage *selected = [[UIImage imageNamed:@"tab_selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	[self setBackgroundImage:selected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
	[self setBackgroundImage:blank2 forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	
}

-(void)awakeFromNib {
    [super awakeFromNib];
	[self setupAppearance];
}

-(instancetype)init {
	if (self = [super init]) {
		[self setupAppearance];
	}
	return self;
}

@end
