//
//  TopAlignedUILabel.h
//  test3
//
//  Created by Frane Poljak on 23/01/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopAlignedUILabel : UILabel

@end
