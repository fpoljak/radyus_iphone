//
//  Profile.m
//  RadyUs
//
//  Created by Frane Poljak on 04/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "Profile.h"
#import "OneOnOneChat.h"
#import "UIImageView+NoAvatar.h"
#import "ZoomableImageView.h"
#import "ViewFriends.h"

@interface Profile () <UIGestureRecognizerDelegate, UIScrollViewDelegate, ApiServiceDelegate, AsyncImageLoadDelegate, TransparentDialogDelegate>

@end

@implementation Profile

@synthesize originY;

static char cloudKey;
static char cloudUserKey;
static char cloudUserListKey;

//CGRect originalFrame;
CGPoint panStartLocation;
bool pageControlBeingUsed;
CGFloat offsetY = 0;

//UIPanGestureRecognizer *imagePan;

- (void)viewDidLoad {
	
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
    pageControlBeingUsed = NO;
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	[_cloudListHeaderLabel setHidden:YES];

	_indexOfCloudBeingLoaded = 0;
	_arrayOfCloudUsersArrays = [[NSMutableArray alloc] init];

	_imageView.image = nil;
	
	[_dropdownButton setEnabled:NO];
	
	_headerHeight = 600;
	
	[_editProfileButton setHidden:YES];
	
	[_friendsLabel setUserInteractionEnabled:NO];
	[_friendsNumberLabel setUserInteractionEnabled:NO]; ///////
	
	[_userInfoContainer setBackgroundColor:UIColorFromRGB(0xF9F9F9)];
	
	[_backgroundView.panGestureRecognizer requireGestureRecognizerToFail:self.menuSlideGesture];
	
	if (_userInfo == nil) {
		[self getUserInfo];
	} else { //handle user info update
		if ([[_userInfo valueForKey:@"id"] isEqualToString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]]) {
			_userInfo = [[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"];
		}
		[self displayUser];
	}
	
}

-(void)getUserInfo {////////?
	
	if (_username != nil) {
		[_apiService getUsersByUsernameList:[NSArray arrayWithObject:_username]];
	} else if (_userID != nil && _userID != [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]) {
		[_apiService getUsersByIdList:[NSArray arrayWithObject:_userID]];
	} else {
		[_apiService getUserInfo:[[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"]];
	}
	
}

-(BOOL)shouldDisableUserInteraction {
	return _userInfo == nil;
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	
}

-(void)receivedUserInfo:(NSJSONSerialization *)userInfo {
	
	if (userInfo == nil) {
		
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Radyus" message:@"User no longer exists!" preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
			[self.navigationController popViewControllerAnimated:YES];
		}];
		
		[alert addAction:close];
		
		[self presentViewController:alert animated:YES completion:nil];

		return;
	}
	
	_userInfo = userInfo;

	[self displayUser];
	
}

-(void)apiService:(ApiService *)service receivedUserIdList:(NSArray *)userIdList {
	
	if ([service isEqual:_getFriendsService]) {
		
		_friendList = [NSMutableArray arrayWithArray:userIdList];
		[_friendsNumberLabel setText:[NSString stringWithFormat:@"%ld", (long)_friendList.count]];
		[_friendsLabel setUserInteractionEnabled:YES];
		[_friendsNumberLabel setUserInteractionEnabled:YES];
		
		if (![[_userInfo valueForKey:@"id"] isEqualToString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]]) {//not own profile
			[_getFriendsService getPendingRequests];
		} else {
			[[NSUserDefaults standardUserDefaults] setValue:[userIdList copy] forKey:@"userFriends"];
			[[NSUserDefaults standardUserDefaults] synchronize];
		}
	
	} else if (userIdList.count > 0){
		[service getUsersByIdList:userIdList];
	} else {
		[self apiService:service receivedUsersList:(NSJSONSerialization *)[NSArray new]];
	}
	
}

-(void)getPendingSuccess:(NSArray *)pending {

	_pending = [NSMutableArray arrayWithArray:pending];
	[_getFriendsService getRequestedFriends];
	
}

-(void)getRequestedSuccess:(NSArray *)requested {
	
	_requested = [NSMutableArray arrayWithArray:requested];
	
	if ([_friendList containsObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]]) {
		
		//is a friend
		_profileMode = UPFriend;
		
		[_apiService getCloudsByUserId:[_userInfo valueForKey:@"id"]];
		
		[_addFriendButton setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
		[_addFriendButton setTitle:@"Unfriend" forState:UIControlStateNormal];
		[_addFriendButton setTitle:@"Unfrined" forState:UIControlStateHighlighted];
		
		[_addFriendButton addTarget:self action:@selector(unfriend) forControlEvents:UIControlEventTouchUpInside];
		
		[_cloudListHeaderLabel setHidden:NO];
		
	} else if ([_requested containsObject:[_userInfo valueForKey:@"id"]]) {
		
		_profileMode = UPRequested;
		
		[_addFriendButton setBackgroundColor:UIColorFromRGB(0x50E3C2)]; //CFCFCF
		[_addFriendButton setTitle:@"Cancel request" forState:UIControlStateNormal];
		[_addFriendButton setTitle:@"Cancel request" forState:UIControlStateHighlighted];

		[_addFriendButton addTarget:self action:@selector(unfriend) forControlEvents:UIControlEventTouchUpInside];
		
	} else if ([_pending containsObject:[_userInfo valueForKey:@"id"]]) {
	
		_profileMode = UPPending;
		
		[_addFriendButton setBackgroundColor:UIColorFromRGB(0x50E3C2)]; //CFCFCF
		[_addFriendButton setTitle:@"Accept request?" forState:UIControlStateNormal];
		[_addFriendButton setTitle:@"Accept request?" forState:UIControlStateHighlighted];
		
		[_addFriendButton addTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside]; //answer
		
	} else {
		
		_profileMode = UPOther;
		
		[_dropdownButton setEnabled:YES];
		[_cloudListHeaderLabel setText:@""];
		[_addFriendButton addTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside];
	}
	
}


-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {
	
	if (_userInfo == nil) {
		[self receivedUserInfo:(NSJSONSerialization *)[(NSArray *)usersList firstObject]];
		return;
	}
	
	NSArray *cloudUsers = (NSArray *)usersList;
	
	[_arrayOfCloudUsersArrays addObject:cloudUsers];
	
	if ([_scrollView viewWithTag:1000 + _indexOfCloudBeingLoaded] != nil) {
		[self displayCloudUsers:cloudUsers inView:[_scrollView viewWithTag:1000 + _indexOfCloudBeingLoaded]];
	}
	
	if (++_indexOfCloudBeingLoaded < _cloudsCount) {
		[service getCloudUsers:(NSString *)[[(NSArray *)_cloudList objectAtIndex:_indexOfCloudBeingLoaded] valueForKey:@"id"]];
	}

}

-(void)receivedCloudIdList:(NSArray *)cloudIdList {
	//[_apiService getCloudsByIdList:cloudIdList];
	////
	NSJSONSerialization *cloudList = (NSJSONSerialization *)[NSDictionary dictionaryWithObject:cloudIdList forKey:@"data"];
	////
	[self apiService:_apiService receivedCloudList:cloudList];
}

-(void)apiService:(ApiService *)service receivedCloudList:(NSJSONSerialization *)cloudList {

	_cloudList = [cloudList valueForKey:@"data"];
	_cloudsCount = [(NSArray *)_cloudList count];
	
	[self performSelector:@selector(drawCloudList:) withObject:(NSArray *)_cloudList afterDelay:0.25]; //ovo je bilo zbog resizeanja headera, maknit delay?
	//[self drawCloudList:(NSArray *)_cloudList];
	[_cloudListHeaderLabel setText:[NSString stringWithFormat:@"%@ is currently in %ld radyus(es)", [_userInfo valueForKey:@"nameFirst"], (long)[(NSArray *)[_cloudList valueForKey:@"data"] count]]];
	
}

-(void)addFriend {
	[self performSegueWithIdentifier:@"HandleFriendRequests" sender:nil];
}

-(void)unfriend {
	[self performSegueWithIdentifier:@"HandleFriendRequests" sender:nil];
}

-(void)confirmSelected{
	if (_profileMode == UPFriend || _profileMode == UPRequested) {
		[_apiService unfriendUser:[_userInfo valueForKey:@"id"]];
	} else {
		[_apiService addFriend:[_userInfo valueForKey:@"id"]];
	}
}

-(void)cancelSelected{
	////
}

-(void)addFriendSuccess:(NSJSONSerialization *)response {
	
	if ([_pending containsObject:[_userInfo valueForKey:@"id"]]) {
		
		[_addFriendButton setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
		[_addFriendButton setTitle:@"Unfriend" forState:UIControlStateNormal];
		[_addFriendButton setTitle:@"Unfrined" forState:UIControlStateHighlighted];
		[_friendList addObject:[_userInfo valueForKey:@"id"]];
		[_pending removeObject:[_userInfo valueForKey:@"id"]];
		[_friendsNumberLabel setText:[NSString stringWithFormat:@"%ld", (long)_friendList.count]];
		
		_profileMode = UPFriend;
		
		[_apiService getCloudsByUserId:[_userInfo valueForKey:@"id"]];
		
	} else {
		
		[_addFriendButton setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
		[_addFriendButton setTitle:@"Cancel request" forState:UIControlStateNormal];
		[_addFriendButton setTitle:@"Cancel request" forState:UIControlStateHighlighted];
		
		_profileMode = UPRequested;
	}

	[_addFriendButton removeTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside];
	[_addFriendButton addTarget:self action:@selector(unfriend) forControlEvents:UIControlEventTouchUpInside];
	
}

-(void)unfriendSuccess:(NSJSONSerialization *)response {

	if (_profileMode == UPFriend) {
		[_friendList removeObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
		[_friendsNumberLabel setText:[NSString stringWithFormat:@"%ld", (long)_friendList.count]];
	}
	
	[_addFriendButton setBackgroundColor:UIColorFromRGB(0xFFD203)];
	[_addFriendButton setTitle:@"Add as friend" forState:UIControlStateNormal];
	[_addFriendButton setTitle:@"Add as friend" forState:UIControlStateHighlighted];

	[_addFriendButton removeTarget:self action:@selector(unfriend) forControlEvents:UIControlEventTouchUpInside];
	[_addFriendButton addTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside];

	_profileMode = UPOther;
	
	[self clearCloudList];
	
}

-(void)displayUser {
	
	_getFriendsService = [[ApiService alloc] init];
	_getFriendsService.delegate = self;
	[_getFriendsService getFriends:[_userInfo valueForKey:@"id"]];
	
	if ([[_userInfo valueForKey:@"id"] isEqualToString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]]) {
	
		//own profile
		_profileMode = UPOwn;
		[_cloudListHeaderLabel setHidden:NO];
		[self clearCloudList];
		[_apiService getCloudsByUserId:[_userInfo valueForKey:@"id"]];
		[_addFriendButton setHidden:YES];
		[_sendMessageButton setHidden:YES];
		[_editProfileButton setHidden:NO];
		
	} else {
		[_dropdownButton setEnabled:YES];
	}
	
	NSString *name = [NSString stringWithFormat:@"%@ %@", [_userInfo valueForKey:@"nameFirst"], [_userInfo valueForKey:@"nameLast"]];
	[_nameField setText:name];
	
	NSString *ageString = @"";
	
	NSString *birthday = [_userInfo valueForKey:@"birthday"] == nil || [[_userInfo valueForKey:@"birthday"] isKindOfClass:[NSNull class]] ? @"" : [_userInfo valueForKey:@"birthday"];
	
	NSLog(@"birthday: %@", birthday);
	NSLog(@"User: %@", _userInfo);
	
	if (![birthday isEqualToString:@""]) {
		NSString *shortDate = [birthday substringToIndex:10];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"YYYY-MM-dd"];
		NSDate *date = [dateFormatter dateFromString:shortDate];
		ageString = [NSString stringWithFormat:@"%ld years old. ", (long)[self ageFromBirthday:date]];
	}
	
	[_descriptionLabel setText:[ageString stringByAppendingString:[_userInfo valueForKey:@"description"] ?: @""]];
	
	CGFloat originalHeight = _descriptionLabel.frame.size.height;
	CGFloat minHeight = 36;
	CGFloat maxWidth = self.view.frame.size.width - 20;
	CGFloat maxHeight = 180;
	
	CGSize maxSize = CGSizeMake(maxWidth, maxHeight);
	CGSize requiredSize = [_descriptionLabel sizeThatFits:maxSize];
	CGFloat actualHeight = MAX(requiredSize.height, minHeight);
	
	_descriptionLabel.frame = CGRectMake(_descriptionLabel.frame.origin.x, _descriptionLabel.frame.origin.y, maxWidth, actualHeight);
	
	CGFloat growth = actualHeight - originalHeight;//MAX(minHeight - originalHeight, actualHeight - originalHeight);
	
	[_userInfoContainer setFrame:CGRectMake(_userInfoContainer.frame.origin.x, _userInfoContainer.frame.origin.y, _userInfoContainer.frame.size.width, _userInfoContainer.frame.size.height + growth)];
	//za own profile još smanjit!
	[self returnImageToOriginalPosition:nil];
	
	_images = (NSArray *)[_userInfo valueForKey:@"pictures"];
	
	_currentImageIndex = 0;
	[_pageControl setNumberOfPages:[_images count]];
	
	UITapGestureRecognizer *friendsNumberTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewFriends:)];
	UITapGestureRecognizer *friendsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewFriends:)];
	//friendsTap.cancelsTouchesInView = YES;
	[_cloudsCreatedLabel setText:[NSString stringWithFormat:@"%ld", (long)[[_userInfo valueForKey:@"cloudCreates"] integerValue]]];
	[_cloudsVisitedLabel setText:[NSString stringWithFormat:@"%ld", (long)[[_userInfo valueForKey:@"cloudVisits"] integerValue]]];
	[_friendsNumberLabel addGestureRecognizer:friendsNumberTap];
	[_friendsLabel addGestureRecognizer:friendsTap];
	
	UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewGallery:)];
	[_imageView addGestureRecognizer:imageTap];
	
	//imagePan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panImage:)];
	//[imagePan setDelegate:self];
	//[_imageView addGestureRecognizer:imagePan];
	
	_backgroundView.decelerationRate = UIScrollViewDecelerationRateFast;
	[_backgroundView setDelegate:self];
	
	[self initImages];
	
	/* //test, works
	AsyncImageLoadManager *test = [[AsyncImageLoadManager alloc] initWithImageUrl:@"https://scontent-vie1-1.xx.fbcdn.net/hphotos-xap1/v/t1.0-9/1496978_10156518733975648_4777691104504876979_n.jpg?oh=92375f01587d8e3e7cacb350ff9e26ef&oe=5786329D" imageView:(UIImageView *)_imageViews[0]  useActivityIndicator:YES];
	test.delegate = self;
	[test loadImage];
	*/
	
}

- (NSInteger)ageFromBirthday:(NSDate *)birthdate {
	NSDate *today = [NSDate date];
	NSDateComponents *ageComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:birthdate toDate:today options:0];
	return ageComponents.year;
}

-(void)initImages {
	
    _imageViews = [[NSMutableArray alloc] init];
	
    CGFloat width = _images.count * self.view.frame.size.width;
    [_backgroundView setContentSize:CGSizeMake(width, _backgroundView.frame.size.height)];
	
    for (int i = 0; i < _images.count; i++) {
		
        CGFloat w = self.view.frame.size.width;
        CGFloat h = self.view.frame.size.width * 9 / 16;
		
		CGRect imageFrame = CGRectMake(self.view.frame.size.width * i, 0, w, h);
		
		ZoomableImageView *imageView = [[ZoomableImageView alloc] init];
		
		if (i == _currentImageIndex) {
			imageView.frame = _imageView.frame;
			[_imageView removeFromSuperview];
			_imageView = imageView;
		}
		
		UIPanGestureRecognizer *swipeUpDown = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panImage:)];
		[swipeUpDown setDelegate:self];
		[imageView addGestureRecognizer:swipeUpDown];
		
		[imageView setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setAutoresizingMask:UIViewAutoresizingNone];
        [imageView setFrame:imageFrame];
		[imageView setClipsToBounds:YES];
        [imageView setUserInteractionEnabled:YES];

		AsyncImageLoadManager *imageLoader = [[AsyncImageLoadManager alloc] initWithImageId:[_images objectAtIndex:i] imageView:imageView useActivityIndicator:YES];
		imageLoader.delegate = self;
		[imageLoader loadImage];
		
        UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewGallery:)];
        [imageView addGestureRecognizer:imageTap];
        [_imageViews addObject:imageView];
        [_backgroundView addSubview:imageView];
		
    }
    
    [_backgroundView setContentOffset:CGPointMake(_currentImageIndex * self.view.frame.size.width, 0)];
    
    [_pageControl addTarget:self action:@selector(setCurrentImageWithPageControl:) forControlEvents:UIControlEventValueChanged];
    
    [self updateImage];
	
}

-(void)loadSuccess:(UIImageView *)imageView {
	////
}

-(IBAction)showActionSheet:(id)sender{
	
	UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"User actions" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
	
	
	UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"Report user" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		//
	}];
	
	UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
	
	[actionSheet addAction:reportAction];
	[actionSheet addAction:cancelAction];
	
	[self presentViewController:actionSheet animated:YES completion:nil];
	
}

-(IBAction)panImage:(UIPanGestureRecognizer *)sender {
	
	/*
	if ([sender isEqual:nil]) { // should not happen!
		//sender = imagePan;
		return;
    }
    */
	
    if (sender.state == UIGestureRecognizerStateBegan) {
        panStartLocation = [sender locationInView:_backgroundView];
        //[_userImageView setAlpha:0.4f];
        [_backgroundView setScrollEnabled:NO];
    }
    else if (sender.state == UIGestureRecognizerStateEnded) {
        
        CGPoint stopLocation = [sender locationInView:_backgroundView];
        CGFloat dy = stopLocation.y - panStartLocation.y;
        CGPoint velocity = [sender velocityInView:sender.view];
		
        if (dy > 160 || velocity.y > 1600) {
            [self performSelector:@selector(viewGallery:) withObject:sender afterDelay:0.1f];
        } else {
            [self returnImageToOriginalPosition:sender];
        }
        
        [_backgroundView setScrollEnabled:YES];
        
    } else if (sender.state == UIGestureRecognizerStateChanged) { //swipe in progress
        
        CGPoint currentLocation = [sender locationInView:sender.view];
        CGFloat dy = currentLocation.y - panStartLocation.y;
        
        if (dy < 20 || dy >= self.view.frame.size.width * 9 / 16) {
            return;
        }
        
        CGFloat x = -8 * dy / 9; // + self.view.frame.size.width * _currentImageIndex (ali content offset se pomakne)
        CGFloat y = 0;
        CGFloat w = self.view.frame.size.width + 16 * dy / 9;
        CGFloat h = self.view.frame.size.width * 9 / 16 + dy;
        
        sender.view.frame = CGRectMake(x, y, w, h);
        
        [_pageControl setFrame:CGRectMake(_pageControl.frame.origin.x, sender.view.frame.size.height - 30, _pageControl.frame.size.width, _pageControl.frame.size.height)];
        [_backgroundView setFrame:CGRectMake(0, 0, self.view.frame.size.width, h)];
        [_backgroundView setBounds:CGRectMake(0, 0, self.view.frame.size.width, h)];
        
        [_userInfoContainer setFrame:CGRectMake(0, sender.view.frame.origin.y + sender.view.frame.size.height, _userInfoContainer.frame.size.width, _userInfoContainer.frame.size.height)];
		
	}
   
}

-(IBAction)viewGallery:(UIGestureRecognizer *)sender {
	
	if (_images.count > 0) { //check if already presenting?
		[self performSegueWithIdentifier:@"ViewGallery" sender:sender];
	} else {
		[self returnImageToOriginalPosition:sender];
	}
	
}

-(IBAction)returnImageToOriginalPosition:(UIGestureRecognizer *)sender {
	
	UIImageView *imageToAdjust;
	
    if (sender == nil) {
		imageToAdjust = _imageViews.count > 0 ? [_imageViews objectAtIndex:_currentImageIndex] : _imageView;
	} else {
		imageToAdjust = (UIImageView *)sender.view;
	}
	
	[imageToAdjust setContentMode:UIViewContentModeScaleAspectFill];
    [imageToAdjust setAutoresizingMask:UIViewAutoresizingNone];
    [_backgroundView setAutoresizesSubviews:NO];
    [_backgroundView setClipsToBounds:YES];
	
	CGRect frame = [self originalFrame];
	
    [UIView animateWithDuration:0.2f animations:^{
        
		imageToAdjust.frame = CGRectMake(frame.origin.x, 0, frame.size.width, frame.size.height); 
        [_backgroundView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [_backgroundView setBounds:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [_backgroundView setContentSize:CGSizeMake(_backgroundView.contentSize.width, frame.size.height)];
        [_backgroundView setContentOffset:CGPointMake(frame.origin.x, 0) animated:NO];
        
        [_pageControl setFrame:CGRectMake(_pageControl.frame.origin.x, frame.size.height - 30, _pageControl.frame.size.width, _pageControl.frame.size.height)];
        
        [_userInfoContainer setFrame:CGRectMake(0, frame.size.height, _userInfoContainer.frame.size.width, _userInfoContainer.frame.size.height)];
		[_tableViewHeader setFrame:CGRectMake(0, _tableViewHeader.frame.origin.y, _tableViewHeader.frame.size.width, _userInfoContainer.frame.origin.y + _userInfoContainer.frame.size.height)];
        
    } completion:^(BOOL finished){
		
		[_pageControl setFrame:CGRectMake(_pageControl.frame.origin.x, frame.size.height - 30, _pageControl.frame.size.width, _pageControl.frame.size.height)];
        [imageToAdjust setContentMode:UIViewContentModeScaleAspectFill];
        [imageToAdjust setAutoresizingMask:UIViewAutoresizingNone];
        [_backgroundView setAutoresizesSubviews:NO];
        [_backgroundView setClipsToBounds:YES];
   
    }];
	
	[_scrollView setContentSize:CGSizeMake(self.view.frame.size.width, MAX(_tableViewHeader.frame.origin.y + _tableViewHeader.frame.size.height, _scrollView.contentSize.height))];
	
}


-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
	
	/*
	if ([gestureRecognizer isEqual:_backgroundView.panGestureRecognizer]) {
		NSLog(@"Pan location (x): %f", [(UIPanGestureRecognizer *)gestureRecognizer locationInView:_backgroundView].x);
		return [(UIPanGestureRecognizer *)gestureRecognizer locationInView:_backgroundView].x > 20;
	}
	*/
	
	if ([gestureRecognizer.view isKindOfClass:[UIImageView class]]) {
        CGPoint velocity = [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:gestureRecognizer.view];
        return velocity.y > 2 * fabs(velocity.x);
    }
	
	return YES;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	
	if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
		return [otherGestureRecognizer.view isEqual:_backgroundView];
	}
	
	return !([[gestureRecognizer view] isKindOfClass:[UIImageView class]] && [[otherGestureRecognizer view] isEqual:_scrollView]);
	
}


-(IBAction)viewFriends:(id)sender {
    [self performSegueWithIdentifier:@"ViewFriends" sender:sender];
}

/*
-(void)sidemenuWillShow {
	[super sidemenuWillShow];
	//NSLog(@"gallery frame: %@", NSStringFromCGRect(self.containerView.frame));
}

-(void)sidemenuDidShow {
	[super sidemenuDidShow];
	//NSLog(@"gallery frame: %@", NSStringFromCGRect(self.containerView.frame));
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)errorLoadingImage:(UIImageView *)imageView {
	/////
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
	
	self.title = self.previousTitle ?: self.title;
	
	CGRect frame = [self originalFrame];
	
	[_userInfoContainer setFrame:CGRectMake(0, _imageView.frame.origin.y + _imageView.frame.size.height, _userInfoContainer.frame.size.width, _userInfoContainer.frame.size.height)];
    [_backgroundView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [_backgroundView setBounds:CGRectMake(0, 0, frame.size.width, frame.size.height)];
	
	_headerHeight = _userInfoContainer.frame.size.height + _imageView.frame.size.height;
	
    [_backgroundView.superview bringSubviewToFront:_pageControl];
    
    if (_imageView.frame.size.height != self.view.frame.size.height * 9 / 16) {
        [self returnImageToOriginalPosition:nil];
    }
    
    originY = 0;
	
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {

	if (pageControlBeingUsed) {
        return;
    }
	
    CGFloat pageWidth = _backgroundView.frame.size.width;
    int page = floor((_backgroundView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page > - 1 && page < _images.count) {
        [self setCurrentImage:page];
    }
	
}

-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    
    //za velik velocity povećat targetcontentoffset
    
    NSUInteger nearestIndex = (NSUInteger)(targetContentOffset->x / scrollView.bounds.size.width + 0.5f);
    nearestIndex = MAX( MIN( nearestIndex, _images.count - 1 ), 0 );
    
    CGFloat xOffset = nearestIndex * scrollView.bounds.size.width;
    //nepotrebno?
    xOffset = xOffset==0?1:xOffset;
    
    *targetContentOffset = CGPointMake(xOffset, targetContentOffset->y);

}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(!decelerate)
    {
        NSUInteger currentIndex = (NSUInteger)(scrollView.contentOffset.x / scrollView.bounds.size.width);
        [scrollView setContentOffset:CGPointMake(scrollView.bounds.size.width * currentIndex, 0) animated:YES];
    }
}


-(void)updateImage {
    [_pageControl setCurrentPage:_currentImageIndex];
}

-(IBAction)setCurrentImageWithPageControl:(UIPageControl *)sender {
    CGRect frame;
    frame.origin.x = self.view.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.self.view.frame.size;
    [_backgroundView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
    [self setCurrentImage:[sender currentPage]];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

-(void)setCurrentImage:(NSInteger)currentImage {
    _currentImageIndex = currentImage;
    [self updateImage];
}

-(CGRect) originalFrame {
    return CGRectMake(self.view.frame.size.width * _currentImageIndex, 0, self.view.frame.size.width, self.view.frame.size.width * 9 / 16);
}

-(void)drawCloudList:(NSArray *)cloudList {
	
	CGFloat currentY = _tableViewHeader.frame.origin.y + _tableViewHeader.frame.size.height;
	
	NSInteger index = 0;
	for (NSJSONSerialization *cloud in cloudList) {
		currentY += [self drawCloudListItem:cloud startingY:currentY index:index++];
	}
	
	[_scrollView setContentSize:CGSizeMake(self.view.frame.size.width, currentY)];
	
	//[_apiService getUsersByIdList:(NSArray *)[[(NSArray *)[_cloudList valueForKey:@"data"] objectAtIndex:0] valueForKey:@"users"]];
	
	if ([cloudList count] > 0) {
		[_apiService getCloudUsers:(NSString *)[[cloudList objectAtIndex:0] valueForKey:@"id"]];
	}
	
}


-(void)clearCloudList {
	
	_cloudsCount = 0;
	_indexOfCloudBeingLoaded = 0;
	[_arrayOfCloudUsersArrays removeAllObjects];
	
	if (_scrollView != nil) {
		for (__strong UIView *subview in _scrollView.subviews) {
			if (![subview isEqual:_tableViewHeader]) {
				[subview removeFromSuperview];
				subview = nil;
			}
		}
		[_scrollView setContentSize:_scrollView.frame.size];
	}
	
	[_cloudListHeaderLabel setText:@""];
}

-(CGFloat)drawCloudListItem:(NSJSONSerialization *)cloud startingY:(CGFloat)startingY index:(NSInteger)index{
	
	UIButton *listItem = [[UIButton alloc] init];
	[listItem setTag:index + 1000];
	[listItem setAutoresizesSubviews:NO];
	[listItem setBackgroundColor:UIColorFromRGB(0xF9F9F9)];
	
	UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 12, self.view.frame.size.width - 16, 1)];
	
	//background color, text color, font, font size, line break, max num of lines,...
	[descLabel setNumberOfLines:5];
	[descLabel setLineBreakMode:NSLineBreakByWordWrapping];
	[descLabel setPreferredMaxLayoutWidth:self.view.frame.size.width - 16];
	[descLabel setText:[cloud valueForKey:@"name"]];
	[descLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:21]];
	
	CGSize maxSize = CGSizeMake(self.view.frame.size.width - 16, CGFLOAT_MAX);
	CGSize requiredSize = [descLabel sizeThatFits:maxSize];
	[descLabel setFrame:CGRectMake(8, 12, requiredSize.width, requiredSize.height)];
	
	[listItem setFrame:CGRectMake(0, startingY, self.view.frame.size.width, descLabel.frame.size.height + 76)];
	
	[listItem addSubview:descLabel];
	
	int max_images = MIN(((int)self.view.frame.size.width - 58) % 50, 9);
	
	for (int i = 0; i < max_images; i++) { //calculate max
		UIImageView *avatar = [[UIImageView alloc] initWithFrame:CGRectMake(8 + 42 * i, descLabel.frame.size.height + 28, 34, 34)];
		[avatar setTag:i + 11];
		[avatar setBackgroundColor:[UIColor clearColor]];
		[avatar.layer setCornerRadius:17.0f];
		[avatar setClipsToBounds:YES];
		[listItem addSubview:avatar];
	}
	
	UIButton *viewAll = [[UIButton alloc] initWithFrame:CGRectMake(8 + 42 * 4, descLabel.frame.size.height + 28, 34, 34)];
	[viewAll setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
	[viewAll setTag:20];
	[viewAll.layer setCornerRadius:17.0f];
	[viewAll setHidden: YES];
	
	[listItem addSubview:viewAll];
	
	objc_setAssociatedObject(listItem, &cloudKey, cloud, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	
	[listItem addTarget:self action:@selector(cloudListItemSelected:) forControlEvents:UIControlEventTouchUpInside];
	[listItem addTarget:self action:@selector(highlightItem:) forControlEvents:UIControlEventTouchUpOutside|UIControlEventTouchDown|UIControlEventTouchCancel]; ////samo touchdown?
	
	[_scrollView addSubview: listItem];
	
	UIView *listDivider = [[UIView alloc] initWithFrame:CGRectMake(8, listItem.frame.size.height - 0.5f, listItem.frame.size.width - 8, 0.5f)];
	[listDivider setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
	[listItem addSubview:listDivider];
	
	return listItem.frame.size.height;
	
}

-(void)cloudListItemSelected:(UIButton *)sender {
	
	NSJSONSerialization *cloud = objc_getAssociatedObject(sender, &cloudKey);
	_selectedCloud = cloud;
	
	if (![[_selectedCloud valueForKey:@"inRange"] boolValue]) {
		[self performSegueWithIdentifier:@"ViewCloudUnavailableProfile" sender:sender];
	} else {
		[self performSegueWithIdentifier:@"ViewCloudProfile" sender:sender];
	}
	
}

-(void)highlightItem:(UIView *)sender {
	[sender setAlpha:0.75f];
	[self performSelector:@selector(deselectCloudListItem:) withObject:sender afterDelay:0.5f];
}


-(void)deselectCloudListItem:(UIView *)sender {
	[sender setAlpha:1.0f];
}

-(void)displayCloudUsers:(NSArray *)cloudUsersList inView:(UIView *) listItem {
	
	//NSLog(@"displayCloudUsers, count = %ld", (long)cloudUsersList.count);
	
	int max_images = MIN(((int)self.view.frame.size.width - 58) % 50, 9);;
	
	for (int i = 0; i < max_images && i < cloudUsersList.count; i++) {
		
		NSJSONSerialization *user = [cloudUsersList objectAtIndex:i];
		
		UIImageView *avatar = (UIImageView *)[listItem viewWithTag:i + 11];
		if (avatar != nil) { //privremeno
			objc_setAssociatedObject(avatar, &cloudUserKey, user, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
			
			NSArray *pictures = (NSArray *)[user valueForKey:@"pictures"];
			
			if ([pictures count] == 0) {
				[avatar setDefaultImageWithFirstName:[user valueForKey:@"nameFirst"] lastName:[user valueForKey:@"nameLast"]];
			} else {
				[avatar setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
				AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:[pictures firstObject] imageView:avatar useActivityIndicator:YES];
				loader.delegate = self;
				[loader loadImage];
			}
			
			UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewCloudUserProfile:)];
			
			[avatar setUserInteractionEnabled:YES];
			[avatar addGestureRecognizer:avatarTap];
			
		} else {
			return; //privremeno
		}
		
	}
	
	if (cloudUsersList.count > max_images) {
		UIButton *more = (UIButton *)[listItem viewWithTag:20];
		[more setHidden:NO];
		[more setTitle:[NSString stringWithFormat:@"+%ld", (long)cloudUsersList.count - max_images] forState:UIControlStateNormal];
		objc_setAssociatedObject(more, &cloudUserListKey, cloudUsersList, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		[more addTarget:self action:@selector(viewCloudUsers:) forControlEvents:UIControlEventTouchUpInside];
	}
	
	[listItem setNeedsDisplay];
	
}

-(void)viewCloudUsers:(id)sender {
	
	NSArray *cloudUsers = objc_getAssociatedObject(sender, &cloudUserListKey);
	
	ViewFriends *viewUsers = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewFriends"];
	[viewUsers setFriendsList:[NSMutableArray arrayWithArray: cloudUsers]];
	[viewUsers setTitle:@"People in cloud"];
	
	[self.navigationController pushViewController:viewUsers animated:YES];
	
}

-(void)viewCloudUserProfile:(UITapGestureRecognizer *)sender {
	
	UIImageView *avatar = (UIImageView *)sender.view;
	[self highlightItem:avatar];
	NSJSONSerialization *user = objc_getAssociatedObject(avatar, &cloudUserKey);
	if (![[user valueForKey:@"id"] isEqualToString:[_userInfo valueForKey:@"id"]]) {
		Profile *profile = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
		[profile setUserInfo:user];
		[self.navigationController pushViewController:profile animated:YES];
	} else {
		[_scrollView setContentOffset:CGPointZero animated:YES];
	}
	
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"ViewGallery"]) {
		
		Gallery *dvc = [segue destinationViewController];
        [dvc setCurrentImageIndex:_currentImageIndex];
        [dvc setImages:_images];
		[dvc setProfileImageViews:[_imageViews copy]];
        [dvc setParentVC:self];
        [dvc setOpeningGestureRecognizer:(UIGestureRecognizer *)sender];
		[dvc setClosingAnimationFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.width * 9 / 16)]; ////(0,0,...?

	} else if ([[segue identifier] isEqualToString:@"ViewFriends"]) {
	
		ViewFriends *dvc = [segue destinationViewController];
		[dvc setFriendIdList:_friendList];

	} else if ([segue.identifier isEqualToString:@"HandleFriendRequests"]) {

		TransparentDialog *dvc = [segue destinationViewController];
		
		NSString *cancelTitle;
		NSString *confirmTitle;
		NSString *questionText;
		//button color
		
		switch (_profileMode) {

			case UPOther:
			{
				cancelTitle = @"Nope, I give up";
				confirmTitle = @"Send";
				questionText = @"Send friend request?";
				break;
			}
			
			case UPPending:
			{
				cancelTitle = @"Reject";
				confirmTitle = @"Accept";
				questionText = @"Accept friend request?";
				break;
			}
			
			case UPRequested:
			{
				cancelTitle = @"Abort, I changed my mind!";
				confirmTitle = @"Cancel";
				questionText = @"Cancel friend request";
				break;
			}
			
			case UPFriend:
			{
				cancelTitle = @"Abort, I changed my mind!";
				confirmTitle = @"Unfriend";
				questionText = @"Remove from friends?";
				break;
			}

			default:
			break;
			
		}
		
		[dvc setCancelTitle:cancelTitle];
		[dvc setConfirmTitle:confirmTitle];
		[dvc setQuestionText:questionText];
		[dvc setDelegate:self];

	} else if ([segue.identifier isEqualToString:@"ViewCloudUnavailableProfile"]) {
	
		UnavailableCloudView *dvc = [segue destinationViewController];
		double lat = [[(NSArray *)[_userInfo valueForKey:@"location"] objectAtIndex:1] doubleValue];
		double lng = [[(NSArray *)[_userInfo valueForKey:@"location"] objectAtIndex:0] doubleValue];
		[dvc setCloud:_selectedCloud];
		[dvc setParentVC:self];
		[dvc setUserLocation:CLLocationCoordinate2DMake(lat, lng)];

	} else if ([segue.identifier isEqualToString:@"ViewCloudProfile"]) {
	
		CloudView *dvc = [segue destinationViewController];
		[dvc setCloud:_selectedCloud];
		//double lat = [[(NSArray *)[_userInfo valueForKey:@"location"] objectAtIndex:1] doubleValue];
		//double lng = [[(NSArray *)[_userInfo valueForKey:@"location"] objectAtIndex:0] doubleValue];
		//[dvc setCloud:_selectedCloud];
		//[dvc setUserLocation:CLLocationCoordinate2DMake(lat, lng)];

	} else if ([segue.identifier isEqualToString:@"EditProfile"]) {
	
		EditProfile *dvc = [segue destinationViewController];
		[dvc setProfileViewController:self];
	
	} else if ([segue.identifier isEqualToString:@"Send1on1Message"]) {
		
		OneOnOneChat *dvc = [segue destinationViewController];
		[dvc setFriend:(NSDictionary *)_userInfo];
		
	}

}

-(void)viewWillDisappear:(BOOL)animated {
	
	if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) { // in dealloc!
		[_apiService cancelRequests];
		[_getFriendsService cancelRequests];
	}
	
	[super viewWillDisappear:animated];

}

/*
 - (UIImage*)takeScreenshot:(CALayer*)layer{
 //layer.
 UIGraphicsBeginImageContextWithOptions(self.ew.bounds.size, YES, [[UIScreen mainScreen] scale]);
 [layer renderInContext:UIGraphicsGetCurrentContext()];
 UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
 //UIImageView *screnshot = [[UIImageView alloc] initWithImage:image];
 
 return image;
 }
 */

@end
