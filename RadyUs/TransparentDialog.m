//
//  TransparentDialog.m
//  RadyUs
//
//  Created by Frane Poljak on 04/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "TransparentDialog.h"

@interface TransparentDialog ()

@end

@implementation TransparentDialog

- (void)viewDidLoad {
	
    [super viewDidLoad];

	[_cancelLabel setText:_cancelTitle ?: @"Cancel"];
	
	if (_questionLabel != nil) {
		[_questionLabel setText:_questionText ?: @""];
	}
	
	NSString *confirmTitle = _confirmTitle ?: @"Confirm";
	
	[_confirmButton.titleLabel setText:confirmTitle];
	[_confirmButton setTitle:confirmTitle forState:UIControlStateNormal];
	[_confirmButton setTitle:confirmTitle forState:UIControlStateSelected];
	[_confirmButton setTitle:confirmTitle forState:UIControlStateHighlighted];
	
	[_confirmButton setHidden:_hideConfirmButton];
	
    [self.view sendSubviewToBack:_transparentBackground];
	
	// TODO: add array of custom actions (title, action block) and present if exists (max 3?)
	
}

-(void)viewDidAppear:(BOOL)animated {
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[self setNeedsStatusBarAppearanceUpdate];
	// TODO: if pasteboard image didn't exist and now does, add action? (then it's not custom action? - add this as some custom appearance block?)
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)prefersStatusBarHidden {
    return YES;
}

-(IBAction)dismiss:(id)sender {
	
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	[self setNeedsStatusBarAppearanceUpdate];

    [self dismissViewControllerAnimated:YES completion:^{
		if ([sender isEqual:_confirmButton]) {
			if ([_delegate respondsToSelector:@selector(confirmSelected)]) {
				[_delegate confirmSelected];
			}
		} else {
			if ([_delegate respondsToSelector:@selector(cancelSelected)]) {
				[_delegate cancelSelected];
			}
		}
    }];
}

-(IBAction)addPhoto:(id)sender {
	
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	[self setNeedsStatusBarAppearanceUpdate];

    [self dismissViewControllerAnimated:YES completion:^{
		
		if ([sender tag] == 103) {
			
			// TODO: add button for paste image (if there is image in pasteboard)
			// TODO: add as custom action
			
			if ([self.delegate respondsToSelector:@selector(customDialogAction:)]) {
				NSDictionary *info = [NSDictionary dictionaryWithObject:[UIPasteboard generalPasteboard].image forKey:@"image"];
				[self.delegate customDialogAction:info];
			}
			
			return;
			
		}
		
		// TODO: load from url?
		
		// TODO: check if camera available, if not disable button
		
		UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
		[imagePicker.view setFrame:_parentVC.view.frame]; //window frame?
		
        UIImagePickerControllerSourceType sourceType;
		
		imagePicker.navigationBar.tintColor = [UIColor redColor];//Cancel button text color
		[imagePicker.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
		
        switch ([sender tag]) {
            case 101:
				
				if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
					//alert
					NSLog(@"Camera not available!");
					return;
				}
				
                sourceType = UIImagePickerControllerSourceTypeCamera;
                break;
                
            case 102:
                sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                break;
				
            default:
                break;
        }

        [imagePicker setSourceType:sourceType]; //[_imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        if (sourceType == UIImagePickerControllerSourceTypeCamera) {
            [imagePicker setCameraDevice:([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront]) ? UIImagePickerControllerCameraDeviceFront : UIImagePickerControllerCameraDeviceRear];
        }
		
        [imagePicker setDelegate:(UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate> *)_parentVC];
        
		[imagePicker setAllowsEditing:NO];
		
        if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
		
			double delayInSeconds = 0.36; //??
			dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
			
			dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
				[_parentVC presentViewController:imagePicker animated:YES completion:nil];
			});
			
        } else {
			//not available
			// TODO: hide button if not available
        }
		
    }];

}

@end
