//
//  LocationManagerHandler.h
//  Radyus
//
//  Created by Locastic MacbookPro on 17/03/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LocationManagerHandler : NSObject <CLLocationManagerDelegate>

@end
