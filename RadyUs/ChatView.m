//
//  ChatView.m
//  RadyUs
//
//  Created by Frane Poljak on 03/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ChatView.h"

@implementation ChatView

@synthesize firstLoad;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)_init {
	
	firstLoad = YES;
	
	_userId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"];
	
	_currentY = 4;
	_messageCounter = 0;
	_lastUserID = @"";
	_lastMessageDate = @"";
	_tempLastMessageDate = @"";
	_firstMessagedate = @"";
	_tempFirstMessageDate = @"";
	_firstMessage = nil;
	_lastMessage = nil;
	_tempFirstMessage = nil;
	_tempLastMessage = nil;
	_tempMessage = nil;
	
	[self setAutoresizesSubviews:NO];
	[self setBounces:YES];
	
	_isInLoadPreviousMode = NO;
	_chatviewRepositioning = NO;
	_isBeingSwiped = NO;
	
	_messageBuffer = [[NSMutableArray alloc] init];
	
	_thumbnailTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleThumbnailTap:)];
	
	self.delaysContentTouches = YES;
	
	UIPanGestureRecognizer *chatSwipe = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(chatSwiped:)];
	
	UIViewController *parentVC = [self parentViewController];
	
	if ([parentVC conformsToProtocol:@protocol(UIGestureRecognizerDelegate)]) {
		chatSwipe.delegate = (id<UIGestureRecognizerDelegate>)parentVC;
	}
	
	[chatSwipe setDelaysTouchesEnded:YES];
	[self addGestureRecognizer:chatSwipe];
	
	_dateFormatter = [[NSDateFormatter alloc] init];
	[_dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
	[_dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	
	_calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
	[_calendar setTimeZone:[NSTimeZone localTimeZone]];
	
	_progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
	[_progressView setUserInteractionEnabled:NO];
	[_progressView.layer setZPosition:9999.0f];
	//[_progressView setTintColor:[UIColor blueColor]];
	[_progressView setTrackTintColor:self.backgroundColor]; //trackImage
	[_progressView setProgressTintColor:[UIColor blueColor]]; //progressImage?
	[_progressView setHidden:YES];
	[_progressView setAlpha:0.8f];

	/*
	//crash!
	CAGradientLayer *progressVerticalGradient = [CAGradientLayer layer];
	[progressVerticalGradient setStartPoint:CGPointZero];
	[progressVerticalGradient setEndPoint:CGPointMake(0, 1)];
	CGColorRef c1 = [[[UIColor whiteColor] colorWithAlphaComponent:0.4f] CGColor];
	CGColorRef c2 = [[[UIColor whiteColor] colorWithAlphaComponent:0.2f] CGColor];
	[progressVerticalGradient setColors:@[(id)CFBridgingRelease(c1), (id)CFBridgingRelease(c2), (id)CFBridgingRelease(c1)]];
	[progressVerticalGradient setLocations:@[@0.0, @0.5, @1.0]];
	[progressVerticalGradient setFrame:_progressView.bounds];
	[progressVerticalGradient setZPosition:1000.0f];
	 
	[_progressView.layer addSublayer:progressVerticalGradient];
	*/
	
	[self setScrollEnabled:YES];
	
	NSLog(@"chatView initialized");
	
	//bounce
	
	//NSLog(@"Test: %ld", [self daysBetweenDate:@"2015-06-06 15:01:34.857Z" andDate:@"2015-07-24 11:41:57.573Z"]);

}

-(void)awakeFromNib {
	
	[super awakeFromNib];
	[self _init];
	
}

/* //on first add to superview!
-(instancetype)init {
	
	if (self = [super init]) {
		[self _init];
	}
	
	return self;
	
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
	
	if (self = [super initWithCoder:aDecoder]) {
		[self _init];
	}
	
	return self;
}

-(instancetype)initWithFrame:(CGRect)frame {

	if (self = [super initWithFrame:frame]) {
		[self _init];
	}
	
	return self;
	
}
*/

-(void)chatSwiped:(UIScreenEdgePanGestureRecognizer *)sender{
	
	static CGFloat currentTranslation = 0.0f;
	
	if (sender.state == UIGestureRecognizerStateBegan) {
		_isBeingSwiped = YES;
		return;
	}
	
	BOOL moving = sender.state == UIGestureRecognizerStateChanged;

	__weak typeof (self) this = self;
	
	CGFloat translation = [sender translationInView:self].x + 32;
	translation = - [sender translationInView:self].x > 72 ? -40 : translation;
	translation = [sender translationInView:self].x > -32 ? 0 : translation;
	
	if (translation == currentTranslation && moving) {
		return;
	}
	
	currentTranslation = translation;
	
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{

		if (this == nil) {
			return;
		}
		
		if (moving) {
		
			CGAffineTransform transform = CGAffineTransformMakeTranslation(translation, 0);

			for (UIView *subview in self.subviews) {
			
				if ((![subview isKindOfClass:[ChatSingleMessage class]] || [(ChatSingleMessage *)subview messageType] == MTUserMessage) && ![subview isKindOfClass:[UIImageView class]]) {
					subview.transform = transform;
				}
				
			}
			
		} else {
			
			[UIView animateWithDuration:currentTranslation != 0.0f ? 0.2f : 0.0f animations:^{
				
				for (UIView *subview in self.subviews) {
				
					if (![subview isKindOfClass:[ChatSingleMessage class]] || [(ChatSingleMessage *)subview messageType] == MTUserMessage) {
						subview.transform = CGAffineTransformIdentity;
					}
					
				}
				
			} completion:^(BOOL finished) {
			
				currentTranslation = 0;
				_isBeingSwiped = NO;
		
			}];
		
		}
		
	}];
	
}

-(void)checkForDateChange:(NSDictionary *)message {

	NSString *timeString = (NSString *)[message objectForKey:@"time"];
	
	if ([_lastMessageDate isEqualToString:@""] || [self daysBetweenDate:_lastMessageDate andDate:timeString] != 0) { //// > 0, zasad ovako zbog kontrole redosljeda poruka
		
		NSDate *date = [_dateFormatter dateFromString:timeString];
		
		BOOL isToday = [_calendar isDateInToday:date];
		BOOL isYesterday = [_calendar isDateInYesterday:date];
		
		/* //za drugačiji format prikaza datuma, odlučit točni koji format
		 dateFormatter = [[NSDateFormatter alloc] init];
		 [dateFormatter setDateStyle:NSDateFormatterShortStyle];
		 [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		 [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
		*/
		
		NSString *displayDate = isToday ? @"Today" : (isYesterday ? @"Yesterday" : [timeString substringToIndex:10]);
		
		[self displayAppMessage:[NSDictionary dictionaryWithObject:displayDate forKey:@"text"]];
		
	}
	
	_lastMessageDate = timeString;

}

-(void)displayMessage:(NSDictionary *)message {
	
	[self checkForDateChange:message];
		
	NSString *userID = [message objectForKey:@"user"];
	BOOL displayHeader = ![_lastUserID isEqualToString:userID];
		
	if (displayHeader) {
		_currentY += 4;
	}
		
	ChatSingleMessage *msg = [[ChatSingleMessage alloc] initWithType:MTFriendMessage message:message displayHeader:displayHeader];
    msg.shouldAnimate = !_isInLoadPreviousMode && !firstLoad;
	[self insertInChainedList:msg];
		
	_currentY += [msg displayInContainer:self startingY:_currentY];
	_lastUserID = userID;
    [self didDisplayMessage:msg];
	
}

-(void)displayUserMessage:(NSDictionary *)message {
	
	[self checkForDateChange:message];
		
	NSString *userID = [message objectForKey:@"user"];
	BOOL displayHeader = ![_lastUserID isEqualToString:userID];
	ChatSingleMessage *msg = [[ChatSingleMessage alloc] initWithType:MTUserMessage message:message displayHeader:displayHeader];
    msg.shouldAnimate = !_isInLoadPreviousMode && !firstLoad;
    [self insertInChainedList:msg];

	_currentY += [msg displayInContainer:self startingY:_currentY];
	_lastUserID = userID;
    [self didDisplayMessage:msg];
	
}

-(void)displayAppMessage:(NSDictionary *)message {
	
	_currentY += 12;
	ChatSingleMessage *msg = [[ChatSingleMessage alloc] initWithType:MTAppMessage message:message displayHeader:NO];
    msg.shouldAnimate = false;
	[self insertInChainedList:msg];
		
	_currentY += [msg displayInContainer:self startingY:_currentY];
	_lastUserID = @"";
    [self didDisplayMessage:nil];
	
}

-(void)insertInChainedList:(ChatSingleMessage *)msg { //DOBRO PROVJERIT!!!

	if (_isInLoadPreviousMode && !firstLoad) {
		
		if (_tempFirstMessage == nil) {
			_tempFirstMessage = _firstMessage;
			_firstMessage = msg;
			_tempLastMessage = msg;
		} else {
			_tempLastMessage.nextMessage = msg;
			msg.previousMessage = _tempLastMessage;
			_tempLastMessage = msg;
		}
		
		if (_tempFirstMessage != nil) {
			msg.nextMessage = _tempFirstMessage;
			_tempFirstMessage.previousMessage = msg;
		}
		
	} else {
		
		if (_firstMessage == nil) {
			_firstMessage = msg;
		} else {
			_lastMessage.nextMessage = msg;
			msg.previousMessage = _lastMessage;
		}
		
		_lastMessage = msg;
		
	}
	
	ChatSingleMessage *previousMessage = msg.previousMessage;
	
	if (!msg.displayHeader && previousMessage != nil && previousMessage.messageType != MTAppMessage) {
		[previousMessage adjustCorners];
	}
	
	[msg adjustCorners];
	
	return; //privremeno
	[self syncMessage:msg];
	
}

-(void)syncMessage:(ChatSingleMessage *)message { ////DOBRO IZTESTIRAT!!! (šta sa headerima??) //check previous, add header if needed, adjust corners
	
	if (_isInLoadPreviousMode || message == nil || message.messageType == MTAppMessage) {
		return;
	}

	ChatSingleMessage *previousMessage = message.previousMessage;
	
	while (previousMessage != nil && previousMessage.messageType == MTAppMessage) {
		previousMessage = previousMessage.previousMessage;
	}
	
	if (previousMessage == nil) {
		return;
	}

	if ([[message.message valueForKey:@"time"] compare:[previousMessage.message valueForKey:@"time"] options:0] != NSOrderedAscending) {//compare times
		NSLog(@"new message time: %@, previous message time: %@", [message.message valueForKey:@"time"], [previousMessage.message valueForKey:@"time"]);
		return; //message is not older than previous, ok
	}
	
	CGFloat messageFrameY = message.frame.origin.y;
	CGFloat previousMessageFrameY = previousMessage.frame.origin.y;
	CGFloat messageDTFY = message.dateTimeLabel.frame.origin.y;
	CGFloat previousMessageDTFY = previousMessage.dateTimeLabel.frame.origin.y;
	
	_chatviewRepositioning = YES;
	
	__block ChatSingleMessage *_message = message;
	__block ChatSingleMessage *_previousMessage = previousMessage;
	
	[UIView animateWithDuration:_isBeingSwiped ? 0.0f : 0.2f animations:^{
		
		[message setFrame:CGRectMake(message.frame.origin.x, previousMessageFrameY, message.frame.size.width, message.frame.size.height)];
		[previousMessage setFrame:CGRectMake(previousMessage.frame.origin.x, messageFrameY, previousMessage.frame.size.width, previousMessage.frame.size.height)];
		[message.dateTimeLabel setFrame:CGRectMake(previousMessage.dateTimeLabel.frame.origin.x, previousMessageDTFY, 
												   previousMessage.dateTimeLabel.frame.size.width, previousMessage.dateTimeLabel.frame.size.height)];
		[previousMessage.dateTimeLabel setFrame:CGRectMake(message.dateTimeLabel.frame.origin.x, messageDTFY, message.dateTimeLabel.frame.size.width, message.dateTimeLabel.frame.size.height)];
		
	} completion:^(BOOL finished) {
		
		/////zamjena prije animacije?
		ChatSingleMessage *prevPrev = _previousMessage.previousMessage;
		ChatSingleMessage *potentialNew = _message.nextMessage;
		
		_message.nextMessage = _previousMessage;
		_previousMessage.previousMessage = _message;
		_message.previousMessage = prevPrev;
		_previousMessage.nextMessage = potentialNew;
		
		if (prevPrev == nil) {
			_firstMessage = _message;
		} else {
			prevPrev.nextMessage = _message;
		}
		
		[_message adjustCorners];
		[_previousMessage adjustCorners];
		if (potentialNew != nil) {
			potentialNew.previousMessage = _previousMessage;
			[potentialNew adjustCorners];
		}
		
		_chatviewRepositioning = NO;
		
		[self syncMessage:_message];
		
	}];

}

-(ChatSingleMessage *)getMessageViewFromSubview:(UIView *)subview { //vejrojatno nepotrebno (bar zasad)

	if (subview == nil) {
		return nil;
	}
	
	if ([subview isKindOfClass:[ChatSingleMessage class]]) {
		return (ChatSingleMessage *)subview;
	}
	
	UIView *temp = subview;
	while (temp.superview != nil && ![temp.superview isKindOfClass:[ChatSingleMessage class]]) {
		temp = temp.superview;
	}
	
	return (ChatSingleMessage *)temp.superview;
	
}

-(void)willMoveToSuperview:(UIView *)newSuperview {
	[super willMoveToSuperview:newSuperview];
	[_progressView removeFromSuperview];
}

-(void)didMoveToSuperview {
	[super didMoveToSuperview];
	[self.superview insertSubview:_progressView belowSubview:self];
}

-(void)adjustProgressViewFrame {
	CGRect pvframe = CGRectMake(self.frame.origin.x, self.frame.origin.y - 1, self.frame.size.width, 1.0f);
	[_progressView setFrame:pvframe];
}

-(void)layoutSubviews {
	[super layoutSubviews];
	[self adjustProgressViewFrame];
	// TODO: adjust content offset?
}

// TODO: handle image uploading here!
-(void) updateUploadProgress:(CGFloat)progress { // animated parameter?
	
	[_progressView setHidden:NO];
	// TODO: in showProgressView method, call before upload start! (because of possible flick on finish)
	
	//flick happens because of second call to updateprogress with 1.0 in apiservice!!
	
	NSLog(@"updateUploadProgress: %f", progress);
	[_progressView setProgress:progress animated:YES];
	
	if (progress == 1.0f) {
		[self performSelector:@selector(hideProgressView) withObject:nil afterDelay:0.05f];
	}
	
}

-(void) hideProgressView {
	[_progressView setProgress:0.0f];
	[_progressView setHidden:YES];
}

-(void) loadSuccess:(UIImageView *)imageView {
	
	ChatSingleMessage *message = [self getMessageViewFromSubview:imageView]; //imageView.superview.superview
	
	if (message == nil || message.deallocated) {
		return;
	}
	
	[message.message setValue:imageView.image forKey:@"imageToDisplay"];
	
	CGFloat oldHeight = message.frame.size.height;
	
	NSInteger newImageSize = [message displayImage:imageView.image inContainer:self startingY: imageView.superview.frame.origin.y - 2];
	CGFloat newHeight = imageView.superview.frame.origin.y + newImageSize;// + 4; ////////
	
	CGFloat heightDiff = newHeight - oldHeight;
	_currentY += heightDiff;

	__block ChatSingleMessage *_message = message;
	__block ChatSingleMessage *__message = message;
	
	[message.backgroundLayer removeFromSuperlayer];
	
	message.backgroundLayer = [CALayer layer];
	[message.backgroundLayer setFrame:message.layerFrame];
	[message.backgroundLayer setBackgroundColor:(message.messageType == MTUserMessage ? UIColorFromRGB(0xFFD203) : UIColorFromRGB(0xEBEBEB)).CGColor];
	[message.backgroundLayer setZPosition:-0.9f];
	[message.layer addSublayer:message.backgroundLayer];
	
	[message adjustCorners];
	
	//message.layerFrame = CGRectMake(message.frameX, message.frameY, message.frame.size.width - message.frameX - 15, message.layerFrame.size.height + heightDiff);
	//why it didn't exist? (or width was smaller) - TEST!!!
	
	message.layerFrame = CGRectMake(message.frameX + 1, message.frameY, message.frame.size.width - message.frameX - (message.messageType == MTFriendMessage ? 24 : 16), newHeight - 4 - (message.displayHeader ? 23 : 0));
	
	[UIView animateWithDuration: _isInLoadPreviousMode || _isBeingSwiped || _chatviewRepositioning ? 0.0 : 0.25 animations:^{

		imageView.alpha = 0.0;
		
		[message setFrame:CGRectMake(message.frame.origin.x, message.frame.origin.y, message.frame.size.width, newHeight)];
		[message.dateTimeLabel setFrame:CGRectMake(message.dateTimeLabel.frame.origin.x, message.dateTimeLabel.frame.origin.y + (heightDiff) / 2,
												   message.dateTimeLabel.frame.size.width, message.dateTimeLabel.frame.size.height)];
		
		//if (![[message.message objectForKey:@"text"] isEqualToString:@""]){
			[message.backgroundLayer setFrame:message.layerFrame];
		//}
		////adjust message textVeiw frame?
		
		if (![message isEqual:_lastMessage]) { //move newer messages
			
			while (_message.nextMessage != nil) {
			
				_message = _message.nextMessage;
				[_message setFrame:CGRectMake(_message.frame.origin.x, _message.frame.origin.y + heightDiff, _message.frame.size.width, _message.frame.size.height)];
				[_message.dateTimeLabel setFrame:CGRectMake(_message.dateTimeLabel.frame.origin.x, _message.dateTimeLabel.frame.origin.y + heightDiff,
														   _message.dateTimeLabel.frame.size.width, _message.dateTimeLabel.frame.size.height)];
				
			}
			
		}
		
		if (!_isInLoadPreviousMode) {
			[self setContentSize:CGSizeMake(self.frame.size.width, _currentY + 4)];
			[self setContentOffset:CGPointMake(0, MIN(MAX(self.contentOffset.y, self.contentOffset.y + heightDiff), self.contentSize.height - self.frame.size.height))];
		}
		
	} completion:^(BOOL finished) {
		
		[__message adjustCorners];
		[imageView removeFromSuperview];
		[imageView.superview removeFromSuperview]; //imageViewContainer
		
		if (_lastMessage != nil) { // extra space fix
		
			if (_lastMessage.previousMessage != nil) {
				
				CGFloat requiredOriginY = _lastMessage.previousMessage.frame.origin.y + _lastMessage.previousMessage.frame.size.height;// + 4;
				CGFloat diff = _lastMessage.frame.origin.y - requiredOriginY;
				
				_lastMessage.frame = CGRectMake(_lastMessage.frame.origin.x, requiredOriginY, _lastMessage.frame.size.width, _lastMessage.frame.size.height);
				
				CGRect dateTimeLabelFrame = _lastMessage.dateTimeLabel.frame;
				dateTimeLabelFrame.origin.y = dateTimeLabelFrame.origin.y - diff;
				
				_lastMessage.dateTimeLabel.frame = dateTimeLabelFrame;
				
			}
			
			_currentY = _lastMessage.frame.origin.y + _lastMessage.frame.size.height;// + 4;
			
		}
		
	}];
	
}

-(void) errorLoadingImage:(UIImageView *)imageView {
	
	ChatSingleMessage *message = [self getMessageViewFromSubview:imageView];
	
	if (message == nil) {
		return;
	}

	[imageView setImage:[UIImage imageNamed:@"failure.png"]]; //error image
	
	//HANDLED IN SINGLE MESSAGE!
	
	//UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reloadImage:)]; //check if already exists!
	//[imageView addGestureRecognizer:imageTap]; //remove on success (if exists), messagetapgesture??
	
}

/* //HANDLED IN SINGLE MESSAGE!
-(void)reloadImage:(UITapGestureRecognizer *)sender {

	UIImageView *iw = (UIImageView *)sender.view;
	
	if (iw != nil) {
		
		ChatSingleMessage *message = [self getMessageViewFromSubview:iw];
		
		if (message != nil) {
			AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:[message.message valueForKey:@"image_id"] imageView:iw useActivityIndicator:YES];
			[loader loadImage];
		}
		
	}
	
}
*/

-(NSInteger)daysBetweenDate:(NSString *)date1 andDate:(NSString *)date2 {
	
	NSDate *startDate = [_dateFormatter dateFromString:date1];
	NSDate *endDate = [_dateFormatter dateFromString:date2];

	NSDateComponents *currentMessageDateComponents = [_calendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:endDate]; ////seconds?
	NSInteger currentMessageTimeComponent = currentMessageDateComponents.hour * 60 + currentMessageDateComponents.minute; ////seconds?
	
	NSDateComponents *components = [_calendar components:NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute ////seconds?
														fromDate:startDate == nil ? [NSDate dateWithTimeIntervalSince1970:0] : startDate
														  toDate:endDate == nil ? [NSDate date] : endDate
														 options:NSCalendarWrapComponents];
	
	return components.day + (components.hour * 60 + components.minute > currentMessageTimeComponent ? 1 : 0);////seconds?
    ////ispast će nekad npr 2 umisto 1, jer još triba provjerit i time components od prvog datuma, ali u ovom slučaju nije bitno!
	
}

-(void)didDisplayMessage:(ChatSingleMessage *)message {
	
	BOOL isAppMessage = [_lastUserID isEqualToString:@""];
	
	if (!isAppMessage) {
		
		if ([_firstMessagedate isEqualToString:@""]) {
			_firstMessagedate = _lastMessageDate;
		}
		
		_messageCounter ++;
		
	}
	
	if (_isInLoadPreviousMode) {
		
		/*
		if ([_tempFirstMessageDate isEqualToString:@""]) {
			_tempFirstMessageDate = _lastMessageDate;
		}
		*/
		
		return;
		
	}
	
	[self setContentSize:CGSizeMake(self.frame.size.width, MAX(_currentY + 8, self.frame.size.height))];

	BOOL shouldScrollToBottom = [_lastUserID isEqualToString: _userId] || (self.contentSize.height - self.contentOffset.y < self.frame.size.height * 1.6);// || isAppMessage; ////
	
    if (shouldScrollToBottom) {
		
        [self setContentOffset:CGPointMake(0, MAX(0, _currentY - self.frame.size.height + 8))];
		
        if (_messageButton != nil) {
            [_messageButton removeFromSuperview];
            _messageButton = nil;
        }
        
    } else if (_messageButton == nil) {
        [self showMessageButton];
    }
	
	if (!isAppMessage) {
		[_counterLabel setText:[NSString stringWithFormat:@"There are %lu messages below", (long)_messageCounter]];
		//delegate didisplaymessage -> reset timer?
	}

    if (!message) {
        return;
    }

}

-(void)clearChatView { //handle case when messages are being loaded or in loadprevoiusmode, dealloc all messages from chained list,
	
	//will(should) this be used at all?
	
	for (__strong UIView *subView in self.subviews) {
		
		if (![subView isKindOfClass:[UIImageView class]]) { //scroll indicator
			[subView removeFromSuperview];
		}
		
		if ([subView isKindOfClass:[ChatSingleMessage class]]) {
			subView = nil;
		}
		
	}
	
	_lastUserID = @"";
	_lastMessageDate = @"";
	_tempLastMessageDate = @"";
	_firstMessagedate = @"";
	_tempFirstMessageDate = @"";
	_currentY = 4;
	_messageCounter = 0;
	
	_firstMessage = nil;
	_lastMessage = nil;
	_tempFirstMessage = nil;
	_tempLastMessage = nil;
	_tempCurrentYOffset = 0;
	_tempLastUserID = @"";
	_tempMessage = nil;
	
	[self setAutoresizesSubviews:NO];
	[self setBounces:YES];
	
	_isInLoadPreviousMode = NO;
	_chatviewRepositioning = NO;
	_isBeingSwiped = NO;
	
	_messageBuffer = [[NSMutableArray alloc] init];

	[_counterLabel setText:[NSString stringWithFormat:@"There are %lu messages below", (long)_messageCounter]];
	[self setContentSize:self.frame.size];
	[self setContentOffset:CGPointZero animated:NO];

}

-(void)showMessageButton {
	
    _messageButton = [[RoundedButton alloc] initWithFrame:CGRectMake(self.frame.size.width / 2 - 45, self.frame.size.height + self.frame.origin.y - 30, 90, 22)];
    [_messageButton setBackgroundColor:[UIColor darkTextColor]];
    [_messageButton.titleLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:10]];
    [_messageButton setTitle:@"New message(s)" forState:UIControlStateNormal];
    [_messageButton addTarget:self action:@selector(newMessageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [_messageButton setAlpha:0.6f];
    [_messageButton.layer setCornerRadius:10.0f];
    //[_messageButton.layer setShadowColor:[UIColor darkGrayColor].CGColor];
    //[_messageButton.layer setShadowRadius:10.0f];
    //[_messageButton.layer setShadowOffset:CGSizeMake(3.0f, 3.0f)];
    [_messageButton.layer setZPosition:99.0f];
    [self.superview addSubview:_messageButton];
	
}

-(void)newMessageButtonClicked {
	
    [_messageButton removeFromSuperview];
    _messageButton = nil;
    [self setContentOffset:CGPointMake(0, MAX(0, _currentY - self.frame.size.height)) animated:YES];
	
}

-(void)addPhoto:(id)sender {
	
	[self removeThumbnail:sender];
	
    UIImagePickerControllerSourceType sourceType;
    
    switch ([sender tag]) {
        case 102:
			
			if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
				//alert
				NSLog(@"Camera not available!");
				return;
			}

            sourceType = UIImagePickerControllerSourceTypeCamera;
            break;
            
        case 101:
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
            
        default:
            break;
			
    }
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker.view setFrame:self.window.frame];   //check if sourcetypeavailable
    [imagePicker setSourceType:sourceType]; //[_imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        [imagePicker setCameraDevice:([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront]) ? UIImagePickerControllerCameraDeviceFront : UIImagePickerControllerCameraDeviceRear];
        [imagePicker setCameraCaptureMode:UIImagePickerControllerCameraCaptureModePhoto]; //video?
    }
    [imagePicker setDelegate:(id)[self parentViewController]];
    [imagePicker setAllowsEditing:NO];
    
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        [[self parentViewController] presentViewController:imagePicker animated:YES completion:nil];
    } else {
        //not available
    }

}

-(void)addThumbnail:(id)sender {
	
    if (_imageToDisplay == nil) {
        return;
    }
    
    UITextView *messageInput = (UITextView *)sender;
    UIImageView *thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(8, 4, 32, 32)];
    [thumbnail setTag:1001];
    [thumbnail setContentMode:UIViewContentModeScaleAspectFill];
    [thumbnail setUserInteractionEnabled:YES];
    [thumbnail setImage:_imageToDisplay];
    UIEdgeInsets inset = messageInput.textContainerInset;
    inset.left += 52;
    
    UIButton *removeButton = [[RoundedButton alloc] initWithFrame:CGRectMake(29, -4, 19, 19)];
    [removeButton setBackgroundColor:[UIColor whiteColor]];
    [removeButton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    [removeButton.layer setCornerRadius:9.5f];
    [removeButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    [removeButton.layer setBorderWidth:3.0f];
    //[removeButton awakeFromNib];
    removeButton.clipsToBounds = YES;
    //[removeButton addTarget:self action:@selector(removeThumbnail:) forControlEvents:UIControlEventTouchUpInside];
    [thumbnail addSubview:removeButton];
    
    [messageInput setTextContainerInset:inset];
    [messageInput addSubview:thumbnail];
    [messageInput addGestureRecognizer:_thumbnailTap];
	
}

-(void)removeThumbnail:(id)sender {
	
    if (_imageToDisplay == nil) {
        return;
    }
    
    //BOOL buttonClicked = [sender isKindOfClass:[RoundedButton class]];

    //UITextView *messageInput = buttonClicked ? (UITextView *)[[sender superview] superview] : (UITextView *)sender;
    //UIImageView *thumbnail = buttonClicked ? (UIImageView *)[sender superview] : (UIImageView *)[sender viewWithTag:1001];
    
    UITextView *messageInput = (UITextView *)sender;
    UIImageView *thumbnail = (UIImageView *)[sender viewWithTag:1001];
    
    [thumbnail removeFromSuperview];
    
    UIEdgeInsets inset = messageInput.textContainerInset;
    inset.left -= 52;
    [messageInput setTextContainerInset:inset];
    
    [messageInput removeGestureRecognizer:_thumbnailTap];
    _imageToDisplay = nil;
    
    if ([self.delegate respondsToSelector:@selector(textViewDidChange:)]) {
        [self.delegate performSelector:@selector(textViewDidChange:) withObject:messageInput];
    }
    
    
}

-(void)handleThumbnailTap:(UITapGestureRecognizer *)sender {
	
    if (CGRectContainsPoint(CGRectMake(37, 0, 19, 19), [sender locationInView:sender.view])) {
        [self removeThumbnail:sender.view]; //highlight button?
    } else if (!CGRectContainsPoint([sender.view viewWithTag:1001].frame, [sender locationInView:sender.view])) {
        [sender.view becomeFirstResponder];
    }
	
}

-(void)postMessage:(id)sender withTime:(NSString *)time {
    
    UITextView *messageInput = (UITextView *)sender;
	
	NSString *messageText = [messageInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
    if (messageText.length == 0 && _imageToDisplay == nil) {
        return;
    }
    
    NSArray *_keys = @[@"user", @"nameFirst", @"nameLast", @"time", @"text"]; //save message to local database?
    NSMutableArray *keys = [NSMutableArray arrayWithArray:_keys];
	
	if (_imageToDisplay != nil) {
        [keys addObject:@"imageToDisplay"];
    }
	
	//NSString *dateString = @"";
	
	//NSDateFormatter *df = [[NSDateFormatter alloc] init];
	//[df setDateFormat:@"MM/dd/YYYY h:mm:ss"];
	//dateString = [df stringFromDate:[NSDate date]];
	
    NSArray *objects = [NSArray arrayWithObjects: _userId, @"Me", @"", time, messageText, _imageToDisplay, nil];
    NSDictionary *message = [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
    
    [messageInput setText:@""];
	
	if (_isInLoadPreviousMode) {
		[_messageBuffer addObject:message];
	} else {
		[self displayUserMessage:message];
	}
	
	[self removeThumbnail:messageInput];
    
    _imageToDisplay = nil;
	
}

-(void) enterLoadPreviousMode {

	for (UIView *subview in self.subviews) {
		[subview setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
	}
	
	[self setAutoresizesSubviews:YES];
	
	_tempLastUserID = _lastUserID;
	_lastUserID = @"";
	
	_tempLastMessageDate = _lastMessageDate;
	_lastMessageDate = @"";
	_tempFirstMessageDate = _firstMessagedate;
	_firstMessagedate = @"";
	_tempCurrentYOffset = _currentY;
	
	_currentY = 4;
	_isInLoadPreviousMode = YES;
	
	
}

-(void) leaveLoadPreviousMode {
	
	CGFloat currentY = _currentY;
	_currentY += _tempCurrentYOffset;
	
	[self setContentSize:CGSizeMake(self.frame.size.width, MAX(self.contentSize.height + currentY + 4, self.frame.size.height))];
	
	CGFloat oldHeight = self.frame.size.height;
	
	[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, oldHeight + currentY + 4)];
	
	[self setAutoresizesSubviews:NO];
	
	for (UIView *subview in self.subviews) {
		[subview setAutoresizingMask:UIViewAutoresizingNone];
	}
	
	[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, oldHeight)];
	
	NSLog(@"pre - _currentY: %f:", _currentY);
	_currentY = _lastMessage.frame.origin.y + _lastMessage.frame.size.height;// + 4;
	NSLog(@"post - _currentY: %f:", _currentY);
	[self setContentSize:CGSizeMake(self.frame.size.width, MAX(_currentY + 4, self.frame.size.height))];
	
	if (self.contentOffset.y < currentY + 80) {
		
		[self setContentOffset:CGPointMake(0, MAX(0, (firstLoad ? self.contentSize.height - self.frame.size.height : currentY))) animated:NO];
	
		if (firstLoad) {
			firstLoad = NO;
		} else {
			
			[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
				[self setContentOffset:CGPointMake(0, MAX(0, (currentY - 140)))];
			} completion:nil];
			
		}
		
	}
	
	[_counterLabel setText:[NSString stringWithFormat:@"There are %lu messages below", (long)_messageCounter]];////maknit?
	
	_isInLoadPreviousMode = NO;

	_tempFirstMessage = nil;
	_tempLastMessage = nil;
	
	//_tempMessage = nil; //not used at all!

	_lastMessageDate = [_tempLastMessageDate isEqualToString:@""] || _tempLastMessageDate == nil ? _lastMessageDate : _tempLastMessageDate;
	_lastUserID = [_tempLastUserID isEqualToString:@""] || _tempLastUserID == nil ? _lastUserID : _tempLastUserID;
	
	if ([_firstMessagedate isEqualToString:@""]) {
		_firstMessagedate = _tempFirstMessageDate;
	}
	
	for (int i = 0; i < _messageBuffer.count; i++) {
		
		NSDictionary *message = [_messageBuffer objectAtIndex:i];
		NSLog(@"Displaying message from buffer: %@", message);
		
		if ([message valueForKey:@"user"] == nil || [[message valueForKey:@"user"] isEqualToString:@""]) {
			[self displayAppMessage:message];
		} else if ([[message valueForKey:@"user"] isEqualToString:_userId]) {
			[self displayUserMessage:message];
		} else {
			[self displayMessage:message];
		}
	}

	_messageBuffer = [[NSMutableArray alloc] init];
	
	/*
	NSLog(@"Debugging message list: ");
	ChatSingleMessage *temp = _lastMessage;
	while (temp.previousMessage != nil) {
		NSLog(@"message text: %@", temp.message[@"text"]);
		temp = temp.previousMessage;
	}
	NSLog(@"message text: %@", temp.message[@"text"]);
	*/
	
}

- (UIViewController *)parentViewController {
    UIResponder *responder = self;
    while (![responder isKindOfClass:[UIViewController class]] && responder != nil)
        responder = [responder nextResponder];
    return (UIViewController *)responder;
}

-(void)dealloc {
	
	_firstMessage = nil;
	_lastMessage = nil;
	[self.layer removeAllAnimations];
	[UIView cancelPreviousPerformRequestsWithTarget:self];
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	
}

@end
