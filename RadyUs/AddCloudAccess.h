//
//  AddCloudAccess.h
//  RadyUs
//
//  Created by Frane Poljak on 17/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCloud.h"
#import "ViewFriends.h"

@interface AddCloudAccess : UIViewController <ChooseFriendsDelegate>

@property (nonatomic, retain) IBOutlet UIView *accessPublic;
@property (nonatomic, retain) IBOutlet UIImageView *publicChecked;
@property (nonatomic, retain) IBOutlet UIView *accessFriends;
@property (nonatomic, retain) IBOutlet UIView *friendsChecked;
@property (nonatomic, retain) IBOutlet UIView *access1on1;
@property (nonatomic, retain) IBOutlet UIImageView *_1on1Checked;
@property (nonatomic, retain) IBOutlet UILabel *friendsNumLabel;

@property (nonatomic) enum CloudAccessType accessTtype;

@end
