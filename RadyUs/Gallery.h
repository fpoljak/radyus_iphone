//
//  Gallery.h
//  RadyUs
//
//  Created by Frane Poljak on 06/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"
#import "AsyncImageLoadManager.h"
#import "SideMenuUtilizer.h"

@interface Gallery : SideMenuUtilizer <UIGestureRecognizerDelegate, UIScrollViewDelegate, AsyncImageLoadDelegate>

@property (nonatomic, retain) IBOutlet UIScrollView *backgroundView;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) NSArray *images;
@property (nonatomic, retain) NSArray *profileImageViews;
@property (nonatomic) NSInteger currentImageIndex;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) UIViewController *parentVC;
@property (nonatomic, retain) NSMutableArray *imageViews;
@property (nonatomic, retain) UIGestureRecognizer *openingGestureRecognizer;
@property (nonatomic) CGRect closingAnimationFrame;

@end
