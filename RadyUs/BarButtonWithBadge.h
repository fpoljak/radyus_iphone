//
//  BarButtonWithBadge.h
//  Radyus
//
//  Created by Locastic MacbookPro on 07/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarButtonWithBadge : UIBarButtonItem

//@property (nonatomic, retain) UIButton *customView;
@property (nonatomic, retain) UIView *badge;
@property (nonatomic) NSInteger badgeNumber;
@property (nonatomic, retain) UILabel *badgeLabel;

-(void)setBadgeNumber:(NSInteger)badgeNumber;

@end
