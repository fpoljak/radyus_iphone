//
//  CloudView.h
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ApiService.h"
#import "AsyncImageLoadManager.h"
#import "SideMenuUtilizer.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface UnavailableCloudView : SideMenuUtilizer <ApiServiceDelegate, MKMapViewDelegate, MKAnnotation, MKOverlay>

@property (nonatomic, retain) NSJSONSerialization *cloud;
@property (nonatomic) CLLocationCoordinate2D userLocation;
@property (nonatomic, retain) IBOutlet UILabel *distanceLabel;
@property (nonatomic, retain) ApiService *apiService;
@property (nonatomic, retain) IBOutlet UIView *friendsInCloudDisplayView;
@property (nonatomic, retain) IBOutlet UILabel *friendsInCloudLabel;
@property (nonatomic, retain) UIViewController *parentVC;
@property (nonatomic, retain) IBOutlet MKMapView *mapView;

@end
