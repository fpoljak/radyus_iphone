//
//  AddCloud.h
//  RadyUs
//
//  Created by Frane Poljak on 16/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

@protocol AddCloudDelegate <NSObject>

-(void)cloudCreated:(NSJSONSerialization *)cloud;

@end

#import "SideMenuUtilizer.h"
#import <MapKit/MapKit.h>
#import "MapPin.h"
#import "ApiService.h"


@interface AddCloud : SideMenuUtilizer <UITextViewDelegate, MKMapViewDelegate, MKAnnotation, MKOverlay, CLLocationManagerDelegate, ApiServiceDelegate>

enum CloudAccessType : NSUInteger{
    CAUndefined = 0,
    CAPublic = 1,
    CAFriendsAll = 2,
    CAFriendsList = 3,
    CA1on1 = 4,
};

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) IBOutlet UITextView *cloudDescriptionTextView;
@property (nonatomic, retain) IBOutlet UITextView *cloudDescriptionPlaceholderView;
@property (nonatomic, retain) IBOutlet UIView *containerView2; //ne triba?
@property (nonatomic, retain) IBOutlet UIView *buttonContainer;
@property (nonatomic, retain) IBOutlet UIButton *titleButton;
@property (nonatomic, retain) IBOutlet UIButton *audienceButton;
@property (nonatomic, retain) IBOutlet UIButton *radiusButton;
@property (nonatomic, retain) IBOutlet UIButton *timeButton;
@property (nonatomic, retain) IBOutlet UIButton *createButton;
@property (nonatomic, retain) IBOutlet UILabel *counterLabel;
@property (nonatomic, retain) IBOutlet UIView *anonymusView;
@property (nonatomic, retain) IBOutlet UISwitch *anonymusSwitch;
@property (nonatomic) BOOL loaded;
@property (nonatomic, retain) MapPin *addCloudPin;
@property (nonatomic, retain) MKCircle *radiusCircle;
@property (nonatomic) CGFloat radius;
@property (nonatomic) BOOL mapStretched;
@property (nonatomic) NSInteger cloudDuration;
@property (nonatomic) enum CloudAccessType accessType;
@property (nonatomic, retain) NSMutableArray *cloudUsers;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic) CLLocationCoordinate2D cloudLocation;
@property (nonatomic, retain) UIImage *icon;
@property (nonatomic) BOOL radyusDefined;
@property (nonatomic) BOOL durationDefined;

@property (nonatomic, retain) ApiService *apiService;

@property (nonatomic, assign) id<AddCloudDelegate> delegate;

-(void)refreshAddCloudPin;
-(void) setMapBoundsToRadiusWithCenterIn:(CLLocationCoordinate2D)location;
-(BOOL) validateForm;
-(void)showAnonymusView;
-(void)hideAnonymusView;

@end

