//
//  DBManager.h
//  RadyUs
//
//  Created by Frane Poljak on 10/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

@property (nonatomic, strong) NSString *documentsDir;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSMutableArray *arrResults;
@property (nonatomic, strong) NSMutableArray *arrColumnNames;
@property (nonatomic) int affectedRows;
@property (nonatomic) long long lastInsertedRowID;

-(NSArray *)loadDataFromDB:(NSString *)query;
-(void)executeQuery:(NSString *)query;
-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;
-(void)copyDatabaseIntoDocumentsDirectory;

-(void) saveUser:(NSJSONSerialization *) user;
-(void) saveUsers:(NSArray *)users;
-(void) saveCloud:(NSJSONSerialization *) cloud;
-(void) saveClouds:(NSArray *)clouds;
-(void) setCloud: (NSString *)cloudId active: (BOOL) active;
-(void) saveMessage:(NSJSONSerialization *) message;
-(void) saveMessages: (NSArray *)messages;
-(NSArray *) loadMessages: (NSString *) cloudID;
-(void) clearOldMessagesForCloud:(NSString *)cloudId;
-(NSArray *) loadClouds:(BOOL)own active:(BOOL)active;
-(NSArray *) loadUsers;
-(NSDictionary *) loadUserWithId: (NSString *)userId;

@end
