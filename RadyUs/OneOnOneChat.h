//
//  OneOnOneChat.h
//  Radyus
//
//  Created by Locastic MacbookPro on 24/09/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "SideMenuUtilizer.h"
#import "ApiService.h"
#import "DBManager.h"
#import "ChatView.h"
#import "ExploreMap.h"

@interface OneOnOneChat : SideMenuUtilizer <ApiServiceDelegate, UIScrollViewDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, retain) NSDictionary *friend;
@property (nonatomic, retain) NSString *friendId;
@property (nonatomic, retain) IBOutlet ChatView *chatView;
@property (nonatomic, retain) IBOutlet UITextView *messageInputView;
@property (nonatomic, retain) IBOutlet UITextView *messageInputPlaceholderView;
@property (nonatomic, retain) IBOutlet UIView *chatControlsContainer;
@property (nonatomic, retain) IBOutlet UIView *addPhotoControlsContainer;
@property (nonatomic, retain) IBOutlet UIButton *sendButton;

@property (nonatomic, retain) NSMutableArray *loadingMessageBuffer;
@property (nonatomic) BOOL messageLoading;
@property (nonatomic) BOOL checkedForNewMessages;

@property (nonatomic) CGSize kbSize;
@property (nonatomic) double kbAnimationDuration;
@property (nonatomic) UIViewAnimationOptions kbAnimationEffect;

@property (nonatomic, retain) ApiService *apiService;
@property (nonatomic, retain) DBManager *dbManager;

@property (nonatomic) enum CloudViewStates cloudViewState;

@end
