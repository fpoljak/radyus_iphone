//
//  PopoverView.h
//  Radyus
//
//  Created by Locastic MacbookPro on 08/03/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverView : UIView

@property (nonatomic) BOOL hidesCloseButton;
@property (nonatomic, retain) UIButton *closeButton;

-(void)dismiss:(id)sender;

@end
