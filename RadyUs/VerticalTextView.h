//
//  VerticalTextView.h
//  RadyUs
//
//  Created by Frane Poljak on 10/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerticalTextView : UITextView

@end
