//
//  ExploreTabBarController.m
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ExploreTabBarController.h"

@interface ExploreTabBarController ()

@end

@implementation ExploreTabBarController

BOOL firstLoad = YES;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	NSLog(@"1 viewDidLoad");
	
	//self.navigationController.navigationBar.topItem.title = @" ";
	
	_previousTitle = self.title ?: @"#search";
	
	if ([_previousTitle isEqualToString:@""]) {
		_previousTitle = @"#search";
	}
	
	self.title = _previousTitle;
    
    [self.tabBar setFrame:CGRectMake(0, 68, self.tabBar.frame.size.width, self.tabBar.frame.size.height)];//72
    
    //[[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitlePositionAdjustment:UIOffsetMake(0, -16)];
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [[UITabBar appearanceWhenContainedIn:[self class], nil] setBackgroundImage:blank];
    [[UITabBar appearanceWhenContainedIn:[self class], nil] setShadowImage:blank];
	
	UIImage *backButtonBackgroundImage = [UIImage imageNamed:@"back_button"];
	backButtonBackgroundImage = [backButtonBackgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 11, 0, 8)];
	[[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setBackButtonBackgroundImage:[backButtonBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	// TODO: prevent it from happening in image picker, subclass UINavigationBar?
	
	UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:NULL];
	self.navigationItem.backBarButtonItem = backBarButton; ////////izbacit?
	
	[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamRounded-Medium" size:14.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
	
    //[[UITabBar appearanceWhenContainedIn:[self class], nil] setSelectionIndicatorImage:[UIImage imageNamed:@"tab_selected.png"]];
    
    //[(UITabBarItem*)[[[self tabBar] items] objectAtIndex:0] setSelectedImage:[[UIImage imageNamed:@"login_tab_selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    //[(UITabBarItem*)[[[self tabBar] items] objectAtIndex:1] setSelectedImage:[[UIImage imageNamed:@"login_tab_selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDistanceLabels) name:@"RadyusDistanceUnitChangedNotification" object:nil];

}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)toggleSlideMenu:(id)sender {
	[(SideMenuUtilizer *)self.selectedViewController toggleSlideMenu:sender];
}

-(void)commitSettings:(NSDictionary *)settings {
    
    [[NSUserDefaults standardUserDefaults] setFloat:[[settings objectForKey:@"radius"] floatValue] forKey:@"RadiusForReadingClouds"];
    [[NSUserDefaults standardUserDefaults] setObject:[settings objectForKey:@"access"] forKey:@"AccessForReadingClouds"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [(ExploreList *)[self.childViewControllers lastObject] commitSettings:settings];
    [(ExploreMap *)[self.childViewControllers firstObject] commitSettings:settings];
	
}

-(void)setDistanceLabels {
	
	NSDictionary *settings = [NSDictionary dictionaryWithObjects:@[[NSNumber numberWithFloat:[[[NSUserDefaults standardUserDefaults] valueForKey:@"RadiusForReadingClouds"] floatValue]],
																   [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessForReadingClouds"]]
														 forKeys:@[@"radius",
																   @"access"]];
	
	[(ExploreList *)[self.childViewControllers lastObject] commitSettings:settings];
	[(ExploreMap *)[self.childViewControllers firstObject] commitSettings:settings];
	
}

-(void)cloudCreated:(NSJSONSerialization *)cloud {
	
	_selectedCloud = cloud;
	[self performSegueWithIdentifier:@"ViewRadyus" sender:nil];
	
	//[(ExploreList *)[self.childViewControllers lastObject] cloudCreated:cloud];
	[(ExploreMap *)[self.childViewControllers firstObject] cloudCreated:cloud];
	
}

-(void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
	
	if (firstLoad) {
		firstLoad = NO;
	} else {
		//[self.selectedViewController viewDidAppear:animated];
		//radi notification observera
		// TODO: change approach!
	}
	
	self.title = (_previousTitle ?: self.title) ?: @"#search";
	self.navigationController.navigationBar.topItem.title = self.title;
	
	UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bars.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleSlideMenu:)];
	//BarButtonWithBadge *menuButton = [[BarButtonWithBadge alloc] initWithImage:[UIImage imageNamed:@"bars.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleSlideMenu:)];
	
	//[menuButton setBadgeNumber:6];
	
	self.navigationItem.leftBarButtonItem = menuButton;
	
	[self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
	
	[self.navigationItem setHidesBackButton:YES animated:NO];
	
	//[[self navigationController] setViewControllers:[NSArray arrayWithObject:self]]; ////////????

}

-(void)setSelectedViewController:(__kindof UIViewController *)selectedViewController {
    for (UIViewController *vc in self.childViewControllers) {
        if ([vc isEqual:self.selectedViewController]) {
            [vc viewWillDisappear:YES];
        }
    }
    [super setSelectedViewController:selectedViewController];
    [selectedViewController viewDidAppear:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
	
	[super viewWillDisappear:animated];
	
	_previousTitle = self.title;
	//self.navigationController.navigationBar.topItem.title = @" ";
	//self.title = @" ";

    for (UIViewController *vc in self.childViewControllers) {
        if ([vc isEqual:self.selectedViewController]) {
            [vc viewWillDisappear:animated];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([segue.identifier isEqualToString:@"ViewRadyus"]) {
	
		CloudView *dvc = [segue destinationViewController];
		//[dvc setCloud:_selectedCloud];
        [dvc setCloudId:[_selectedCloud valueForKey:@"id"]];
		
	} else if ([segue.identifier isEqualToString:@"ViewCloudUnavailable"]) {
		
		UnavailableCloudView *dvc = [segue destinationViewController];
		[dvc setCloud:_selectedCloud];
		[dvc setUserLocation:_userLocation];
		[dvc setParentVC:self];
		
	} else if ([[segue identifier] isEqualToString:@"CreateCloud"]) {
		
		AddCloud *dvc = [segue destinationViewController];
		[dvc setDelegate:self];
		
	}
		
}

-(void)dealloc {

	[[(ExploreMap *)[self.viewControllers firstObject] apiService] cancelRequests];
	[[(ExploreList *)[self.viewControllers lastObject] apiService] cancelRequests];

}


@end
