//
//  SafeBrowsingService.m
//  Radyus
//
//  Created by Frane Poljak on 25/11/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "SafeBrowsingService.h"

@implementation SafeBrowsingService

-(instancetype)initWithURL:(NSString *)urlString {
	
	_url = urlString;
	_reqData = [[NSMutableData alloc] init];
	
	return [super init];
	
}

-(void)performCheck {
	
	/*
	 Alternative:
	 http://www.malwaredomainlist.com
	 http://www.brightcloud.com/developers/api-overview.php
	 
	 Test:
	 http://security.stackexchange.com/questions/37383/are-there-faux-fake-malicious-websites-to-test-web-reputation-services-similar-t
	 https://safeweb.norton.com/report/show?url=jktdc.in%2Fimages%2Fklb%2Fazxvas.gif
	 */
	
	NSString *__url = [@"https://sb-ssl.google.com/safebrowsing/api/lookup?client=Radyus&key=AIzaSyDO1oL0po2wkTxhJs8cLZuke61BLOUkOsI&appver=1&pver=3.1&url=" stringByAppendingString:[_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:__url]];
	[request setHTTPMethod:@"GET"];
	[request setValue:@"utf-8" forHTTPHeaderField:@"Accept-Charset"];
	[request setValue:@"utf-8" forHTTPHeaderField:@"Accept-Encoding"]; ////////ne triba?
	[request setValue:@"text" forHTTPHeaderField:@"Accept"];
	[request setValue:@"text" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"max-age=10000" forHTTPHeaderField:@"Cache-Control"];
	[request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
	
	NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];

	[conn start];
	
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	_reqData = [[NSMutableData alloc] init];
	//try multiple times?
	[_delegate urlCheckFailedWithError:error];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[_reqData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"Response code: %@", response); //204-ok, 200-harmful
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {

	NSString *result = [[NSString alloc] initWithData:_reqData encoding:NSUTF8StringEncoding];
	
	/*
	 https://developers.google.com/safe-browsing/lookup_guide, see error codes and warnings, see more links, provided by, different message (and link?) for different type...
	 */
	
	if ([result isEqualToString:@""]) {
		[_delegate urlSafe:_url];
	} else {
		[_delegate urlHarmful:result];
	}
	
	_reqData = [[NSMutableData alloc] init];
	
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	return cachedResponse;
}

@end
