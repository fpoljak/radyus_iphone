//
//  TopAlignedUILabel.m
//  test3
//
//  Created by Frane Poljak on 23/01/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "TopAlignedUILabel.h"

@implementation TopAlignedUILabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib {

	[super awakeFromNib];
	
	CGFloat width = [self superview].frame.size.width - self.frame.origin.x * 2;
	CGRect f = self.frame;
	[self setFrame:CGRectMake(f.origin.x, f.origin.y, width, f.size.height)];
	self.bounds = CGRectMake(0, 0, width, f.size.height);
	self.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds);

}

- (void)drawTextInRect:(CGRect)rect {
    CGFloat verticalOffset = 4;
    UIEdgeInsets insets = {verticalOffset, 0, [self bottomPadding] - verticalOffset, 0};
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

/*
-(BOOL)isTableViewCellChild {
    //NSLog(@"%@", NSStringFromClass([self.superview class]));
    return  [NSStringFromClass([self.superview class]) isEqualToString:@"UITableViewCellContentView"] ||
    [[self.superview superview] isKindOfClass:[UITableViewCell class]] ||
    [[self superview] isKindOfClass:[UITableViewCell class]];
}
*/

-(CGFloat)bottomPadding {
    NSDictionary *attr = @{NSFontAttributeName:self.font};
    CGSize fontSize = [self.text sizeWithAttributes:attr];
    CGRect theStringSize = [self.text boundingRectWithSize:fontSize options:NSStringDrawingTruncatesLastVisibleLine attributes:attr context:nil];
    return self.frame.size.height - theStringSize.size.height;
}

@end
