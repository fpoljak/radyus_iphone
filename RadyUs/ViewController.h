//
//  ViewController.h
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMotion/CoreMotion.h>
#import "Profile.h"
#import "ApiService.h"

@interface ViewController : UIViewController <ApiServiceDelegate, FBSDKLoginButtonDelegate, FBSDKAppInviteDialogDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) IBOutlet UILabel *messagesLabel;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginView;
@property (weak, nonatomic) IBOutlet UIButton *emailLoginButton;
@property (nonatomic, retain) IBOutlet UIImageView *logoIV;
@property (nonatomic, retain) IBOutlet UILabel *logoSubtitle;

@property (nonatomic, retain) CMMotionManager *motionManager;
@property (nonatomic) __block double adx, ady, adz;
@property (nonatomic,retain) NSTimer *accTimer;

@property (nonatomic, retain) ApiService *apiService;

@end

