//
//  PopoverView.m
//  Radyus
//
//  Created by Locastic MacbookPro on 08/03/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "PopoverView.h"

#define kArrowHeight 16.0f
#define edgeRadius 12.0f

@implementation PopoverView

-(instancetype)init {
	
	if (self = [super init]) {
		self.layer.zPosition = 9999;
		[self setBackgroundColor:[UIColor clearColor]];
	}
	
	return self;
	
}

-(instancetype)initWithFrame:(CGRect)frame {
	
	if (self = [super initWithFrame:frame]) {
		self.layer.zPosition = 9999;
		[self setBackgroundColor:[UIColor clearColor]];
        self.hidesCloseButton = NO;
	}
	
	return self;
	
}

- (void)drawRect:(CGRect)rect {
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	UIBezierPath *fillPath = [UIBezierPath bezierPath];
	
	[fillPath moveToPoint:CGPointMake(edgeRadius, self.bounds.origin.y + kArrowHeight)];
	
	//arrow up!
	[fillPath addLineToPoint:CGPointMake(self.bounds.size.width / 2 - kArrowHeight / 2, self.bounds.origin.y + kArrowHeight)]; // define arrow position percentage!
	[fillPath addLineToPoint:CGPointMake(self.bounds.size.width / 2, 5)]; //shadow height!
	[fillPath addLineToPoint:CGPointMake(self.bounds.size.width / 2 + kArrowHeight / 2, self.bounds.origin.y + kArrowHeight)];
	[fillPath addLineToPoint:CGPointMake(self.bounds.size.width - edgeRadius, self.bounds.origin.y + kArrowHeight)];
	
	[fillPath addQuadCurveToPoint:CGPointMake(self.bounds.size.width, self.bounds.origin.y + kArrowHeight + edgeRadius)
					 controlPoint:CGPointMake(self.bounds.size.width, self.bounds.origin.y + kArrowHeight)];
	
	[fillPath addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height - edgeRadius)];
	
	[fillPath addQuadCurveToPoint:CGPointMake(self.bounds.size.width - edgeRadius, self.bounds.origin.y + self.bounds.size.height)
					 controlPoint:CGPointMake(self.bounds.size.width, self.bounds.origin.y + self.bounds.size.height)];
	
	[fillPath addLineToPoint:CGPointMake(edgeRadius, self.bounds.origin.y + self.bounds.size.height)];
	
	[fillPath addQuadCurveToPoint:CGPointMake(0, self.bounds.origin.y + self.bounds.size.height - edgeRadius)
					 controlPoint:CGPointMake(0, self.bounds.origin.y + self.bounds.size.height)];
	
	[fillPath addLineToPoint:CGPointMake(0, edgeRadius + self.bounds.origin.y + kArrowHeight)];
	
	[fillPath addQuadCurveToPoint:CGPointMake(edgeRadius, self.bounds.origin.y + kArrowHeight)
					 controlPoint:CGPointMake(0, self.bounds.origin.y + kArrowHeight)];

    [fillPath closePath]; //?

	CGContextAddPath(context, fillPath.CGPath);
	CGContextSetFillColorWithColor(context, [[UIColor whiteColor] colorWithAlphaComponent:0.85f].CGColor);
	CGContextSetShadowWithColor(context, CGSizeMake(0, -3.5), 10.0f, [[UIColor darkGrayColor] colorWithAlphaComponent:0.75f].CGColor); //offset depending on arrow position!
	CGContextFillPath(context);
	
	//[self addGradientToPath:fillPath inContext:context]; //ne valja
	
	// TODO: add shadow, gradient, colors, alpha?

    if (_closeButton) {
        return;
    }

    _closeButton = [[UIButton alloc] initWithFrame: CGRectMake(self.frame.size.width - 30, 20, 20, 20)];
    [_closeButton setTitle:@"x" forState:UIControlStateNormal]; // test, TODO: background image instead
    [_closeButton addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];

    [_closeButton setHidden:self.hidesCloseButton];

    [self addSubview:_closeButton];
}

-(void)layoutSubviews {
    if (_closeButton) {
        _closeButton.frame = CGRectMake(self.frame.size.width - 30, 20, 20, 20);
    }
}

-(void)dismiss:(id)sender {

    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)sethidesCloseButton:(BOOL)hidesCloseButton {
    if (_closeButton) {
        [_closeButton setHidden:hidesCloseButton];
    }
}

-(void)addGradientToPath:(UIBezierPath *)roundedRectanglePath inContext:(CGContextRef)context {
	
	CGFloat myRed=0, myGreen=0, myBlue=0, myWhite=0, alpha=1;
	UIColor *gradientBaseColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.65]; //change
	UIColor *gradientTopColor;
	UIColor *gradientMiddleColor;
	UIColor *gradientBottomColor;
	
	BOOL s = [gradientBaseColor getRed:&myRed green:&myGreen blue:&myBlue alpha:&alpha ];
	
	if(!s) {
		[gradientBaseColor getWhite:&myWhite alpha:&alpha];
	}
	
	if(myRed < 0) myRed = 0;
	if(myGreen < 0) myGreen = 0;
	if(myBlue < 0) myBlue = 0;
	if(myWhite < 0) myWhite = 0;
	
	if(myWhite > 0.0f){
	
		gradientTopColor = [UIColor colorWithWhite:myWhite+0.35f alpha:1];
		gradientMiddleColor = [UIColor colorWithWhite:myWhite+0.13f alpha:1];
		gradientBottomColor = [UIColor colorWithWhite:myWhite alpha:1];
		
	} else {
		
		gradientTopColor = [UIColor colorWithRed:myRed+0.35f green:myGreen+0.35f blue:myBlue+0.35f alpha: 1];
		gradientMiddleColor = [UIColor colorWithRed:myRed+0.13f green:myGreen+0.13f blue:myBlue+0.13f alpha: 1];
		gradientBottomColor = [UIColor colorWithRed:myRed green:myGreen blue:myBlue alpha: 1];
		
	}
	
	NSArray* bgGradientColors = [NSArray arrayWithObjects:
								 (id)gradientBottomColor.CGColor,
								 (id)gradientBottomColor.CGColor,
								 (id)gradientMiddleColor.CGColor,
								 (id)gradientTopColor.CGColor, nil];
	
	CGFloat bgGradientLocations[] = {0, 0.4, 0.5, 1};
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGGradientRef bgGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)bgGradientColors, bgGradientLocations);
	
	[gradientBottomColor setFill];
	[roundedRectanglePath fill];

	CGContextDrawLinearGradient(context, bgGradient, CGPointMake(0, kArrowHeight), CGPointMake(0, kArrowHeight + 20), 0);
	
}

- (void)dealloc {
    _closeButton = nil;
}

@end
