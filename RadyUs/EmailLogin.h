//
//  EmailLogin.h
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiService.h"
#import "Profile.h"

@interface EmailLogin : UIViewController <UITextFieldDelegate, ApiServiceDelegate>

@property (nonatomic, retain) IBOutlet UITextField *userTF;
@property (nonatomic, retain) IBOutlet UITextField *passTF;

@property (nonatomic, retain) ApiService *apiService;

@end
