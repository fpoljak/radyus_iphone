//
//  LoginTabBarController.m
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "LoginTabBarController.h"

@interface LoginTabBarController ()

@property (nonatomic, retain) NSString *previousTitle;

@end

@implementation LoginTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	NSString *title = [[self.childViewControllers firstObject] isKindOfClass:[Archive class]] ? @"Archive" : @"Radyus";
	
	[self.navigationItem setTitle:title];

    _previousTitle = self.title ?: title;

    if ([_previousTitle isEqualToString:@""]) {
        _previousTitle = title;
    }

    self.title = _previousTitle;

    
    [self.tabBar setFrame:CGRectMake(0, 68, self.tabBar.frame.size.width, self.tabBar.frame.size.height)];//72
    
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitlePositionAdjustment:UIOffsetMake(0, -16)];
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [[UITabBar appearanceWhenContainedIn:[self class], nil] setBackgroundImage:blank];
    [[UITabBar appearanceWhenContainedIn:[self class], nil] setShadowImage:blank];
    
    //[[UITabBar appearance] setBackgroundColor:[UIColor clearColor]];
    
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamRounded-Medium" size:14.0f], NSFontAttributeName, UIColorFromRGB(0xCFCFCF), NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamRounded-Medium" size:14.0f], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamRounded-Medium" size:14.0f], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateHighlighted];
    
    //zbog bug-a u xcode-u 6.1 selected image se treba postaviti kroz kod
    //nedostupno prije iOS 7.0
    //UIImageRenderingModeAlwaysOriginal da ne promijeni boju
    
    [[UITabBar appearanceWhenContainedIn:[self class], nil] setSelectionIndicatorImage:[UIImage imageNamed:@"login_tab_selected.png"]];
    
    //if (IS_ATLEAST_IOS7) {
    
    [(UITabBarItem*)[[[self tabBar] items] objectAtIndex:0] setSelectedImage:[[UIImage imageNamed:@"login_tab_selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [(UITabBarItem*)[[[self tabBar] items] objectAtIndex:1] setSelectedImage:[[UIImage imageNamed:@"login_tab_selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
	
	//self.delegate = self;
    //}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setSelectedViewController:(__kindof UIViewController *)selectedViewController {
    for (UIViewController *vc in self.childViewControllers) {
        if ([vc isEqual:self.selectedViewController]) {
            [vc viewWillDisappear:YES];
        }
    }
    [super setSelectedViewController:selectedViewController];
    [selectedViewController viewDidAppear:YES];
}


-(void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];

    NSString *title = [[self.childViewControllers firstObject] isKindOfClass:[Archive class]] ? @"Archive" : @"Radyus";

    self.title = (_previousTitle ?: self.title) ?: title;
    self.navigationController.navigationBar.topItem.title = self.title;
}

-(void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];

    _previousTitle = self.title;
    //self.navigationController.navigationBar.topItem.title = @" ";
    //self.title = @" ";

    for (UIViewController *vc in self.childViewControllers) {
        if ([vc isEqual:self.selectedViewController]) {
            [vc viewWillDisappear:animated];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
