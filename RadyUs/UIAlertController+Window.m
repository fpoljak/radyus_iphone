//
//  UIAlertController+Window.m
//  Radyus
//
//  Created by Frane Poljak on 14/03/16., implemented from: http://stackoverflow.com/a/30941356/1630623
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "UIAlertController+Window.h"
#import <objc/runtime.h>


@interface UIAlertController (Private)

@property (nonatomic, strong) UIWindow *alertWindow;

@end

@implementation UIAlertController (Private)

@dynamic alertWindow;

- (void)setAlertWindow:(UIWindow *)alertWindow {
	objc_setAssociatedObject(self, @selector(alertWindow), alertWindow, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIWindow *)alertWindow {
	return objc_getAssociatedObject(self, @selector(alertWindow));
}

@end

@implementation UIAlertController (Window)

- (void)show {
	[self show:YES completion:nil];
}

- (void)show:(BOOL)animated completion:(void (^ _Nullable)(void))completion {
	
	self.alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	self.alertWindow.rootViewController = [[UIViewController alloc] init];
	
	self.alertWindow.tintColor = [UIApplication sharedApplication].delegate.window.tintColor;
	
	// window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
	UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
	self.alertWindow.windowLevel = topWindow.windowLevel + 1;
	
	[self.alertWindow makeKeyAndVisible];
	[self.alertWindow.rootViewController presentViewController:self animated:animated completion:completion];
	
}

@end
