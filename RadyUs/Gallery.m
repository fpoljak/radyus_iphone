//
//  Gallery.m
//  RadyUs
//
//  Created by Frane Poljak on 06/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "Gallery.h"
#import "ActionSuccessIndicator.h"
#import "ZoomableImageView.h"

@interface Gallery ()

@end

@implementation Gallery

CGRect originalFrame;
CGPoint panStartLocation;
BOOL pageControlBeingUsed;
CGRect statusBarFrame;

BOOL initialized;

static char imageViewIndex;

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	[_imageView setImage:nil];
    [_backgroundView setBackgroundColor:[[UIColor darkTextColor] colorWithAlphaComponent:0.0f]];
    [_imageView setAlpha:0.0f];
    _backgroundView.decelerationRate = UIScrollViewDecelerationRateFast;
	
	[_pageControl setHidesForSinglePage:YES];
    [_pageControl setNumberOfPages:[_images count]];
    
	pageControlBeingUsed = NO;
	
	statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
	
	initialized = NO;
	//[self initImages]; // Must be in viewDidAppear!!! (because of animation)
	
}

-(BOOL)prefersStatusBarHidden {
    return YES;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return [_imageViews objectAtIndex:_currentImageIndex];
}

-(void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {

	[scrollView setScrollEnabled:YES];
	
	if (scrollView.zoomScale != 1 || _pageControl.isHidden) { //zoomScale always 1!!! (why?)
		return;
	}
	
	[_pageControl setHidden:YES];

	for (UIImageView *iv in _imageViews) {
		if (![view isEqual:iv]) {
			[iv removeFromSuperview];
		}
	}
	
	[self adjustFrameForImageView:(UIImageView *)view atIndex:0];
	[_backgroundView setContentOffset:view.frame.origin];
	[_backgroundView setContentSize:view.frame.size];
	
}

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
	
	if (scale != 1) {
		
		return;
		
	}
	
	[_backgroundView setContentSize:CGSizeMake(_imageViews.count * self.view.frame.size.width, self.view.frame.size.height)];
	[_backgroundView setContentOffset:CGPointMake(_currentImageIndex * self.view.frame.size.width, 0)];
	scrollView.contentInset = UIEdgeInsetsZero;
	[self adjustFrameForImageView:(UIImageView *)view atIndex:_currentImageIndex];
	
	for (UIImageView *iv in _imageViews) {
		if (![view isEqual:iv]) {
			[_backgroundView addSubview:iv];
		}
	}
	
	[_pageControl setHidden:(_imageViews.count <= 1)];
	[scrollView setScrollEnabled:(_imageViews.count > 1)];
	
}

-(void)scrollViewDidZoom:(UIScrollView *)scrollView {
	
	UIImageView *imageView = (UIImageView *)[_imageViews objectAtIndex:_currentImageIndex];
	
	CGSize imgViewSize = imageView.frame.size;
	CGSize imageSize = imageView.image.size;
	
	CGSize realImgSize;
	if(imageSize.width / imageSize.height > imgViewSize.width / imgViewSize.height) {
		realImgSize = CGSizeMake(imgViewSize.width, imgViewSize.width / imageSize.width * imageSize.height);
	}
	else {
		realImgSize = CGSizeMake(imgViewSize.height / imageSize.height * imageSize.width, imgViewSize.height);
	}
	
	CGRect fr = CGRectMake(0, 0, 0, 0);
	fr.size = realImgSize;
	imageView.frame = fr;
	
	CGSize scrSize = scrollView.frame.size;
	float offx = (scrSize.width > realImgSize.width ? (scrSize.width - realImgSize.width) / 2 : 0);
	float offy = (scrSize.height > realImgSize.height ? (scrSize.height - realImgSize.height) / 2 : 0);
	
	scrollView.contentInset = UIEdgeInsetsMake(offy, offx, offy, offx);
	
}

-(void)initImages {
	
	[_backgroundView setMinimumZoomScale:1.0f];
	[_backgroundView setMaximumZoomScale:3.0f];
    
    _imageViews = [[NSMutableArray alloc] init];
    
    CGFloat width = _images.count * self.view.frame.size.width;
    [_backgroundView setContentSize:CGSizeMake(width, self.view.frame.size.height)];
	
	//[_imageView removeFromSuperview];
	
    for (int i = 0; i < _images.count; i++) {
		
		ZoomableImageView *imageView = [[ZoomableImageView alloc] init];
		
		UIImage *image = [(UIImageView *)[_profileImageViews objectAtIndex:i] image];
		
		if (i == _currentImageIndex) {
			
			originalFrame = [_imageView frame];
			//imageView.frame = originalFrame;
			[_imageView removeFromSuperview];
			_imageView = nil;
			
			//opening animation!
			CGRect frame = CGRectIsNull(_closingAnimationFrame) || CGRectIsEmpty(_closingAnimationFrame) ? originalFrame : _closingAnimationFrame;
			// add in-call status bar fix
			frame.origin.x += _currentImageIndex * _backgroundView.frame.size.width;
			imageView.frame = frame;
			
			imageView.alpha = 0.2f;
			
			if (image != nil) {
				[imageView setImage:image];
			}
			
			[_backgroundView addSubview:imageView];
			[_imageViews addObject:imageView];
			
			[UIView animateWithDuration:0.4f animations:^{
				
				imageView.alpha = 1.0f;
				
				if (image != nil) {
					imageView.frame = [self frameForImageView:imageView atIndex:_currentImageIndex];
				}
				
			}];
		
			//_imageView = imageView; not needed anymore!
			
		}
		
		if (image != nil) {
			
			if (i != _currentImageIndex) {
				[imageView setImage:image];
				[self adjustFrameForImageView:imageView atIndex:i];
			}
			
			UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imageLongPressed:)];
			[imageView addGestureRecognizer:longPress];
			
			UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageDoubleTapped:)];
			[doubleTap setNumberOfTapsRequired:2];
			[imageView addGestureRecognizer:doubleTap];
			
		} else {
			
			CGFloat w = 320;
			CGFloat h = 180;
			
			if (w > self.view.frame.size.width) {
				CGFloat aspect = w / self.view.frame.size.width;
				w = self.view.frame.size.width;
				h /= aspect;
			}
			
			CGFloat maxHeight = self.view.frame.size.height + (statusBarFrame.size.height <= 20 ? 0 : 20); // in-call status bar fix
			
			if (h > maxHeight) {
				CGFloat aspect = h / maxHeight;
				h = maxHeight;
				w /= aspect;
			}
			
			CGFloat x = self.view.frame.size.width / 2 - w / 2;
			CGFloat y = maxHeight / 2 - h / 2;
			
			if (statusBarFrame.size.height <= 20) {
				y += (_backgroundView.frame.size.height - self.view.frame.size.height) / 2; // in-call status bar fix
			}
			
			[imageView setFrame:CGRectMake(self.view.frame.size.width * i + x, y, w, h)];
			objc_setAssociatedObject(imageView, &imageViewIndex, [_images objectAtIndex:i], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
			AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:[_images objectAtIndex:i] imageView:imageView useActivityIndicator:YES];
			loader.delegate = self;
			[loader loadImage];
			
		}
		
        [imageView setUserInteractionEnabled:YES];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
		[imageView setClipsToBounds:YES];

        UIPanGestureRecognizer *swipeUpDown = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpDown:)];
        [swipeUpDown setDelegate:self];
		//[swipeUpDown requireGestureRecognizerToFail:_backgroundView.panGestureRecognizer];////////
        [imageView addGestureRecognizer:swipeUpDown];
		
		if (i != _currentImageIndex) {
			[_imageViews addObject:imageView];
			[_backgroundView addSubview:imageView];
		}
		
    }
    
    [self.view sendSubviewToBack:_backgroundView];
    
    [_backgroundView setContentOffset:CGPointMake(_currentImageIndex * self.view.frame.size.width, 0)];
    [_backgroundView setScrollEnabled:_imageViews.count > 1];
	
    [_pageControl addTarget:self action:@selector(setCurrentImageWithPageControl:) forControlEvents:UIControlEventValueChanged];

    [self updateImage];
	
}

-(void)loadSuccess:(UIImageView *)imageView {
	
	NSString *imageId = objc_getAssociatedObject(imageView, &imageViewIndex);
	
	if (imageId != nil && [_images indexOfObject:imageId] != NSNotFound) {
		
		[self adjustFrameForImageView:imageView atIndex:[_images indexOfObject:imageId]];
		
		UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imageLongPressed:)];
		[imageView addGestureRecognizer:longPress];
				
		UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageDoubleTapped:)];
		[doubleTap setNumberOfTapsRequired:2];
		[imageView addGestureRecognizer:doubleTap];
		
	}
	
}

-(void)adjustFrameForImageView:(UIImageView *)imageView atIndex:(NSInteger)index {
	
	[imageView setFrame:[self frameForImageView:imageView atIndex:index]];
	
}

-(CGRect)frameForImageView:(UIImageView *)imageView atIndex:(NSInteger)index {
	
	UIImage *image = imageView.image;
	
	CGFloat w = image.size.width;
	CGFloat h = image.size.height;
	
	if (w > self.view.frame.size.width) {
		CGFloat aspect = w / self.view.frame.size.width;
		w = self.view.frame.size.width;
		h /= aspect;
	}
	
	CGFloat maxHeight = self.view.frame.size.height + (statusBarFrame.size.height <= 20 ? 0 : 20); // in-call status bar fix
	if (h > maxHeight) {
		CGFloat aspect = h / maxHeight;
		h = maxHeight;
		w /= aspect;
	}
	
	CGFloat x = self.view.frame.size.width / 2 - w / 2;
	CGFloat y = maxHeight / 2 - h / 2;
	
	if (statusBarFrame.size.height <= 20) {
		y += (_backgroundView.frame.size.height - self.view.frame.size.height) / 2; // in-call status bar fix
	}
	
	return CGRectMake(self.view.frame.size.width * index + x, y, w, h);
	
}

-(void)viewDidAppear:(BOOL)animated{
	
	if (initialized) {
		return;
	}
	
	initialized = YES;
	
	[self initImages];
    [self animatebackgoundAlpha];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self setNeedsStatusBarAppearanceUpdate];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)animatebackgoundAlpha {
	
	[UIView animateWithDuration:0.35f delay:0.3f options:UIViewAnimationOptionCurveEaseInOut animations:^{
		_backgroundView.backgroundColor = [[UIColor darkTextColor] colorWithAlphaComponent:1.0f];
		//[_imageView setAlpha:1.0];
	} completion:nil];
	
}

-(void)imageDoubleTapped:(UIGestureRecognizer *)sender {
	
	[_backgroundView setZoomScale:(_backgroundView.zoomScale == 1 ? 2.0 : 1.0) animated:YES];
	
}

-(void)swipeUpDown:(UIPanGestureRecognizer *)sender {
	
	if (!CGAffineTransformIsIdentity(sender.view.transform)) {
		return;
	}
	
	// TODO: use translationInView!
    if (sender.state == UIGestureRecognizerStateBegan) {
        panStartLocation = [sender locationInView:_backgroundView];
        [_backgroundView setScrollEnabled:NO];
    }
	
	else if (sender.state == UIGestureRecognizerStateEnded) { //canceled?
		
        CGPoint stopLocation = [sender locationInView:_backgroundView];
        CGFloat dy = stopLocation.y - panStartLocation.y;
        CGPoint velocity = [sender velocityInView:sender.view];
		
        if (fabs(dy) < 80 && fabs(velocity.y) < 1600) {
            [self cancelFadeOut:sender];
        } else {
            [self fadeOutBackground:sender];
        }
        
    } else if (sender.state == UIGestureRecognizerStateChanged) {
		
        CGPoint currentLocation = [sender locationInView:_backgroundView];
        CGFloat dy = currentLocation.y - panStartLocation.y;
		CGFloat dx = currentLocation.x - panStartLocation.x;
		
        if (fabs(dy) < 20 || fabs(dy) > _backgroundView.frame.size.height / 2) {
            return;
        }
		
        CGFloat x = originalFrame.origin.x + dx;
        CGFloat y = originalFrame.origin.y + dy;
        CGFloat w = originalFrame.size.width;
        CGFloat h = originalFrame.size.height;
        CGFloat alpha = 1 - fabs(dy) / (self.view.frame.size.height / 2);
		
        sender.view.frame = CGRectMake(x, y, w, h);
        _backgroundView.backgroundColor = [[UIColor darkTextColor] colorWithAlphaComponent:alpha];
		
    }
	
}

-(IBAction)fadeOutBackground:(UIPanGestureRecognizer *)sender {
	
	if (CGRectIsEmpty(_closingAnimationFrame)) {
		_closingAnimationFrame = _openingGestureRecognizer != nil ? _openingGestureRecognizer.view.superview.frame : sender.view.bounds;
	}
	
    CGFloat x = _currentImageIndex * self.view.frame.size.width + _closingAnimationFrame.origin.x;
	CGFloat y = _closingAnimationFrame.origin.y;
    CGFloat w = _closingAnimationFrame.size.width;
    CGFloat h = _closingAnimationFrame.size.height;
	
	y += (_backgroundView.frame.size.height - self.view.frame.size.height) / 2; // in-call status bar fix
	
	if ([_parentVC isKindOfClass:Profile.class] && [sender.view isKindOfClass:UIImageView.class] && [(UIImageView *)sender.view image] != nil) {

		CGSize imageSize = [(UIImageView *)sender.view image].size;
		CGSize imageViewSize = [(UIImageView *)sender.view frame].size;
		
		CGFloat aspectRatio = imageSize.width / imageSize.height;
		CGFloat profileImageRatio = _closingAnimationFrame.size.width / _closingAnimationFrame.size.height;
		CGFloat compensationRatio = aspectRatio / profileImageRatio;
		
		if (compensationRatio < 1) {
		
			x -= imageViewSize.width * (1 - compensationRatio) / 2;
			w += imageViewSize.width * (1 - compensationRatio);
			y -= imageViewSize.height * (1 - compensationRatio) / 2;
			h += imageViewSize.height * (1 - compensationRatio);
		
		} else if (compensationRatio > 1) {

			x -= imageViewSize.width * (compensationRatio - 1) / 2;
			w += imageViewSize.width * (compensationRatio - 1);
			y -= imageViewSize.height * (compensationRatio - 1) / 2;
			h += imageViewSize.height * (compensationRatio - 1);

		}
		
	}
	
	CGRect newFrame = CGRectMake(x, y, w, h);
	
	if ([_parentVC isKindOfClass:Profile.class] && ([_parentVC respondsToSelector:@selector(returnImageToOriginalPosition:)])) {
		[(Profile *)_parentVC performSelector:@selector(returnImageToOriginalPosition:) withObject:_openingGestureRecognizer afterDelay:0.1f];
	}
	
	[UIView animateWithDuration:0.5f delay:0.0f usingSpringWithDamping:0.7f initialSpringVelocity:0.1f options:UIViewAnimationOptionCurveEaseInOut animations:^{
		sender.view.frame = newFrame;
		sender.view.alpha = 0.4f;
		_backgroundView.backgroundColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.0f];
	} completion:^(BOOL finished) {
		[self dismiss:sender];
	}];
	
}

-(IBAction)cancelFadeOut:(UIPanGestureRecognizer *)sender {
	
	[UIView animateWithDuration:0.2f animations:^{
		sender.view.frame = originalFrame;
		sender.view.alpha = 1.0f;
		_backgroundView.backgroundColor = [[UIColor darkTextColor] colorWithAlphaComponent:1.0f];
	} completion:^(BOOL finished) {
		[_backgroundView setScrollEnabled:_imageViews.count > 1];
	}];

}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
	
    if ([gestureRecognizer.view isKindOfClass:[UIImageView class]]) {
		
		if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
			CGPoint velocity = [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:gestureRecognizer.view];
			return fabs(velocity.y) > 2 * fabs(velocity.x) && _backgroundView.zoomScale == 1;
		}
		
		return YES;
    }
	
	return YES;
	
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	
	if ([gestureRecognizer isKindOfClass:UIPanGestureRecognizer.class] && [gestureRecognizer.view isKindOfClass:UIImageView.class] && [otherGestureRecognizer isEqual:_backgroundView.panGestureRecognizer]) {
		return NO;
	}
	
    return YES;
	
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
	
    if (pageControlBeingUsed || sender.zoomScale != 1 || _pageControl.isHidden) {
        return;
    }
	
    CGFloat pageWidth = _backgroundView.frame.size.width;
    int page = floor((_backgroundView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
	
    if (page > - 1 && page < _images.count) {
        [self setCurrentImage:page];
    }
	
}

-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
	
	if (scrollView.zoomScale != 1 || _pageControl.isHidden) {
		return;
	}
	
    CGPoint currentOffset = [scrollView contentOffset];
    CGFloat diff = targetContentOffset->x - currentOffset.x;
	diff *= fabs(velocity.x) * 0.8;
	//nać 'optimalnu' vrijednost
	//NSLog(@"current offset: %f, velocity: %f", currentOffset.x, velocity.x);
        
    *targetContentOffset = CGPointMake(currentOffset.x + diff, targetContentOffset->y);
    NSLog(@"targetContentOffset: %f, target index %f", targetContentOffset->x, targetContentOffset->x / scrollView.bounds.size.width);
    
    
    NSUInteger nearestIndex = (NSUInteger)(targetContentOffset->x / scrollView.bounds.size.width + 0.5f);
    nearestIndex = MAX(MIN(nearestIndex, _images.count - 1), 0);
    
    CGFloat xOffset = nearestIndex * scrollView.bounds.size.width;
	
    *targetContentOffset = CGPointMake(xOffset, targetContentOffset->y);
	
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate && scrollView.zoomScale == 1) {
        NSUInteger currentIndex = (NSUInteger)(scrollView.contentOffset.x / scrollView.bounds.size.width);
        [scrollView setContentOffset:CGPointMake(scrollView.bounds.size.width * currentIndex, 0) animated:YES];
    }
}

-(void)imageLongPressed:(UILongPressGestureRecognizer *)sender {
	
	__block UIImage *imageToSave = [(UIImageView *)sender.view image];
	
	UIAlertController *actionList = [UIAlertController alertControllerWithTitle:@"Image actions" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
	
	UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"Save image" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		[self saveImage:imageToSave];
	}];
	
	UIAlertAction *copyAction = [UIAlertAction actionWithTitle:@"Copy image" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		[self copyImage:imageToSave];
	}];
	
	UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
		//
	}];
	
	[actionList addAction:saveAction];
	[actionList addAction:copyAction];
	[actionList addAction:cancelAction];
	
	[self presentViewController:actionList animated:YES completion:nil];

}

-(BOOL)isPasteboardAvailable {
	return [[UIPasteboard generalPasteboard] isKindOfClass:UIPasteboard.class];
}

-(BOOL)image:(UIImage *)image isEqualToByBytes:(UIImage *)otherImage {
	
	NSData *imagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage)));
	NSData *otherImagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(otherImage.CGImage)));
	
	BOOL comparison = [imagePixelsData isEqualToData:otherImagePixelsData];
	
	return comparison;
	
}

-(void)copyImage:(UIImage *)image {
	
	BOOL success = NO;
	
	if ([self isPasteboardAvailable]) {
		
		UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
		[pasteboard setImage:image];
		
		NSData *data1 = UIImagePNGRepresentation(image);
		NSData *data2 = UIImagePNGRepresentation(pasteboard.image);
		
		success = data1.length == data2.length && /*data1.hash == data2.hash &&*/ [self image:image isEqualToByBytes:pasteboard.image]; // is it overhead?
		// ne radi dobro! (vjerojatno zbog hash-a?)
		
	}
	
	// http://stackoverflow.com/a/21551989/1630623
	// http://stackoverflow.com/questions/18942030/memory-leak-related-to-cgdataprovidercopydata
	// http://stackoverflow.com/a/12613632/1630623
	
	ActionSuccessIndicator *indicator = [ActionSuccessIndicator new];
	[indicator showWithSuccess:success inViewController:self duration:1.2f message:success?@"Copied":@"Failed to copy" completion:nil];
	
}

-(void)saveImage:(UIImage *)image {
	UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
	ActionSuccessIndicator *indicator = [ActionSuccessIndicator new];
	[indicator showWithSuccess:error==nil inViewController:self duration:1.4f message:error==nil?@"Image saved":@"Failed to save image!" completion:nil];
}

-(void)updateImage {
    [_pageControl setCurrentPage:_currentImageIndex];
    originalFrame = [(UIImageView *)[_imageViews objectAtIndex:_currentImageIndex] frame];
}

-(IBAction)setCurrentImageWithPageControl:(UIPageControl *)sender {
	
    CGRect frame;
    frame.origin.x = self.view.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.view.frame.size;
    [_backgroundView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
    [self setCurrentImage:[sender currentPage]];
	
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

-(void)setCurrentImage:(NSInteger)currentImage {
    _currentImageIndex = currentImage;
    [self updateImage];
}

-(IBAction)dismiss:(id)sender {
	
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	[self setNeedsStatusBarAppearanceUpdate];
	
    [self dismissViewControllerAnimated:YES completion:^{
	
    }];
	
	// na x
	if (![sender isKindOfClass:[UIPanGestureRecognizer class]] && [_parentVC isKindOfClass:Profile.class] && ([_parentVC respondsToSelector:@selector(returnImageToOriginalPosition:)])) {
		[(Profile *)_parentVC performSelector:@selector(returnImageToOriginalPosition:) withObject:_openingGestureRecognizer afterDelay:0.1f];
	}

    if ([_profileImageViews.firstObject respondsToSelector:@selector(setHidden:)]) {
        [_profileImageViews.firstObject setHidden:NO];
    }	
}

@end
