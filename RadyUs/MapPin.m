//
//  MapPin.m
//  RadyUs
//
//  Created by Frane Poljak on 16/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "MapPin.h"

@implementation MapPin

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;
@synthesize unique_id;
@synthesize isInReach;

- (id)initWithCoordinates:(CLLocationCoordinate2D)location id:(NSString *)_id placeName:(NSString *)placeName description:(NSString *)description isInRange:(BOOL)isInRange
{
	
    if (self = [super init]) {
        coordinate = location;
        title = placeName;
        subtitle = description;
		isInReach = isInRange;
        unique_id = _id;
    }
	
    return self;
	
}

-(void) setSubtitle:(NSString *)newSubtitle
{
    subtitle = newSubtitle;
}

// TODO: custom callout, check http://stackoverflow.com/a/19404994/1630623, alternativly show view over the map on pintapped, for arrow check: http://stackoverflow.com/a/16913096/1630623

@end

