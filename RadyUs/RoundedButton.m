//
//  RoundedButton.m
//  RadyUs
//
//  Created by Frane Poljak on 03/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "RoundedButton.h"

@implementation RoundedButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib {

    [super awakeFromNib];
    
    self.layer.cornerRadius = self.frame.size.height / 2;
    
}

@end
