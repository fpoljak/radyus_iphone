//
//  ActionSuccessIndicator.m
//  Radyus
//
//  Created by Locastic MacbookPro on 18/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "ActionSuccessIndicator.h"

@implementation ActionSuccessIndicator

-(instancetype)init {
	
	if ((self = [super init])) {
		//additional intialization
	}
	
	return self;
	
}

-(void)showWithSuccess:(BOOL)success inViewController:(UIViewController * _Nonnull)viewController duration:(CGFloat)duration message:(NSString * _Nullable)message completion:(void (^ _Nullable)(BOOL finished))completion {
	
	if (viewController == nil) {
		return;
	}
	
	UIImage *successImage = success ? [UIImage imageNamed:@"success.png"] : [UIImage imageNamed:@"failure.png"];
	UIImageView *successIndicator = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, successImage.size.width / 2, successImage.size.height / 2)];
	[successIndicator setContentMode:UIViewContentModeScaleAspectFit];
	[successIndicator setImage:successImage];
	
	[self addSubview:successIndicator];
	
	CGFloat height = successIndicator.frame.size.height;
	CGFloat width = successIndicator.frame.size.width;
	
	if (message != nil && ![message isEqualToString:@""]) {
		
		UILabel *messageLabel = [[UILabel alloc] init];
		[messageLabel setText:message]; //limit size
		[messageLabel sizeToFit];
		[messageLabel setFrame:CGRectMake(successIndicator.frame.size.width + 16, (successIndicator.frame.size.height - messageLabel.frame.size.height) / 2 + 8,
										  messageLabel.frame.size.width, messageLabel.frame.size.height)];
		
		[messageLabel setBackgroundColor:[UIColor clearColor]];
		[messageLabel setTextColor:[UIColor whiteColor]];
		[messageLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:13.0f]];
		
		[self addSubview:messageLabel];
	
		width = CGRectGetMaxX(messageLabel.frame) - 8;
		
	}

	[self setBackgroundColor:[[UIColor darkTextColor] colorWithAlphaComponent:0.7f]];
	[self.layer setCornerRadius:8.0f];
	[self.layer setZPosition:9999.0f];
	
	[self setAlpha:0.1f];

	self.frame = CGRectMake((viewController.view.frame.size.width - width) / 2 - 8, (viewController.view.frame.size.height - height) / 2 - 8, width + 16, height + 16);
	
	[viewController.view addSubview:self];
	
	[UIView animateWithDuration:0.25f animations:^{
		self.alpha = 1.0f;
	} completion:^(BOOL finished) {
		
		[UIView animateWithDuration:0.4f delay:duration options:UIViewAnimationOptionCurveEaseOut animations:^{
			self.alpha = 0.0f;
		} completion:^(BOOL finished) {
			[self removeFromSuperview];
			if (completion != nil) {
				completion(finished);
			}
		}];
		
	}];
	
}

@end
