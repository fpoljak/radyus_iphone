//
//  EditProfile.h
//  Radyus
//
//  Created by Locastic MacbookPro on 11/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiService.h"
#import "TransparentDialog.h"
#import "Profile.h"
#import "AsyncImageLoadManager.h"
#import "SideMenuUtilizer.h"

@interface EditProfile : SideMenuUtilizer

@property (nonatomic, retain) UIViewController *profileViewController;

@property (nonatomic, retain) ApiService *apiService;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) IBOutlet UIImageView *profileImage;

@property (nonatomic, retain) IBOutlet UIImageView *galleryImage1;
@property (nonatomic, retain) IBOutlet UIImageView *galleryImage2;
@property (nonatomic, retain) IBOutlet UIImageView *galleryImage3;
@property (nonatomic, retain) IBOutlet UIImageView *galleryImage4;
@property (nonatomic, retain) IBOutlet UIImageView *galleryImage5;

//@property (nonatomic, retain) NSMutableArray *images;
@property (nonatomic, retain) NSArray *imagesArray;

@property (nonatomic, retain) UIImageView *currentImageView;
@property (nonatomic, retain) UIImageView *highlightedImageView;

@property (nonatomic, retain) IBOutlet UIButton *editGalleryButton;

@property (nonatomic, retain) NSMutableDictionary *userInfo;

@property (nonatomic, retain) IBOutlet UITextField *firstLastName;
@property (nonatomic, retain) IBOutlet UITextField *userName;
@property (nonatomic, retain) IBOutlet UITextField *birthday;
@property (nonatomic, retain) IBOutlet UITextView *descriptionTV;
@property (nonatomic, retain) IBOutlet UILabel *counterLabel;
@property (nonatomic, retain) IBOutlet UILabel *email;
@property (nonatomic, retain) IBOutlet UITextField *gender;

@property (nonatomic, retain) UIView<UITextInput> *editingTextInput;

@property (nonatomic, retain) UIPickerView *genderPicker;
@property (nonatomic, retain) UIDatePicker *datePicker;
@property (nonatomic, retain) UIButton *endEditingButton;

@property (nonatomic, retain) NSMutableArray *editedImageIdArray;
@property (nonatomic, retain) NSMutableArray *imagesToUpload;
@property (nonatomic) NSInteger indexOfImageBeingUploaded;

@end
