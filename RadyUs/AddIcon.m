//
//  AddIcon.m
//  RadyUs
//
//  Created by Frane Poljak on 16/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AddIcon.h"
#import "AddCloud.h"

@interface AddIcon ()

@end

@implementation AddIcon

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

-(void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];

	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[self setNeedsStatusBarAppearanceUpdate];
	
}

-(BOOL)prefersStatusBarHidden {
	return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    int pages = floor(collectionView.contentSize.width / collectionView.frame.size.width);
    if (0.0f == fmodf(pages, 1.0f))
    {
        [_pageControl setNumberOfPages:pages];
    } else {
        [_pageControl setNumberOfPages:pages + 1];
    }
        
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AddIconCell" forIndexPath:indexPath];
    
    //set image
    
    return  cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 80;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //set icon
    UIImageView *iv = (UIImageView *)[[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:1];
    [self dismiss:[iv image]];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    CGFloat pageWidth = _collectionView.frame.size.width;
    float currentPage = _collectionView.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        _pageControl.currentPage = currentPage + 1;
    }
    else
    {
        _pageControl.currentPage = currentPage;
    }
    
}

-(IBAction)dismiss:(id)sender {
	
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	[self setNeedsStatusBarAppearanceUpdate];
	
    [self dismissViewControllerAnimated:YES completion:^{
        if ([sender isKindOfClass:[UIImage class]]) {
            //NSLog(@"changing icon, pinID = %@", [_mapPin unique_id]);
            [(AddCloud *)_parentVC setIcon:sender];
            [(AddCloud *)_parentVC refreshAddCloudPin];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
