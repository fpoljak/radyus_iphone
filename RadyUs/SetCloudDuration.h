//
//  SetCloudDuration.h
//  RadyUs
//
//  Created by Frane Poljak on 18/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCloud.h"

@interface SetCloudDuration : UIViewController

@property (nonatomic, retain) IBOutlet UISlider *durationSlider;

@end
