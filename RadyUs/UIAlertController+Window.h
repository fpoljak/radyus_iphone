//
//  UIAlertController+Window.h
//  Radyus
//
//  Created by Locastic MacbookPro on 14/03/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Window)

- (void)show;
- (void)show:(BOOL)animated completion:(void (^ _Nullable)(void))completion;

@end
