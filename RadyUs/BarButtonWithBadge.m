//
//  BarButtonWithBadge.m
//  Radyus
//
//  Created by Locastic MacbookPro on 07/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "BarButtonWithBadge.h"

@implementation BarButtonWithBadge
//@synthesize customView;

-(instancetype)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action {
	
	self = (typeof (self))[[UIBarButtonItem alloc] initWithImage:image style:style target:target action:action];
	
	UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width + 20, image.size.height + 20)];
	
	[button setContentMode:UIViewContentModeCenter];
	[button setBackgroundImage:image forState:UIControlStateNormal];
	
	[button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	
	_badgeNumber = 0;
	_badge = [[UIView alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 20, 0, 20, 20)];
	[_badge.layer setCornerRadius:10.0f];
	[_badge.layer setBorderColor:UIColorFromRGB(0xFFD203).CGColor];
	[_badge.layer setBorderWidth:2.0f];
	[_badge setBackgroundColor:UIColorFromRGB(0x252627)];
	_badgeLabel = [[UILabel alloc] initWithFrame:_badge.frame];
	[_badgeLabel setBackgroundColor:[UIColor clearColor]];
	[_badgeLabel setFont:[UIFont fontWithName:@"GothamRounded-Bold" size:8.0f]];
	[_badgeLabel setTextColor:UIColorFromRGB(0xFFD203)];
	[_badgeLabel setTextAlignment:NSTextAlignmentCenter];
	[_badge addSubview:_badgeLabel];
	[_badge setAlpha:0.0f];
	
	[button addSubview:_badge];
	[button bringSubviewToFront:_badge];
	
	self.customView = button;
	
	return self;
	
}

-(void)setTintColor:(UIColor *)tintColor {
	[super setTintColor:tintColor];
	[self.customView setTintColor:tintColor];
}

-(void)setBadgeNumber:(NSInteger)badgeNumber {
	
	_badgeNumber = badgeNumber;
	
	if (_badgeNumber > 0) {
		[_badge setAlpha:1.0f];
	} else {
		[_badge setAlpha:0.0f];
	}
	
	[_badgeLabel setText:[NSString stringWithFormat:@"%ld", (long)_badgeNumber]]; //>999?
	
}

@end
