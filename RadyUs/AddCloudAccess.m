//
//  AddCloudAccess.m
//  RadyUs
//
//  Created by Frane Poljak on 17/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AddCloudAccess.h"

@interface AddCloudAccess ()

@end

@implementation AddCloudAccess

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _accessTtype = CAUndefined;
    
	UITapGestureRecognizer *publicTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(publicAccessChecked:)];
    [_accessPublic addGestureRecognizer:publicTap];
    UITapGestureRecognizer *friendsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(friendsAccessChecked)];
    [_accessFriends addGestureRecognizer:friendsTap];
    UITapGestureRecognizer *_1on1Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_1on1AccessChecked:)];
    [_access1on1 addGestureRecognizer:_1on1Tap];
	
	AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];
	[parent.anonymusSwitch setEnabled:NO];
	[parent.anonymusView setAlpha:0.75];
	[parent.anonymusSwitch setOn:NO animated:YES];
	
}

-(void)publicAccessChecked:(UITapGestureRecognizer *)sender {
	
	AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];
	
    if (_accessTtype != CAPublic) {
        if (_accessTtype == CA1on1) {
            [__1on1Checked setAlpha:0.0f];
        } else {
            [_friendsChecked setAlpha:0.0f];
            [[_friendsChecked viewWithTag:1] setAlpha:0.0f];
            [[_friendsChecked viewWithTag:2] setAlpha:0.0f];
			[__1on1Checked setAlpha:0.0f];
        }

		[_publicChecked setAlpha:1.0f];
		//if (sender != nil) {
			[parent showAnonymusView];
			[parent.anonymusSwitch setEnabled:YES];
			[parent.anonymusView setAlpha:1];
		//}

    }
    _accessTtype = CAPublic;
	
    [[parent audienceButton] setTitle:@" Public" forState:UIControlStateNormal];
    [[parent audienceButton] setTitle:@" Public" forState:UIControlStateHighlighted];
    [[parent audienceButton] setTitle:@" Public" forState:UIControlStateSelected];
    [[parent audienceButton] setSelected:YES];
	[[parent timeButton] setEnabled:YES];
    [parent setAccessType:_accessTtype];
    [parent validateForm];
}

-(void)friendsAccessChecked {
    if (_accessTtype != CAFriendsAll || _accessTtype != CAFriendsList) {
        if (_accessTtype == CA1on1) {
            [__1on1Checked setAlpha:0.0f];
        } else {
            [_publicChecked setAlpha:0.0f];
        }
        
    }
    
    AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];
    
    [parent performSegueWithIdentifier:@"CloudAddFriends" sender:@"multiple"];
    
}

-(void)_1on1AccessChecked:(id)sender {
    if (_accessTtype != CA1on1) {
        if (_accessTtype == CAPublic) {
            [_publicChecked setAlpha:0.0f];
        }
    }
    
    if (sender == nil) {
        [__1on1Checked setAlpha:1.0f];
        return;
    }
    
    AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];
    
    [parent performSegueWithIdentifier:@"CloudAddFriends" sender:@"single"];
    
}

-(void)didChooseFriends:(id)sender {

    NSString *title = @" Friends";
	AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];

    enum CloudAccessType previousAccess = _accessTtype;
    
	if ([sender isKindOfClass:[NSString class]]) { ////prominit
        _accessTtype = CAFriendsAll;
        //title = [title stringByAppendingString:@"All"];
        [_friendsNumLabel setText:@"All"];
    } else if ([sender isKindOfClass:[NSMutableArray class]] && [(NSMutableArray *)sender count] > 0) {
        _accessTtype = CAFriendsList;
        NSString *friendsNum = [NSString stringWithFormat:@"%lu", (unsigned long)[(NSMutableArray *)sender count]];
        //savefriendlist (id)
        //title = [title stringByAppendingString:[NSString stringWithFormat:@" (%@)", friendsNum]];
        [_friendsNumLabel setText:friendsNum];
		[parent setCloudUsers:[[sender mutableCopy] valueForKey:@"id"]];
    } else {
        _accessTtype = CAUndefined;
		if (previousAccess == CAPublic) {
			[self publicAccessChecked: nil];
			[__1on1Checked setAlpha:0.0f];
		}
        else if (previousAccess == CA1on1) [self _1on1AccessChecked:nil];
        return;
    }
    
    [_friendsChecked setAlpha:1.0f];
    [[_friendsChecked viewWithTag:1] setAlpha:1.0f];
    [[_friendsChecked viewWithTag:2] setAlpha:1.0f];
	
	if (_accessTtype != CAPublic) {
		//[parent hideAnonymusView];
		[parent.anonymusSwitch setEnabled:NO];
		[parent.anonymusView setAlpha:0.75];
		[parent.anonymusSwitch setOn:NO animated:YES];
	}
    [[parent audienceButton] setTitle:title forState:UIControlStateNormal];
    [[parent audienceButton] setTitle:title forState:UIControlStateHighlighted];
    [[parent audienceButton] setTitle:title forState:UIControlStateSelected];
    [[parent audienceButton] setSelected:YES];
	[[parent timeButton] setEnabled:YES];
    [parent setAccessType:_accessTtype];
    [parent validateForm];
}

-(void)didChooseSingleFriend:(id)sender {
    
    enum CloudAccessType previousAccess = _accessTtype;

    if (sender == nil) {
        _accessTtype = CAUndefined;
		if (previousAccess == CAPublic) [self publicAccessChecked:nil];
        return;
    }
    
    [_friendsChecked setAlpha:0.0f];
    [[_friendsChecked viewWithTag:1] setAlpha:0.0f];
    [[_friendsChecked viewWithTag:2] setAlpha:0.0f];
    
    [__1on1Checked setAlpha:1.0f];
    
    _accessTtype = CA1on1;
    
    NSDictionary *selectedFriend = (NSDictionary *)sender;
    
    AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];
	//[parent hideAnonymusView];
	[parent.anonymusSwitch setEnabled:NO];
	[parent.anonymusView setAlpha:0.75];
	[parent.anonymusSwitch setOn:NO animated:YES];
    [[parent audienceButton] setTitle:@" 1on1" forState:UIControlStateNormal];
    [[parent audienceButton] setTitle:@" 1on1" forState:UIControlStateHighlighted];
    [[parent audienceButton] setTitle:@" 1on1" forState:UIControlStateSelected];
    [parent setAccessType:_accessTtype];
	[parent setCloudUsers:[NSMutableArray arrayWithObject:[selectedFriend valueForKey:@"id"]]];
    [[parent audienceButton] setSelected:YES];
	[[parent timeButton] setEnabled:NO];
    [parent validateForm];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
