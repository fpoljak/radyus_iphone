//
//  ChangeRadius.h
//  RadyUs
//
//  Created by Frane Poljak on 18/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCloud.h"

@interface ChangeRadius : UIViewController

@property (nonatomic, retain) IBOutlet UISlider *radiusSlider;

@property (nonatomic, retain) IBOutlet UILabel *minDistanceLabel;
@property (nonatomic, retain) IBOutlet UILabel *medDistanceLabel;
@property (nonatomic, retain) IBOutlet UILabel *maxDistanceLabel;

@end
