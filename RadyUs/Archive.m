//
//  Archive.m
//  Radyus
//
//  Created by Locastic MacbookPro on 30/07/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "Archive.h"

@interface Archive ()

@end

@implementation Archive

static char cloudKey;
BOOL loaded;

- (void)viewDidLoad {
	
	_nonArchive = !!_nonArchive;

    BOOL myOnly = self.tabBarController != nil && (self.tabBarController.selectedIndex == (int)!loaded || self.tabBarController.selectedIndex == NSNotFound);
    BOOL own = _nonArchive || myOnly;

    self.title = own ? @"My clouds" : @"All clouds";

    [super viewDidLoad];
	
	NSString *dbName = [NSString stringWithFormat:@"%@.sqlt", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	_dbManager = [[DBManager alloc] initWithDatabaseFilename: dbName];
	
	_clouds = [NSMutableArray arrayWithArray:[_dbManager loadClouds:NO active:_nonArchive]];
	
	_searchBar = [[UISearchBar alloc] init];
	_searchBar.delegate = self;
	_searchBar.placeholder = @"filter";

	[(self.tabBarController != nil ? self.tabBarController.navigationItem : self.navigationItem) setTitleView:_searchBar];
	
	_filteredClouds = [[NSMutableArray alloc] initWithArray:_clouds]; //copyItems?
	
	_scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
	
	loaded = NO;
	[self drawCloudList];
	loaded = YES;
	
}

-(void)viewDidAppear:(BOOL)animated {

	[super viewDidAppear:animated];

	//[(self.tabBarController != nil ? self.tabBarController.navigationItem : self.navigationItem) setTitleView:nil];
	[(self.tabBarController != nil ? self.tabBarController.navigationItem : self.navigationItem) setTitleView:_searchBar];

}

-(void) clearCloudList	{

	while (_scrollView.subviews.count > 0) { //uiimageview (scroll indicator) ?
		UIView *subview = _scrollView.subviews.firstObject;
        //if (!([subview isKindOfClass:[UIImageView class]] && [subview.superview isEqual:_scrollView])) {
            [subview removeFromSuperview];
            subview = nil;
        //}
	}

	//setcontentsize
	
}

-(void) drawCloudList {

	BOOL myOnly = self.tabBarController != nil && (self.tabBarController.selectedIndex == (int)!loaded || self.tabBarController.selectedIndex == NSNotFound);
	BOOL own = _nonArchive || myOnly;
	
	CGFloat currentY = 0 - (own ? 57 : 0); ////privremeno, rješit u IB-u
	
	NSPredicate *predicate;
	
	if (own) {
		predicate = [NSPredicate predicateWithFormat:@"creator = %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	} else {
		predicate = [NSPredicate predicateWithValue:YES];
	}
	
	for (NSDictionary *cloud in [_filteredClouds filteredArrayUsingPredicate:predicate]) {
		currentY += [self drawCloudListItem:cloud startingY:currentY];
	}
	
	[_scrollView setContentSize:CGSizeMake(self.view.frame.size.width, currentY)];
	
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:YES animated:YES];
	return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	[self filterCloudList:searchText];
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
	[searchBar setShowsCancelButton:NO animated:YES];
	return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:NO animated:YES];
	searchBar.text = @"";
	[self.view endEditing:YES];
	_filteredClouds = [NSMutableArray arrayWithArray:_clouds];
	[self clearCloudList];
	[self drawCloudList];
}

-(void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:NO animated:YES];
	[self.view endEditing:YES];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar setShowsCancelButton:NO animated:YES];
	[self.view endEditing:YES];
}

-(void)filterCloudList:(NSString *)filter {

	NSPredicate *predicate;
	
	if ([filter isEqualToString:@""] || filter == nil) {
		predicate = [NSPredicate predicateWithValue:YES];
	} else {
		predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", filter];
	}
	
	_filteredClouds = [NSMutableArray arrayWithArray:[_clouds filteredArrayUsingPredicate:predicate]];
	
	[self clearCloudList];
	[self drawCloudList];
							  
}

-(CGFloat)drawCloudListItem:(NSDictionary *)cloud startingY:(CGFloat)startingY{
	
	static NSInteger index = 1000; //reset
	
	UIButton *listItem = [[UIButton alloc] init];
	[listItem setTag:index++];
	[listItem setAutoresizesSubviews:NO];
	[listItem setBackgroundColor:_scrollView.backgroundColor];
	
	UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 12, self.view.frame.size.width - 16, 1)];
	
	//background color, text color, font, font size, line break, max num of lines,...
	[descLabel setNumberOfLines:5];
	[descLabel setLineBreakMode:NSLineBreakByWordWrapping];
	[descLabel setPreferredMaxLayoutWidth:self.view.frame.size.width - 16];
	[descLabel setText:[cloud valueForKey:@"name"]];
	[descLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:21]];
	
	CGSize maxSize = CGSizeMake(self.view.frame.size.width - 16, CGFLOAT_MAX);
	CGSize requiredSize = [descLabel sizeThatFits:maxSize];
	[descLabel setFrame:CGRectMake(8, 12, requiredSize.width, requiredSize.height)];
	
	[listItem setFrame:CGRectMake(0, startingY, self.view.frame.size.width, descLabel.frame.size.height + 24)];
	
	[listItem addSubview:descLabel];
	
	objc_setAssociatedObject(listItem, &cloudKey, cloud, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

	[listItem addTarget:self action:@selector(cloudListItemSelected:) forControlEvents:UIControlEventTouchUpInside];
	[listItem addTarget:self action:@selector(highlightItem:) forControlEvents:UIControlEventTouchUpOutside|UIControlEventTouchDown|UIControlEventTouchCancel];
	
	UIView *listDivider = [[UIView alloc] initWithFrame:CGRectMake(8, listItem.frame.size.height - 0.5f, listItem.frame.size.width - 8, 0.5f)];
	[listDivider setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
	[listItem addSubview:listDivider];
	
	[_scrollView addSubview: listItem];

	return listItem.frame.size.height;
	
}

-(void)cloudListItemSelected:(UIButton *)sender {
	
	NSDictionary *cloud = objc_getAssociatedObject(sender, &cloudKey);
	_selectedCloud = cloud;
	
	CloudView *cloudView = [self.storyboard instantiateViewControllerWithIdentifier:@"CloudView"];
	[cloudView setCloud:(NSJSONSerialization *)_selectedCloud];
	[self.navigationController pushViewController:cloudView animated:YES];
	
}

-(void)highlightItem:(UIView *)sender {
	[sender setAlpha:0.75f];
	[self performSelector:@selector(deselectCloudListItem:) withObject:sender afterDelay:0.5f];
}


-(void)deselectCloudListItem:(UIView *)sender {
	[sender setAlpha:1.0f];
}


//filter predicate

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
