//
//  CustomImageWithBadge.h
//  RadyUs
//
//  Created by Frane Poljak on 24/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "InputContainer.h"

@interface CustomImageWithBadge : UIImageView

@property (nonatomic, retain) UIView *badge;
@property (nonatomic) NSInteger badgeNumber;
@property (nonatomic, retain) UILabel *badgeLabel;

-(void)setBadgeNumber:(NSInteger)badgeNumber;

@end
