//
//  ApiService.m
//  RadyUs
//
//  Created by Frane Poljak on 15/04/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ApiService.h"
#import "SideMenuUtilizer.h"

@implementation ApiService

const NSString *client_id = @"test";
const NSString *client_secret = @"test";
const NSString *apiUrl = @"http://api.rady.us/v1";

@synthesize uploadTaskID;

static char delegateStrongReferenceKey;

/*
static char connectionDataKey;

static NSOperationQueue *_operationQueue;

-(NSOperationQueue *)apiCallsQueue {
	return _operationQueue;
}
*/

-(instancetype)init {

	/*
	static dispatch_once_t oncePredicate;
	
	dispatch_once(&oncePredicate, ^{
		_operationQueue = [[NSOperationQueue alloc] init];
		[_operationQueue setQualityOfService:NSQualityOfServiceUserInitiated];
		[_operationQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
	});
	 */
	
	_trackProgress = NO;
	
	_responseData = [[NSMutableData alloc] init];
	_messagesData = [[NSMutableData alloc] init];
	_activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	_loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 70)];
	[_loadingView addSubview:_activityIndicator];
	[_loadingView setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.75]];
	[_loadingView.layer setCornerRadius:8.0f];
	[_loadingView.layer setZPosition:101.0f];
	[_loadingView setUserInteractionEnabled:NO];////
	_activityIndicator.center = _loadingView.center;
	_activityIndicator.frame = CGRectOffset(_activityIndicator.frame, 0, -8);
	_loadingMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 46, 180, 16)];
	[_loadingMessage setTextColor:[UIColor whiteColor]];
	[_loadingMessage setTextAlignment:NSTextAlignmentCenter];
	[_loadingMessage setFont:[UIFont fontWithName:@"GothamRounded-Book" size:14]];
	[_loadingView addSubview:_loadingMessage];
	
	NSString *dbName = [NSString stringWithFormat:@"%@.sqlt", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	_dbManager = [[DBManager alloc] initWithDatabaseFilename: dbName];
	
	_delegateStrongReferenceCounter = 0;
	
	_runningOperations = [NSMutableArray<NSURLConnection *> new];
	
	return [super init];

}

-(NSMutableURLRequest *)defaultRequestWithMethod:(NSString *)method endpoint:(NSString *)endpoint timeout:(NSTimeInterval)timeout params:(NSDictionary *)params useAuth:(BOOL)useAuth {
	
	if (![endpoint hasPrefix:@"/"]) {
		endpoint = [@"/" stringByAppendingString:endpoint];
	}
	
	NSString *urlString = [apiUrl stringByAppendingString:endpoint];
	
	NSURL *url = [[NSURL alloc] initWithString:urlString];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
	
	[request setHTTPMethod:method];
	[request setTimeoutInterval:timeout];
	[request setValue:@"utf-8" forHTTPHeaderField:@"Accept-Charset"];
	[request setValue:@"utf-8" forHTTPHeaderField:@"Accept-Encoding"]; ////////ne triba?
	[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	
	if (useAuth) {
		
		NSString *authField = [[NSUserDefaults standardUserDefaults] stringForKey:@"accessToken"];
		
		if (authField != nil) {
			[request setValue:authField forHTTPHeaderField:@"Authorization"];
		}
		
	}
	
	if (params != nil) {

		NSError *error;
		NSData *paramsData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
		
		if (!error) {
			[request setHTTPBody: paramsData];
		}
	
	}
    
    NSLog(@"API Request: %@, %@, %@, %@", method, endpoint, params, request.allHTTPHeaderFields);
	
	return request;
	
}

-(void)doSignUpWithFirstName:(NSString *)first lastName:(NSString *)last userName:(NSString *)user email:(NSString *)mail password:(NSString *)password birthday:(NSString *)birthday gender:(NSInteger) gender {
	
	NSString *endpoint = @"/signup";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:client_id forKey:@"clientId"];
	[_params setObject:client_secret forKey:@"clientSecret"];
	[_params setObject:user forKey:@"username"];
	[_params setObject:password forKey:@"password"];
	[_params setObject:first forKey:@"nameFirst"];
	[_params setObject:last forKey:@"nameLast"];
	[_params setObject:mail forKey:@"email"];
	[_params setObject:birthday forKey:@"birthday"];
	[_params setObject:[NSNumber numberWithInteger: gender ] forKey:@"gender"];

	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:NO];
	
	[_loadingMessage setText:@"Signing up..."];
	
	_signupConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_signupConn start];
	
	
}

-(void)getAccesTokenForUser:(NSString *)userName password:(NSString *)password {
	
	NSString *endpoint = @"/auth";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:client_id forKey:@"clientId"];
	[_params setObject:client_secret forKey:@"clientSecret"];
	[_params setObject:@"password" forKey:@"grantType"];
	[_params setObject:userName forKey:@"username"];
	[_params setObject:password forKey:@"password"];
	[_params setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"] forKey:@"device"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:NO];

	[_loadingMessage setText:@"Logging in..."];
	
	_getTokenConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getTokenConn start];
	
}

-(void)fbAuth:(NSString *)fbAccessToken {
	
	NSString *endpoint = @"/auth";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:client_id forKey:@"clientId"];
	[_params setObject:client_secret forKey:@"clientSecret"];
	[_params setObject:@"facebook" forKey:@"grantType"];
	[_params setObject:fbAccessToken forKey:@"token"];
	[_params setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"] forKey:@"device"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:NO];

	[_loadingMessage setText:@"Logging in..."];
	
	_fbAuthConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_fbAuthConnection start];

}

-(void)refreshAccesToken {
	
	NSString *refreshToken;
	
	if ((refreshToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"refreshToken"]) == nil) {
		if ([_delegate respondsToSelector:@selector(apiService:taskFailedWithErrorCode:)]) {
			[_delegate apiService:self taskFailedWithErrorCode:401];//error_code
		}
		return; //get new tokens automatically?
	}
	
	NSString *endpoint = @"/auth";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:client_id forKey:@"clientId"];
	[_params setObject:client_secret forKey:@"clientSecret"];
	[_params setObject:@"refreshToken" forKey:@"grant_type"];
	[_params setObject:refreshToken forKey:@"refreshToken"];
	[_params setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"] forKey:@"device"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:NO];

	[_loadingMessage setText:@"Logging in..."]; ////
	
	_getTokenConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getTokenConn start];

}

-(void)getUserInfo:(NSString *)accessToken {
	
	NSString *endpoint = @"/user";
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:20.0f params:nil useAuth:YES];

	[_loadingMessage setText:@"Loading..."];
	
	_getProfileInfoConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getProfileInfoConn start];
	
}

-(void)postUserInfo:(NSDictionary *)userInfo {
	
	NSString *endpoint = @"/user";

	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:[userInfo valueForKey:@"username"] forKey:@"username"];
	[_params setObject:[userInfo valueForKey:@"nameFirst"] forKey:@"nameFirst"];
	[_params setObject:[userInfo valueForKey:@"nameLast"] forKey:@"nameLast"];
	[_params setObject:[userInfo valueForKey:@"birthday"] forKey:@"birthday"];
	[_params setObject:[userInfo valueForKey:@"gender"] forKey:@"gender"];
	[_params setObject:[userInfo valueForKey:@"description"] forKey:@"description"];
	[_params setObject:[userInfo valueForKey:@"pictures"] forKey:@"pictures"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:20.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Updating user info..."];
	
	_postUserInfoConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_postUserInfoConnection start];
	
}

-(void)postUserLatitude:(double)lat longitude:(double)lng {
	
	NSString *endpoint = @"/user";

	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:[NSNumber numberWithDouble:lng] forKey:@"longitude"];
	[_params setObject:[NSNumber numberWithDouble:lat] forKey:@"latitude"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:YES];

	[_loadingMessage setText:@"Updating location..."];

	_postUserLocationConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_postUserLocationConnection start];
	
}

-(void)getUsersByIdList:(NSArray *)usersIdList {
	
	NSString *endpoint = @"/user/byId";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:usersIdList forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:30.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Loading..."];
	
	_getUsersConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getUsersConnection start];

}

-(void)getUsersByUsernameList:(NSArray *)usernameList {
	
	NSString *endpoint = @"/user/byUsername";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:usernameList forKey:@"username"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:25.0f params:_params useAuth:YES]; //mutable
	//[request setValue:@"max-age=3600" forHTTPHeaderField:@"Cache-Control"];
	//setcachepolicy
	[_loadingMessage setText:@"Loading..."];
	
	_getUsersConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getUsersConnection start];
	
}

- (void)createCloudWithName:(NSString *)name description:(NSString *)description centerLat:(double)lat centerLng:(double)lng
					 radius:(NSInteger)radius duration:(NSInteger)duration anonymus:(BOOL)anonymus public:(BOOL)public users:(NSArray *)users {

	NSString *endpoint = @"/cloud";

	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:[NSNumber numberWithDouble:lng] forKey:@"longitude"];
	[_params setObject:[NSNumber numberWithDouble:lat] forKey:@"latitude"];
	[_params setObject:description forKey:@"name"];
	[_params setObject:[NSNumber numberWithInteger:radius] forKey:@"radius"];
	[_params setObject:[NSNumber numberWithInteger:duration] forKey:@"duration"];
	[_params setObject:[NSNumber numberWithBool:anonymus] forKey:@"creatorAnon"];
	[_params setObject:[NSNumber numberWithBool:public] forKey:@"public"];
	[_params setObject:users forKey:@"access"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:25.0f params:_params useAuth:YES];

	[_loadingMessage setText:@"Creating cloud..."];
	
	_createCloudConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_createCloudConn start];
	
}

-(void)getCloudsByUserId:(NSString *)userId {
	
	NSString *endpoint = @"/user/clouds";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:userId forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:30.0f params:_params useAuth:YES];

	_getCloudsByUserIdConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getCloudsByUserIdConn start];

}

-(void) getCloudsByIdList:(NSArray *)idList {

	NSString *endpoint = @"/cloud/byId";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:idList forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:30.0f params:_params useAuth:YES];

	[_loadingMessage setText:@"Loading..."];
	
	_getCloudsByIdListConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getCloudsByIdListConn start];

}

-(void)getCloudsByLatitude:(double)latitude longitude:(double)longitude inRadius:(NSInteger)maxDistance {//access
	
	NSString *endpoint = @"/cloud/byLocation";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
	[_params setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
	[_params setObject:[NSNumber numberWithInteger:maxDistance] forKey:@"distance"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:30.0f params:_params useAuth:YES];

	[_loadingMessage setText:@"Loading..."];

	_getCloudsByLocationConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getCloudsByLocationConn start];
	
}

-(void)getCloudUsers:(NSString *)cloudID {
	
	NSString *endpoint = @"/cloud/users";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:cloudID forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:25.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Loading..."];
	
	_getCloudUsersConn = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getCloudUsersConn start];
	
}


-(void)joinCloud:(NSString *)cloudID {
	
	NSString *endpoint = @"/cloud/user";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:cloudID forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:20.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Joining cloud..."];
	
	_joinCloudConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_joinCloudConnection start];

}

-(void)leaveCloud:(NSString *)cloudID {
	
	NSString *endpoint = @"/cloud/user";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:cloudID forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"DELETE" endpoint:endpoint timeout:20.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Leaving cloud..."];
	
	_leaveCloudConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_leaveCloudConnection start];

}

-(void)addFriend:(NSString *)userId {

	NSString *endpoint = @"/user/friend";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:userId forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Processing..."];
	
	_addFriendConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_addFriendConnection start];
	
}

-(void)unfriendUser:(NSString *)userId {

	NSString *endpoint = @"/user/friend";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:userId forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"DELETE" endpoint:endpoint timeout:15.0f params:_params useAuth:YES];

	[_loadingMessage setText:@"Processing..."];

	_unfriendConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_unfriendConnection start];

}

-(void)getFriends:(NSString *)userId {

	NSString *endpoint = @"/user/friends";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:userId forKey:@"id"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:25.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Loading..."];
	
	_getFriendsConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getFriendsConnection start];

}

-(void)getPendingRequests {

	NSString *endpoint = @"/user/friends/pending";
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:20.0f params:nil useAuth:YES];
	
	[_loadingMessage setText:@"Loading..."];
	
	_getPendingConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getPendingConnection start];

}

-(void)getRequestedFriends {

	NSString *endpoint = @"/user/friends/requested";
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:20.0f params:nil useAuth:YES];
	
	[_loadingMessage setText:@"Loading..."];
	
	_getRequestedConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_getRequestedConnection start];

}

-(void)changePassword:(NSString *)newPassword {
	
	NSString *endpoint = @"/user/password";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:newPassword forKey:@"password"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:20.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Changing password..."];
	
	_changePasswordConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_changePasswordConnection start];

}

-(void)postNewImage:(UIImage *)image {

	NSString *endpoint = @"/picture/new";
	
	//resize image?
	NSData *imageData = UIImageJPEGRepresentation(image, 0.85f);
	NSString *imageString = [imageData base64EncodedStringWithOptions:0];
	_currentImage = imageString;

	NSString *params = [NSString stringWithFormat:@"{\"picture\": \"%@\"}", imageString];
	
	NSMutableURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:45.0f params:nil useAuth:YES];

	NSData *bodyData = [params dataUsingEncoding:NSUTF8StringEncoding];
	NSInputStream *istream = [[NSInputStream alloc] initWithData:bodyData];
	[request setHTTPBodyStream:istream];
	
	[request setValue:[NSString stringWithFormat:@"%ld", bodyData.length] forHTTPHeaderField:@"Content-Length"];
	// for progress, do it in default request too (at least if _trackProgress)?
	
	[_loadingMessage setText:@"Uploading image..."];
	
	_anewPictureConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	
	uploadTaskID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
		[_anewPictureConnection cancel];
		[[UIApplication sharedApplication] endBackgroundTask:uploadTaskID];
		uploadTaskID = UIBackgroundTaskInvalid;
		//notify user? try again?
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}];

}

-(void)postImagesIdArray:(NSArray *)imageIdList {
	
	NSString *endpoint = @"/user";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:imageIdList forKey:@"pictures"];
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:25.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Updating user info..."];
	
	_postUserImagesConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_postUserImagesConnection start];
	
}

-(void)postMessage:(NSString *)messsage toCloud:(NSString *)cloudId withImage:(NSString *)image{
	
	NSString *endpoint = @"/message";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:cloudId forKey:@"cloud"];
	[_params setObject:[messsage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"text"];
	
	if (image != nil && ![image isEqualToString:@""]) {
		[_params setObject:image forKey:@"picture"];
	}
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:YES];
	
	_postMessageConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_postMessageConnection start];
	
}

-(void) post1on1Message:(NSString *)message toFriend:(NSString *)friendId withImage:(NSString *)image {
	
	NSString *endpoint = @"/whisper";
	
	NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
	[_params setObject:friendId forKey:@"to"];
	[_params setObject:[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"text"];
	
	if (image != nil && ![image isEqualToString:@""]) {
		[_params setObject:image forKey:@"picture"];
	}
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:15.0f params:_params useAuth:YES];
	
	_post1on1MessageConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_post1on1MessageConnection start];

}

-(void)loadMessagesInCloud:(NSString *)cloudId fromTime:(NSString *)fromTime toTime:(NSString *)toTime limit:(NSUInteger)limit { //since, until
	
	NSString *endpoint = @"/messages";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:cloudId forKey:@"cloud"];
	
	if (fromTime != nil && ![fromTime isEqualToString:@""]) {
		[_params setObject:fromTime forKey:@"start"];
	}

	if (toTime != nil && ![toTime isEqualToString:@""]) {
		[_params setObject:toTime forKey:@"end"];
	}

	if (limit != 0 && limit != NSNotFound) {
		[_params setObject:[NSNumber numberWithUnsignedInteger:limit] forKey:@"limit"];
	}
	
	NSLog(@"load message params: %@", _params);
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:30.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Loading messages..."];
	
	_loadMessagesConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_loadMessagesConnection start];

}

-(void)load1on1MessagesWithFriend:(NSString *)friendId since:(NSString *)since until:(NSString *)until limit:(NSUInteger)limit {
	
	NSString *endpoint = @"/whispers";
	
	NSMutableDictionary *_params = [NSMutableDictionary new];
	[_params setObject:friendId forKey:@"to"];
	
	if (since != nil) {
		[_params setObject:since forKey:@"start"];
	}
	
	if (until != nil) {
		[_params setObject:until forKey:@"end"];
	}
	
	if (limit != 0 && limit != NSNotFound) {
		[_params setObject:[NSNumber numberWithUnsignedInteger:limit] forKey:@"limit"];
	}
	
	NSURLRequest *request = [self defaultRequestWithMethod:@"POST" endpoint:endpoint timeout:30.0f params:_params useAuth:YES];
	
	[_loadingMessage setText:@"Loading messages..."];
	
	_loadMessagesConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	[_loadMessagesConnection start];

}

-(BOOL)isViewControllerActive:(UIViewController *)viewController {
	
	if (viewController == nil || ![viewController isKindOfClass:[UIViewController class]] ) {
		return NO;
	}
	
	if (viewController.navigationController != nil) {
		return [viewController.navigationController.visibleViewController isEqual:viewController];
	}
	
	if (viewController.tabBarController != nil) {
		return [viewController.tabBarController.selectedViewController isEqual:viewController] && viewController.presentedViewController == nil;
	}
	
	return viewController.presentedViewController == nil && viewController.view.window != nil;

}

- (UIViewController *)parentViewControllerOf:(id)view {
	UIResponder *responder = view;
	while (![responder isKindOfClass:[UIViewController class]] && responder != nil)
		responder = [responder nextResponder];
	return (UIViewController *)responder;
}

			/***********************************************************************************/
			/********  NSURLConnectionDelegate and NSURLConnectionDataDelegate methods  ********/
			/***********************************************************************************/



-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	[_runningOperations removeObject:connection];
	NSLog(@"error: %@", error);
	
	UIViewController *currentVC = ([_delegate isKindOfClass:[UIViewController class]]) ? (UIViewController *)_delegate : [self parentViewControllerOf:_delegate];
	
	if ([connection isEqual:_anewPictureConnection]) {
		
		[[UIApplication sharedApplication] endBackgroundTask:uploadTaskID];
		uploadTaskID = UIBackgroundTaskInvalid;
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	}
	
	if ([connection isEqual:_postMessageConnection] || [connection isEqual:_post1on1MessageConnection]) {

		if ([_delegate respondsToSelector:@selector(postMessageFailed)]) {
			[_delegate postMessageFailed];
		}
		
	} else if ([self isViewControllerActive:currentVC]) { //if is last viewcontroller in stack

		NSString *errMessage = [error localizedDescription];
		
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error" message:errMessage preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
		
		[alert addAction:close];
		
		[currentVC presentViewController:alert animated:YES completion:^{
			if ([_delegate respondsToSelector:@selector(apiService:taskFailedWithErrorCode:)]) {
				[_delegate apiService:self taskFailedWithErrorCode:10001]; //////// u close handler?
			}
		}];

	} else {
		
		NSLog(@"Delegate already presenting something"); ////////
		if ([_delegate respondsToSelector:@selector(apiService:taskFailedWithErrorCode:)]) {
			[_delegate apiService:self taskFailedWithErrorCode:10001];
		}
		
	}
	
	[self hideLoadingView];
	
	_delegateStrongReferenceCounter--;
	//NSLog(@"_delegateStrongReferenceCounter: %ld", (long)_delegateStrongReferenceCounter);
	if (_delegateStrongReferenceCounter <= 0) {
		objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
	}
	
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	
	if ([connection isEqual:_loadMessagesConnection] || [connection isEqual:_load1on1MessagesConnection]) {
		[_messagesData appendData:data];
	} else {
		[_responseData appendData:data];
	}
	
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	// TODO: downloadProgress
	//if (response.statusCode == 200) { // no statusCode?
	//long long expectedDownloadSize = [response expectedContentLength]; // for download, use property 'expectedDownloadSize', content-length must be implemented in api?
	//}
	
	//
	
}

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite {

	if (!_trackProgress) {
		return;
	}
	
	//NSLog(@"totalBytesExpectedToWrite: %ld", totalBytesExpectedToWrite);
	NSInteger uploadSize = totalBytesExpectedToWrite;
	
	if (uploadSize < 1 && [connection isEqual:_anewPictureConnection]) {
		// TODO: remember only once on start!, calculate whole body data size instead!
		uploadSize = [_currentImage lengthOfBytesUsingEncoding:NSUTF8StringEncoding]; //temp
		//uploadSize = connection.currentRequest.HTTPBody.length; // always zero! (originalRequest too)
		// TODO: try this: http://stackoverflow.com/a/15749527/1630623, then probably totalBytesExpectedToWrite will be correct! - works!
	}
	
	//NSLog(@"connection didSendBodyData, totalBytesWritten: %ld, totalBytesExpectedToWrite: %ld", totalBytesWritten, uploadSize);
	
	if ([_delegate respondsToSelector:@selector(apiService:uploadProgress:)]) {
		CGFloat progress = MIN(1.0f, (CGFloat)((float)totalBytesWritten / (float)uploadSize));
		progress = MAX(0, progress);
		[_delegate apiService:self uploadProgress:progress];
	}
	
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{

	// TODO: use this throughotu the whole app for displaying alerts! http://stackoverflow.com/a/30941356/1630623, check this out too: http://stackoverflow.com/a/35211571/1630623
	
	// just in case!, move a little lower
	// TODO: only if it hadn't reached 1 before for some reason
	if (_trackProgress) {
		if ([_delegate respondsToSelector:@selector(apiService:uploadProgress:)]) {
			[_delegate apiService:self uploadProgress:1.0f];
		}
		if ([_delegate respondsToSelector:@selector(apiService:downloadProgress:)]) {
			[_delegate apiService:self downloadProgress:1.0f];
		}
	}
	
	//get data from assoc. obj.
	[_runningOperations removeObject:connection];
	
	NSData *responseData = [connection isEqual:_loadMessagesConnection] || [connection isEqual:_load1on1MessagesConnection] ? _messagesData : _responseData;
	
	NSError *responseError;
	NSJSONSerialization *response = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves error:&responseError];
	
	if ([connection isEqual:_loadMessagesConnection] || [connection isEqual:_load1on1MessagesConnection]) {
		_messagesData = [[NSMutableData alloc] init];
	} else {
		_responseData = [[NSMutableData alloc] init];
	}
	
	[self hideLoadingView];
	
	/*
	if ([connection isEqual:_fbAuthConnection]) {
		NSLog(@"FBAtuh response: %@", response);
	}
	 */
	
	if ([response valueForKey:@"status"] == nil || ![[response valueForKey:@"status"] boolValue]) { ////posebno hendlat ako je message connectiomn
		
		uint errorCode = (uint)[([response valueForKey:@"code"] ?: [NSNumber numberWithUnsignedInt:500]) integerValue];
		NSLog(@"Task failed with code: %d", errorCode);
		NSLog(@"Response: %@", (NSDictionary *)response);
		
		if ([connection isEqual:_postMessageConnection] || [connection isEqual:_post1on1MessageConnection]) {
			
			if ([_delegate respondsToSelector:@selector(postMessageFailed)]) {
				[_delegate postMessageFailed];
			}
			
		} else if (errorCode < 2 || errorCode == 401 || errorCode == 500) {//unauthorized, invalid refresh token, 500 - unknown?
			
			if ([_delegate respondsToSelector:@selector(apiService:taskFailedWithErrorCode:)]) {
				[_delegate apiService:self taskFailedWithErrorCode:errorCode]; //automatski vratit na login?
			}
			
			_delegateStrongReferenceCounter--;
			//NSLog(@"_delegateStrongReferenceCounter: %ld", (long)_delegateStrongReferenceCounter);
			if (_delegateStrongReferenceCounter <= 0) {
				objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
			}
			
			return;
			
		}
		
		NSString *errMessage = @"";
		
		switch (errorCode) {
			
			case 1000: {
				errMessage = @"Something went wrong";
				break;
			}
				
			case 1001: {
				errMessage = @"Invalid password";
				break;
			}
				
			case 1002: {
				errMessage = @"Password must be at least 6 characters long";
				break;
			}
				
			case 1003: {
				errMessage = @"Username is already taken";
				break;
			}
				
			case 1004: {
				//user location
				break;
			}
				
				////
			case 1007: {
				errMessage = @"That cloud has become inactive or no longer exists";
				break;
			}
				
			default: {
				errMessage = (NSString *)[(NSArray *)[response valueForKey:@"errors"] firstObject] ?: @"Unknown error ocurred";
				break;
			}
				
		}
		
		UIViewController *currentVC =  ([_delegate isKindOfClass:[UIViewController class]]) ? (UIViewController *)_delegate : [self parentViewControllerOf:_delegate];
		
		if ([self isViewControllerActive:currentVC]) {
			
			UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Radyus error" message:errMessage preferredStyle:UIAlertControllerStyleAlert];
		
			UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
		
			[alert addAction:close];
		
			[currentVC presentViewController:alert animated:YES completion:^{
				if ([_delegate respondsToSelector:@selector(apiService:taskFailedWithErrorCode:)]) { // u close handler?
					[_delegate apiService:self taskFailedWithErrorCode:errorCode];
				}
			}];
			
		} else {
			
			NSLog(@"Delegate already presenting something");
			if ([_delegate respondsToSelector:@selector(apiService:taskFailedWithErrorCode:)]) {
				[_delegate apiService:self taskFailedWithErrorCode:errorCode];
			}
			
		}
		
		_delegateStrongReferenceCounter--;
		//NSLog(@"_delegateStrongReferenceCounter: %ld", (long)_delegateStrongReferenceCounter);
		if (_delegateStrongReferenceCounter <= 0) {
			objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
		}
		
		return;
	}

    NSLog(@"API response: %@, %@", connection.originalRequest, [response valueForKey:@"data"]);

	if ([connection isEqual:_postMessageConnection] || [connection isEqual:_post1on1MessageConnection]) {
		
		if ([_delegate respondsToSelector:@selector(postMessageSuccess:)]) {
			[_delegate postMessageSuccess:[response valueForKey:@"data"]];
		}
		
	} else if ([connection isEqual:_signupConn]) {
		
		if ([_delegate respondsToSelector:@selector(signupSuccess:)]) {
			[_delegate signupSuccess:[response valueForKey:@"data"]];
		}
		
	} else if ([connection isEqual:_getTokenConn]) {
		
		NSJSONSerialization *data = [response valueForKey:@"data"];
		
		NSString *accessToken = [data valueForKey:@"accessToken"];
		NSString *refreshToken = [data valueForKey:@"refreshToken"];
		NSString *socketToken = [data valueForKey:@"socketToken"];
		
		//NSLog(@"Access token: %@", accessToken);
		if (accessToken == nil || socketToken == nil) {
			
			if ([_delegate respondsToSelector:@selector(apiService:taskFailedWithErrorCode:)]) {
				[_delegate apiService:self taskFailedWithErrorCode:1]; //rezervirat neki code i message
			}
			
		} else if ([_delegate respondsToSelector:@selector(receivedAccessToken:refreshToken:socketToken:)]) {
			
			[[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:@"accessToken"];
			[[NSUserDefaults standardUserDefaults] setValue:refreshToken forKey:@"refreshToken"];
			[[NSUserDefaults standardUserDefaults] setValue:socketToken forKey:@"socketToken"];
			[[NSUserDefaults standardUserDefaults] synchronize];
			[_delegate receivedAccessToken:accessToken refreshToken:refreshToken socketToken: socketToken];
			
		}
	
	} else if ([connection isEqual:_fbAuthConnection]) {
		
		NSJSONSerialization *data = [response valueForKey:@"data"];
		
		NSString *accessToken = [data valueForKey:@"accessToken"];
		NSString *refreshToken = [data valueForKey:@"refreshToken"];
		NSString *socketToken = [data valueForKey:@"socketToken"];
		
		[[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:@"accessToken"];
		[[NSUserDefaults standardUserDefaults] setValue:refreshToken forKey:@"refreshToken"];
		[[NSUserDefaults standardUserDefaults] setValue:socketToken forKey:@"socketToken"];
		[[NSUserDefaults standardUserDefaults] synchronize];
				
		if ([_delegate respondsToSelector:@selector(receivedAccessToken:refreshToken:socketToken:)]) {
			[_delegate receivedAccessToken:accessToken refreshToken:refreshToken socketToken: socketToken];
		}
	
	} else if ([connection isEqual:_getProfileInfoConn] || [connection isEqual:_postUserImagesConnection]) {

		NSJSONSerialization *userInfo = [response valueForKey:@"data"];
		
		[[NSUserDefaults standardUserDefaults] setValue:userInfo forKey:@"userInfo"];
		[[NSUserDefaults standardUserDefaults] synchronize];

		if ([_delegate respondsToSelector:@selector(receivedUserInfo:)]) {
			[_delegate receivedUserInfo:userInfo];
		}
		
	} else if ([connection isEqual:_createCloudConn]) {
		
		[_dbManager saveCloud:[response valueForKey:@"data"]];
		
		if ([_delegate respondsToSelector:@selector(cloudCreated:)]) {
			[_delegate cloudCreated:[response valueForKey:@"data"]];
		}

	} else if ([connection isEqual:_getUsersConnection]) {
		
		if ([(NSArray *)[response valueForKey:@"data"] count] > 0) {
			[_dbManager saveUsers:(NSArray *)[response valueForKey:@"data"]];
		}
	
		if ([_delegate respondsToSelector:@selector(apiService:receivedUsersList:)]) {
			[_delegate apiService:self receivedUsersList:[response valueForKey:@"data"]];
		}
	
	} else if ([connection isEqual:_getCloudsByLocationConn] || [connection isEqual:_getCloudsByIdListConn]) {
		
		[_dbManager saveClouds:(NSArray *)[response valueForKey:@"data"]];
		
		if ([_delegate respondsToSelector:@selector(apiService:receivedCloudList:)]) {
			[_delegate apiService:self receivedCloudList:response]; //valueForKey:@data" //refaktorirat exploremap.h
		}
		
	} else if ([connection isEqual:_getCloudsByUserIdConn]) {
		
		if ([_delegate respondsToSelector:@selector(receivedCloudIdList:)]) {
			[_delegate receivedCloudIdList:(NSArray *)[response valueForKey:@"data"]];
		}
	
	} else if ([connection isEqual:_getCloudUsersConn] || [connection isEqual:_getFriendsConnection]) {
		
		if ([_delegate respondsToSelector:@selector(apiService:receivedUserIdList:)]) {
			[_delegate apiService:self receivedUserIdList:(NSArray *)[response valueForKey:@"data"]];
		}
		
	} else if ([connection isEqual:_postUserInfoConnection]) {
	
		NSJSONSerialization *userInfo = [response valueForKey:@"data"];
		
		[[NSUserDefaults standardUserDefaults] setValue:userInfo forKey:@"userInfo"];
		[[NSUserDefaults standardUserDefaults] synchronize];
		
		if ([_delegate respondsToSelector:@selector(userInfoPostSuccess:)]) {
			[_delegate userInfoPostSuccess:userInfo];
		}
		
	} else if ([connection isEqual:_postUserLocationConnection]) {
		
		//save user to userdefaults??

		if ([_delegate respondsToSelector:@selector(userLocationPostSuccess)]) {
			[_delegate userLocationPostSuccess];
		}
		
	} else if ([connection isEqual:_joinCloudConnection]) {
		
		if ([_delegate respondsToSelector:@selector(joinCloudSuccess:)]) {
			[_delegate joinCloudSuccess:[response valueForKey:@"data"]];
		}
		
	} else if ([connection isEqual:_leaveCloudConnection]) {
		
		if ([_delegate respondsToSelector:@selector(didLeaveCloud:)]) {
			[_delegate didLeaveCloud:[response valueForKey:@"data"]];
		}
		
	} else if ([connection isEqual:_addFriendConnection]) {
		
		NSString *userId = [[response valueForKey:@"data"] valueForKey:@"id"];
		
		NSMutableArray *friends = [NSMutableArray arrayWithArray:(NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"userFriends"] ?: [NSArray array]];
		if (userId != nil && ![friends containsObject:userId]) {
			//TODO: what if just requested
			[friends addObject:userId];
			[[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithArray:friends] forKey:@"userFriends"];
			[[NSUserDefaults standardUserDefaults] synchronize];
		}
		
		if ([_delegate respondsToSelector:@selector(addFriendSuccess:)]) {
			[_delegate addFriendSuccess:[response valueForKey:@"data"]];
		}

	} else if ([connection isEqual:_unfriendConnection]) {
		
		NSString *userId = [[response valueForKey:@"data"] valueForKey:@"id"];
		
		NSMutableArray *friends = [NSMutableArray arrayWithArray:(NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"userFriends"] ?: [NSArray array]];
		
		if (userId != nil && [friends containsObject:userId]) {
			[friends removeObject:userId];
			[[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithArray:friends] forKey:@"userFriends"];
			[[NSUserDefaults standardUserDefaults] synchronize];
		}
		
		if ([_delegate respondsToSelector:@selector(unfriendSuccess:)]) {
			[_delegate unfriendSuccess:[response valueForKey:@"data"]];
		}

	} else if ([connection isEqual:_getPendingConnection]) {
	
		if ([_delegate respondsToSelector:@selector(getPendingSuccess:)]) {
			[_delegate getPendingSuccess:(NSArray *)[response valueForKey:@"data"]];
		}
		
	} else if ([connection isEqual:_getRequestedConnection]) {
	
		if ([_delegate respondsToSelector:@selector(getRequestedSuccess:)]) {
			[_delegate getRequestedSuccess:(NSArray *)[response valueForKey:@"data"]];
		}
		
	} else if ([connection isEqual:_changePasswordConnection]) {
		
		if ([_delegate respondsToSelector:@selector(changePasswordSuccess:)]) {
			[_delegate changePasswordSuccess:[response valueForKey:@"data"]];
		}
	
	} else if ([connection isEqual:_anewPictureConnection]) {
		
		[[UIApplication sharedApplication] endBackgroundTask:uploadTaskID];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		
		_currentImageId = [[response valueForKey:@"data"] valueForKey:@"id"];
		
		NSString *queryFormat = @"INSERT OR IGNORE INTO 'images' ('id', 'image_content') VALUES ('%@', '%@')";
		[_dbManager executeQuery:[NSString stringWithFormat:queryFormat, _currentImageId, _currentImage]];

		_currentImage = nil;
		
		if ([_delegate respondsToSelector:@selector(newPictureSuccess:)]) {
			[_delegate newPictureSuccess:_currentImageId];
		}
		
	} else if ([connection isEqual:_loadMessagesConnection] || [connection isEqual:_load1on1MessagesConnection]) { ////////?
	
		//save to db ovde ili ostavit un exploremap?
		if ([_delegate respondsToSelector:@selector(loadMessagesSuccess:)]) { //sort
			[_delegate loadMessagesSuccess:[response valueForKey:@"data"]];
		}
	}
	
	_delegateStrongReferenceCounter--;
	//NSLog(@"_delegateStrongReferenceCounter: %ld", (long)_delegateStrongReferenceCounter);
	if (_delegateStrongReferenceCounter <= 0) {
		objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
	}
	
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
	NSLog(@"Cached response: %@", cachedResponse);
	return cachedResponse; //handle cache for each request
}

-(NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
	
	if (_delegate == nil) {
		return nil;
	}
	
	if ([_delegate shouldDisableUserInteraction]) { //if _deleagte is UIView disable only that!
		
		UIViewController *parentVC = ([_delegate isKindOfClass:[UIViewController class]]) ? (UIViewController *)_delegate : [self parentViewControllerOf:_delegate];
		UIView *viewToDisableInteraction = [parentVC isKindOfClass:[SideMenuUtilizer class]] && [(UIViewController *)parentVC navigationController] != nil ? [(SideMenuUtilizer *)parentVC containerView] : [(UIViewController *)parentVC view];
		
		[viewToDisableInteraction setUserInteractionEnabled:NO];
		[self showLoadingViewWithTitle:_loadingMessage.text];
		
	}
	
	//NSLog(@"request http body: %@", [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding]);
	
	if (_delegateStrongReferenceCounter <= 0) {
		objc_setAssociatedObject(self, &delegateStrongReferenceKey, _delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		_delegateStrongReferenceCounter = 1;
	} else {
		_delegateStrongReferenceCounter++;
	}
	
	[_runningOperations addObject:connection];
	
	return  request;
}


-(void) showLoadingViewWithTitle:(NSString *)title {

	[_loadingMessage setText:title];
	
	if (self.delegate != nil && ![_activityIndicator isAnimating] && [self.delegate isKindOfClass:[UIViewController class]]) {
		
		UIView *currentView =  [self.delegate isKindOfClass:[SideMenuUtilizer class]] ? [(SideMenuUtilizer *)self.delegate containerView] : ((UIViewController*)self.delegate).view;
		_loadingView.center = CGPointMake(currentView.center.x, currentView.center.y + 16);
		[currentView addSubview:_loadingView];
		[_activityIndicator startAnimating];
		
	}

}

-(void) hideLoadingView {
	
	if (self.delegate != nil){
		
		if ([_activityIndicator isAnimating]) {
			[_activityIndicator stopAnimating];
		}
		
		if (_loadingView.superview != nil) {
			[_loadingView removeFromSuperview];
		}
		
		if ([_delegate shouldDisableUserInteraction]) {
			
			UIViewController *parentVC = ([_delegate isKindOfClass:[UIViewController class]]) ? (UIViewController *)_delegate : [self parentViewControllerOf:_delegate];
			UIView *viewToDisableInteraction = [parentVC isKindOfClass:[SideMenuUtilizer class]] && [(UIViewController *)parentVC navigationController] != nil ? [(SideMenuUtilizer *)parentVC containerView] : [(UIViewController *)parentVC view];
			
			[viewToDisableInteraction setUserInteractionEnabled:YES];
			
		}
		
	}

}

-(void)cancelRequests {
	
	//[self hideLoadingView]; //???
	
	self.delegate = nil;
	
	//post message shouldn't be cancelled?
	[_runningOperations makeObjectsPerformSelector:@selector(cancel)];
	[_runningOperations removeAllObjects];

	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

	_delegateStrongReferenceCounter = 0;
	objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
	
	//TODO: multiple instances of same or/and different connection types runnng symultaneously, data as associated object, add conection types
	//TODO: operationqueue (3 at a time) ???
	
}

-(void)dealloc {
	[self cancelRequests]; //just in case
}

@end
