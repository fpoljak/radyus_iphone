//
//  Profile.h
//  RadyUs
//
//  Created by Frane Poljak on 04/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "TransparentDialog.h"
#import "Gallery.h"
#import "SideMenuUtilizer.h"
#import "ApiService.h"
#import "AsyncImageLoadManager.h"
#import "UnavailableCloudView.h"
#import "ExploreMap.h"
#import "EditProfile.h"

@interface Profile : SideMenuUtilizer

enum UserProfileMode : NSUInteger{
	UPOwn = 0,
	UPFriend = 1,
	UPPending = 2,
	UPRequested = 3,
	UPOther = 4
};

@property (nonatomic) enum UserProfileMode profileMode;

@property (nonatomic) NSInteger cloudsCount;
@property (nonatomic) NSInteger indexOfCloudBeingLoaded;
@property (nonatomic, retain) NSMutableArray *arrayOfCloudUsersArrays;

//@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIView *tableViewHeader;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *dropdownButton;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) NSArray *images;
@property (nonatomic) NSInteger currentImageIndex;
@property (nonatomic, retain) IBOutlet UIView *userInfoContainer;
@property (nonatomic, retain) IBOutlet UIButton *addFriendButton;
@property (nonatomic, retain) IBOutlet UIButton *sendMessageButton;
@property (nonatomic, retain) IBOutlet UIButton *editProfileButton;
@property (nonatomic, retain) IBOutlet UILabel *cloudsCreatedLabel;
@property (nonatomic, retain) IBOutlet UILabel *cloudsVisitedLabel;
@property (nonatomic, retain) IBOutlet UILabel *friendsNumberLabel;
@property (nonatomic, retain) IBOutlet UILabel *friendsLabel;
@property (nonatomic, retain) IBOutlet UILabel *cloudListHeaderLabel;
@property (nonatomic, retain) NSMutableArray *friendList;
@property (nonatomic, retain) NSJSONSerialization *cloudList;
@property (nonatomic, retain) NSJSONSerialization *selectedCloud;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) IBOutlet UIScrollView *backgroundView;
@property (nonatomic, retain) NSMutableArray *imageViews;
@property (nonatomic, retain) NSString *userID;
@property (nonatomic, retain) NSString *username;

@property (nonatomic, retain) IBOutlet UILabel *nameField;
@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) NSJSONSerialization *userInfo;
@property (nonatomic, retain) UIImage *userImage;

@property (nonatomic, retain) NSMutableArray *pending;
@property (nonatomic, retain) NSMutableArray *requested;

@property (nonatomic) CGFloat headerHeight;

@property (nonatomic, retain) ApiService *apiService;
@property (nonatomic, retain) ApiService *getFriendsService;

@property (nonatomic, retain) AsyncImageLoadManager *imageLoader;

-(IBAction)returnImageToOriginalPosition:(UIGestureRecognizer *)sender;
-(void)displayUser;

@end
