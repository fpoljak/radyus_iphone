//
//  CustomImageWithBadge.m
//  RadyUs
//
//  Created by Frane Poljak on 24/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "CustomImageWithBadge.h"

@implementation CustomImageWithBadge

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    _badgeNumber = 0;
    _badge = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [_badge.layer setCornerRadius:10.0f];
    [_badge.layer setBorderColor:UIColorFromRGB(0x252627).CGColor];
    [_badge.layer setBorderWidth:2.0f];
    [_badge setBackgroundColor:UIColorFromRGB(0xFFD203)];
    _badgeLabel = [[UILabel alloc] initWithFrame:_badge.frame];
    [_badgeLabel setBackgroundColor:[UIColor clearColor]];
    [_badgeLabel setFont:[UIFont fontWithName:@"GothamRounded-Bold" size:8.0f]];
    [_badgeLabel setTextColor:[UIColor darkTextColor]];
    [_badgeLabel setTextAlignment:NSTextAlignmentCenter];
    [_badge addSubview:_badgeLabel];
    //[_badge setAlpha:0.0f];
    [self addSubview:_badge];
    [self bringSubviewToFront:_badge];
    
}

-(void)setBadgeNumber:(NSInteger)badgeNumber{
    _badgeNumber = badgeNumber;
    if (_badgeNumber > 0) {
        [_badge setAlpha:1.0f];
    } else {
        [_badge setAlpha:0.0f];
    }
    [_badgeLabel setText:[NSString stringWithFormat:@"%ld", (long)_badgeNumber]]; //>99?
    
}

@end
