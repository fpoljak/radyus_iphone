//
//  ChangePassword.h
//  Radyus
//
//  Created by Locastic MacbookPro on 15/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiService.h"
#import "SideMenuUtilizer.h"

@interface ChangePassword : SideMenuUtilizer <ApiServiceDelegate, UITextFieldDelegate>

@property (nonatomic, retain) IBOutlet UITextField *oldPassword;
@property (nonatomic, retain) IBOutlet UITextField *anewPassword;
@property (nonatomic, retain) IBOutlet UITextField *confirmPassword;

@property (nonatomic, retain) ApiService *apiService;

@end
