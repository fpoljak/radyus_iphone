//
//  ChatSingleMessage.m
//  RadyUs
//
//  Created by Frane Poljak on 03/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ChatSingleMessage.h"
#import "Profile.h"
#import "Gallery.h"
#import "UIImageView+NoAvatar.h"

@implementation ChatSingleMessage

const char linkPreviewDelegateStrongReferenceKey;
static char hashtagOrMentionKey;
BOOL imageBeingLoaded;
//ChatView *chatView;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithType:(enum MessageType)type message:(NSDictionary *)message displayHeader:(BOOL)displayHeader{
	
	self = [super init];
	
	_deallocated = NO;
	
	_messageType = type;
    _message = [NSMutableDictionary dictionaryWithDictionary:message];
    _displayHeader = displayHeader;
    [self setAutoresizesSubviews:NO];
    [self setAutoresizingMask: UIViewAutoresizingNone];
    _layerFrame = CGRectZero;
	imageBeingLoaded = NO;
	
	if ([_message valueForKey:@"picture"] != nil && ![[_message valueForKey:@"picture"] isEqualToString:@""]) {
		[_message setValue:[_message valueForKey:@"picture"] forKey:@"image_id"];
	}
	
	self.layer.shouldRasterize = YES;
	self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
		
    return self;
	
}

-(CGFloat)displayInContainer:(UIScrollView *) messageContainer startingY:(CGFloat)startingY {
    
    CGFloat currentY = startingY;
    
    [self setFrame:CGRectMake(0, startingY, messageContainer.frame.size.width, 0)];
    [self setBackgroundColor:[UIColor clearColor]];
    
	NSString *messageText = [_message objectForKey:@"text"] ?: @"";
    
    if (_displayHeader) {
        currentY += [self displayMessageHeader:messageContainer startingY:currentY - startingY];
    }
	
	//maknit " + 2" na 3 mista za uklanjanje razmaka
    _frameY = currentY - startingY + 2;
	_frameX = 54; //46?
	
	if (messageText.length > 0) {
		
        UITextView *messageTextLabel = [[UITextView alloc] initWithFrame:CGRectMake(54, currentY - startingY + 2, 0, 1)];
        //[messageTextLabel setNumberOfLines:0]; //0
        [messageTextLabel setTextColor:UIColorFromRGB(0x757575)];
        [messageTextLabel setFont:[UIFont fontWithName:@"GothamRounded-Book" size:14]];
        [messageTextLabel.textContainer setLineBreakMode:NSLineBreakByWordWrapping];
        [messageTextLabel setText:messageText];
        
        [messageTextLabel setScrollEnabled:NO];
        messageTextLabel.selectable = YES;
        messageTextLabel.editable = NO;
		
		messageTextLabel.dataDetectorTypes = UIDataDetectorTypeAll;
		messageTextLabel.linkTextAttributes = @{NSForegroundColorAttributeName: [UIColor blueColor], NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]};
		
        messageTextLabel.delegate = self;
        
        //[messageTextLabel setPreferredMaxLayoutWidth:messageContainer.frame.size.width - 62];

        [messageTextLabel setTextAlignment:NSTextAlignmentLeft];
        
		[messageTextLabel setClipsToBounds:YES]; //because of links
		
		CGSize maxSize = CGSizeMake(messageContainer.frame.size.width - 86, CGFLOAT_MAX); ////////78//__//
        CGSize requiredSize = [messageTextLabel sizeThatFits:maxSize];
        messageTextLabel.frame = CGRectMake(messageTextLabel.frame.origin.x, messageTextLabel.frame.origin.y, requiredSize.width, requiredSize.height);
    
		[messageTextLabel setContentInset:UIEdgeInsetsMake(1, 5, 7, 3)];  //usermessage- 0, 2, 8, 6
        messageTextLabel.frame = CGRectInset(messageTextLabel.frame, -8, 0);
        
        //if (_messageType == MTUserMessage && messageTextLabel.frame.size.width < messageTextLabel.preferredMaxLayoutWidth) {//messageContainer.frame.size.width - 62
		if (_messageType == MTUserMessage && messageTextLabel.frame.size.width < messageContainer.frame.size.width - 62) { ////////62
            CGFloat x = messageTextLabel.frame.origin.x;
            //x += messageTextLabel.preferredMaxLayoutWidth - messageTextLabel.frame.size.width;
			x += messageContainer.frame.size.width - messageTextLabel.frame.size.width - 62; ////////62
            [messageTextLabel setFrame:CGRectMake(x, messageTextLabel.frame.origin.y, messageTextLabel.frame.size.width, messageTextLabel.frame.size.height)];

        } else if (_messageType == MTAppMessage) {

			[messageTextLabel setBackgroundColor:[UIColor clearColor]];
			[messageTextLabel setFrame:CGRectMake((messageContainer.frame.size.width - messageTextLabel.frame.size.width) / 2, messageTextLabel.frame.origin.y,
												  messageTextLabel.frame.size.width, messageTextLabel.frame.size.height)];
			
			[messageTextLabel setTextColor:UIColorFromRGB(0x9B9B9B)];
			
			/* //add shadow
			CALayer *textLayer = ((CALayer *)[messageTextLabel.layer.sublayers objectAtIndex:0]);
			textLayer.shadowColor = [UIColor grayColor].CGColor;
			textLayer.shadowOffset = CGSizeMake(0.0f, 1.0f);
			textLayer.shadowOpacity = 1.0f;
			textLayer.shadowRadius = 1.0f;
			*/
			
			[self addSubview:messageTextLabel];
			
			currentY += messageTextLabel.frame.size.height + 4;
			
			__weak typeof (self) this = self;
			[[NSOperationQueue mainQueue] addOperationWithBlock:^{
				if (this != nil) {
					[messageContainer addSubview:this];
				}
			}];

			
			return MAX(currentY - startingY, 32);
			
		}
		
        _frameX = messageTextLabel.frame.origin.x;
		
        if ([_message objectForKey:@"imageToDisplay"] == nil && ([_message objectForKey:@"image_id"] == nil || [[_message objectForKey:@"image_id"] isEqualToString:@""])) {
            [messageTextLabel setBackgroundColor:(_messageType == MTUserMessage ? UIColorFromRGB(0xFFD203) : UIColorFromRGB(0xEBEBEB))];
			UITapGestureRecognizer *messageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageTapped:)];
			[messageTextLabel addGestureRecognizer:messageTap];
        } else {
            [messageTextLabel setBackgroundColor:[UIColor clearColor]];
        }
        
        [self addSubview:messageTextLabel];
        
        currentY += messageTextLabel.frame.size.height + 2;
		
		[self highlightHashags:messageTextLabel];
		//if 1 on 1 no mentions!
		[self highlightMentions:messageTextLabel];
		
		_linkStartingY = currentY - startingY;
		currentY += [self parseURLs:messageTextLabel startingY:_linkStartingY];
		
    }
	
    if ([_message objectForKey:@"imageToDisplay"] != nil) {
        currentY += [self displayImage:(UIImage *)[_message objectForKey:@"imageToDisplay"] inContainer:messageContainer startingY:currentY - startingY];
	} else if ([_message objectForKey:@"image_id"] != nil && ![[_message objectForKey:@"image_id"] isEqualToString:@""]) {
		currentY += [self prepareToDownloadImage:[_message objectForKey:@"image_id"] inContainer:messageContainer startingY:currentY - startingY];
	}
	
    CGFloat height = currentY - startingY;
    
	[self setFrame:CGRectMake(0, startingY, messageContainer.frame.size.width - 8, height)]; ////////8
	
	//[messageContainer addSubview:self];
	
    if (([_message objectForKey:@"imageToDisplay"] != nil && ![[_message objectForKey:@"text"] isEqualToString:@""]) || _links.count > 0) {

        _layerFrame = CGRectMake(_frameX + 1, _frameY, self.frame.size.width - _frameX - (self.messageType == MTFriendMessage ? 24 : 16)/*16*/, height - _frameY);
        _backgroundLayer = [CALayer layer];
        [_backgroundLayer setFrame:_layerFrame];
        [_backgroundLayer setBackgroundColor:(_messageType == MTUserMessage ? UIColorFromRGB(0xFFD203) : UIColorFromRGB(0xEBEBEB)).CGColor];
        [_backgroundLayer setZPosition:-1.0f];
        [self.layer addSublayer:_backgroundLayer];
    }
	
	__weak typeof (self) this = self;
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		if (this != nil) {
			[messageContainer addSubview:this];
		}
	}];
	
	[self addDateTimeLabelToView:self inContainer:messageContainer];
	
    return height;
	
}

-(CGFloat)createLinkPreview:(NSString *)url startingY:(CGFloat)startingY {
	
	_frameX = MIN(_frameX, 54); //samo 54? // MAX?
	
	CGFloat maxWidth = self.frame.size.width - 92;
	CGFloat minHeight = maxWidth / 2;
	CGFloat maxHeight = maxWidth * 1.5;
	
	LinkPreview *webView = [[LinkPreview alloc] initWithFrame:CGRectMake(_frameX + 9, startingY + 8, (NSInteger)round(maxWidth) - 1/*??*/, minHeight)];
	
	NSLog(@"creating preview: %@", url);
	
	webView.delegate = self;
	
	[webView setPreviewUrl:url];
	[webView setMaxWidth:maxWidth];
	[webView setMaxHeight:maxHeight];
	[webView loadPreview];
	
	[self addSubview:webView];
	
	return minHeight + 16;
	
}

-(CGFloat)_createLinkPreview:(NSString *)url startingY:(CGFloat)startingY { //test with cards
	
	//alternatives
	//SFSafariViewController - iOS9+
	//WKWebView - iOS8+
	
	//calculate max width and height!
	_frameX = MIN(_frameX, MAX(self.frame.size.width - 268, 54));

	UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(_frameX + 8, startingY + 8, 228, 171)];
	
	[webView setMediaPlaybackAllowsAirPlay:YES];
	[webView setMediaPlaybackRequiresUserAction:YES];
	//[webView setAllowsLinkPreview:YES]; //if ios 9.x+
	[webView setAllowsInlineMediaPlayback:YES];
	
	/*
	 embedly("defaults", {
  cards: {
	 override: true, //false
	 width: 700
	 align: 'right',
	 chrome: 0
  }
	 });
	 embedly('on', 'card.rendered', function(iframe){
  // iframe is the card iframe that we used to render the event.
  $card = $(iframe);
	 
  // Grab the width and height.
  console.log($card.width(), $card.height());
	 });
	*/
	
	NSLog(@"creating preview: %@", url);

	webView.delegate = self;
	
	//check if url harmful
	NSString *contentFormat = @"<a href=\"%@\" style=\"display:none;\" class=\"embedly-card\" data-card-chrome=\"0\" data-card-controls=\"0\" data-card-width=\"100%\" data-card-recommend=\"0\">\
	</a>\
	<script>\
	(function(w, d){\
	 var id='embedly-platform', n = 'script';\
	 if (!d.getElementById(id)){\
		 w.embedly = w.embedly || function() {(w.embedly.q = w.embedly.q || []).push(arguments);};\
		 var e = d.createElement(n); e.id = id; e.async=1;\
	e.src = 'https://cdn.embedly.com/widgets/platform.js';\
		 var s = d.getElementsByTagName(n)[0];\
		 s.parentNode.insertBefore(e, s);\
	 }\
	})(window, document);\
	</script>";\
	
	NSString *content = [NSString stringWithFormat:contentFormat, [@"https://" stringByAppendingString:url]]; //build content
	[webView loadHTMLString:content baseURL:nil];
	//[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[@"https://" stringByAppendingString:url]]]]; //test
	
	[webView setBackgroundColor:[UIColor whiteColor]];
	[webView.layer setZPosition:9.0f];
	[self addSubview:webView];
	[self bringSubviewToFront:webView];
	
	return 187;
	
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
	if (_deallocated || webView.delegate == nil) {
		return NO;
	}
	
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		
		NSLog(@"link url: %@", request.URL.absoluteString);
		if ([request.URL.scheme isEqualToString:@"applewebdata"] && [request.URL.absoluteString hasSuffix:@"/reloadPreview"]) {
			[(LinkPreview *)webView reloadPreview];
			return NO;
		}
		
		UITextView *textView = [UITextView new];
		
		if ([(LinkPreview *)webView appLink] != nil) {
			
			/* //example:
				{
					"app_name" = Instagram;
					"app_store_id" = 389801252; //http://itunes.apple.com/app/idXXXXXXXXX, https://itunes.apple.com/app/apple-store/id284882215&mt=8
					type = ios;
					url = "instagram://media?id=1181137237124385328";
				}
			 */
			
			NSString *message = [NSString stringWithFormat:@"You can open this content with \"%@\" app! Click Ok to open in app or Cancel to open in browser",
								 [[(LinkPreview *)webView appLink] valueForKey:@"app_name"]];
			
			UIAlertController *dialog = [UIAlertController alertControllerWithTitle:@"Open link" message:message preferredStyle:UIAlertControllerStyleAlert];
			
			UIAlertAction *openInAppAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
				
				if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[(LinkPreview *)webView appLink] valueForKey:@"url"]]]) {
					
					NSString *urlString = [NSString stringWithFormat:@"https://itunes.apple.com/app/apple-store/id%@&mt=8", [[(LinkPreview *)webView appLink] valueForKey:@"app_store_id"]];
					NSLog(@"app_link urlString: %@", urlString);
					[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
					
					// maybe use this to detect if app is installed? http://stackoverflow.com/a/27767953/1630623 (plus shouldstartloading..., check if not http(s),
					//TODO: move this check in 'shouldInteractWithURL', somehow connect links from textView with LinkPreview, so that links can be opened in apps too (or not, maybe too much?)
					
				}

			}];
			
			UIAlertAction *openInBrowserAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
				[self textView:textView shouldInteractWithURL:request.URL inRange:NSRangeFromString(request.URL.absoluteString)];
			}];
			
			[dialog addAction:openInAppAction];
			[dialog addAction:openInBrowserAction];
			
			[[(ChatView *)self.superview parentViewController] presentViewController:dialog animated:YES completion:nil];
			
		} else {
			[self textView:textView shouldInteractWithURL:request.URL inRange:NSRangeFromString(request.URL.absoluteString)];
		}
		
	}
	
	return navigationType == UIWebViewNavigationTypeOther;
	//TODO: and check if request url on same domain
	
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
	
	if ([(LinkPreview *)webView referenceCounter] <= 0) {
		objc_setAssociatedObject(webView, &linkPreviewDelegateStrongReferenceKey, webView.delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		[(LinkPreview *)webView setReferenceCounter:0];
	}
	
	[(LinkPreview *)webView setReferenceCounter:[(LinkPreview *)webView referenceCounter] + 1];
	
	NSLog(@"webViewDidStartLoad...");
	if (_deallocated || objc_getAssociatedObject(webView, &linkPreviewDelegateStrongReferenceKey) == nil) {
		webView.delegate = nil;
		[webView stopLoading];
	}
	
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
	
	//check if cancelled!

	if (![webView isLoading] && [webView isKindOfClass:[LinkPreview class]]) {
	
		//NSLog(@"webViewDidFinishLoad...");
		[[(LinkPreview *)webView loadingIndicator] stopAnimating];
		[[(LinkPreview *)webView loadingIndicator] removeFromSuperview];
		
		if (_deallocated || objc_getAssociatedObject(webView, &linkPreviewDelegateStrongReferenceKey) == nil) {
			webView.delegate = nil;
			[webView stopLoading];
			NSLog(@"prevented weak referencing of deallocated object");
			return;
		}
		
		__weak typeof (self) _this = self;
		
		__block ChatSingleMessage *__message = _this;
		__block ChatSingleMessage *this = _this;
		
		__block ChatView *chatView = (ChatView *)self.superview;
		
		__block CGFloat heightDiff = 0;
		
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			
			if (_this != nil) {
		
				CGFloat oldHeight = webView.frame.size.height;
				[(LinkPreview *)webView adjustSize];
		
				CGFloat newHeight = webView.frame.size.height;
		
				if (newHeight != oldHeight) {
			
					heightDiff = newHeight - oldHeight;
			
					BOOL shouldRemoveBackgroundLayer = (newHeight == 0 && _imageView == nil);
					heightDiff -= shouldRemoveBackgroundLayer ? 16 : 0;
					
					chatView.currentY += heightDiff;
					
					[_backgroundLayer removeFromSuperlayer];
				
					_backgroundLayer = [CALayer layer];
					[_backgroundLayer setFrame:self.layerFrame];
					[_backgroundLayer setBackgroundColor:(self.messageType == MTUserMessage ? UIColorFromRGB(0xFFD203) : UIColorFromRGB(0xEBEBEB)).CGColor];
					[_backgroundLayer setZPosition:-0.9f];
					[self.layer addSublayer:_backgroundLayer];
				
					[self adjustCorners];
				
					self.layerFrame = CGRectMake(self.frameX + 1, self.frameY, self.frame.size.width - self.frameX - (self.messageType == MTFriendMessage ? 24 : 16), _layerFrame.size.height + heightDiff);
				
					[UIView animateWithDuration: chatView.isInLoadPreviousMode || chatView.isBeingSwiped || chatView.chatviewRepositioning ? 0.0 : 0.25 animations:^{
						
						[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height + heightDiff)];
						[self.dateTimeLabel setFrame:CGRectMake(self.dateTimeLabel.frame.origin.x, self.dateTimeLabel.frame.origin.y + (heightDiff) / 2,
															self.dateTimeLabel.frame.size.width, self.dateTimeLabel.frame.size.height)];
					
						[_backgroundLayer setFrame:self.layerFrame];
					
						for (UIView *subview in self.subviews) {
							if (subview.frame.origin.y > webView.frame.origin.y) { //check this one for offset!
								[subview setFrame:CGRectMake(subview.frame.origin.x, subview.frame.origin.y + heightDiff, subview.frame.size.width, subview.frame.size.height)];
							}
						}
					
						while (__message.nextMessage != nil) {
							
							__message = __message.nextMessage;
							[__message setFrame:CGRectMake(__message.frame.origin.x, __message.frame.origin.y + heightDiff, __message.frame.size.width, __message.frame.size.height)];
							[__message.dateTimeLabel setFrame:CGRectMake(__message.dateTimeLabel.frame.origin.x, __message.dateTimeLabel.frame.origin.y + heightDiff,
																	 __message.dateTimeLabel.frame.size.width, __message.dateTimeLabel.frame.size.height)];
						
							[__message adjustCorners];
							
						}
					
						if (!chatView.isInLoadPreviousMode) { //in completion if creates issues! (shouldn't, but...)
							[chatView setContentSize:CGSizeMake(self.frame.size.width, MAX(chatView.currentY + 8, chatView.frame.size.height))];
						}
						
						//TODO: samo ako smo već pri dnu
						[chatView setContentOffset:CGPointMake(0, MIN(MAX(chatView.contentOffset.y, chatView.contentOffset.y + heightDiff), chatView.contentSize.height - chatView.frame.size.height))];
					
					} completion:^(BOOL finished) {
						
						if (this != nil) {
							
							if (!chatView.isInLoadPreviousMode) { //in completion if creates issues! (shouldn't, but...)
								[chatView setContentSize:CGSizeMake(self.frame.size.width, MAX(chatView.currentY + 8, chatView.frame.size.height))];
							}
							
							if (newHeight == 0) { //invalid url or no content
								
								[webView removeFromSuperview];
								if (++this.linkIndex < this.links.count) {
									[this createLinkPreview:[this.links objectAtIndex:this.linkIndex] startingY:this.linkStartingY];
								} else {
									
									if (this.imageView == nil) {
										[this.backgroundLayer removeFromSuperlayer];
									}
									
									[this adjustCorners];
								}
								
							} else {
								[this adjustCorners];
							}
							
						}
						
						[(LinkPreview *)webView setReferenceCounter:[(LinkPreview *)webView referenceCounter] - 1];
						
						if ([(LinkPreview *)webView referenceCounter] <= 0) {
							objc_setAssociatedObject(webView, &linkPreviewDelegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
						}
						
						if (chatView != nil && chatView.lastMessage != nil) { // extra space fix
							
							if (chatView.lastMessage.previousMessage != nil) {
								
								CGFloat requiredOriginY = chatView.lastMessage.previousMessage.frame.origin.y + chatView.lastMessage.previousMessage.frame.size.height;// + 4;
								CGFloat diff = chatView.lastMessage.frame.origin.y - requiredOriginY;
								
								chatView.lastMessage.frame = CGRectMake(chatView.lastMessage.frame.origin.x, requiredOriginY,
																		chatView.lastMessage.frame.size.width, chatView.lastMessage.frame.size.height);
								
								CGRect dateTimeLabelFrame = chatView.lastMessage.dateTimeLabel.frame;
								dateTimeLabelFrame.origin.y = dateTimeLabelFrame.origin.y - diff;
								
								chatView.lastMessage.dateTimeLabel.frame = dateTimeLabelFrame;
								
							}
							
							chatView.currentY = chatView.lastMessage.frame.origin.y + chatView.lastMessage.frame.size.height;// + 4;
							
						}

					}];
			
				} else {
					
					[(LinkPreview *)webView setReferenceCounter:[(LinkPreview *)webView referenceCounter] - 1];
					
					if ([(LinkPreview *)webView referenceCounter] <= 0) {
						objc_setAssociatedObject(webView, &linkPreviewDelegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
					}
					
				}
				
			}
			
		}];
		
	}
	
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	
	// not needed?
	NSURL* failingURL = [error.userInfo objectForKey:@"NSErrorFailingURLKey"];
	if ([failingURL.absoluteString isEqualToString:@"about:blank"]) {
		//NSLog(@"This is Blank. Ignoring.");
		return;
	}
	
	[[(LinkPreview *)webView loadingIndicator] stopAnimating];
	[[(LinkPreview *)webView loadingIndicator] removeFromSuperview];
	
	[(LinkPreview *)webView setReferenceCounter:[(LinkPreview *)webView referenceCounter] - 1];
	
	if ([(LinkPreview *)webView referenceCounter] <= 0) {
		objc_setAssociatedObject(webView, &linkPreviewDelegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
	} else if (objc_getAssociatedObject(webView, &linkPreviewDelegateStrongReferenceKey) == nil) {
		webView.delegate = nil;
		//return; //not needed currently
	}
	
	//http://stackoverflow.com/questions/16073519/nsurlerrordomain-error-code-999-in-ios //not happening anymore
	//NSLog(@"webView: %@ didFailLoadWithError: %@, %@", webView.request.URL.absoluteString, error.domain, error.localizedDescription);

}

-(CGFloat)parseURLs:(UITextView *)textView startingY:(CGFloat)startingY {
	
	CGFloat currentY = startingY;
	
	NSArray *linkRanges = [self rangesOfLinksInString:textView.text];
	_links = [NSMutableArray new];
	
	//define max no. of link previews in one message
	NSInteger max_links = 1;//3;
	NSInteger count = 0;
	
	for (NSTextCheckingResult *result in linkRanges) {
		
		//NSLog(@"regex: %@", result.regularExpression);
		_linkStartingY = currentY;
		
		NSString *link = [textView.text substringWithRange:result.range];
		[_links addObject:link];
		currentY += [self createLinkPreview:link startingY:currentY];
		
		if (++count >= max_links) {
			return currentY - startingY;
		}
		
	}
	
	return currentY - startingY;
	
}

-(NSArray *)rangesOfLinksInString:(NSString *)string {
	
	/*
	// first tested recognized 'offset:range.length' as url: [-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)
	// other example: (https?:\\/\\/(?:www\\.|(?!www))[^\\s\\.]+\\.[^\\s]{2,}|www\\.[^\\s]+\\.[^\\s]{2,})
	// microsoft docs: ^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$
	// stackoverflow: http://stackoverflow.com/questions/161738/what-is-the-best-regular-expression-to-check-if-a-string-is-a-valid-url
	// more examples: https://mathiasbynens.be/demo/url-regex
	
	// probably the best (Diego Perrini)
	//NSString *urlRegex = @"^(?:(?:https?|ftp):\\/\\/)(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\.254(?:\\.\\d{1,3}){2})(?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\\.(?:[a-z\u00a1-\uffff]{2,})))(?::\\d{2,5})?(?:\\/[^\\s]*)?$";
	// TODO: change non capturing groups to conditional in order to work here!
	
	NSString *urlRegex = @"(https?\\/\\/)?(www\\.)?[^\\s\\.]+\\.[^\\s]{2,}|www\\.[^\\s]+\\.[^\\s]{2,}";
    //not bad so far...
	
	NSRegularExpression *linkDetector = [[NSRegularExpression alloc] initWithPattern:urlRegex options:
										 NSRegularExpressionCaseInsensitive|NSRegularExpressionAnchorsMatchLines error:nil];
	
	NSArray *linkRanges = [linkDetector matchesInString:string options:NSMatchingAnchored range:NSMakeRange(0, string.length)];
	
	//NSLog(@"linkRanges: %@", linkRanges);
	 
	 return linkRanges;
	*/
	
	NSError *error;
	NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
	
	return [linkDetector matchesInString:string options:kNilOptions range:NSMakeRange(0, string.length)];
	
}

-(CGRect)frameOfTextRange:(NSRange)range inTextView:(UITextView *)textView {

	UITextPosition *beginning = textView.beginningOfDocument;
	UITextPosition *start = [textView positionFromPosition:beginning offset:range.location];
	UITextPosition *end = [textView positionFromPosition:start offset:range.length];
	UITextRange *textRange = [textView textRangeFromPosition:start toPosition:end];
	CGRect rect = [textView firstRectForRange:textRange];
	
	return [textView convertRect:rect fromView:textView.textInputView];

}

-(void) highlightHashags:(UITextView *)textView {
	
	NSArray *hashtagRanges = [self rangesOfHashtagsOrMentions:YES inString:textView.text];
	
	for (NSTextCheckingResult *result in hashtagRanges) {
		//NSLog(@"hashtag range: %@", NSStringFromRange(result.range));
		CGRect frame = [self frameOfTextRange:result.range inTextView:textView];
		CALayer *layer = [[CALayer alloc] init];
		[layer setFrame:CGRectInset(CGRectOffset(frame, 0, -1), -2, -1)];
		[layer setZPosition:1.0f];
		[layer setBackgroundColor:UIColorFromRGB(0x0000DD).CGColor];
		[layer setOpacity:0.3f];
		[layer setCornerRadius:6.0f];
		
		NSString *hashtag = [textView.text substringWithRange:result.range];
		
		objc_setAssociatedObject(layer, &hashtagOrMentionKey, hashtag, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

		[textView.layer addSublayer:layer];
	}

}

-(void) highlightMentions:(UITextView *)textView {
	
	NSArray *mentionRanges = [self rangesOfHashtagsOrMentions:NO inString:textView.text];

	for (NSTextCheckingResult *result in mentionRanges) {

		NSString *mention = [textView.text substringWithRange:result.range];
		if (![(NSArray *)[_message valueForKey:@"mentions"] containsObject:mention]) { ////sa ili bez @ ?
			continue;
		}
		
		CGRect frame = [self frameOfTextRange:result.range inTextView:textView];
		CALayer *layer = [[CALayer alloc] init];
		[layer setFrame:CGRectInset(CGRectOffset(frame, 0, -1), -2, -1)];
		[layer setZPosition:1.0f];
		[layer setBackgroundColor:UIColorFromRGB(0x0000BB).CGColor];
		[layer setOpacity:0.3f];
		[layer setCornerRadius:6.0f];
		
		objc_setAssociatedObject(layer, &hashtagOrMentionKey, mention, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		
		[textView.layer addSublayer:layer];
	}

}

-(NSArray *)rangesOfHashtagsOrMentions:(BOOL)yesForHashtags inString:(NSString *)string {
	
	NSRegularExpression *hashtagOrMentionDetector = [[NSRegularExpression alloc] initWithPattern:[NSString stringWithFormat:@"%@\\w\\w*", yesForHashtags ? @"#" : @"@"]
																				 options:NSRegularExpressionCaseInsensitive
																				 error:nil];
	
	NSArray *hashtagOrMentionRanges = [hashtagOrMentionDetector matchesInString:string
																options:NSMatchingWithoutAnchoringBounds
																range:NSMakeRange(0, string.length)];
	
	return hashtagOrMentionRanges;
	
}

-(void) mentionTapped:(NSString *)username {

	Profile *profileView = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"Profile"];
	
	[profileView setUsername:username];
	
	//or tabbarcontroller.navigationcontroller
	[[(ChatView *)self.superview parentViewController].navigationController pushViewController:profileView animated:YES];

}

-(void) hashtagTapped:(NSString *)hashtag {
	
}

-(void)avatarTapped:(UITapGestureRecognizer *)sender {
	
	Profile *profileView = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"Profile"];
	
	[profileView setUserID:[_message objectForKey:@"user"]];
	
	//or tabbarcontroller.navigationcontroller
	[[(ChatView *)self.superview parentViewController].navigationController pushViewController:profileView animated:YES];
	
}

-(void)addDateTimeLabelToView:(UIView *)view inContainer:(UIScrollView *)chatView{

	NSString *timeString = (NSString *)[_message objectForKey:@"time"];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]; //// vidit za user message date format
	NSDate *dateTime = [dateFormatter dateFromString:timeString];
	
	//prebacit u lokalni timezone
	dateFormatter = [[NSDateFormatter alloc] init];
	//[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	//[dateFormatter setTimeStyle:NSDateFormatterShortStyle]; //medium
	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	[dateFormatter setDateFormat:@"HH:mm"];
	timeString = [dateFormatter stringFromDate:dateTime];
	
	_dateTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.origin.y + self.frame.size.height / 2 - 4 + (self.displayHeader ? 13 : 0), 0, 1)];
	[_dateTimeLabel  setTextColor:UIColorFromRGB(0x757575)];
    [_dateTimeLabel setFont:[UIFont fontWithName:@"GothamRounded-Book" size:10]];
    [_dateTimeLabel setBackgroundColor:[UIColor clearColor]];
    [_dateTimeLabel setNumberOfLines:1];
    
    [_dateTimeLabel setText:timeString];
    [_dateTimeLabel sizeToFit];
	[_dateTimeLabel setAutoresizingMask:UIViewAutoresizingNone];
	
	[_dateTimeLabel setFrame:CGRectMake(chatView.frame.size.width, _dateTimeLabel.frame.origin.y, _dateTimeLabel.frame.size.width, _dateTimeLabel.frame.size.height)];
	
	__weak typeof (_dateTimeLabel) __dateTimeLabel = _dateTimeLabel;

	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		if (__dateTimeLabel != nil) {
			[chatView addSubview:__dateTimeLabel];
		}
	}];
	
}

-(void)messageTapped:(UITapGestureRecognizer *)sender {

	CGPoint tapLocation = [sender locationInView:sender.view];
	
	for (UIView *_subview in sender.view.subviews) {
		for (UIView *subview in _subview.subviews) {
		
			CGPoint _tapLocation = [sender locationInView:_subview];
			
			if ([subview isKindOfClass:[UIImageView class]] && CGRectContainsPoint(subview.frame, _tapLocation)) {
			
				[self showImageInFullScreen:(UIImageView *)subview];
			
				return;
			}
		}
	}
	
	for (CALayer *layer in [[sender.view layer] sublayers]) {
		
		if (CGRectContainsPoint([layer frame], tapLocation)) {
			
			NSString *hashtagOrMention = objc_getAssociatedObject(layer, &hashtagOrMentionKey);
						
			if (hashtagOrMention != nil) {
				
				if ([hashtagOrMention hasPrefix:@"#"]) {
					[self hashtagTapped:hashtagOrMention];
				} else { //if...?
					[self mentionTapped:[hashtagOrMention substringFromIndex:1]];
				}
				
				return;
			}
		}
	}
	
	//if (CGRectEqualToRect(_layerFrame, CGRectZero) || CGRectContainsPoint(_layerFrame, [sender locationInView:sender.view])) {
	//   [self selectMessage:sender.view];
	//}
}

-(void)showImageInFullScreen:(UIImageView *)imageView {
	
	Gallery *galleryView = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"Gallery"];
	
	[galleryView setProfileImageViews:[NSMutableArray arrayWithObject:imageView]];
	[galleryView setImages:[NSArray arrayWithObject:[_message valueForKey:@"imageToDisplay"] == nil ? [_message valueForKey:@"image_id"] : @""]];
	[galleryView setCurrentImageIndex:0];
	
	UIScrollView *scrollView = (UIScrollView *)self.superview;
	CGRect frame = [self convertRect:imageView.frame toView:imageView.superview];
	frame = [imageView.superview convertRect:imageView.frame toView:scrollView];
	frame = [scrollView convertRect:frame toView:[(UIViewController *)scrollView.delegate view]];
	[galleryView setClosingAnimationFrame:frame];

	////////or tabbarcontroller kad budu business cludovi
	
	// TODO: if keyboard shown, either lower the closing animation frame for keyboard height, or re-show it on dismissal of gallery
	
	if ([_message valueForKey:@"imageToDisplay"] == nil && !imageBeingLoaded) {
		AsyncImageLoadManager *manager = [[AsyncImageLoadManager alloc] initWithImageId:[_message valueForKey:@"image_id"] imageView:imageView useActivityIndicator:YES];
		manager.delegate = (ChatView *)self.superview;
		[manager loadImage];
		imageBeingLoaded = YES;
	}
	
	[[(ChatView *)self.superview parentViewController] presentViewController:galleryView animated:YES completion:^{
		imageView.hidden = YES;
	}];

}

-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    //check if ok to use? https://developers.google.com/safe-browsing/lookup_guide , https://zeltser.com/malicious-ip-blocklists/ , return NO;
	
	SafeBrowsingService *sbs = [[SafeBrowsingService alloc] initWithURL:[URL absoluteString]];
	sbs.delegate = self;
	[sbs performCheck];
	
	return NO;
	
}

-(void)urlCheckFailedWithError:(NSError *)error {
	NSLog(@"error performing URL check!");
	// TODO: default connection error
}

-(void)urlHarmful:(NSString *)reason {
	NSLog(@"Url harmful, reason: %@", reason);
	// TODO: warn user of unsafe url, ask to open anyway?
}

-(void)urlSafe:(NSString *)urlString {
	
	if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]])
	{
		NSLog(@"Opening safe url: %@", urlString);
	} else {
		NSLog(@"Cannot open URL: %@", urlString);
		// TODO: alert user that url can't be open
	}
	
}

-(CGFloat)prepareToDownloadImage:(NSString *)imageId inContainer:(UIScrollView *)container startingY:(CGFloat)startingY {
	
	imageBeingLoaded = YES; //nepotrebno?
	return [self displayImage:nil inContainer:container startingY:startingY];
	
}

-(void)removeMaskFromLayer:(CALayer *)layer{
	layer.mask = nil;
}

-(void)cutCorners:(NSUInteger)corners withRadius:(CGFloat)radius toLayer:(CALayer *)layer inset:(UIEdgeInsets)inset {

	////http://stackoverflow.com/a/25616833/1630623

	if (_deallocated) {
		return;
	}
	
	if (layer == nil) {
		return;
	}
	
	[self removeMaskFromLayer:layer];
	
	CGFloat x = inset.right - inset.left > 0 ? inset.left - inset.right : inset.right - inset.left; //zašto obrnuto?
	CGFloat y = 0;//-inset.top; //želimo obuhvatit i insete vertikalno, horizontalno bez inseta
	
	CGRect frame = CGRectMake(x, y, layer.bounds.size.width - inset.left - inset.right, layer.bounds.size.height - inset.top/* - inset.bottom*/);
	
	UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:frame byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
 
	CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
	maskLayer.frame = frame;
	maskLayer.path  = maskPath.CGPath;
	layer.mask = maskLayer;
	
}

-(void)cutCorners:(NSUInteger)corners withRadius:(CGFloat)radius {
	
	if (_deallocated) { //never happens?
		NSLog(@"deallocated");
		return;
	}
	
	__weak typeof (self) this = self;
	
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		
		if (this != nil) {
			
			@try { //remove
    
				BOOL hasImage = !([_message objectForKey:@"imageToDisplay"] == nil && ([_message objectForKey:@"image_id"] == nil || [[_message objectForKey:@"image_id"] isEqualToString:@""]));
			
				UIEdgeInsets inset = UIEdgeInsetsZero;
			
				[self cutCorners:corners withRadius:radius toLayer:_backgroundLayer inset:inset];
			
				for (UIView *subview in self.subviews) {
					if (!([subview isEqual:_avatar] || [subview isEqual:_nameLabel])) {
						[self cutCorners:corners withRadius:radius toLayer:subview.layer inset:[subview isKindOfClass:[UITextView class]] ? [(UITextView *)subview contentInset] : inset];
					}
				}
			
				if (hasImage) {
					[self cutCorners:corners withRadius:radius toLayer:self.imageView.layer inset:inset];
				}
			
			}
			@catch (NSException *exception) {
				NSLog(@"caught exception %@", exception.reason);
			}
			@finally {
				
			}
			
		}
		
	}];
	
}

-(void)adjustCorners {
	
	if (_deallocated || self.messageType == MTAppMessage) {
		return;
	}
	
	CGFloat radius = 12.0f;
	
	NSUInteger topSideCorner = (self.messageType == MTUserMessage) ? UIRectCornerTopLeft : UIRectCornerTopRight;
	NSUInteger topCorners = UIRectCornerTopLeft | UIRectCornerTopRight;
	
	NSUInteger bottomSideCorner = (self.messageType == MTUserMessage) ? UIRectCornerBottomLeft : UIRectCornerBottomRight;
	NSUInteger bottomCorners = UIRectCornerBottomLeft | UIRectCornerBottomRight;
	
	NSUInteger allButBottomEdge = topCorners | bottomSideCorner;
	NSUInteger allButTopEdge = bottomCorners | topSideCorner;
	NSUInteger oneSideOnly = topSideCorner | bottomSideCorner;
	
	NSUInteger allCorners = UIRectCornerAllCorners;
	
	if (_displayHeader) {
		if (self.nextMessage != nil) {
			if (self.nextMessage.messageType == self.messageType) {
				/* && [[self.nextMessage.message valueForKey:@"user"] isEqualToString:[self.message valueForKey:@"user"]] //nepotrbno jer _displyHeader pokriva taj uvjet*/
				[self cutCorners:allButBottomEdge withRadius:radius];
			} else {
				[self cutCorners:allCorners withRadius:radius];
			}
		} else {
			[self cutCorners:allCorners withRadius:radius];
		}
	} else {
		if (self.nextMessage != nil) {
			if (self.nextMessage.messageType == self.messageType) {
				[self cutCorners:oneSideOnly withRadius:radius];
			} else {
				[self cutCorners:allButTopEdge withRadius:radius];
			}
		} else {
			[self cutCorners:allButTopEdge withRadius:radius];
		}
	}
	
}

-(CGFloat)displayImage:(UIImage *)image inContainer:(UIScrollView *)container startingY:(CGFloat)startingY {
	
	CGFloat w;
	CGFloat h;
	
	if (image == nil) {
		w = 64;
		h = 64;
	} else {
		w = image.size.width + 16;
		h = image.size.height + 16;
	}
	
	CGFloat maxW = container.frame.size.width - 78; ////////70
    if (w > maxW) {
        CGFloat aspect = w / maxW;
        w = maxW;
        h /= aspect;
    }
    /* height maximum?
    if (h > self.view.frame.size.height) {
        CGFloat aspect = h / self.view.frame.size.height;
        h = self.view.frame.size.height;
        w /= aspect;
    }
    */
    
    UIView *imageViewContainer = [[UIView alloc] init];
    [imageViewContainer setFrame:CGRectMake(54 + (_messageType == MTUserMessage ? (NSInteger)round(maxW - w) + 1 : -7), startingY + 2, w, h)];
    [imageViewContainer setUserInteractionEnabled:YES];

    _frameX = MIN(imageViewContainer.frame.origin.x, _frameX);
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setFrame:CGRectMake(8.0f, 8.0f, imageViewContainer.frame.size.width - 16.0f, imageViewContainer.frame.size.height - 16.0f)];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];

	[imageView setClipsToBounds:YES];
	imageView.layer.shouldRasterize = YES;
	imageView.layer.masksToBounds = YES;

	if (image == nil && [_message valueForKey:@"image_id"] != nil && ![[_message valueForKey:@"image_id"] isEqualToString:@""]) {
		AsyncImageLoadManager *manager = [[AsyncImageLoadManager alloc] initWithImageId:[_message valueForKey:@"image_id"] imageView:imageView useActivityIndicator:YES];
		manager.delegate = (ChatView *)container;
		[manager loadImage];
	} else {
		imageBeingLoaded = NO;
		[imageView setImage:image];
	}

	if (image != nil && [_message valueForKey:@"image_id"] != nil && ![[_message valueForKey:@"image_id"] isEqualToString:@""]) {
		
		imageView.alpha = 0.0f;
		
		[UIView animateWithDuration:0.25 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
			imageView.alpha = 1.0f;
		} completion:nil];
		
	}
	
	_imageView = imageView;
	[imageViewContainer setClipsToBounds:YES];
    [imageViewContainer addSubview:imageView];
	
    if ([[_message objectForKey:@"text"] isEqualToString:@""]) {
        [imageViewContainer setBackgroundColor:(_messageType == MTUserMessage ? UIColorFromRGB(0xFFD203) : UIColorFromRGB(0xEBEBEB))];
    } else {
        [imageViewContainer setBackgroundColor:[UIColor clearColor]];
    }

    [self addSubview:imageViewContainer];
	
	UITapGestureRecognizer *messageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageTapped:)];
	[self addGestureRecognizer:messageTap];
	
    return imageViewContainer.frame.size.height + 2;
    
}

-(void)loadMessageUser {
	ApiService *service = [[ApiService alloc] init];
	service.delegate = self;
	[service getUsersByIdList:[NSArray arrayWithObject:[self.message valueForKey:@"user"]]];
}

-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {
	
	NSLog(@"received message user: %@", usersList);
	NSDictionary *user;
	
	if (usersList == nil || [(NSArray *)usersList count] == 0) {
		user = @{@"id": [self.message valueForKey:@"user"],
				 @"nameFirst": @"Radyus",
				 @"nameLast": @"User",
				 @"username": @"radyus_user_0000000000000000000000000",
				 @"email": @"radyus_user_0000000000000000000000000@rady.us",
			     @"pictures": @[]}; //dummy image?
	} else {
		user = [(NSArray *)usersList firstObject];
	}
	
	//NSLog(@"loaded message user: %@, displayHeader: %d", user, _displayHeader);
	
	[_message setValue:[[user valueForKey:@"pictures"] firstObject] forKey:@"userImage"];
	[_message setValue:[user valueForKey:@"nameFirst"] forKey:@"nameFirst"];
	[_message setValue:[user valueForKey:@"nameLast"] forKey:@"nameLast"];
	
	DBManager *manager = [[DBManager alloc] initWithDatabaseFilename:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"] stringByAppendingString:@".sqlt"]];
	[manager saveUser:(NSJSONSerialization *)user]; //operationqueue?
	[manager saveMessage:(NSJSONSerialization *)self.message];
	
	if (!_displayHeader) {
		return;
	}
	
	NSString *userImage = [[user objectForKey:@"pictures"] firstObject];
	
	if (userImage != nil && ![userImage isEqualToString:@""]) {
		AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:userImage imageView:_avatar useActivityIndicator:NO];
		[loader loadImage];
	} else {
		[_avatar setDefaultImageWithFirstName:[user objectForKey:@"nameFirst"] lastName:[user objectForKey:@"nameLast"]];
	}
	
	NSString *userName = [[NSString stringWithFormat:@"%@ %@", [_message objectForKey:@"nameFirst"], [_message objectForKey:@"nameLast"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
	[_nameLabel setText:userName];
	[_nameLabel sizeToFit];
	
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	[self apiService:service receivedUsersList:nil];
}

-(BOOL)shouldDisableUserInteraction {
	return NO;
}

-(CGFloat)displayMessageHeader:(UIScrollView *)container startingY:(CGFloat)startingY {
    
    CGFloat currentY = startingY + 4;
    BOOL isFriendMessage = _messageType == MTFriendMessage;
	
	
	NSString *userName = isFriendMessage ? [NSString stringWithFormat:@"%@ %@", [_message objectForKey:@"nameFirst"], [_message objectForKey:@"nameLast"]] : @"Me";
	userName = [userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
	//CGFloat distance =  isFriendMessage ? [[_message objectForKey:@"distance"] floatValue] : 0.0f;
	
	NSString *userImage = isFriendMessage ? [_message objectForKey:@"userImage"] : nil;
	
    if (isFriendMessage) {
		
        _avatar = [[UIImageView alloc] initWithFrame:CGRectMake(10, currentY + 2, 34, 34)];
		
		if (userImage != nil && ![userImage isEqualToString:@""]) {
			AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:userImage imageView:_avatar useActivityIndicator:NO];
			[loader loadImage];
		} else {
			if (![[self.message valueForKey:@"loadUser"] isEqualToString:@"yes"]) {
				[_avatar setDefaultImageWithFirstName:[_message objectForKey:@"nameFirst"] lastName:[_message objectForKey:@"nameLast"]];
			}
		}
		
        [_avatar.layer setCornerRadius:17.0f];
        [_avatar.layer setZPosition:99.0f];
		[_avatar setClipsToBounds:YES];
		[_avatar setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
        
        UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTapped:)];
        [_avatar setUserInteractionEnabled:YES];
        [_avatar addGestureRecognizer:avatarTap];
        
        [self addSubview:_avatar];
		
    }
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(54, currentY + 4, 0, 0)];
    [_nameLabel setNumberOfLines:1];
    [_nameLabel setTextAlignment:(isFriendMessage ? NSTextAlignmentLeft : NSTextAlignmentRight)];
    [_nameLabel setTextColor:[UIColor darkTextColor]];
    [_nameLabel setFont:[UIFont fontWithName:@"GothamRounded-Medium" size:12]];
    [_nameLabel setPreferredMaxLayoutWidth:container.frame.size.width / 2 - 56];
    [_nameLabel setBackgroundColor:[UIColor clearColor]];
    [_nameLabel setText:userName];
    [_nameLabel sizeToFit];
	
	UITapGestureRecognizer *nameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTapped:)]; //change method name to userTapped
	[_nameLabel setUserInteractionEnabled:YES];
	[_nameLabel addGestureRecognizer:nameTap];
	
    if (!isFriendMessage) {
        CGFloat x = container.frame.size.width - _nameLabel.frame.size.width - 8;
		[_nameLabel setFrame:CGRectMake(x, _nameLabel.frame.origin.y + 2, _nameLabel.frame.size.width, _nameLabel.frame.size.height)];
    }
	
	[self addSubview:_nameLabel];
	
	if ([[self.message valueForKey:@"shouldLoadUser"] isEqualToString:@"yes"]) {
		[self loadMessageUser];
	}
	
	return 24;
	
}

-(void)animateMessage {

    NSLog(@"Animating message, %@", NSStringFromCGRect(self.frame));

    CGAffineTransform _transform = CGAffineTransformMakeScale(1.2, 1.2);
    CGAffineTransform transform = CGAffineTransformTranslate(_transform, -0.1 * self.frame.size.width, 0);

    [UIView animateWithDuration:0.15 delay:0.05 options:0 animations:^{

        self.transform = transform;

    } completion:^(BOOL finished) {

        [UIView animateWithDuration:0.68 delay:0.07 usingSpringWithDamping:0.25 initialSpringVelocity:0.25 options:UIViewAnimationOptionCurveEaseIn animations:^{

            self.transform = CGAffineTransformIdentity;

        } completion:^(BOOL finished) {
            
        }];
    }];
}

-(void)didMoveToSuperview {
    if (self.shouldAnimate) {
        [self animateMessage];
    }
}

-(void)dealloc {

	_deallocated = YES;

	[self.layer removeAllAnimations];
	[UIView cancelPreviousPerformRequestsWithTarget:self];
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	
	for (UIView *subview in self.subviews) {
		if ([subview isKindOfClass:[LinkPreview class]]) {
			((LinkPreview *)subview).delegate = nil;
		}
	}
	
}

@end
