//
//  ViewController.m
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

NSArray *messagesStrings;
NSTimer *timer;

- (void)viewDidLoad {

	[super viewDidLoad];
	
    //[self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //[self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
	
	_loginView.readPermissions = @[@"public_profile", @"email", @"user_friends"/*, @"user_birthday"*/]; /////
	_loginView.delegate = self; //corner radius in viewdidappear
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
	if (self.view.frame.size.width == 320) {
		[_backgroundImageView setImage:[UIImage imageNamed:@"Default-568h.png"]];
	} else if (self.view.frame.size.width < 730) {
		[_backgroundImageView setImage:[UIImage imageNamed:@"Default-667h.png"]];
	} else {
		[_backgroundImageView setImage:[UIImage imageNamed:@"Default-736h.png"]];
	}
	
	//TODO: if user logged out skip this: (return;)
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registeredForRemoteNotifications:) name:@"RadyusRegisteredForRemoteNotifications" object:nil];

	[_loginView setHidden:YES];
	[_emailLoginButton setHidden:YES];
	[_messagesLabel setHidden:YES];
	[_pageControl setHidden:YES];
	[_logoIV setHidden:YES];
	[_logoSubtitle setHidden:YES];
	
	[self checkIfLoggedIn];
	
	// TODO: else:
	// wrap in function
	_motionManager = [[CMMotionManager alloc] init];
	[_motionManager setAccelerometerUpdateInterval:0.05]; ////?
														  //[_motionManager setDeviceMotionUpdateInterval:0.1]; ////?
	
	if ([FBSDKAccessToken currentAccessToken] != nil) {
		//NSLog(@"FB access token: %@", [FBSDKAccessToken currentAccessToken].tokenString);
		//[_apiService fbAuth:[FBSDKAccessToken currentAccessToken].tokenString];
	}
	
	messagesStrings = [NSArray arrayWithObjects:@"Message 1 \nblablabalbalblblblbalalbal",
					   @"Message 2 \nblablabalbalblblblbalalbal",
					   @"Ask, share and communicate with information wherever you are.",
					   @"Message 4 \nblablabalbalblblblbalalbal",
					   nil];
	
	UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft:)];
	[swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
	[_messagesLabel addGestureRecognizer:swipeLeft];
	
	UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight:)];
	[swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
	[_messagesLabel addGestureRecognizer:swipeRight];
	
	// TODO: scrollview, paging

}

-(void)registeredForRemoteNotifications:(NSNotification *)notification {
	[self checkIfLoggedIn];
}

-(void)checkIfLoggedIn {
	
	NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"];
	NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
	
	if (accessToken != nil && deviceToken != nil) {
		[_apiService getUserInfo:accessToken];
		// TODO: remove observer
	} else if (accessToken == nil) {
		[_loginView setHidden:NO];
		[_emailLoginButton setHidden:NO];
		[_messagesLabel setHidden:NO];
		[_pageControl setHidden:NO];
		[_logoIV setHidden:NO];
		[_logoSubtitle setHidden:NO];
	}
	
	// TODO: what user says "no" or if fails for another reason?
	
}

-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
	
	if (error != nil) {
		NSLog(@"error: %@", error.localizedDescription);
		return;
	}
	
	//NSLog(@"Result: %@", result);
	
	if ([result isCancelled]) {
		return;
	}
	
	/* //which permissions do we need?
	if ([[result declinedPermissions] containsObject:]) {
		
	}*/
	
	//check access token
	
	//if first facebook login
	
	//FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
	//content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1601694480071243"];
	//content.previewImageURL = [NSURL URLWithString:@"https://rady.us/splash.png"];
	
	//[FBSDKAppInviteDialog showWithContent:content delegate:self];
	
	//else
	
	[_apiService fbAuth:[FBSDKAccessToken currentAccessToken].tokenString];
	
	
	
	//test
	//[FBSDKProfile currentProfile].firstName, lastName...
	
	//test
	//FBSDKProfilePictureView *profilePicture = [[FBSDKProfilePictureView alloc] initWithFrame:CGRectMake(0, 64, 64, 64)];
	//[profilePicture setProfileID:@"me"];
	//[self.view addSubview: profilePicture];
	//[self.view bringSubviewToFront:profilePicture];
	
	// TODO: add 'use facebook profile image' option, load profile image from facebook if needed
	
	/* //debug
	[[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
		if (error) {
			 //error
		} else {
			 
			NSLog(@"fetched user:%@", result);
			 
			if ([result objectForKey:@"email"]) {
				NSLog(@"Email: %@",[result objectForKey:@"email"]);
			}

		}
	}];
	*/
	
}

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
	
	[_apiService fbAuth:[FBSDKAccessToken currentAccessToken].tokenString];

}

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
	
	// alert error
	[_apiService fbAuth:[FBSDKAccessToken currentAccessToken].tokenString];

}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
	NSLog(@"loginButtonDidLogOut");
	[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"accessToken"];
}

//////na prvom view-u ?


-(void)checkAccessToken {
	//getuserinfo
}

-(void)receivedAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken socketToken:(NSString *)socketToken {

	[_apiService getUserInfo:accessToken];
}

-(void)receivedUserInfo:(NSJSONSerialization *)userInfo {
	
	//[self performSegueWithIdentifier:@"LoginSuccess" sender:userInfo];
	
	UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Explore"];
	[(ExploreMap *)[[vc childViewControllers] firstObject] setAuthorized:YES];
	[self viewWillDisappear:NO];
	[self.navigationController performSelector:@selector(setViewControllers:) withObject:[NSArray arrayWithObject: vc] afterDelay:0.3];
	// TODO: add small delay so we don't get 'unbalanced' warning
	
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	
	if (errorCode == 401 /*unauthorized*/ || errorCode == 500) {
		
		if ([FBSDKAccessToken currentAccessToken] != nil) {
			FBSDKLoginManager *fbLoginManager = [[FBSDKLoginManager alloc] init];
			[fbLoginManager logOut];
		}
		
		[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"accessToken"];
		
	}

	[_loginView setHidden:NO];
	[_loginView setNeedsDisplay];
	[_loginView setNeedsLayout];
	[_loginView layoutIfNeeded];
	[_emailLoginButton setHidden:NO];
	[_messagesLabel setHidden:NO];
	[_pageControl setHidden:NO];
	[_logoIV setHidden:NO];
	[_logoSubtitle setHidden:NO];
	
}

-(BOOL)shouldDisableUserInteraction {
	return NO;
}

-(IBAction)didSwipeLeft:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *) sender;
    CGPoint swipeLocation = [swipe locationInView:_messagesLabel];
    if (swipeLocation.x <= _messagesLabel.frame.size.width && swipeLocation.y <= _messagesLabel.frame.size.height && swipeLocation.x >= 0 && swipeLocation.y >=  0) {
        [self nextPage:sender];
    }
}

-(IBAction)didSwipeRight:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *) sender;
    CGPoint swipeLocation = [swipe locationInView:_messagesLabel];
    if (swipeLocation.x <= _messagesLabel.frame.size.width && swipeLocation.y <= _messagesLabel.frame.size.height && swipeLocation.x >= 0 && swipeLocation.y >=  0) {
        [self previousPage:sender];
    }
}


-(void)viewWillAppear:(BOOL)animated {
	
	[self.navigationController.navigationBar setHidden:YES];
	
	//kopirat u exploretabbarcontroller
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
	[self.navigationController.navigationBar setShadowImage:blank]; // NEPOTREBNO!?
    //[self.navigationController.navigationBar setBackgroundImage:yellow forBarMetrics:UIBarMetricsDefault];
    
    UIImage *backButtonBackgroundImage = [UIImage imageNamed:@"back_button"];
    backButtonBackgroundImage = [backButtonBackgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 11, 0, 8)];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setBackButtonBackgroundImage:[backButtonBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	// Why it happens on uiimagepickercontroller?
    
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:NULL];
	self.navigationItem.backBarButtonItem = backBarButton; ////////izbacit?
	
	[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
 
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"GothamRounded-Bold" size:22], NSFontAttributeName, nil]];  //ne radi!!
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
	
}

-(void)viewDidAppear:(BOOL)animated {
	
	timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(nextPage:) userInfo:nil repeats:YES];
	
	
	[_motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
		if (error == nil) {
			_adx = accelerometerData.acceleration.x;
			_ady = accelerometerData.acceleration.y;
			_adz = accelerometerData.acceleration.z;
		}
	}];
	
	_accTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(adjustImage) userInfo:nil repeats:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareForBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnFromBackground) name:UIApplicationDidBecomeActiveNotification object:nil];
	
}

-(void)prepareForBackground {

	[timer invalidate];
	
	[_motionManager stopAccelerometerUpdates];
	[_accTimer invalidate];
	
	if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
		[_apiService cancelRequests];
	}

}

-(void)returnFromBackground {
	
	timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(nextPage:) userInfo:nil repeats:YES];
	
	
	[_motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
		if (error == nil) {
			_adx = accelerometerData.acceleration.x;
			_ady = accelerometerData.acceleration.y;
			_adz = accelerometerData.acceleration.z;
		}
	}];
	
	_accTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(adjustImage) userInfo:nil repeats:YES];

}

-(void)adjustImage {
	
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		//NSLog(@"accelerometerData: {x: %f, y:%f, z:%f}", _adx, _ady, _adz);
		//_backgroundImageView.transform = CGAffineTransformMakeTranslation(MIN(MAX(_adx * 100, -16), 16), MIN(MAX(_ady * 100, -16), 16));
		_backgroundImageView.transform = CGAffineTransformMake(1.1, 0, 0, 1.1, - MIN(MAX( _adx * 40, -16), 16), MIN(MAX((_ady + 0.85) * 40, -16), 16));
	}];
	
}

-(IBAction)nextPage:(id)sender {
    if (_pageControl.currentPage == _pageControl.numberOfPages - 1) {
        _pageControl.currentPage = 0;
    } else _pageControl.currentPage++;
	[self changeMessage:(NSString *)[messagesStrings objectAtIndex:_pageControl.currentPage]];
	//[_messagesLabel setText:(NSString *)[messagesStrings objectAtIndex:_pageControl.currentPage]];
}

-(IBAction)previousPage:(id)sender {
    if (_pageControl.currentPage == 0) {
        _pageControl.currentPage = _pageControl.numberOfPages - 1;
    } else _pageControl.currentPage--;
	[self changeMessage:(NSString *)[messagesStrings objectAtIndex:_pageControl.currentPage]];
	//[_messagesLabel setText:(NSString *)[messagesStrings objectAtIndex:_pageControl.currentPage]];
}

-(void)changeMessage:(NSString *)aNewMessage {
	
	[UIView animateWithDuration:0.15f animations:^{
		_messagesLabel.alpha = 0.1f;
	} completion:^(BOOL finished) {
		[_messagesLabel setText:aNewMessage];
		[UIView animateWithDuration:0.2f animations:^{
			_messagesLabel.alpha = 1.0f;
		}];
	}];
	
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	//izbacit
	if ([[segue identifier] isEqualToString:@"LoginSuccess"]) {
		Profile *dvc = [segue destinationViewController]; //Explore, authorized = YES;
		[dvc setUserInfo:sender];
	}
	
}

-(void)viewWillDisappear:(BOOL)animated {
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
	
    [timer invalidate];
	
	[_motionManager stopAccelerometerUpdates];
	[_accTimer invalidate];
	
	if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
		[_apiService cancelRequests];
	}
	
	[super viewWillDisappear:animated];
	
	[self.navigationController.navigationBar setHidden:NO];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
