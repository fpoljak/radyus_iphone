//
//  ExploreList.h
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExploreOptionsDialog.h"
#import "ExploreTabBarController.h"
#import "SideMenuUtilizer.h"
#import "ApiService.h"
#import "AsyncImageLoadManager.h"

@interface ExploreList : SideMenuUtilizer <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, ApiServiceDelegate, AsyncImageLoadDelegate>

@property (nonatomic) NSInteger cloudsCount;
@property (nonatomic) NSInteger indexOfCloudBeingLoaded;
@property (nonatomic, retain) NSMutableArray *arrayOfCloudUsersArrays;

@property (nonatomic, retain) IBOutlet UIButton *mapButton;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIButton *optionsButton;
@property (nonatomic) CGFloat radius;
@property (nonatomic, retain) NSString *access;
@property (nonatomic, retain) ApiService *apiService;
@property (nonatomic, retain) NSArray *cloudList;
@property (nonatomic, retain) NSJSONSerialization *selectedCloud;
@property (nonatomic, retain) IBOutlet UILabel *cloudCountLabel;
@property (nonatomic, retain) UIActivityIndicatorView *reloadIndicator;

@property (nonatomic) CLLocationCoordinate2D userLocation;

-(void)commitSettings:(NSDictionary *)settings;
-(void)reloadTable;
-(void)setCloudCountLabel;

@end
