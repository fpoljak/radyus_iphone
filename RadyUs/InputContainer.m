//
//  InputContainer.m
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "InputContainer.h"

@implementation InputContainer

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib {

    [super awakeFromNib];
    
    //self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = self.frame.size.height / 2;
    if (![[NSStringFromClass([[self parentViewController] class]) substringToIndex:7] isEqualToString:@"Explore"] ) {
        self.layer.borderWidth = 1.0f;
        CGFloat whitecomp = 0.81176f;//0.31834f;
        [self.layer setBorderColor: [[UIColor colorWithWhite:whitecomp alpha:1.0f] CGColor]];
    } else {
       // self.la
    }
    [self.layer setMasksToBounds: YES];

}

- (UIViewController *)parentViewController {
    UIResponder *responder = self;
    while (![responder isKindOfClass:[UIViewController class]])
        responder = [responder nextResponder];
    return (UIViewController *)responder;
}

@end
