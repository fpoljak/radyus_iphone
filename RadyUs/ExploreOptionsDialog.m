//
//  ExploreOptionsDialog.m
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ExploreOptionsDialog.h"

@interface ExploreOptionsDialog ()

@end

@implementation ExploreOptionsDialog
@synthesize delegate;

BOOL isFeet;

- (void)viewDidLoad {
    [super viewDidLoad];
	
	_access = (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:@"AccessForReadingClouds"] ?: @"All";
	
	if ([_access isEqualToString:@"All"]) {
		[self allSelected:nil];
	} else if ([_access isEqualToString:@"Public"]) {
		[self publicSelected:nil];
	} else if ([_access isEqualToString:@"Friends"]) {
		[self friendsSelected:nil];
	} else if ([_access isEqualToString:@"1on1"]) {
		[self oneOnOneSelected:nil];
	} else {
		[self allSelected:nil];
	}
	
	[self setDistanceLabels];
	
	UITapGestureRecognizer *allTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(allSelected:)];
	[_allCheck addGestureRecognizer:allTap];

	UITapGestureRecognizer *publicTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(publicSelected:)];
	[_publicCheck addGestureRecognizer:publicTap];
	
	UITapGestureRecognizer *friendsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(friendsSelected:)];
	[_friendsCheck addGestureRecognizer:friendsTap];

	UITapGestureRecognizer *oneOnOneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneOnOneSelected:)];
	[_oneOnOneCheck addGestureRecognizer:oneOnOneTap];
	
    //NSLog(@"Parent: %@", NSStringFromClass([self.parentViewController class]));
}

-(void)viewWillAppear:(BOOL)animated {

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDistanceLabels) name:@"RadyusDistanceUnitChangedNotification" object:nil];

}

-(void)setDistanceLabels {
	
	isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];
	
	_radiusSlider.value = _radius;
	
	if (isFeet) {
		
		[_radiusLabel setText:[NSString stringWithFormat:@"%.0f feet selected", _radius * 3.28084]];
		
	} else {
	
		[_radiusLabel setText:[NSString stringWithFormat:@"%.0f meters selected", _radius]];
	
	}
}

-(void)allSelected:(UITapGestureRecognizer *)sender {
	_access = @"All";
	[[_allCheck viewWithTag:1] setAlpha:1.0f];
	[[_publicCheck viewWithTag:1] setAlpha:0.0f];
	[[_friendsCheck viewWithTag:1] setAlpha:0.0f];
	[[_oneOnOneCheck viewWithTag:1] setAlpha:0.0f];
}

-(void)publicSelected:(UITapGestureRecognizer *)sender {
	_access = @"Public";
	[[_allCheck viewWithTag:1] setAlpha:0.0f];
	[[_publicCheck viewWithTag:1] setAlpha:1.0f];
	[[_friendsCheck viewWithTag:1] setAlpha:0.0f];
	[[_oneOnOneCheck viewWithTag:1] setAlpha:0.0f];
}

-(void)friendsSelected:(UITapGestureRecognizer *)sender {
	_access = @"Friends";
	[[_allCheck viewWithTag:1] setAlpha:0.0f];
	[[_publicCheck viewWithTag:1] setAlpha:0.0f];
	[[_friendsCheck viewWithTag:1] setAlpha:1.0f];
	[[_oneOnOneCheck viewWithTag:1] setAlpha:0.0f];
}

-(void)oneOnOneSelected:(UITapGestureRecognizer *)sender {
	_access = @"1on1";
	[[_allCheck viewWithTag:1] setAlpha:0.0f];
	[[_publicCheck viewWithTag:1] setAlpha:0.0f];
	[[_friendsCheck viewWithTag:1] setAlpha:0.0f];
	[[_oneOnOneCheck viewWithTag:1] setAlpha:1.0f];
}

-(IBAction)radiusSliderValueChanged:(UISlider *)sender {
    
    CGFloat newRadius = roundf(sender.value / 10) * 10;
    if (newRadius == _radius) {
        return;
    }
	//access
	
    _radius = newRadius;
	
	if (isFeet) {
		[_radiusLabel setText:[NSString stringWithFormat:@"%.0f feet selected", newRadius * 3.28084]];
	} else {
		[_radiusLabel setText:[NSString stringWithFormat:@"%.0f meters selected", newRadius]];
	}
	
}

-(IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([sender tag] != 1) {
			//access
            NSDictionary *settings = @{@"access" : _access, @"radius" : [NSNumber numberWithFloat:_radius]};
            [self.delegate commitSettings:settings];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated {

	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"RadyusDistanceUnitChangedNotification" object:nil];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
