//
//  CloudView.h
//  RadyUs
//
//  Created by Frane Poljak on 11/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <MapKit/MKOverlay.h>
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioToolbox.h>
#import "UnavailableCloudView.h"
#import "MapPin.h"
#import "SideMenuUtilizer.h"
#import "ExploreOptionsDialog.h"
#import "ExploreTabBarController.h"
#import "ChatView.h"
#import "Profile.h"
#import "ApiService.h"
#import "DBManager.h"
#import "AsyncImageLoadManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "FBAnnotationClustering/FBClusteringManager.h"
#import "TextViewWithPasteOverriding.h"

@interface CloudView : SideMenuUtilizer <MKMapViewDelegate, MKOverlay, MKAnnotation, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, ApiServiceDelegate, AsyncImageLoadDelegate>

enum CloudViewStates : NSUInteger{
	CSStateOriginal = 0,
	CSChatExpanded = 1,
	CSChatInFullScreen = 2,
	CSMapExpanded = 3,
	CSCloudInactive = 4
	//CSStateResizing = 9
};

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic) BOOL loaded;
@property (nonatomic) BOOL checkedForNewMessages;
@property (nonatomic) BOOL authorized;
@property (nonatomic) BOOL userInCloud;
@property (nonatomic) CLLocationCoordinate2D userLocation;
@property (nonatomic, retain) CLLocationManager *locationManager;
//@property (nonatomic) int tryCount;
@property (nonatomic) enum CloudViewStates cloudViewState;
@property (nonatomic, retain) NSJSONSerialization *cloudList;
@property (nonatomic, retain) NSMutableArray *cloudUsersList;
@property (nonatomic, retain) NSMutableDictionary *pinHashMap;
@property (nonatomic, retain) IBOutlet UIButton *listButton;
@property (nonatomic) CGFloat radius;
@property (nonatomic, retain) NSString *access; //CloudAccess
@property (nonatomic, retain) IBOutlet UIButton *optionsButton;
@property (nonatomic, retain) IBOutlet UIButton *cloudCreatorButton;
@property (nonatomic, retain) IBOutlet ChatView *chatView;
@property (nonatomic, retain) IBOutlet UIView *chatControlsContainer;
@property (nonatomic, retain) IBOutlet UITextView *messageInputView;
@property (nonatomic, retain) IBOutlet UIView *cloudOverviewContainer;
@property (nonatomic, retain) IBOutlet UIView *addPhotoControlsContainer;
@property (nonatomic, retain) IBOutlet UITextView *messageInputPlaceholderView;
@property (nonatomic, retain) IBOutlet UIButton *sendButton;

@property (nonatomic, retain) UIView *reviveCloudView;

@property (nonatomic) CGSize kbSize;
@property (nonatomic) double kbAnimationDuration;
@property (nonatomic) UIViewAnimationOptions kbAnimationEffect;

@property (nonatomic, retain) NSString *cloudId;
@property (nonatomic, retain) NSJSONSerialization *cloud;
@property (nonatomic, retain) IBOutlet UITextView *descriptionLabel;
@property (nonatomic, retain) IBOutlet UILabel *radiusLabel;
@property (nonatomic, retain) IBOutlet UILabel *durationLabel;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *menuButton;

@property(nonatomic, retain) NSTimer *durationTimer;
@property (nonatomic) NSInteger duration;

@property (nonatomic, retain) IBOutlet UIView *joinView;
@property (nonatomic, retain) IBOutlet UIButton *joinButton;
@property (nonatomic, retain) UIButton *refreshButton;

@property (nonatomic, retain) ApiService *apiService;
@property (nonatomic, retain) ApiService *cloudCreatorService;
@property (nonatomic, retain) ApiService *syncService;
@property (nonatomic, retain) DBManager *dbManager;

@property (nonatomic) BOOL firstLocationUpdate;
@property (nonatomic, retain) FBClusteringManager *clusteringManager;

@property (nonatomic, retain) NSMutableArray *messageBuffer;
//@property (nonatomic, retain) NSMutableArray *loadingMessageBuffer;
//@property (nonatomic, retain) NSMutableArray *loadingPreviousBuffer;
@property (nonatomic) BOOL messageUserLoading;

@end


