//
//  SafeBrowsingService.h
//  Radyus
//
//  Created by Frane Poljak on 25/11/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SafeBrowsingServiceDelegate;

@interface SafeBrowsingService : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, assign) id<SafeBrowsingServiceDelegate> delegate;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSMutableData *reqData;

-(instancetype)initWithURL:(NSString *)urlString;
-(void)performCheck;

@end

@protocol SafeBrowsingServiceDelegate <NSObject>

-(void)urlSafe:(NSString *)urlString;
-(void)urlHarmful:(NSString *)reason;
-(void)urlCheckFailedWithError:(NSError *)error;

@end
