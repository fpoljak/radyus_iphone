//
//  ActionSuccessIndicator.h
//  Radyus
//
//  Created by Locastic MacbookPro on 18/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionSuccessIndicator : UIView

-(instancetype _Nullable)init;

-(void)showWithSuccess:(BOOL)success inViewController:(UIViewController * _Nonnull)viewController duration:(CGFloat)duration message:(NSString * _Nullable)message completion:(void (^ _Nullable)(BOOL finished))completion;

@end
