//
//  UIImageView+NoAvatar.m
//  Radyus
//
//  Created by Frane Poljak on 15/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "UIImageView+NoAvatar.h"

@implementation UIImageView (NoAvatar)

-(UIImage *)imageFromText:(NSString *)text {
	
	CGFloat fontSize = MIN(28, MIN(self.frame.size.width - 4, self.frame.size.height - 4) / 2);
	NSLog(@"font size: %f", fontSize);
	UIFont *font = [UIFont fontWithName:@"GothamRounded-Bold" size:fontSize];
	UIColor *textColor = UIColorFromRGB(0x4A4A4A);
	
	NSDictionary *attr = @{NSFontAttributeName:font, NSForegroundColorAttributeName: textColor};
	CGSize size  = [text sizeWithAttributes:attr];
	size.height += 2; //za shadow
	size.width += 2;
	
	UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetShadowWithColor(ctx, CGSizeMake(2.0, 2.0), 5.0, [[textColor colorWithAlphaComponent:0.85f] CGColor]);
	
	[text drawAtPoint:CGPointMake(0.0, 0.0) withAttributes:attr];
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
	
}

-(void)setDefaultImageWithFirstName:(NSString *)firstName lastName:(NSString *)lastName {
	
	CGSize imageSize = self.frame.size;

	UIColor *fillColor = UIColorFromRGB(0xFFD203); //random?
	UIGraphicsBeginImageContext(imageSize);
	CGContextRef context = UIGraphicsGetCurrentContext();
	[fillColor setFill];
	CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
	UIImage *background = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(imageSize);
	
	NSString *initials = @"";
	
	if (firstName != nil && firstName.length > 0) {
		initials = [initials stringByAppendingString:[[firstName substringToIndex:1] uppercaseString]];
	}
	
	if (lastName != nil && lastName.length > 0) {
		initials = [initials stringByAppendingString: [[lastName substringToIndex:1] uppercaseString]];
	}
	
	//if empty?
	UIImage *letters = [self imageFromText:initials];
	
	[background drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
	[letters drawInRect:CGRectMake((imageSize.width - letters.size.width) / 2 + 1,
								(imageSize.height - letters.size.height) / 2 + 1,
								letters.size.width, letters.size.height)];
	
	UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	[self setImage:combined];

}

@end
