//
//  ResetPasswordError.m
//  RadyUs
//
//  Created by Frane Poljak on 03/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ResetPasswordError.h"

@interface ResetPasswordError ()

@end

@implementation ResetPasswordError

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backToMainPage:(id)sender {
	
	if (self.navigationController != nil) {
		[self.navigationController popToRootViewControllerAnimated:NO];
	} else {
		[self dismiss:nil];
	}
	
}

-(IBAction)dismiss:(id)sender {
	
	[self dismissViewControllerAnimated:YES completion:^{
        if ([sender tag] == 101) {
            [self backToMainPage:sender];
        }
    }];
	
}

-(IBAction)goBack:(id)sender {
	
	if (self.navigationController != nil) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		[self dismiss:nil];
	}
	
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
