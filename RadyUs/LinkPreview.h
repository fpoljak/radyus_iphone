//
//  LinkPreview.h
//  Radyus
//
//  Created by Frane Poljak on 04/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "objc/runtime.h"

@interface LinkPreview : UIWebView <NSURLConnectionDataDelegate, NSURLConnectionDelegate>

@property (nonatomic) CGFloat maxWidth;
@property (nonatomic) CGFloat maxHeight;
@property (nonatomic) BOOL hasImage;
@property (nonatomic) BOOL hasMedia;
@property (nonatomic, retain) NSString *customCSS;
@property (nonatomic) BOOL shouldShowLoadingIndicator;

@property (nonatomic, retain) NSString *previewUrl;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSDictionary *siteData;
@property (nonatomic, retain) NSDictionary *appLink;
@property (nonatomic, retain) NSURLConnection *connection;

@property (nonatomic, retain) UIActivityIndicatorView *loadingIndicator;

@property (nonatomic) NSInteger loadAttemptCount;
@property (nonatomic) NSInteger referenceCounter;
@property (nonatomic) BOOL noContent;

-(instancetype)init;
-(instancetype)initWithCoder:(NSCoder *)aDecoder;
-(instancetype)initWithFrame:(CGRect)frame;// NS_DESIGNATED_INITIALIZER;

-(void)loadPreview;
-(void)displayUnsafeURLWarning;
-(void)adjustSize;
-(void)reloadPreview;
-(void)clearPreview;

@end
