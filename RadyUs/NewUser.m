//
//  NewUser.m
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "NewUser.h"
#import "UIImage+OrientationUpImage.h"
#import "UIImage+Trim.h"

@interface NewUser ()

@end

@implementation NewUser

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
	/*_containerView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
	[_containerView setAutoresizesSubviews:self.view.autoresizesSubviews];
	[_containerView setAutoresizingMask:self.view.autoresizingMask];
	[_containerView setBackgroundColor:self.view.backgroundColor];
	[_containerView setContentSize:CGSizeMake(self.view.frame.size.width, 800)];
	[self.view setBackgroundColor:[UIColor darkTextColor]];
	
	for (UIView *subview in self.view.subviews) {
		[subview removeFromSuperview];
		[_containerView addSubview:subview];
	}
	
	[self.view addSubview:_containerView];*/
	
	[_containerView setContentSize:CGSizeMake(self.view.frame.size.width, 700.0f)];
	
	_endEditingButton = [[UIButton alloc] init];
	[_endEditingButton setBackgroundColor:[UIColor greenColor]];
	[_endEditingButton setTitle:@"Done" forState:UIControlStateNormal];
	[_endEditingButton addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
	
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard:)];
    
    [_containerView addGestureRecognizer:tap];

    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addPhotoDialog:)];
    
    [_userImageView addGestureRecognizer:imageTap];
	
	_datePicker = [[UIDatePicker alloc] init];
	[_datePicker setDatePickerMode:UIDatePickerModeDate];
	[_datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
	[_birthdayTF setInputView:_datePicker];
    
}

-(void)datePickerValueChanged:(UIDatePicker *)sender {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MM/dd/yyyy"];
	_birthdayTF.text = [dateFormatter stringFromDate:[sender date]];
}

-(IBAction)addPhotoDialog:(id)sender {
    [self performSegueWithIdentifier:@"AddPhotoDialog" sender:sender];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	UIImage *_selectedImage = [info valueForKey:UIImagePickerControllerEditedImage] ?: [info valueForKey:UIImagePickerControllerOriginalImage];
	
	/*
	 CGFloat w = _selectedImage.size.width;
	 CGFloat h = _selectedImage.size.height;
	 CGFloat max_width = 828;
	 CGFloat max_height = 1432;
	 CGFloat totalAspect = 1;
	 
	 NSLog(@"width: %f, height: %f, max_width: %f, max_height: %f", w, h, max_width, max_height);
	 
	 if (w > max_width) {
		CGFloat aspect = w / max_width;
		w = max_width;
		h /= aspect;
		totalAspect *= aspect;
	 }
	 
	 if (h > max_height) {
		CGFloat aspect = h / max_height;
		h = max_height;
		w /= aspect;
		totalAspect *= aspect;
	 }
	 
	 CGFloat scale = (totalAspect == 0 || totalAspect < 1 ? 1 : totalAspect);
	 NSLog(@"scale: %f", scale);
	 */
	
	//UIImage *selectedImage = [self orientationUpImage:_selectedImage];
	
	//NSLog(@"New dimensions: %fx%f", selectedImage.size.width, selectedImage.size.height);
	
    
    [_userImageView setContentMode:UIViewContentModeScaleAspectFill];

	//_userImage = [UIImage imageWithCGImage:[_selectedImage CGImage] scale: scale orientation: UIImageOrientationUp];
	
	_userImage = [[[_selectedImage orientationUpImage] imageByTrimmingTransparentPixels] imageByTrimmingBlackPixels];
	
	[_userImageView setImage:_userImage];
    [picker dismissViewControllerAnimated:YES completion:nil];

}

-(IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
	//[_containerView setContentOffset:CGPointZero];
}

-(void)viewWillAppear:(BOOL)animated {
  
    [super viewWillAppear:animated];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	NSLog(@"textFieldShouldBeginEditing, textfield originY = %f", textField.superview.frame.origin.y);
	if (textField.superview.frame.origin.y > 200) {
		[_containerView setContentOffset:CGPointMake(0, textField.superview.frame.origin.y - 120) animated:YES];
	}
	return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {

    if(textField.returnKeyType==UIReturnKeyNext) {
        // find the text field with next tag
        UIView *next = [_containerView viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone || textField.returnKeyType==UIReturnKeyDefault) {
        [textField resignFirstResponder];
		//[_containerView setContentOffset:CGPointZero animated:YES];
    }
    return YES;
}

-(void)viewDidAppear:(BOOL)animated {
	
    [super viewDidAppear:animated];
    
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
	
	if ([_birthdayTF  isFirstResponder]) {
		NSDictionary* info = [aNotification userInfo];
		CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
		if (kbSize.height == 0) {
			return;
		}
		double kbAnimationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
		UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
		NSInteger kbAnimationEffect = curve << 16;
		NSLog(@"Keyboard will show, size: %@, duration: %lf, animationEffect: %ld", NSStringFromCGSize(kbSize), kbAnimationDuration, (long)kbAnimationEffect);
		
		[_endEditingButton setFrame:CGRectMake(0, self.view.frame.size.height - 45, self.view.frame.size.width, 45)];
		[self.view addSubview:_endEditingButton];
		[self.view bringSubviewToFront:_endEditingButton];
		[UIView animateWithDuration:kbAnimationDuration + 0.025 delay:0.025f options:kbAnimationEffect animations:^{
			[_endEditingButton setFrame:CGRectMake(0, self.view.frame.size.height - kbSize.height - 45, self.view.frame.size.width, 45)];
		} completion:^(BOOL finished) {
			//
		}];
	}
	
}

-(void)doneEditing:(id)sender {
	
	[self hideKeyboard:sender];
	[_endEditingButton removeFromSuperview];
	[_containerView setContentOffset:CGPointMake(0, _containerView.contentSize.height - _containerView.frame.size.height) animated:YES];
}


-(BOOL)validateForm {

	//// checkfield, highlight field?
	
	if (_firstNameTF.text.length == 0) {
		//message
		return NO;
	}
	
	if (_lastNameTF.text.length == 0) {
		//message
		return NO;
	}
	
	if (_userNameTF.text.length == 0) {
		//message
		return NO;
	}
	
	if (_passwordTF.text.length == 0) {
		//message
		return NO;
	}
	
	if (![_confirmPasswordTF.text isEqualToString:_passwordTF.text]) {
		//message
		return NO;
	}
	
	if (_emailTF.text.length == 0) {//validate email
									//http://stackoverflow.com/a/1149894/1630623
									//http://stackoverflow.com/a/3638271/1630623
		//message?
		return NO;
	}
	
	if (_birthdayTF.text.length == 0) {
		//message
		return NO;
	}
	
	return YES;
}

-(IBAction)doSignUp:(id)sender {

	if (![self validateForm]) {
		return;
	}
	
	NSString *first = [_firstNameTF text];
	NSString *last = [_lastNameTF text];
	NSString *user = [_userNameTF text];
	NSString *mail = [_emailTF text];
	NSString *password = [_passwordTF text];
	NSString *birthday = [_birthdayTF text];
	
	NSDateFormatter *bdf = [[NSDateFormatter alloc] init];
	[bdf setDateFormat:@"MM/dd/YYYY"];
	NSDate *date = [bdf dateFromString:birthday];
	[bdf setDateFormat:@"YYYY-MM-dd"];
	NSString *birthdayFormatted = [bdf stringFromDate:date];
	
	int gender = [_mfSegmentedCtrl selectedSegmentIndex] == 0 ? 2 : 1;
	
	[_apiService doSignUpWithFirstName:first lastName:last userName:user email:mail password:password birthday:birthdayFormatted gender:gender];
	
}

-(void)getAccessToken{
	
	NSString *user = [_userNameTF text];
	NSString *password = [_passwordTF text];
	
	[_apiService getAccesTokenForUser:user password:password];

}

-(void)getUserInfo:(NSString *)accessToken {
	
	[_apiService getUserInfo:accessToken];
	
}


/* Api Service Delegate methods */

-(void)signupSuccess:(NSJSONSerialization *)responseData {
	
		[self getAccessToken];
}

-(void)receivedAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken socketToken:(NSString *)socketToken{
	
	[self getUserInfo:accessToken];
}

-(void)receivedUserInfo:(NSJSONSerialization *)userInfo {
	
	if (_userImage != nil) { ///picture ide odma
		[_apiService postNewImage:_userImage];
	} else {
		UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Explore"];
		[(ExploreMap *)[[vc childViewControllers] firstObject] setAuthorized:YES];
		[self.navigationController performSelector:@selector(setViewControllers:) withObject:[NSArray arrayWithObject: vc] afterDelay:0.3];
	}
	
}

-(void)newPictureSuccess:(NSString *)pictureId {
	
	_userImage = nil;
	[_apiService postImagesIdArray:[NSArray arrayWithObject:pictureId]];

}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	//ActionSuccessIndicator *indicator = [ActionSuccessIndicator new];
	//[indicator showWithSuccess:YES inViewController:self duration:1.4f message:@"Error creating account" completion:nil];
}

-(BOOL)shouldDisableUserInteraction {
	return YES; ////NO, disejblat botun?
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([[segue identifier] isEqualToString:@"SignupSuccess"]) {//izbacit
		Profile *dvc = [segue destinationViewController]; //explore, authorized = YES;
		[dvc setUserInfo:sender];
	} else {
		TransparentDialog *dvc = [segue destinationViewController];
		[dvc setParentVC:self];
		[dvc setHideConfirmButton:YES];
	}
	
}

-(void)viewWillDisappear:(BOOL)animated {
	if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
		[_apiService cancelRequests];
	}
	[super viewWillDisappear:animated];
}

@end
