//
//  ExploreTabBarController.h
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AddCloud.h"
#import "ExploreMap.h"
#import "CloudView.h"
#import "ExploreList.h"
#import "ExploreOptionsDialog.h"
#import "BarButtonWithBadge.h"

@interface ExploreTabBarController : UITabBarController <ExploreSettingsDelegate, AddCloudDelegate>

@property (nonatomic, retain) NSString *previousTitle;
@property (nonatomic, retain) NSJSONSerialization *selectedCloud;
@property (nonatomic, retain) NSJSONSerialization *userInfo;
@property (nonatomic) CLLocationCoordinate2D userLocation;

@end
