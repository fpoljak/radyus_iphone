//
//  InfoViewController.h
//  Radyus
//
//  Created by Locastic MacbookPro on 29/07/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController

@property (nonatomic, retain) IBOutlet UITextView *contentTV;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@end
