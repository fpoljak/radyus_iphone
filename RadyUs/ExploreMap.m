//
//  ExploreMap.m
//  RadyUs
//
//  Created by Frane Poljak on 11/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ExploreMap.h"
#import "UIImage+OrientationUpImage.h"
#import "UIImage+Trim.h"
#import "UIImageView+NoAvatar.h"
#import "CloudView.h"
#import "PopoverView.h"

@interface ExploreMap ()

@end

@implementation ExploreMap

@synthesize boundingMapRect;
@synthesize coordinate;
@synthesize loaded;
@synthesize radius;
@synthesize access;

static char clusterKey;
static char syncKey;
static char viewCloudKey;

enum CloudViewStates _cloudViewState;

BOOL shouldCenterMap;
BOOL hasOlderMessages;
//BOOL checkedForNewMessages;

NSInteger currentZoom = -1;

CGFloat startingVelocity;

PopoverView *popoverView;

////////////
//For tutorial: https://github.com/JoeFryer/JDFTooltips
////////////

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	NSString *dbName = [NSString stringWithFormat:@"%@.sqlt", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	_dbManager = [[DBManager alloc] initWithDatabaseFilename: dbName];
	
	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
	_joinButton.enabled = NO;

	_messageUserLoading = NO;
	
	_kbAnimationDuration = 0;
	
    loaded = NO;
	_firstLocationUpdate = YES;
	shouldCenterMap = YES;
	
	radius = [[[NSUserDefaults standardUserDefaults] objectForKey:@"RadiusForReadingClouds"] doubleValue] ?: 1000.0f;
	access = [[NSUserDefaults standardUserDefaults] objectForKey:@"AccessForReadingClouds"] ?: @"All";
	
	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];
	
	[_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateNormal];
    [_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateHighlighted];
	
	[_optionsButton addTarget:self action:@selector(highlightButton:) forControlEvents:UIControlEventTouchDown];
	[_optionsButton addTarget:self action:@selector(unhighlightButton:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    
    //_mapView.delegate = self;
    _mapView.showsUserLocation = YES;
	
	if (!loaded) {
		
		//[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
		[self viewDidAppear:NO];
		[_menuButton setEnabled:NO]; //join view
		//invisible?
		
		UIImageView *ddarr = [[UIImageView alloc] initWithFrame:CGRectMake(_optionsButton.frame.size.width - 40, _optionsButton.frame.size.height / 2 - 10, 24, 21)];
		[ddarr setContentMode:UIViewContentModeCenter];
		[ddarr setImage:[UIImage imageNamed:@"dropdownarrow.png"]];
		[_optionsButton addSubview:ddarr];

		[self createRefreshButton];
		
	}
	
	for (UIView *subview in _mapView.subviews)
		for (UIGestureRecognizer *gr in subview.gestureRecognizers)
			[gr requireGestureRecognizerToFail:self.menuSlideGesture];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(mapTapped:)];
    [_mapView addGestureRecognizer: tapGesture];
	
}

-(void) highlightButton:(UIButton *)sender {
	[sender setBackgroundColor:UIColorFromRGB(0xCFCFCF)];
}

-(void) unhighlightButton:(UIButton *)sender {
	[sender setBackgroundColor:UIColorFromRGB(0xE7E7E7)];
}

-(void)createRefreshButton {
	
	if (_refreshButton != nil) {
		return;
	}
	
	_refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 50, self.view.frame.size.height - 50, 43, 43)];
	
	[_refreshButton.layer setZPosition:100.0f];
	[_refreshButton setAlpha:0.9f];
	[_refreshButton setBackgroundColor:[UIColor clearColor]];
	[_refreshButton setBackgroundImage:[UIImage imageNamed:@"resetmap.png"] forState:UIControlStateNormal];
	[_refreshButton addTarget:self action:@selector(refreshButtonClicked) forControlEvents:UIControlEventTouchUpInside];
	
	[self.containerView addSubview:_refreshButton];
	
}


-(void)refreshButtonClicked {
	shouldCenterMap = YES;
	[self reloadClouds];
}


-(void)sidemenuWillShow {
	[super sidemenuWillShow];
	//[self chatViewToFullScreen]; //try UIResponder+FirstResponder.h hack in the sidemenu
    if (popoverView) {
        [popoverView dismiss:nil];
    }
}


-(IBAction)tabChanged:(UIButton *)sender {
	[self.tabBarController setSelectedIndex:1];
}

-(void)viewDidAppear:(BOOL)animated {
	
	NSLog(@"b viewDidAppear");

    /*
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnFromBackground) name:UIApplicationWillEnterForegroundNotification object:nil]; ////????
	*/

	[super viewDidAppear:animated];
	
    if (loaded) {
        return;
    }
	
	if (!_firstLocationUpdate) {
		//_firstLocationUpdate = YES;
		[_locationManager stopUpdatingLocation];
	} else {
		
		[_apiService showLoadingViewWithTitle:@"Getting your location"];

		if ([[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] != nil) {
			ApiService *getFriendsService = [[ApiService alloc] init];
			getFriendsService.delegate = self;
			
			[getFriendsService getFriends:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
		}
		
	}
	
	_pinHashMap = [[NSMutableDictionary alloc] init];
		
		_locationManager = [[CLLocationManager alloc] init];
		_locationManager.delegate = self;
		_locationManager.distanceFilter = 20.0f;
		_locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;//prominit?
    
		if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
			[_locationManager requestAlwaysAuthorization];
		}
		
		[_locationManager startUpdatingLocation];
	
	loaded = YES;
	
}

-(void)prepareForBackground {
	
	[super prepareForBackground];
	
	NSLog(@"Explore map preparing for background...");
	[_locationManager stopUpdatingLocation];
	[CLLocationManager cancelPreviousPerformRequestsWithTarget:self]; //jedno od ova dva, pretpostavljam prvo
	[NSObject cancelPreviousPerformRequestsWithTarget:_locationManager]; //self?
	
	if (_firstLocationUpdate) {
		[_apiService hideLoadingView];
	}
	
	[_locationManager startMonitoringSignificantLocationChanges];
	
}

-(void)returnFromBackground {
	
	NSLog(@"Explore map returning from background...");
	[_locationManager stopMonitoringSignificantLocationChanges];
	
	//TODO: vidit koliko je prošlo od zadnjeg update-a pa neki delay?
	if (_firstLocationUpdate) {
		[_apiService showLoadingViewWithTitle:@"Getting your location"];
		[_locationManager startUpdatingLocation];
	} else {
		[_locationManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay:20.0f];
	}
	
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
	
	if (status == kCLAuthorizationStatusDenied) {
		[self handleDenialOfLocationServiceAccess];
	}
	
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	
	if (error.code == kCLErrorDenied) {
		[self handleDenialOfLocationServiceAccess];
	}
	
}

-(void)handleDenialOfLocationServiceAccess {
	// TODO: handle denied location permission!
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	
	static int tryCount = 0;
	
	CLLocation *location = [locations lastObject]; //parse all, look for best accuracy?
	
	if (_firstLocationUpdate) {
		
		if (location.horizontalAccuracy > 60 && ++tryCount < 4) {
			NSLog(@"Location accuracy: %lf, try count: %d", location.horizontalAccuracy, tryCount);
			return;
		}
		
		tryCount = 0;
		
		[_apiService hideLoadingView];
	
	} else {
		
		if (location.horizontalAccuracy > 100) {
			// TODO: find optimal value
			return;
		}
		
		if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive && location.speed > 1.0) {
			
			//TODO: find optimal value
			NSLog(@"Speed: %f", location.speed);
			[_locationManager setDistanceFilter:10 + location.speed * 10];
			[_locationManager stopUpdatingLocation];
			[_locationManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay: 25 + location.speed / 2];
			// TODO: odredit neki maksimum (i minimum)?
			// TODO: desired accuracy?
			
		}

	}
	
	[_mapView setCenterCoordinate:location.coordinate animated:YES];
	
	_userLocation = location.coordinate;

	[[NSNotificationCenter defaultCenter] postNotificationName:@"UserLocationChanged" object:self userInfo:[NSDictionary dictionaryWithObject:location forKey:@"newLocation"]];
	
	if (_firstLocationUpdate) {
		[self initializeMap:location.coordinate];
		_firstLocationUpdate = NO;
	} else {
		[_apiService postUserLatitude:_userLocation.latitude longitude:_userLocation.longitude];
	}
	
}


-(void)initializeMap:(CLLocationCoordinate2D)location {
	
    [_mapView setCenterCoordinate:location animated:YES];
    
	MKCircle *radiusCircle = [MKCircle circleWithCenterCoordinate:location radius:radius];
    [_mapView addOverlay:radiusCircle];
	
	[self setMapBoundsToRadiusWithCenterIn:location];
	
    if (_cloudList == nil) {

		if (!_authorized) { // not needed anymore?
			[_apiService getUserInfo:[[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"]];
		} else {
			[_apiService postUserLatitude:_userLocation.latitude longitude:_userLocation.longitude];
		}
		
	} else {
		
		ExploreList *exploreList = (ExploreList *)[[self.tabBarController viewControllers] objectAtIndex: 1];
		
		NSPredicate *inRangeFilter = [NSPredicate predicateWithFormat:@"inRange = true"];
		
		NSArray *exploreCloudList = [NSArray arrayWithArray:[(NSArray *)[_cloudList valueForKey:@"data"] filteredArrayUsingPredicate:inRangeFilter]];
		
		[exploreList setCloudList:exploreCloudList];
		[exploreList reloadTable];
		[self displayCloudList:_cloudList];
		[exploreList setCloudCountLabel]; //viewdidappear?
		
	}
		
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {
	
	NSLog(@"Task failed, code: %d", errorCode);
	//if invalid refresh token go to login page
	//display message to user (your session has exipred....)
	//pribacit u apiservice
	if (errorCode == 401 /*???*/ || errorCode == 500) {
		
		if ([FBSDKAccessToken currentAccessToken] != nil) {
			FBSDKLoginManager *fbLoginManager = [[FBSDKLoginManager alloc] init];
			[fbLoginManager logOut];
		}

		[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"accessToken"];
		
		UIViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstPage"];
		
		for (UIViewController *vc in self.navigationController.viewControllers) { //remove all observers?
			
			if ([vc isKindOfClass:[UITabBarController class]]) {
				
				for (UIViewController *_vc in vc.childViewControllers) {
				
					[[NSNotificationCenter defaultCenter] removeObserver:_vc];
					
					if ([_vc isKindOfClass:[ExploreMap class]]) {
						[CLLocationManager cancelPreviousPerformRequestsWithTarget:_vc]; //nepotrebno?
					}
					
					[NSObject cancelPreviousPerformRequestsWithTarget:_vc];
					//[_vc viewWillDisappear:NO];
					
				}
				
			}
			
			[[NSNotificationCenter defaultCenter] removeObserver:vc];
			[NSObject cancelPreviousPerformRequestsWithTarget:vc];
			[vc viewWillDisappear:NO];
			
		}

		[self.navigationController performSelector:@selector(setViewControllers:) withObject:[NSArray arrayWithObject: loginView] afterDelay:0.3];
	}
	
	//invalid access token
	// TODO: in login view controler (ili api service, u tom slučaju pamtit zadnji request pa ga ponovit ako prođe!)
	/*
	if (errorCode == 401) {
		_authorized = NO;
		[_apiService refreshAccesToken];
	}
	*/
	
}

-(BOOL)shouldDisableUserInteraction {
	// TODO: disable options button while loading instead?
	return NO;
}

-(void)receivedAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken socketToken:(NSString *)socketToken {
	
	// not needed anymore?
	[_mapView setAlpha:1.0];
	_authorized = YES;
	[_apiService getUserInfo:accessToken];
	
}

-(void)receivedUserInfo:(NSJSONSerialization *)userInfo {
	
	ApiService *getFriendsService = [[ApiService alloc] init];
	getFriendsService.delegate = self;
	
	[getFriendsService getFriends:[userInfo valueForKey:@"id"]];
	
	[_mapView setAlpha:1.0];

	if (CLLocationCoordinate2DIsValid(_userLocation)) {
		[_apiService postUserLatitude:_userLocation.latitude longitude:_userLocation.longitude];
	}
	
}

-(void)userLocationPostSuccess { //access
	[_apiService getCloudsByLatitude:_userLocation.latitude longitude:_userLocation.longitude inRadius:radius];
}

-(void)apiService:(ApiService *)service receivedCloudList:(NSJSONSerialization *)cloudList {
	
	if ([service isEqual:_syncService]) {
		// already saved in ApiService
		return;
	}
	
	_cloudList = cloudList;
	ExploreList *exploreList = (ExploreList *)[[self.tabBarController viewControllers] objectAtIndex: 1];
		
	NSPredicate *inRangeFilter = [NSPredicate predicateWithFormat:@"inRange = true"];
	
	NSArray *exploreCloudList = [NSArray arrayWithArray:[(NSArray *)[_cloudList valueForKey:@"data"] filteredArrayUsingPredicate:inRangeFilter]];
	
	//NSLog(@"Clouds in range: %ld", (long)[(NSArray *)exploreCloudList count]);
	
	[exploreList setCloudList:exploreCloudList];
	[exploreList reloadTable];
	[exploreList setCloudCountLabel];
	[self clearMap];
	
	MKCircle *radiusCircle = [MKCircle circleWithCenterCoordinate:_userLocation radius:radius];
	[_mapView addOverlay:radiusCircle];
	
	if (shouldCenterMap) {
		[self setMapBoundsToRadiusWithCenterIn:_userLocation];
		shouldCenterMap = NO;
	}
	
	[self displayCloudList:cloudList];
	
}

-(void)loadMessagesSuccess:(NSJSONSerialization *)response {
	
	// first run sync
	// TODO: consider moving somewhere else!
		
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			
		[_dbManager saveMessages:(NSArray *)response];
			
		NSMutableArray *controlArray = (NSMutableArray *)objc_getAssociatedObject(_syncService, &syncKey);
		[controlArray removeLastObject];
			
		if (controlArray.count == 0) {
				
			controlArray = nil;
				
			[[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:[@"isSynchronized-" stringByAppendingString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]]];
			[[NSUserDefaults standardUserDefaults] synchronize];
			
			[[NSNotificationCenter defaultCenter] postNotificationName:@"RadyusFirstLoadSyncComplete" object:nil];
				
		}
			
	}];
	
}

-(void)displaySingleCloud:(NSDictionary *)cloudObj {
	
	double lat = [[(NSArray *)[cloudObj valueForKey:@"center"] objectAtIndex:1] doubleValue];
	double lng = [[(NSArray *)[cloudObj valueForKey:@"center"] objectAtIndex:0] doubleValue];
	MapPin *cloud = [[MapPin alloc] initWithCoordinates:CLLocationCoordinate2DMake(lat, lng) id:[cloudObj valueForKey:@"id"] placeName:[cloudObj valueForKey:@"name"] description:[cloudObj valueForKey:@"name"] isInRange:[[cloudObj valueForKey:@"inRange"] boolValue]];
	[_pinHashMap setValue:cloudObj forKey:[cloudObj valueForKey:@"id"]];
	[_mapView addAnnotation:cloud];
	
}

-(void)displayCloudList:(NSJSONSerialization *)cloudList { //in cloudList
	
	for (NSDictionary *cloudObj in [cloudList valueForKey:@"data"]) {
		[self displaySingleCloud:cloudObj];
	}
	
	self.clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:[_mapView annotations]]; //clusteringManager
	
}

-(void)apiService:(ApiService *)service receivedUserIdList:(NSArray *)userIdList {
		
		[[NSUserDefaults standardUserDefaults] setValue:[userIdList copy] forKey:@"userFriends"];
		[[NSUserDefaults standardUserDefaults] synchronize];
		
		////sinkronizacija ako je prvi login!
		NSString *userId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"];
		NSString *syncedKey = [@"isSynchronized-" stringByAppendingString:userId];
	
		if ([[NSUserDefaults standardUserDefaults] valueForKey:syncedKey] == nil) {
		
			_syncService = [[ApiService alloc] init];
			_syncService.delegate = self;
			NSMutableArray *friendsArr = [[NSMutableArray alloc] initWithArray:userIdList copyItems:YES];
			objc_setAssociatedObject(_syncService, &syncKey, friendsArr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
			[_syncService getCloudsByUserId:userId];
			
			for (NSString *friendId in userIdList) {
				ApiService *service = [[ApiService alloc] init];
				service.delegate = self;
				[service load1on1MessagesWithFriend:friendId since:nil until:nil limit:20];
			}
			
		}
	
}

-(NSInteger) zoomScaleToZoomLevel:(MKZoomScale) scale {
	
	double totalTilesAtMaxZoom = MKMapSizeWorld.width / 256.0;
	NSInteger zoomLevelAtMaxZoom = log2(totalTilesAtMaxZoom);
	NSInteger zoomLevel = MAX(0, zoomLevelAtMaxZoom + floor(log2f(scale) + 0.5));
	
	return zoomLevel;
	
}


- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
	
	[[NSOperationQueue new] addOperationWithBlock:^{
		
		double scale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
		NSInteger zoom = [self zoomScaleToZoomLevel:scale];
		
		if (currentZoom != zoom) {
		
			currentZoom = zoom;
			NSArray *annotations = [self.clusteringManager clusteredAnnotationsWithinMapRect:mapView.visibleMapRect withZoomScale:scale];
			[self.clusteringManager displayAnnotations:annotations onMapView:mapView];
			
		}
		
	}]; //clusteringManager
	
}


-(CLLocationDistance)distanceBetweenCoordinate:(CLLocationCoordinate2D)originCoordinate andCoordinate:(CLLocationCoordinate2D)destinationCoordinate {
	
    CLLocation *originLocation = [[CLLocation alloc] initWithLatitude:originCoordinate.latitude longitude:originCoordinate.longitude];
    CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:destinationCoordinate.latitude longitude:destinationCoordinate.longitude];
    CLLocationDistance distance = [originLocation distanceFromLocation:destinationLocation];
    
    return distance;
	
}


-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if ([overlay isKindOfClass:[MKCircle class]]) {
		
        MKCircleRenderer *radiusView = [[MKCircleRenderer alloc] initWithCircle:overlay];
		radiusView.fillColor = [UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:0.275f];
		
        return radiusView;
    
    } else return nil;

}

-(void)mapTapped:(id)sender {
    if (popoverView) {
        [popoverView dismiss:sender];
    }
}

-(void)pinTapped:(UITapGestureRecognizer *)sender {
		
	id<MKAnnotation> pin = (id<MKAnnotation>)sender.view;
	MKAnnotationView *cloudPin = (MKAnnotationView *)pin;
	
	NSJSONSerialization *cloud = [_pinHashMap valueForKey:[cloudPin reuseIdentifier]];
	
	if (cloud == nil) {
		return;
	}
		
	//[self viewCloud:cloud];
	
	//test

    if (popoverView) {
        [popoverView removeFromSuperview];
        popoverView = nil;
    }

	popoverView = [[PopoverView alloc] initWithFrame:CGRectMake(10, _mapView.frame.origin.y + 10, self.view.frame.size.width - 20, 120)];
    popoverView.layer.zPosition = 10000;

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, popoverView.frame.size.width - 110, 60)];
    [titleLabel setText:[cloud valueForKey:@"name"]];
    [popoverView addSubview:titleLabel];

    UIButton *viewCloudButton = [[UIButton alloc] initWithFrame:CGRectMake(popoverView.frame.size.width - 100, 40, 90, 50)];
    [viewCloudButton setTitle:@"View cloud" forState:UIControlStateNormal];

    objc_setAssociatedObject(viewCloudButton, &viewCloudKey, cloud, OBJC_ASSOCIATION_RETAIN);

    [viewCloudButton addTarget:self action:@selector(viewButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    [popoverView addSubview:viewCloudButton];

	[self.view addSubview:popoverView];

}

-(void)viewButtonTapped:(UIButton *)sender {

    NSJSONSerialization *cloud = objc_getAssociatedObject(sender, &viewCloudKey);

    if (cloud) {
        [self viewCloud:cloud];
    }
}

-(void)viewCloud:(NSJSONSerialization *)cloud {
	
	[(ExploreTabBarController *)self.tabBarController setSelectedCloud:cloud];
	[(ExploreTabBarController *)self.tabBarController setUserLocation:_userLocation];
	
	if (![[cloud valueForKey:@"inRange"] boolValue]) {
		[self.tabBarController performSegueWithIdentifier:@"ViewCloudUnavailable" sender:nil];
	} else {
		[self.tabBarController performSegueWithIdentifier:@"ViewRadyus" sender:nil];
	}

}

-(void)clusterTapped:(UITapGestureRecognizer *)sender {
	
	id<MKAnnotation> pin = (id<MKAnnotation>)sender.view;
	
	NSArray *annotations = objc_getAssociatedObject(pin, &clusterKey);
	
	//ako je zoom veći od neke razine, prikazat listu
	
	CLLocationDegrees oldDelta = _mapView.region.span.latitudeDelta;
	
	[self zoomMapViewToFitAnnotations:annotations animated:NO];

	CLLocationDegrees newDelta = _mapView.region.span.latitudeDelta;
	
	if (oldDelta <= newDelta) {
		
		NSMutableArray *cloudsToDisplay = [NSMutableArray new];
		for (id <MKAnnotation> annotation in annotations) {
			if (![annotation isKindOfClass:[MapPin class]]) {
				continue;
			}
			NSJSONSerialization *cloud = [_pinHashMap valueForKey:[(MapPin *)annotation unique_id]];
			NSLog(@"Cloud in cluster: %@", (NSDictionary *)cloud);
			[cloudsToDisplay addObject:cloud];
		}
		
		[self performSegueWithIdentifier:@"ViewClusteredClouds" sender:cloudsToDisplay];
		
	}
	
}

#define MINIMUM_ZOOM_ARC 0.0003 ////
#define ANNOTATION_REGION_PAD_FACTOR 1.3
#define MAX_DEGREES_ARC 360
- (void)zoomMapViewToFitAnnotations:(NSArray *)annotations animated:(BOOL)animated
{

	CLLocationDegrees oldDelta = _mapView.region.span.latitudeDelta;
	
	int count = (int)[annotations count];
	if ( count == 0) { return; }
	
	MKMapPoint points[count];
	for( int i=0; i<count; i++ )
	{
		CLLocationCoordinate2D _coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
		points[i] = MKMapPointForCoordinate(_coordinate);
	}
	
	MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
	MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
	
	region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
	region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
	
	if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
	if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
	
	//don't zoom in stupid-close on small samples
	if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
	if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
	
	//and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
	//if( count == 1 )
	//{
	//	region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
	//	region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
	//}
	
	CLLocationDegrees newDelta = region.span.latitudeDelta;
	
	if (newDelta >= oldDelta) { //sprječavanje zoom-out-a
		return;
	}
	
	[_mapView setRegion:region animated:animated];

}

-(void) setMapBoundsToRadiusWithCenterIn:(CLLocationCoordinate2D)location {
	
	CGFloat factor = 4;
	CGFloat fractionLatLon = _mapView.region.span.latitudeDelta / _mapView.region.span.longitudeDelta;
	
	CGFloat latDistance = (fractionLatLon > 1) ? radius * factor * fractionLatLon : radius * factor;
	CGFloat lonDistance = (fractionLatLon > 1) ? radius * factor : radius * factor * fractionLatLon;
	
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, latDistance, lonDistance);
	
	[_mapView setRegion:region animated:YES];
	
}

-(UIImage *)imageFromText:(NSString *)text
{

	UIFont *font = [UIFont fontWithName:@"GothamRounded-Bold" size:20.0f]; //size as parameter?
	
	NSDictionary *attr = @{NSFontAttributeName:font, NSForegroundColorAttributeName: UIColorFromRGB(0x4A4A4A)};
	CGSize size  = [text sizeWithAttributes:attr];
	
	UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
	
	[text drawAtPoint:CGPointMake(0.0, 0.0) withAttributes:attr];
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
	
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {

	
	if ([annotation isKindOfClass:[FBAnnotationCluster class]]) { //clusteringManager
		
		NSString *AnnotatioViewReuseID = @"FBAnnotatioViewReuseID";

		FBAnnotationCluster *cluster = (FBAnnotationCluster *)annotation;
		
		/*
		BOOL containsLocationMarker = NO;
		for (id<MKAnnotation> ca in cluster.annotations) {
			if ([ca isKindOfClass:[MKUserLocation class]]) {
				containsLocationMarker = YES;
				break;
			}
		}
		*/
		///provjerit ako je jedan + location, drugi selector u tapgesture, drugačija ikona!!!
		
		cluster.title = [NSString stringWithFormat:@"%lu", (unsigned long)cluster.annotations.count/* - (containsLocationMarker ? 1 : 0)*/];
		
		//MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotatioViewReuseID];
		
		UITapGestureRecognizer *clusterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clusterTapped:)];
		
		//if (!annotationView) {
			MKAnnotationView	*annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotatioViewReuseID];
			[annotationView addGestureRecognizer:clusterTap];
		//}

		objc_setAssociatedObject(annotationView, &clusterKey, cluster.annotations, OBJC_ASSOCIATION_ASSIGN);
		
		//annotationView.canShowCallout = NO; ////
		//[annotationView setAlpha:1.0f]; /////

		UIImage *cloud = [UIImage imageNamed:@"map_cloud.png"];
		
		CGSize cloudSize = cloud.size;
		
		UIGraphicsBeginImageContext(cloudSize);

		
		UIImage *icon = [self imageFromText:cluster.title];
		
		
		[cloud drawInRect:CGRectMake(0, 0, cloudSize.width, cloudSize.height)];
		[icon drawInRect:CGRectMake((cloudSize.width - icon.size.width) / 2 + 1,
									(cloudSize.height - icon.size.height) / 2 + 3,
									icon.size.width, icon.size.height)];
		
		UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
		
		UIGraphicsEndImageContext();
		
		[annotationView setImage: combined];
		annotationView.layer.anchorPoint = CGPointMake(0.5f, 0.5f);
		
		return annotationView;
		
	}
	
	
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        MKAnnotationView *ann = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"RadyUsUserLocationAnnotation"];
        
        if (ann) {
            return ann;
        }
        
        ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"RadyUsUserLocationAnnotation"];
        ann.enabled = YES;
        ann.draggable = NO;
        ann.canShowCallout = NO;
        
        CGSize pinSize = CGSizeMake(33, 43);
        
        UIGraphicsBeginImageContext(pinSize);
        
        UIImage *cloud = [UIImage imageNamed:@"user_location_pin.png"];
        UIImage *icon = [UIImage imageNamed:@"radyus_icon.png"];
        
        [icon drawInRect:CGRectMake(3, 8, 26, 26)];
        [cloud drawInRect:CGRectMake(0, 0, 33, 43)];
        
        UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        
        [ann setImage: combined];
        //[ann setCenterOffset:CGPointMake(-17, 0)];
        ann.layer.anchorPoint = CGPointMake(0.5f, 1.0f);
        
        return ann;
    }
    
    if ([annotation isKindOfClass:[MapPin class]]) {
        
        //MKAnnotationView *ann = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:[(MapPin *)annotation unique_id]];
        
        UITapGestureRecognizer *pinTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pinTapped:)];
        
        //if (ann) {
            //[ann addGestureRecognizer:pinTap];
            //return ann;
        //}
        
        MKAnnotationView *ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[(MapPin *)annotation unique_id]];
        ann.enabled = YES;
        ann.draggable = NO;
        ann.canShowCallout = NO;
        [ann addGestureRecognizer:pinTap];
		
		
		BOOL isInRange = [(MapPin *)annotation isInReach];
		
		UIImage *cloud = (isInRange) ? [UIImage imageNamed:@"map_cloud_small.png"] : [UIImage imageNamed:@"map_cloud_grey.png"];

        CGSize cloudSize = cloud.size;
        
        UIGraphicsBeginImageContext(cloudSize);
		
		//BOOL isInRange = [self distanceBetweenCoordinate:[annotation coordinate] andCoordinate:_userLocation] < radius;
		
		UIImage *icon = [UIImage imageNamed:@"icon_grill.png"];
        [cloud drawInRect:CGRectMake(0, 0, cloudSize.width, cloudSize.height)];
		[icon drawInRect:CGRectMake((cloudSize.width - icon.size.width) / 2,
									(cloudSize.height - icon.size.height) / 2,
									icon.size.width, icon.size.height)];
		//livo za explore, dodat broj ljudi (ann addsubview)
		
        UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        [ann setImage: combined];
        ann.layer.anchorPoint = CGPointMake(0.5f, 0.5f);
		
        return  ann;

    }

    return nil;
	
}

-(void)commitSettings:(NSDictionary *)settings {

	BOOL needsReload = NO;
	
	if (radius != [(NSNumber *)[settings objectForKey:@"radius"] floatValue] || ![access isEqualToString: (NSString *)[settings objectForKey:@"access"]]) {
		needsReload = YES;
	}
	
	radius = [(NSNumber *)[settings objectForKey:@"radius"] floatValue];
	access = (NSString *)[settings objectForKey:@"access"];
	
	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];
	
	[_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateNormal];
	[_optionsButton setTitle:[NSString stringWithFormat:@" %@, %.0f %@", access, radius * (isFeet ? 3.28084 : 1), isFeet ? @"ft": @"m"] forState:UIControlStateHighlighted];
	
	if (needsReload) {
		[self reloadClouds];
	}
	
}

-(void)cloudCreated:(NSJSONSerialization *)cloud {
	
	//dodat samo novi na mapu?
	[self reloadClouds];
	
}

-(void)reloadClouds {

	[self clearMap];
	
	if (loaded) {
		loaded = NO;
		[self viewDidAppear:NO];
	}

}

-(void)clearMap {
	
	[_mapView removeOverlays:[_mapView overlays]];
	[_mapView removeAnnotations:[_mapView annotations]];
	
	_pinHashMap = [[NSMutableDictionary alloc] init]; //izbacit?

	_cloudList = nil;
	
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	
	if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]] && [otherGestureRecognizer.view isKindOfClass:[MKMapView class]]) {
		return NO;
	}
	
    return YES;
	
}

-(void)viewWillDisappear:(BOOL)animated {
	
	if ((!animated && [self.navigationController.viewControllers indexOfObject:self.tabBarController ?: self] + 1 < self.navigationController.viewControllers.count) || [self.navigationController.viewControllers indexOfObject:self.tabBarController ?: self]==NSNotFound) {
		
		NSLog(@"Canceling requests, deallocating map objects...");
		[_apiService cancelRequests];
		_apiService = nil;
		[_cloudCreatorService cancelRequests];
		[_mapView removeAnnotations:_mapView.annotations];
		self.clusteringManager = nil;
		//_mapView.delegate = nil;
		[_mapView removeFromSuperview]; ////nije testirano
		_mapView = nil;					////	-\\-
		
		[self.view.layer removeAllAnimations];
		[UIView cancelPreviousPerformRequestsWithTarget:self];
		[NSObject cancelPreviousPerformRequestsWithTarget:self];
		
	}
	
	//[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
	
	[super viewWillDisappear:animated];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
    if ([[segue identifier] isEqualToString:@"ChangeExploreOptionsMap"]) {
        ExploreOptionsDialog *dvc = [segue destinationViewController];
		ExploreTabBarController *parent = (ExploreTabBarController *)self.parentViewController;//tabbarcontroller?
		//access
        [dvc setDelegate:parent];
        [dvc setRadius:radius];
	} else if ([segue.identifier isEqualToString:@"ViewClusteredClouds"]) {
		ExploreList *dvc = [segue destinationViewController];
		[dvc setCloudList:sender];
		[dvc setUserLocation:_userLocation];
	}
	
}

-(void)dealloc {
    popoverView = nil;
}

@end
