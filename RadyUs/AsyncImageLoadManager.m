//
//  AsyncImageLoadManager.m
//  Radyus
//
//  Created by Frane Poljak on 05/05/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AsyncImageLoadManager.h"
#import <objc/runtime.h>

@implementation AsyncImageLoadManager

const static NSString *apiUrl = @"http://api.rady.us/";
const static NSString *apiVersion = @"v1/";
const static NSString *imageEndpoint = @"picture";

static char delegateStrongReferenceKey;

-(instancetype)initWithImageId:(NSString *)imageId imageView:(UIImageView *)imageView useActivityIndicator:(BOOL)useActivityIndicator {

	if (self = [super init]) {
		
		_imageId = imageId;
		_imageView = imageView; //can be nil (just save to database)
		_useActivityIndicator = useActivityIndicator && _imageView != nil;
		NSString *dbName = [NSString stringWithFormat:@"%@.sqlt", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
		_dbManager = [[DBManager alloc] initWithDatabaseFilename: dbName];
		_delegateStrongReferenceCounter = 0;
		
	}
	
	return self;
	
}

-(instancetype)initWithImageUrl:(NSString *)url imageView:(UIImageView *)imageView useActivityIndicator:(BOOL)useActivityIndicator {

	if (self = [super init]) {
		
		_imageUrl = url;
		_imageView = imageView; //can be nil (just save to database)
		_useActivityIndicator = useActivityIndicator && _imageView != nil;
		NSString *dbName = [NSString stringWithFormat:@"%@.sqlt", [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
		_dbManager = [[DBManager alloc] initWithDatabaseFilename: dbName];
		_delegateStrongReferenceCounter = 0;
		
	}
	
	return self;

}

-(void)loadImage {
	
	BOOL loadWithId = _imageId != nil && ![_imageId isEqualToString:@""];
	BOOL loadWithUrl = !loadWithId && _imageUrl != nil && ![_imageUrl isEqualToString:@""];
	
	if (!loadWithId && !loadWithUrl) { // id or url must be defined
		return;
	}
	
	_imageData = [[NSMutableData alloc] init];
	
	NSString *urlString = loadWithId ? [NSString stringWithFormat:@"%@%@%@", apiUrl, apiVersion, imageEndpoint] : _imageUrl;
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
	
	if (loadWithUrl) {
		
		[request setHTTPMethod:@"GET"];
		[request setValue:@"max-age=31536000" forHTTPHeaderField:@"Cache-Control"];
		[request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
		
		//Content-Type, ...?
		
	} else {
	
	NSString *authField = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"]];
		NSString *params = [NSString stringWithFormat:@"{\"id\": \"%@\"}", _imageId]; //NSDictionary!
	
		[request setHTTPMethod:@"POST"];
		[request setValue:authField forHTTPHeaderField:@"Authorization"];
		[request setValue:@"utf-8" forHTTPHeaderField:@"Accept-Charset"];
		[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
		[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
		[request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
		[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
	
	}
	
	[request setTimeoutInterval:30.0f];
	
	_imageConnection = [NSURLConnection connectionWithRequest:request delegate:self];
	
	if (_delegateStrongReferenceCounter <= 0) {
		objc_setAssociatedObject(self, &delegateStrongReferenceKey, _delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		_delegateStrongReferenceCounter = 1;
	} else {
		_delegateStrongReferenceCounter++;
	}
	
	_downloadTaskID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
		[_imageConnection cancel];
		[[UIApplication sharedApplication] endBackgroundTask:_downloadTaskID];
		_downloadTaskID = UIBackgroundTaskInvalid;
		
		_delegateStrongReferenceCounter--;
		if (_delegateStrongReferenceCounter == 0) {
			objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
		}

	}];
	
}

-(UIImage *)imageFromString:(NSString *)imageString {
	
	if (imageString == nil) {
		return nil;
	}
	
	NSData *imageData = [[NSData alloc] initWithBase64EncodedString:imageString options:NSDataBase64DecodingIgnoreUnknownCharacters];
	return [UIImage imageWithData:imageData];
	
}

-(BOOL)isViewControllerActive:(nullable id)viewController {
	
	if (viewController == nil) {
		return NO;
	}
	
	if (![viewController isKindOfClass:[UIViewController class]]) {
		return [viewController isKindOfClass:[UIView class]] && [self isViewControllerActive:[self parentViewControllerOfView:(UIView *)viewController]];
	}
	
	UIViewController *vc = (UIViewController *)viewController;
	
	if (vc.presentedViewController != nil) {
		return NO;
	}
	
	if (vc.navigationController != nil) {
		return [vc.navigationController.visibleViewController isEqual:viewController];
	}
	
	if (vc.tabBarController != nil) {
		return [vc.tabBarController.selectedViewController isEqual:viewController];
	}
	
	return vc.view.window != nil;
	
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	
	[[UIApplication sharedApplication] endBackgroundTask:_downloadTaskID];

	if (_actInd != nil) {
		if ([_actInd isAnimating]) {
			[_actInd stopAnimating];
		}
		[_actInd removeFromSuperview];
	}
	
	if ([self isViewControllerActive:_delegate]) {
		
		// TODO: don't show alert, set some failure image?
		
		if ([_delegate isKindOfClass:[UIViewController class]]) {
			UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
			
			UIAlertAction *close = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
			
			[alert addAction:close];
			
			[(UIViewController *)_delegate presentViewController:alert animated:YES completion:^{ // u close handler?
				if ([_delegate respondsToSelector:@selector(errorLoadingImage:)]) {
					[_delegate errorLoadingImage:_imageView];
				}
			}];

		} else {
		
			if ([_delegate respondsToSelector:@selector(errorLoadingImage:)]) {
				[_delegate errorLoadingImage:_imageView];
			}
			
		}
		
	}
	
	_delegateStrongReferenceCounter--;
	//NSLog(@"_delegateStrongReferenceCounter: %ld", (long)_delegateStrongReferenceCounter);
	if (_delegateStrongReferenceCounter <= 0) {
		objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
	}

}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[_imageData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {

	NSString *stringData;
	
	//if url convert data to base64encodedstring
	
	BOOL loadWithId = _imageId != nil && ![_imageId isEqualToString:@""];
	BOOL loadWithUrl = !loadWithId && _imageUrl != nil && ![_imageUrl isEqualToString:@""];
	
	BOOL failedToLoad = loadWithUrl ? _imageData == nil : NO;
	
	if (loadWithId) {
		
		NSError *error;
		NSJSONSerialization *response = [NSJSONSerialization JSONObjectWithData:_imageData options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves error:&error];
		
		stringData = [[response valueForKey:@"data"] valueForKey:@"picture"];
		
		failedToLoad = [response valueForKey:@"status"] == nil || ![[response valueForKey:@"status"] boolValue];
		
	} else {
		//stringData = NSString stringfrom
		// save url based images to datanase?
		// no need, cache will be used! (?)
	}
	
	if (stringData != nil) {
		NSString *queryFormat = @"INSERT OR IGNORE INTO 'images' ('id', 'image_content') VALUES ('%@', '%@')";
		[_dbManager executeQuery:[NSString stringWithFormat:queryFormat, _imageId/*loadWithId ? _imageId : _imageUrl*/, stringData]];
	}
	
	if (_actInd != nil) {
		
		if ([_actInd isAnimating]) {
			[_actInd stopAnimating];
		}
		
		[_actInd removeFromSuperview];
		
	}
	
	if (_delegate != nil) {
		
		if (failedToLoad) {
			
			if ([_delegate respondsToSelector:@selector(errorLoadingImage:)]) {
				[_delegate errorLoadingImage:_imageView];
			}
			
		} else {
			
			if (_imageView != nil) {
				UIImage *image = loadWithId ? [self imageFromString:stringData] : [UIImage imageWithData:_imageData];
				[self performSelectorOnMainThread:@selector(applyImage:) withObject:image waitUntilDone:YES];
			}
			
			if ([_delegate respondsToSelector:@selector(loadSuccess:)]) {
				[_delegate loadSuccess:_imageView];
			}
			
		}
		
	}
	
	_imageData = [[NSMutableData alloc] init];
	
	[[UIApplication sharedApplication] endBackgroundTask:_downloadTaskID];
	
	_delegateStrongReferenceCounter--;

	if (_delegateStrongReferenceCounter <= 0) {
		objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
	}

}

-(void)applyImage:(UIImage *)image {
	_imageView.image = image;
	[_imageView setNeedsDisplay];
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
	return _imageUrl != nil && ![_imageUrl isEqualToString:@""] ? cachedResponse : nil;
}

-(NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
	
	NSArray *imageObj;
	
	if (_imageId != nil && ![_imageId isEqualToString:@""]) { // load with id, check local db first
		NSString *queryFormat = @"SELECT * from images WHERE id = \"%@\"";
		imageObj = (NSArray *)[(NSArray *)[_dbManager loadDataFromDB:[NSString stringWithFormat:queryFormat, _imageId]] firstObject];
	}
	
	if (imageObj == nil || imageObj.count == 0) {
		
		if (_useActivityIndicator) {
			
			_actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
			[_imageView addSubview:_actInd];
			_actInd.center = CGPointMake(_imageView.frame.size.width / 2, _imageView.frame.size.height / 2);
			[_imageView bringSubviewToFront:_actInd];
			[_actInd.layer setZPosition:99.0f];
			[_actInd startAnimating];
			
		}

		return request;
		
	}
	
	// else loaded from local db:
	
	NSString *stringData = [imageObj objectAtIndex:[_dbManager.arrColumnNames indexOfObject:@"image_content"]];
	
	if (_imageView != nil) {
		_imageView.image = [self imageFromString:stringData];
	}
	
	if ([_delegate respondsToSelector:@selector(loadSuccess:)]) {
		[_delegate loadSuccess:_imageView];
	}
	
	_imageData = [[NSMutableData alloc] init];
	
	[connection cancel];
	[[UIApplication sharedApplication] endBackgroundTask:_downloadTaskID];
	_downloadTaskID = UIBackgroundTaskInvalid;
	
	_delegateStrongReferenceCounter--;

	if (_delegateStrongReferenceCounter <= 0) {
		objc_setAssociatedObject(self, &delegateStrongReferenceKey, nil, OBJC_ASSOCIATION_ASSIGN);
	}
	
	return nil;
	
}

- (UIViewController *)parentViewControllerOfView:(UIView *)view {
	
	UIResponder *responder = view;
	
	while (![responder isKindOfClass:[UIViewController class]] && responder != nil) responder = [responder nextResponder];
	
	return (UIViewController *)responder;
	
}

@end
