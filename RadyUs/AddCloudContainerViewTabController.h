//
//  AddCloudContainerViewTabController.h
//  RadyUs
//
//  Created by Frane Poljak on 16/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCloud.h"

@interface AddCloudContainerViewTabController : UITabBarController

@end
