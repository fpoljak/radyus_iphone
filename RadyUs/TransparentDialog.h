//
//  TransparentDialog.h
//  RadyUs
//
//  Created by Frane Poljak on 04/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewUser.h"

@protocol TransparentDialogDelegate;

@interface TransparentDialog : UIViewController

@property (nonatomic, retain) IBOutlet UIView *transparentBackground;
@property (nonatomic, retain) UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate> *parentVC;

@property (nonatomic, retain) IBOutlet UILabel *cancelLabel;
@property (nonatomic, retain) IBOutlet UIButton *confirmButton;
@property (nonatomic, retain) IBOutlet UILabel *questionLabel;

@property (nonatomic, retain) NSString *cancelTitle;
@property (nonatomic, retain) NSString *confirmTitle;
@property (nonatomic, retain) NSString *questionText;

@property (nonatomic) BOOL hideConfirmButton;

@property (nonatomic, assign) id <TransparentDialogDelegate> delegate;

@end

@protocol TransparentDialogDelegate <NSObject>

@optional

-(void) cancelSelected;
-(void) confirmSelected;
-(void) customDialogAction:(NSDictionary *)info;

@end
