//
//  CustomSlider.m
//  RadyUs
//
//  Created by Frane Poljak on 18/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "CustomSlider.h"

@implementation CustomSlider

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setThumbImage:[UIImage imageNamed:@"line-circle-active.png"] forState:UIControlStateNormal];
    [self setThumbImage:[UIImage imageNamed:@"line-btn-press.png"] forState:UIControlStateHighlighted];
    
    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"left-line.png"];
    sliderLeftTrackImage = [sliderLeftTrackImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 7, 0, 0) resizingMode:UIImageResizingModeStretch];
    UIImage *sliderRightTrackImage = [UIImage imageNamed: @"right-line.png"];
    sliderRightTrackImage = [sliderRightTrackImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 7) resizingMode:UIImageResizingModeStretch];
    [self setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
}

@end
