//
//  ResetPassword.m
//  RadyUs
//
//  Created by Frane Poljak on 03/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "ResetPassword.h"

@interface ResetPassword ()

@end

@implementation ResetPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard:)];
	
    [self.view addGestureRecognizer:tap];

}

-(IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismiss:(id)sender {
	
	[self dismissViewControllerAnimated:YES completion:^{
		
	}];
	
}

-(IBAction)goBack:(id)sender {
	
	if (self.navigationController != nil) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		[self dismiss:nil];
	}
	
}


/*
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:NULL];
    self.navigationItem.backBarButtonItem = backBarButton;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
