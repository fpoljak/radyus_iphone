//
//  UIImageView+NoAvatar.h
//  Radyus
//
//  Created by Frane Poljak on 15/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (NoAvatar)

-(void)setDefaultImageWithFirstName:(NSString *)firstName lastName:(NSString *)lastName;

@end
