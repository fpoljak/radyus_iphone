//
//  CloudView.m
//  RadyUs
//
//  Created by Frane Poljak on 12/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "UnavailableCloudView.h"
#import "Profile.h"
#import "UIImageView+NoAvatar.h"
#import "ViewFriends.h"

@interface UnavailableCloudView ()

@end

@implementation UnavailableCloudView
@synthesize coordinate;
@synthesize boundingMapRect;

static char friendKey;

- (void)viewDidLoad {
    [super viewDidLoad];

	_apiService = [[ApiService alloc] init];
	_apiService.delegate = self;
	
	[_apiService getCloudUsers:[_cloud valueForKey:@"id"]];
	
	double lat = [[(NSArray *)[_cloud valueForKey:@"center"] objectAtIndex:1] doubleValue];
	double lng = [[(NSArray *)[_cloud valueForKey:@"center"] objectAtIndex:0] doubleValue];
	
	NSInteger radius = [[_cloud valueForKey:@"radius"] integerValue];
	
	/*if (_userLocation) {
		
		double lat = [[(NSArray *)[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"location"] objectAtIndex:1] doubleValue];
		double lng = [[(NSArray *)[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"location"] objectAtIndex:0] doubleValue];
		
		_userLocation = CLLocationCoordinate2DMake(lat, lng);
	}*/
	
	NSInteger distance = round([self distanceBetweenCoordinate:_userLocation andCoordinate:CLLocationCoordinate2DMake(lat, lng)]);
	
	NSString *labelTextFormat = @"Radyus for this cloud is\n%ld meters.\nYou must get %ld m closer\nto see content.";
	
	[_distanceLabel setText:[NSString stringWithFormat:labelTextFormat, radius, distance - radius]];

    ////////

    _mapView.showsUserLocation = YES;

    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((lat + _userLocation.latitude) / 2, (lng + _userLocation.longitude) / 2);

    [_mapView setCenterCoordinate:center animated:YES];

    MapPin *cloud = [[MapPin alloc] initWithCoordinates:CLLocationCoordinate2DMake(lat, lng) id:[_cloud valueForKey:@"id"] placeName:[_cloud valueForKey:@"name"] description:[_cloud valueForKey:@"name"] isInRange:false];

    [_mapView addAnnotation:cloud];

    MKCircle *radiusCircle = [MKCircle circleWithCenterCoordinate:CLLocationCoordinate2DMake(lat, lng) radius:radius];
    [_mapView addOverlay:radiusCircle];

    CGFloat m = (radius + distance) * 1.1;
    CGFloat fractionLatLon = _mapView.region.span.latitudeDelta / _mapView.region.span.longitudeDelta;

    CGFloat latDistance = (fractionLatLon > 1) ? m * fractionLatLon : m;
    CGFloat lonDistance = (fractionLatLon > 1) ? m : m * fractionLatLon;

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center, latDistance, lonDistance);

    [_mapView setRegion:region animated:YES];
	
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {

    if ([overlay isKindOfClass:[MKCircle class]]) {

        MKCircleRenderer *radiusView = [[MKCircleRenderer alloc] initWithCircle:overlay];
        radiusView.fillColor = [UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:0.275f];

        return radiusView;

    } else return nil;
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {

    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        MKAnnotationView *ann = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"RadyUsUserLocationAnnotation"];

        if (ann) {
            return ann;
        }

        ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"RadyUsUserLocationAnnotation"];
        ann.enabled = YES;
        ann.draggable = NO;
        ann.canShowCallout = NO;

        CGSize pinSize = CGSizeMake(33, 43);

        UIGraphicsBeginImageContext(pinSize);

        UIImage *cloud = [UIImage imageNamed:@"user_location_pin.png"];
        UIImage *icon = [UIImage imageNamed:@"radyus_icon.png"];

        [icon drawInRect:CGRectMake(3, 8, 26, 26)];
        [cloud drawInRect:CGRectMake(0, 0, 33, 43)];

        UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();

        UIGraphicsEndImageContext();


        [ann setImage: combined];
        //[ann setCenterOffset:CGPointMake(-17, 0)];
        ann.layer.anchorPoint = CGPointMake(0.5f, 1.0f);

        return ann;
    }

    if ([annotation isKindOfClass:[MapPin class]]) {

        MKAnnotationView *ann = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[(MapPin *)annotation unique_id]];
        ann.enabled = YES;
        ann.draggable = NO;
        ann.canShowCallout = NO;

        BOOL isInRange = [(MapPin *)annotation isInReach];

        UIImage *cloud = (isInRange) ? [UIImage imageNamed:@"map_cloud_small.png"] : [UIImage imageNamed:@"map_cloud_grey.png"];

        CGSize cloudSize = cloud.size;

        UIGraphicsBeginImageContext(cloudSize);

        UIImage *icon = [UIImage imageNamed:@"icon_grill.png"];
        [cloud drawInRect:CGRectMake(0, 0, cloudSize.width, cloudSize.height)];
        [icon drawInRect:CGRectMake((cloudSize.width - icon.size.width) / 2,
                                    (cloudSize.height - icon.size.height) / 2,
                                    icon.size.width, icon.size.height)];
        
        UIImage *combined = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        [ann setImage: combined];
        ann.layer.anchorPoint = CGPointMake(0.5f, 0.5f);
        
        return  ann;
        
    }
    
    return nil;
    
}


-(BOOL)shouldDisableUserInteraction {
	return NO;
}

-(void)apiService:(ApiService *)service taskFailedWithErrorCode:(uint)errorCode {

}

-(void)apiService:(ApiService *)service receivedUserIdList:(NSArray *)userIdList {
	[service getUsersByIdList:userIdList];
}

-(void)apiService:(ApiService *)service receivedUsersList:(NSJSONSerialization *)usersList {

	NSArray *friends = (NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"userFriends"];
	
	NSMutableArray *friendsInCloud = [[NSMutableArray alloc] init];
	
	for (NSDictionary *friend in (NSArray *)usersList) {
		if ([friends containsObject:[friend valueForKey:@"id"]]) {
			[friendsInCloud addObject:friend];
		}
	}

	[self displayFriendsInCloud:friendsInCloud];
	
}

-(void)displayFriendsInCloud:(NSArray *) friendsInCloud {
	
	if (friendsInCloud.count == 0) {
		return;
	}
	
	CGFloat width = _friendsInCloudDisplayView.frame.size.width;
	CGFloat size = 34;
	CGFloat spacing = 8;
	int max_count = (width - spacing) / (size + spacing);
	CGFloat padding = ((CGFloat)((int)(width - spacing) % (int)(size + spacing))) / 2;
	CGFloat currentX = padding + spacing + ((size + spacing) * (max_count - MIN((int)friendsInCloud.count, max_count))) / 2;
	CGFloat top = 24;
	
	if (friendsInCloud.count == 1) {
		[_friendsInCloudLabel setText:[NSString stringWithFormat:@"Your friend %@ is here.", [friendsInCloud.firstObject valueForKey:@"nameFirst"]]];
	} else {
		[_friendsInCloudLabel setText:[NSString stringWithFormat:@"%d of your friends are here.", (int)friendsInCloud.count]];
	}
	
	for (int i = 0; i < MIN(max_count, friendsInCloud.count); i++) {
		
		if (i == max_count - 1 && friendsInCloud.count > max_count) {
			
			UIButton *viewAll = [[UIButton alloc] initWithFrame:CGRectMake(currentX, top, size, size)];
			[viewAll setClipsToBounds:YES];
			[viewAll setBackgroundColor:[UIColor grayColor]];
			[viewAll.layer setCornerRadius:size / 2];
			[viewAll setTitle:[NSString stringWithFormat:@"+%d", (int)friendsInCloud.count - max_count + 1] forState:UIControlStateNormal];
			
			objc_setAssociatedObject(viewAll, &friendKey, friendsInCloud, OBJC_ASSOCIATION_RETAIN);
			[viewAll addTarget:self action:@selector(viewAllTapped:) forControlEvents:UIControlEventTouchUpInside];

			break;
		}
		
		UIImageView *avatar = [[UIImageView alloc] initWithFrame:CGRectMake(currentX, top, size, size)];
		[avatar setClipsToBounds:YES];
		[avatar setBackgroundColor:[UIColor grayColor]];
		[avatar.layer setCornerRadius:size / 2];
		[avatar setUserInteractionEnabled:YES];
		
		NSDictionary *friend = [friendsInCloud objectAtIndex:i];
		objc_setAssociatedObject(avatar, &friendKey, friend, OBJC_ASSOCIATION_RETAIN);
		
		UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTapped:)];
		[avatar addGestureRecognizer:avatarTap];
		
		NSString *imageId = [[friend valueForKey:@"pictures"] firstObject];
		
		if (imageId != nil) {
			AsyncImageLoadManager *loader = [[AsyncImageLoadManager alloc] initWithImageId:imageId imageView:avatar useActivityIndicator:YES];
			[loader loadImage];
		} else {
			[avatar setDefaultImageWithFirstName:[friend objectForKey:@"nameFirst"] lastName:[friend objectForKey:@"nameLast"]];
		}
		
		[_friendsInCloudDisplayView addSubview:avatar];
		
		
		currentX += size + spacing;
		
	}
	
}

-(void)avatarTapped:(UITapGestureRecognizer *)sender {
	[self dismiss:(NSDictionary *)objc_getAssociatedObject(sender.view, &friendKey)];
}

-(void) viewAllTapped:(id) sender {
	[self viewAllFriendsInCloud:(NSArray *)objc_getAssociatedObject(sender, &friendKey)];
}

-(void) viewAllFriendsInCloud: (NSArray *) friendsInCloud {
	ViewFriends *viewUsers = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewFriends"];
	[viewUsers setFriendsList:[NSMutableArray arrayWithArray: friendsInCloud]];
	[viewUsers setTitle:@"Friends in cloud"];
	
	[self presentViewController:viewUsers animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismiss:(id)sender {
	
	[self dismissViewControllerAnimated:YES completion:^{
		
		if ([sender isKindOfClass:[NSDictionary class]] && _parentVC != nil) {
			
			UINavigationController *navCtrl = _parentVC.tabBarController != nil ? _parentVC.tabBarController.navigationController : _parentVC.navigationController;
			
			if (navCtrl != nil) {
			
				Profile *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
				[profileVC setUserInfo:(NSJSONSerialization *)sender];

				double delayInSeconds = 0.2; ////////
				dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
				dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
					[navCtrl pushViewController:profileVC animated:YES];
				});
				
			}
		}

	}];
}

-(CLLocationDistance)distanceBetweenCoordinate:(CLLocationCoordinate2D)originCoordinate andCoordinate:(CLLocationCoordinate2D)destinationCoordinate {
	CLLocation *originLocation = [[CLLocation alloc] initWithLatitude:originCoordinate.latitude longitude:originCoordinate.longitude];
	CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:destinationCoordinate.latitude longitude:destinationCoordinate.longitude];
	CLLocationDistance distance = [originLocation distanceFromLocation:destinationLocation];
	
	return distance;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
