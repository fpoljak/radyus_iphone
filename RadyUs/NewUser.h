//
//  NewUser.h
//  RadyUs
//
//  Created by Frane Poljak on 02/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransparentDialog.h"
#import "Profile.h"
#import "ApiService.h"

@interface NewUser : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, ApiServiceDelegate>

@property (nonatomic, retain) IBOutlet UIScrollView *containerView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *mfSegmentedCtrl;

@property (nonatomic, retain) UIDatePicker *datePicker;

@property (nonatomic, retain) IBOutlet UIImageView *userImageView;
@property (nonatomic, retain) IBOutlet UITextField *firstNameTF;
@property (nonatomic, retain) IBOutlet UITextField *lastNameTF;
@property (nonatomic, retain) IBOutlet UITextField *userNameTF;
@property (nonatomic, retain) IBOutlet UITextField *passwordTF;
@property (nonatomic, retain) IBOutlet UITextField *confirmPasswordTF;
@property (nonatomic, retain) IBOutlet UITextField *emailTF;
@property (nonatomic, retain) IBOutlet UITextField *birthdayTF;

@property (nonatomic, retain) UIButton *endEditingButton;

@property (nonatomic, retain) UIImage *userImage;

@property (nonatomic, retain) ApiService *apiService;

@end
