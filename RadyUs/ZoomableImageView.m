//
//  ZoomableImageView.m
//  Radyus
//
//  Created by Locastic MacbookPro on 26/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "ZoomableImageView.h"

@implementation ZoomableImageView

+(Class)layerClass {
	return [CATiledLayer class];
}

-(void)awakeFromNib {
	
	[super awakeFromNib];
	CATiledLayer *tempTiledLayer = (CATiledLayer*)self.layer;
	tempTiledLayer.levelsOfDetail = 5;
	tempTiledLayer.levelsOfDetailBias = 2;
	self.opaque=YES;
	
}

-(instancetype)init {
	
	if (self = [super init]) {
	
		CATiledLayer *tempTiledLayer = (CATiledLayer*)self.layer;
		tempTiledLayer.levelsOfDetail = 5;
		tempTiledLayer.levelsOfDetailBias = 2;
		self.opaque=YES;
		
	}
	
	return self;
	
}

@end
