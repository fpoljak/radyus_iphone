//
//  DBManager.m
//  RadyUs
//
//  Created by Frane Poljak on 10/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "DBManager.h"

@implementation DBManager

NSString *databaseTemplateName = @"radyus1.sqlt";

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
	
	self = [super init];
	
	if (self && dbFilename != nil) {
		
		//check if new version, update database if needed
		
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _documentsDir = [paths firstObject];
        
        _databaseFilename = dbFilename;
        
        [self copyDatabaseIntoDocumentsDirectory];
		
    }
	
    return self;

}

-(void)copyDatabaseIntoDocumentsDirectory{
	
    NSString *destinationPath = [_documentsDir stringByAppendingPathComponent:_databaseFilename];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
		
		NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseTemplateName];
        NSError *error;
		
		[[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]); 
		} else {
			NSURL *dbUrl = [NSURL fileURLWithPath:destinationPath];
			[dbUrl setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
			if (error != nil) {
				NSLog(@"%@", [error localizedDescription]);
			}
		}
		
    }
	
}

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable {

	sqlite3 *sqlite3Database;
	
	NSString *databasePath = [_documentsDir stringByAppendingPathComponent:_databaseFilename];
	
	if (_arrResults != nil) {
		[_arrResults removeAllObjects];
		_arrResults = nil;
	}
	
	_arrResults = [[NSMutableArray alloc] init];
	
	if (_arrColumnNames != nil) {
		[_arrColumnNames removeAllObjects];
		_arrColumnNames = nil;
	}

	_arrColumnNames = [[NSMutableArray alloc] init];

	BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
	
	if(openDatabaseResult == SQLITE_OK) {
	
		sqlite3_stmt *compiledStatement;
	
		BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);
		
		if (prepareStatementResult == SQLITE_OK) {
			
			if (!queryExecutable){
				
				NSMutableArray *arrDataRow;
				
				while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
					
					arrDataRow = [[NSMutableArray alloc] init];
					
					int totalColumns = sqlite3_column_count(compiledStatement);
					
					for (int i = 0; i < totalColumns; i++){
						
						char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
						
						if (dbDataAsChars != NULL) {
							[arrDataRow addObject:[NSString stringWithUTF8String:dbDataAsChars]];
						}
						
						if (_arrColumnNames.count != totalColumns) {
							dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
							[_arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
						}
					}
					
					if (arrDataRow.count > 0) {
						[_arrResults addObject:arrDataRow];
					}
					
				}
				
			} else {//executable query (insert, update, ...).
				
				int executeQueryResults = sqlite3_step(compiledStatement);
				
				if (executeQueryResults == SQLITE_DONE) {
					
					_affectedRows = sqlite3_changes(sqlite3Database);
					_lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
					
				} else {//could not execute the query
					NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
				}
				
			}
			
		} else {//database cannot be opened
			NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
		}
	
		sqlite3_finalize(compiledStatement);
		
	}
	
	sqlite3_close(sqlite3Database);
	
}

-(NSArray *)loadDataFromDB:(NSString *)query{
	
	[self runQuery:[query UTF8String] isQueryExecutable:NO];
	return (NSArray *)_arrResults;
	
}

-(void)executeQuery:(NSString *)query{
	
	[self runQuery:[query UTF8String] isQueryExecutable:YES];
	
}

-(void) saveUser:(NSJSONSerialization *) user {
	
	NSLog(@"Saving user: %@", user);
	//friends, images?
	NSString *queryFormat = @"INSERT OR REPLACE INTO users (id, username, nameFirst, nameLast, pictures) VALUES (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\")";
	
	NSString *user_id = [user valueForKey:@"id"];
	
	if (user_id == nil) {
		return;
	}
	
	NSString *username = [user valueForKey:@"username"] ?: @"";
	NSString * first_name = [user valueForKey:@"nameFirst"] ?: @"";
	NSString *last_name = [user valueForKey:@"nameLast"] ?: @"";
	NSString *pictures = [user valueForKey:@"pictures"] == nil ? @"" : [[user valueForKey:@"pictures"] componentsJoinedByString:@","];
	
	NSString *query = [NSString stringWithFormat:queryFormat, user_id, username, first_name, last_name, pictures];
	
	[self executeQuery:query];
	
}

-(void) saveUsers:(NSArray *)users {

	for (NSJSONSerialization *user in users) {
		[self saveUser:user];
	}
	
}

-(void) saveCloud:(NSJSONSerialization *) cloud {
	
	NSString *queryFormat = @"INSERT OR REPLACE INTO clouds (id, name, is_active, latitude, longitude, creator, creatorAnon) VALUES (\"%@\", \"%@\", %d, %lf, %lf, \"%@\", %d)";
	
	NSString *cloud_id = [cloud valueForKey:@"id"];
	
	if (cloud_id == nil) {
		return;
	}
	
	NSString *cloud_name = [cloud valueForKey:@"name"] ?: @"";
	BOOL is_active = [[cloud valueForKey:@"active"] boolValue];
	double latitude = [[[cloud valueForKey:@"center"] lastObject] doubleValue] ?: 0.0;
	double longitude = [[[cloud valueForKey:@"center"] firstObject] doubleValue] ?: 0.0;
	NSString *user_id = [cloud valueForKey:@"creator"] ?: @"NULL";
	BOOL creatorAnon = [[cloud valueForKey:@"creatorAnon"] boolValue];

	NSString *query = [NSString stringWithFormat:queryFormat, cloud_id, cloud_name, is_active, latitude, longitude, user_id, creatorAnon];
	
	[self executeQuery:query];
	
}

-(void) saveClouds:(NSArray *)clouds {
	
	for (NSJSONSerialization *cloud in clouds) {
		[self saveCloud:cloud];
	}
	
}

-(void) setCloud: (NSString *)cloudId active: (BOOL) active {
	NSString *queryFormat = @"UPDATE clouds SET is_active = %d WHERE id = \"%@\"";
	
	[self executeQuery:[NSString stringWithFormat:queryFormat, active, cloudId]];
}

-(void) saveMessage:(NSJSONSerialization *) message { // add 'read' (seen) parameter (and field in db, temporarily use 'distnce' instead?)!

	NSString *queryFormat = @"INSERT OR REPLACE INTO messages (id, cloud, user, distance, text, image, time, mentions, hashtags) VALUES (\"%@\", \"%@\", \"%@\", %lf, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\" )";
	
	NSString *message_id = [message valueForKey:@"id"];
	
	if (message_id == nil) {	
		return;
	}
	
	NSString *cloud_id = [message valueForKey:@"cloud"] ?:
	([[message valueForKey:@"to"] isEqualToString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]] ? [message valueForKey:@"user"] : [message valueForKey:@"to"]);
	NSString *user_id = [message valueForKey:@"user"] ?: @"";
	double distance = [[message valueForKey:@"distance"] doubleValue] ?: 0.0; ////izbacit
	NSString *message_text = [message valueForKey:@"text"] ?: @"";
	NSString *picture = [message valueForKey:@"picture"] ?: @"";
	NSString *date_time = [message valueForKey:@"time"] ?: @"";
	NSString *mentions = [([message valueForKey:@"mentions"] ?: [NSArray new]) componentsJoinedByString:@","];
	NSString *hashtags = [([message valueForKey:@"hashtags"] ?: [NSArray new]) componentsJoinedByString:@","];
	
	NSString *query = [NSString stringWithFormat:queryFormat, message_id, cloud_id, user_id, distance, message_text, picture, date_time, mentions, hashtags];
	
	NSLog(@"query: %@", query);
	
	[self executeQuery:query];
	
}

-(void) saveMessages: (NSArray *)messages {

	for (NSJSONSerialization *message in messages) {
		[self saveMessage:message];
	}
	
}

-(void) clearOldMessagesForCloud:(NSString *)cloudId {
	
	NSString *queryFormat = @"SELECT id FROM messages WHERE cloud = '%@' ORDER BY time DESC LIMIT 30"; //20?
	NSString *query = [NSString stringWithFormat:queryFormat, cloudId];
	
	NSArray *messagesToKeep = [self loadDataFromDB:query];
	NSMutableArray *idsOfMessagesToKeep = [NSMutableArray new];
	
	for (NSArray *idArr in messagesToKeep) {
		[idsOfMessagesToKeep addObject:[idArr lastObject] ?: @""];
	}
	
	////  po vrimenu prve poruke? ostavit ih vise pa na load more dohvacat iz baze dok ima?
	NSString *messagesToKeepSet = [NSString stringWithFormat:@"('%@')", [idsOfMessagesToKeep componentsJoinedByString:@"','"]];
	
	queryFormat = @"DELETE FROM messages WHERE cloud = '%@' AND id NOT IN %@";
	query = [NSString stringWithFormat:queryFormat, cloudId, messagesToKeepSet];
	NSLog(@"%@", query);
	[self executeQuery:query];
	
}

-(NSArray *) loadMessages:(NSString *)cloudID { //from, to, page?
	
														  //, users.username?
	NSString *messagesQueryFormat = @"SELECT * FROM (SELECT messages.id, messages.user, users.nameFirst, users.nameLast, \"\" AS avatar, messages.distance, messages.text, messages.image, messages.time, messages.hashtags, messages.mentions\n"
		"FROM messages\n"
		"LEFT OUTER JOIN users ON users.id = messages.user\n"
		"WHERE messages.cloud = '%@'\n"
		"ORDER BY messages.time DESC LIMIT 20 OFFSET 0) ORDER BY time ASC"; //limit, if full automatically delete? or "load more" takes from database until exists?
														//reverse
	//LIMIT <count> OFFSET <skip>
	
	NSString *query = [NSString stringWithFormat:messagesQueryFormat, cloudID];
	
	NSArray *messages = [self loadDataFromDB:query];
	
	NSMutableArray *formattedMessages = [NSMutableArray new];
	
	for (NSArray *arr in messages) {
		
		@try {
			
			NSString *messageId = [arr objectAtIndex:[_arrColumnNames indexOfObject:@"id"]];
			NSString *userId = [_arrColumnNames indexOfObject:@"user"] != NSNotFound ? [arr objectAtIndex:[_arrColumnNames indexOfObject:@"user"]] : @"0000000000000000000000000";
			//distance? - remove from table!
			NSString *text = [_arrColumnNames indexOfObject:@"text"] != NSNotFound ? [arr objectAtIndex:[_arrColumnNames indexOfObject:@"text"]] : @"";
			NSString *picture = [_arrColumnNames indexOfObject:@"image"] != NSNotFound ? [arr objectAtIndex:[_arrColumnNames indexOfObject:@"image"]]: nil; //@""
			NSString *time = [_arrColumnNames indexOfObject:@"time"] != NSNotFound ? [arr objectAtIndex:[_arrColumnNames indexOfObject:@"time"]] : @"2015-01-01T00:00:00.000Z"; //?
			NSArray *mentions = [_arrColumnNames indexOfObject:@"mentions"] != NSNotFound ? [([arr objectAtIndex:[_arrColumnNames indexOfObject:@"mentions"]] ?: @"") componentsSeparatedByString:@","] : [NSArray array];
			NSArray *hashtags = [_arrColumnNames indexOfObject:@"hashtags"] != NSNotFound ? [([arr objectAtIndex:[_arrColumnNames indexOfObject:@"hashtags"]] ?: @"") componentsSeparatedByString:@","] : [NSArray array];
			
			NSArray *_keys = @[@"id",
							   @"user",
							   @"time",
							   @"text",
							   @"picture",
							   @"mentions",
							   @"hashtags"];
			
			NSMutableArray *keys = [NSMutableArray arrayWithArray:_keys];
			
			NSArray *objects = [NSArray arrayWithObjects:
								messageId,
								userId,
								time,
								text,
								picture ?: @"",
								mentions,
								hashtags,
								nil];
			
			NSDictionary *message = [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
			
			[formattedMessages addObject:message];
			
		}
		@catch (NSException *exception) {
			NSLog(@"Error formatting message from database: %@", exception.reason);
		}
		
	}
	
	return [[NSArray alloc] initWithArray:formattedMessages];
	
}

-(NSArray *) loadClouds:(BOOL)own active:(BOOL)active {

	NSString *messagesQueryFormat = @"SELECT * FROM clouds\nWHERE (is_active = %d) AND (%d OR id = \"%@\")";
	
	NSString *query = [NSString stringWithFormat:messagesQueryFormat, active, !own, [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"id"]];
	
	NSArray *clouds = [self loadDataFromDB:query];
	NSMutableArray *_clouds = [NSMutableArray new];
	
	for (NSArray *cloud in clouds) {
		
		NSMutableDictionary *fc = [NSMutableDictionary new];
		for (int i = 0; i < _arrColumnNames.count; i++) {
			NSString *key = [_arrColumnNames objectAtIndex:i];
			[fc setValue:[cloud objectAtIndex:i] forKey:key];
		}
		
		[_clouds addObject:fc];
	}

	return [[NSArray alloc] initWithArray:_clouds];

}

-(NSArray *) loadUsers {
	
	NSArray *users = [self loadDataFromDB:@"SELECT * FROM users"];
	NSMutableArray *_users = [NSMutableArray new];
	
	for (NSArray *user in users) {
		
		NSMutableDictionary *fc = [NSMutableDictionary new];
		for (int i = 0; i < _arrColumnNames.count; i++) {
			NSString *key = [_arrColumnNames objectAtIndex:i];
			[fc setValue:[user objectAtIndex:i] forKey:key];
		}
		
		[_users addObject:fc];
	}
	
	return [[NSArray alloc] initWithArray:_users];
	
}

-(NSDictionary *) loadUserWithId: (NSString *)userId {
	
	NSString *query = [NSString stringWithFormat:@"SELECT * FROM users WHERE id = \"%@\"", userId];
	NSArray *user = [[self loadDataFromDB:query] lastObject];
	
	if (user == nil) {
		return nil;
	}
	
	NSMutableDictionary *fc = [NSMutableDictionary new];
	for (int i = 0; i < _arrColumnNames.count; i++) {
		NSString *key = [_arrColumnNames objectAtIndex:i];
		[fc setValue:[key isEqualToString:@"pictures"] ? [[user objectAtIndex:i] componentsSeparatedByString:@","] : [user objectAtIndex:i] forKey:key];
	}

	return fc;
	
}

@end
