//
//  VerticalTextView.m
//  RadyUs
//
//  Created by Frane Poljak on 10/03/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "VerticalTextView.h"

@implementation VerticalTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)layoutSubviews {
    self.contentOffset = CGPointMake(-4, self.contentOffset.y );
    [super layoutSubviews];
}

@end
