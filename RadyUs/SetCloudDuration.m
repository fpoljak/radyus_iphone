//
//  SetCloudDuration.m
//  RadyUs
//
//  Created by Frane Poljak on 18/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "SetCloudDuration.h"

@interface SetCloudDuration ()

@end

@implementation SetCloudDuration

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self sliderValueChanged:_durationSlider];
}

-(IBAction)sliderValueChanged:(UISlider *)sender {
    AddCloud *parent = (AddCloud *)[self.tabBarController parentViewController];
    [parent setCloudDuration:sender.value];
    NSString *title = parent.cloudDuration == 60 ? @" 1:00" : [NSString stringWithFormat:@" 0:%ld", (long)parent.cloudDuration];
    [parent.timeButton setTitle:title forState:UIControlStateNormal];
    [parent.timeButton setTitle:title forState:UIControlStateHighlighted];
    [parent.timeButton setTitle:title forState:UIControlStateSelected];
    //[parent.mapView setRegion:parent.mapView.region animated:TRUE];
    
    [parent validateForm];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
