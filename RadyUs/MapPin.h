//
//  MapPin.h
//  RadyUs
//
//  Created by Frane Poljak on 16/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapPin : NSObject <MKAnnotation>

- (id)initWithCoordinates:(CLLocationCoordinate2D)location id:(NSString*)_id placeName:(NSString *)placeName description:(NSString *)description isInRange:(BOOL)isInRange;

-(void) setSubtitle:(NSString*) newSubtitle;

@property (nonatomic, retain) NSString *unique_id;
@property (nonatomic) BOOL isInReach;

@end