//
//  TextViewWithPasteOverriding.m
//  Radyus
//
//  Created by Locastic MacbookPro on 17/02/16.
//  Copyright © 2016 Locastic. All rights reserved.
//

#import "TextViewWithPasteOverriding.h"

@implementation TextViewWithPasteOverriding

-(void)paste:(id)sender {
	
	//NOT WORKING!!!
	//http://stackoverflow.com/questions/29175089/how-to-paste-image-from-pasteboard-on-uitextview
	
	NSLog(@"paste detected...");
	if (self.delegate != nil/* && [self.delegate conformsToProtocol:@protocol(UITextViewDelegate)]*/ && [self.delegate respondsToSelector:@selector(overridePaste:)]) {
		NSLog(@"overriding paste...");
		[self.delegate performSelector:@selector(overridePaste:) withObject:self];
	} else {
		[self overridePaste:sender];
	}

}

-(void)overridePaste:(id)sender { //prevent warning
	[super paste:sender];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
	NSLog(@"canPerformAction:");
	if (action == @selector(paste:)) return YES;
	return [super canPerformAction:action withSender:sender];
}

@end
