//
//  SideMenu.m
//  RadyUs
//
//  Created by Frane Poljak on 19/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "SideMenu.h"
#import "SideMenuUtilizer.h"
#import "CustomImageWithBadge.h"
#import "Archive.h"
#import "AddCloud.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface SideMenu ()

@end

@implementation SideMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
	NSArray *__menuItems = @[ //MUST BE MUTABLE!
						//@{@"icon": @"bell.png", @"title": @"Notifications", @"badgeNumber": @0},
						@{@"icon": @"compass.png", @"title": @"Explore", @"badgeNumber": @0},
						@{@"icon": @"mailbox.png", @"title": @"Inbox", @"badgeNumber": @0},
						@{@"icon": @"userminus.png", @"title": @"Profile", @"badgeNumber": @0},
						@{@"icon": @"cloud.png", @"title": @"My Clouds", @"badgeNumber": @0},
						@{@"icon": @"folder.png", @"title": @"Inactive clouds", @"badgeNumber": @0},
						@{@"icon": @"settings.png", @"title": @"Settings", @"badgeNumber": @0},
						//@{@"icon": @"star.png", @"title": @"Favorites", @"badgeNumber": @0},
						@{@"icon": @"compass.png", @"title": @"Create Cloud", @"badgeNumber": @0}
						//@{@"icon": @"power.png", @"title": @"Logout", @"badgeNumber": @0}
                     ];
	
	//_menuItems = [NSMutableArray arrayWithArray:__menuItems];
	_menuItems = [NSMutableArray new];
	
	for (NSDictionary *item in __menuItems) {
		[_menuItems addObject:[NSMutableDictionary dictionaryWithDictionary:item]];
	}
	
	NSJSONSerialization *userInfo = [[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"];
	[_userNameLabel setText:[NSString stringWithFormat:@"%@ %@", [userInfo valueForKey:@"nameFirst"], [userInfo valueForKey:@"nameLast"]]];
	
    UIPanGestureRecognizer *menuSlideGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(slideMenuPan:)];
    [menuSlideGesture setDelegate:self];
    [self.view addGestureRecognizer:menuSlideGesture];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMessage:) name:@"RadyusReceivedRemoteNotification" object:nil]; // add whisper, message, request...
	
}

-(void)viewDidAppear:(BOOL)animated {
	[self refreshBadgeNumbers];
}

-(void)refreshBadgeNumbers {
	
	int row = 0;
	
	for (NSDictionary *item in _menuItems) {
		[self setBadgeNumber:[item[@"badgeNumber"] integerValue] forRow:row++];
	}
	
}

-(void)receivedMessage:(NSNotification *)notification {
	
	//check type
	//check user id
	//check current (parent) view controller
	
	//whisper test:
	
	[self incrementBadge:[_menuItems objectAtIndex:1]];
	
	// if open refresh immedeately
	[self refreshBadgeNumbers];
	
}

// TODO: menu item class
-(void)incrementBadge:(NSMutableDictionary *)item {
	NSInteger bn = [item[@"badgeNumber"] integerValue];
	bn++;
	[item setValue:[NSNumber numberWithInteger:bn] forKey:@"badgeNumber"];
}

-(void)decrementBadge:(NSMutableDictionary *)item {
	NSInteger bn = [item[@"badgeNumber"] integerValue];
	bn--;
	[item setValue:[NSNumber numberWithInteger:bn] forKey:@"badgeNumber"];
}

-(void)menuItem:(NSMutableDictionary *)item setBadgeNumber:(NSNumber *)number {
	[item setValue:number forKey:@"badgeNumber"];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {

    if (![gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        return  YES;
    }
    CGPoint velocity = [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:self.view];
    //NSLog(@"location.x: %f, width: %f, velocity: %f", [gestureRecognizer locationInView:self.view].x, self.view.frame.size.width, velocity.x);
    return [gestureRecognizer locationInView:self.view].x > self.view.frame.size.width - 32 && velocity.x < 0 && fabs(velocity.x) > 2 * fabs(velocity.y);
	
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return !([[gestureRecognizer view] isEqual:self.view] && [[otherGestureRecognizer view] isKindOfClass:[UITableView class]]);
}

-(void)slideMenuPan:(UIPanGestureRecognizer *)sender {
	
	/*
	if (![self.parentViewController isKindOfClass:[SideMenuUtilizer class]]) { // should NEVER happen
		return;
	}
	 */
	
    [(SideMenuUtilizer *)self.parentViewController slideMenuPan:(UIScreenEdgePanGestureRecognizer *)sender];
	
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [_menuItems count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//if (indexPath.row < [_menuItems count] - 1) {
        return  44.0f;
	//}
	//return MAX(self.view.frame.size.height - ([_menuItems count] - 1) * 44 - 80, 44);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SideMenuCell"];
    }
    
    CustomImageWithBadge *item = (CustomImageWithBadge *)[cell viewWithTag:1];
    UILabel *label = (UILabel *)[cell viewWithTag:2];
	
    [item setImage:[UIImage imageNamed:[[_menuItems objectAtIndex:indexPath.row] objectForKey:@"icon"]]];
    [item setBadgeNumber:[[[_menuItems objectAtIndex:indexPath.row] objectForKey:@"badgeNumber"] integerValue]];
	//item setHighlightedImage:?
	
    [label setText:[[_menuItems objectAtIndex:indexPath.row] objectForKey:@"title"]];
	[label setHighlightedTextColor:UIColorFromRGB(0xFFD203)];
	
	UIView *cellSelectionBackgroundView = [[UIView alloc] init];
    [cellSelectionBackgroundView setBackgroundColor:[UIColor darkTextColor]];
	cell.selectedBackgroundView = cellSelectionBackgroundView;
	
    return  cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	[self hide:nil];

    switch (indexPath.row) {
			
		case 0:{
			
			NSString *classString = NSStringFromClass([[self parentViewController] class]);
			
			if (![[classString substringToIndex:MIN(7, classString.length)] isEqualToString:@"Explore"]) {
				
				for (UIViewController *vc in self.navigationController.viewControllers) { //remove all observers?
					if ([vc isKindOfClass:[UITabBarController class]]) {
						for (UIViewController *_vc in vc.childViewControllers) {
							[[NSNotificationCenter defaultCenter] removeObserver:_vc];
							if ([_vc isKindOfClass:[ExploreMap class]]) {
								[CLLocationManager cancelPreviousPerformRequestsWithTarget:_vc]; //nepotrebno?
							}
							[NSObject cancelPreviousPerformRequestsWithTarget:_vc];
							//[_vc viewWillDisappear:NO];
						}
					}
					[[NSNotificationCenter defaultCenter] removeObserver:vc];
					[NSObject cancelPreviousPerformRequestsWithTarget:vc];
					[vc viewWillDisappear:NO];
				}
				
				UIViewController *explore = [self.storyboard instantiateViewControllerWithIdentifier:@"Explore"];
				[self.navigationController setViewControllers:[NSArray arrayWithObject:explore] animated:YES];
				//[self.navigationController performSelector:@selector(setViewControllers:) withObject:[NSArray arrayWithObject:explore] afterDelay:0.3];
            }
			
            break;
			
        }
			
        case 1:{
			
			if (![NSStringFromClass([self parentViewController].class) isEqualToString:@"Inbox"]) {
				UIViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"Inbox"];
				[self.navigationController pushViewController:next animated:YES];
			}
			
			break;
			
        }
			
        case 2:{ //provjerit jesmo li tu (next iskindofclass [profile class] && person == user
			
            UIViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
            [self.navigationController pushViewController:next animated:YES];
            break;
			
        }
			
        case 3:{
			
			UIViewController *parent = [self parentViewController];
			
			if (!([[NSStringFromClass([parent class]) substringToIndex:7] isEqualToString:@"Archive"] && [(Archive *)parent nonArchive])) {
			
				Archive *archive = [self.storyboard instantiateViewControllerWithIdentifier:@"MyClouds"];
				[archive setNonArchive:YES];
				[self.navigationController pushViewController:archive animated:YES];
				
			}
			
            break;
			
        }
			
        case 4:{
			
			UIViewController *parent = [self parentViewController];
			
			if (!([[NSStringFromClass([parent class]) substringToIndex:7] isEqualToString:@"Archive"] && ![(Archive *)parent nonArchive])) {
				
				Archive *archive = [self.storyboard instantiateViewControllerWithIdentifier:@"Archive"];
				[archive setTitle:@"Archive"];
				[self.navigationController pushViewController:archive animated:YES];
				
			}
			
            break;
			
        }
			
        case 5:{
			
            UIViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AppSettings"];
            [self.navigationController presentViewController:next animated:YES completion:^{
                //
            }];

            break;
			
        }
			
		case 6: {
			
            UIViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCloud"];
            [self.navigationController presentViewController:next animated:YES completion:^{
                //
            }];
			
            break;
			
        }
			
        default:
            break;
			
    }
    
}

-(IBAction)logout:(id)sender{
	
	[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"accessToken"];
	UIViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstPage"];
	[self.navigationController.navigationBar setFrame:CGRectMake(0, 20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
	
	if ([FBSDKAccessToken currentAccessToken] != nil) {
		FBSDKLoginManager *fbLoginManager = [[FBSDKLoginManager alloc] init];
		[fbLoginManager logOut];
	}
	
	for (UIViewController *vc in self.navigationController.viewControllers) { //remove all observers?
		if ([vc isKindOfClass:[UITabBarController class]]) {
			for (UIViewController *_vc in vc.childViewControllers) {
				[[NSNotificationCenter defaultCenter] removeObserver:_vc];
				if ([_vc isKindOfClass:[ExploreMap class]]) {
					[CLLocationManager cancelPreviousPerformRequestsWithTarget:_vc]; //nepotrebno?
				}
				[NSObject cancelPreviousPerformRequestsWithTarget:_vc];
				//[_vc viewWillDisappear:NO];
			}
		}
		[[NSNotificationCenter defaultCenter] removeObserver:vc];
		[NSObject cancelPreviousPerformRequestsWithTarget:vc];
		[vc viewWillDisappear:NO];
	}
	
	[self.navigationController setViewControllers:[NSArray arrayWithObject:loginView] animated:YES];
	//[self.navigationController performSelector:@selector(setViewControllers:) withObject:[NSArray arrayWithObject:loginView] afterDelay:0.3];
	
}

-(void)hide:(id)sender {
    [(SideMenuUtilizer *)self.parentViewController hideSideMenu:sender];
}

-(void)setBadgeNumber:(NSInteger)badgeNumber forRow:(NSInteger)row {
    NSLog(@"Setting badge number to %ld for row %ld", badgeNumber, row);
	NSNumber *number = [NSNumber numberWithInteger:MAX(badgeNumber, 0)];
	[[_menuItems objectAtIndex:row] setValue:number forKey:@"badgeNumber"]; // support "two way" badge change
	[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)incrementBadgeNumberForRow:(NSInteger)row {
    [self setBadgeNumber:[[[_menuItems objectAtIndex:row] objectForKey:@"badgeNumber"] integerValue] + 1 forRow:row];
}

-(void)decrementBadgeNumberForRow:(NSInteger)row {
    [self setBadgeNumber:[[[_menuItems objectAtIndex:row] objectForKey:@"badgeNumber"] integerValue] -1 forRow:row];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
