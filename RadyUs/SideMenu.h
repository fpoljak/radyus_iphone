//
//  SideMenu.h
//  RadyUs
//
//  Created by Frane Poljak on 19/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SideMenu : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (nonatomic, retain) NSMutableArray *menuItems;
@property (nonatomic, retain) IBOutlet UILabel *userNameLabel;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

-(void)setBadgeNumber:(NSInteger)badgeNumber forRow:(NSInteger)row;
-(void)incrementBadgeNumberForRow:(NSInteger)row;
-(void)decrementBadgeNumberForRow:(NSInteger)row;

@end
