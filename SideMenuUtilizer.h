//
//  SideMenuUtilizer.h
//  RadyUs
//
//  Created by Frane Poljak on 19/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SideMenu.h"

@interface SideMenuUtilizer : UIViewController

-(IBAction)showSideMenu:(id)sender;
-(IBAction)hideSideMenu:(id)sender;
-(IBAction)toggleSlideMenu:(id)sender;
-(IBAction)slideMenuPan:(UIScreenEdgePanGestureRecognizer *)sender;

@property (nonatomic, retain) UIView *containerView;
@property (nonatomic, retain) UIView *notificationView;
@property (nonatomic, retain) NSTimer *notificationTimer;
@property (nonatomic, retain) UIViewController *sideMenu;
@property (nonatomic) BOOL slideMenuCreated;
@property (nonatomic) BOOL slideMenuShown;
@property (nonatomic) BOOL slideMenuSliding;
@property (nonatomic, retain) UIScreenEdgePanGestureRecognizer *menuSlideGesture;
@property (nonatomic, retain) UIScreenEdgePanGestureRecognizer *menuSlideGestureForNavigationBar;
@property (nonatomic) CGFloat originY;
@property (nonatomic, retain) NSString *previousTitle;
@property (nonatomic, retain) UIView *viewForHidingMenu;
//@property (nonatomic, retain) UIResponder *lastFirstResponder;

-(void)setBadgeNumber:(NSInteger)badgeNumber forRow:(NSInteger)row;
-(void)incrementBadgeNumberForRow:(NSInteger)row;
-(void)decrementBadgeNumberForRow:(NSInteger)row;
-(void)prepareForBackground;
-(void)returnFromBackground;

-(void)receivedNotification:(NSDictionary *)aNotification;

-(void)sidemenuWillShow __attribute__((objc_requires_super));
-(void)sidemenuDidShow __attribute__((objc_requires_super));
-(void)sidemenuWillHide __attribute__((objc_requires_super));
-(void)sidemenuDidHide __attribute__((objc_requires_super));

@end
