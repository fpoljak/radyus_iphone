//
//  AppSettings.h
//  RadyUs
//
//  Created by Frane Poljak on 23/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoViewController.h"

@interface AppSettings : UITableViewController

@property (nonatomic, retain) IBOutlet UILabel *distanceUnitLabel;

@end
