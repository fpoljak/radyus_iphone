//
//  AppSettings.m
//  RadyUs
//
//  Created by Frane Poljak on 23/02/15.
//  Copyright (c) 2015 Locastic. All rights reserved.
//

#import "AppSettings.h"

@interface AppSettings ()

@end

@implementation AppSettings

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	
	BOOL isFeet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"distanceUnit"] isEqualToString:@"Feet"];
	
	[_distanceUnitLabel setText:(isFeet ? @"Feet" : @"Meters")];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        //action
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    switch (section) {
        case 0:
            return 2;
            
        case 1:
            return 1;
		
        case 2:
            return 7;
            
        case 3:
            return 3;
            
        default:
            return 0;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	switch (indexPath.section) {
		case 0: //PROFILE
		{
			switch (indexPath.row) {
				case 0: //Edit Profile
				{
					
					break;
				}
				
				case 1: //Change Password
				{
					
					break;
				}
					
				default:
					break;
			}
		
			break;
	
		}
			
		case 1: //APP
		{
			switch (indexPath.row) {
				case 0: //Distance units in
				{
					
					BOOL isMeters = [_distanceUnitLabel.text isEqualToString:@"Meters"];
					
					[_distanceUnitLabel setText:(isMeters ? @"Feet" : @"Meters")];
					
					[[NSUserDefaults standardUserDefaults] setValue:_distanceUnitLabel.text forKey:@"distanceUnit"];
					
					[[NSNotificationCenter defaultCenter] postNotificationName:@"RadyusDistanceUnitChangedNotification" object:_distanceUnitLabel.text];
					
					break;
					
				}
					
				/*case 1: //One other link ////
				{
					
					break;
				}*/
				
				default:
				break;
			}
			
			break;
		}
			
		case 2: //SHARE THE LOVE
		{
			switch (indexPath.row) {
				case 0: //Rate on app store
				{
					NSString *urlString = @"https://itunes.apple.com/app/id964172106"; ///radyus
					NSURL *itunesURL = [NSURL URLWithString:urlString];
					[[UIApplication sharedApplication] openURL:itunesURL];
					break;
				}
					
				case 1: //Share Radyus
				{
					
					break;
				}
				
				case 2: //Follow on Twitter
				{
					
					break;
				}

				case 3: //Like on Facebook
				{
					
					break;
				}
					
				case 4: //Follow on Instagram
				{
					
					break;
				}
					
				case 5: //Contact us
				{
					
					break;
				}
					
				case 6: //Report a problem
				{
					
					break;
				}

				default:
					break;
			}
			
			break;
		}
			
		case 3: //IMPORTANT
		{
			switch (indexPath.row) {
				case 0: //Rules and Info
				{
					
					break;
				}
					
				case 1: //Terms of Services
				{
					
					break;
				}
					
				case 2: //Privacy policy
				{
					
					break;
				}
					
				default:
					break;
			}
		
			break;
		}

		default:
			break;
		}

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	InfoViewController *dvc = [segue destinationViewController];
	if ([[segue identifier] isEqualToString:@"RulesAndInfo"]) {
		[dvc setTitle:@"Rules and Info"];
	} else if ([[segue identifier] isEqualToString:@"TermsOfServices"]) {
		[dvc setTitle:@"Terms of Services"];
	} else if ([[segue identifier] isEqualToString:@"PrivacyPolicy"]) {
		[dvc setTitle:@"Privacy Policy"];
	}
}


@end
